// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebaseConfig : {
        apiKey: "AIzaSyCij1PocMECKnkJD_ZCDiN-LZM6jHjdPGw",
        authDomain: "meetdax4.firebaseapp.com",
        databaseURL: "https://meetdax4.firebaseio.com",
        projectId: "meetdax4",
        storageBucket: "meetdax4.appspot.com",
        messagingSenderId: "394538011830"
    }
};
