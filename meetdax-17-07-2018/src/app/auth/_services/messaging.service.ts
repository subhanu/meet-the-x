import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FirestoreDataService } from '../_services/firestore-data.service';
import { StylistM } from '../_models/stylistM';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Injectable()
export class MessagingService {

  authS: any;
  messaging = firebase.messaging();
  private messageSource = new Subject();
  currentMessage = this.messageSource.asObservable();
  stylistArr: StylistM[] = [];

  constructor(private _firestoreservices: FirestoreDataService, private af: AngularFireAuth ) { 
    

    //console.log(this.authS.uid);
   
  }

  someMethod(){
    
  }

  getPermission() {
    this.messaging.requestPermission()
    .then(() => {
      console.log('Notification permission granted.');
      return this.messaging.getToken()
    })
    .then(token => {
      console.log(token)
      //console.log(this.stylistArr[0].id);
      this.updateToken(token);
      
    })
    .catch((err) => {
        console.log('Unable to get permission to notify.', err);
    });
  }

  receiveMessage() {
     this.messaging.onMessage((payload) => {
      console.log("Message received. ", payload);
      this.messageSource.next(payload);
    });
  }

  updateToken(token){
    this.af.authState.subscribe(auth => {
      if (auth) {
          this.authS = auth;
          console.log(this.authS.uid);

          this._firestoreservices.getStylistByUID(this.authS.uid).subscribe(
            (data: StylistM[]) => {
                this.stylistArr = data;
                //console.log(this.stylistArr[0]);
                this._firestoreservices.updateStylistToken(this.stylistArr[0].id, token)
                .then(()=>{

                })
                .catch((err)=>{

                });
            });
      }
    });
  }

}
