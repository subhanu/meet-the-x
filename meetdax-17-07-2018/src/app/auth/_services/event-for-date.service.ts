import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EventC } from '../_models/index';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FirestoreDataService } from './firestore-data.service';
import { CustomerM } from '../_models/customerM';

@Injectable()
export class EventForDateService {
    //private eventArrSource = new BehaviorSubject<EventM[]>(null);
    private eventArrSource = new BehaviorSubject<EventC[]>(null);
    currentEventArr = this.eventArrSource.asObservable();

    private dateSrc = new BehaviorSubject<NgbDateStruct>(null);
    changedDate = this.dateSrc.asObservable();

    eventArr: EventC[] = [];
    customerArr: CustomerM[] = [];

    constructor(private _firestoreservices: FirestoreDataService) {
        var d = new Date();
        
        _firestoreservices.getAllCustomers().subscribe(
            (data) => this.customerArr = data
        );

        _firestoreservices.getEventsForDate(d.getFullYear()+"-"+this.getTwoDigitValue(d.getMonth()+1)+"-"+this.getTwoDigitValue(d.getDate())).subscribe(
            data => {
                this.eventArr = data;
                
                for(let i=0;i<this.eventArr.length;i++){
                    for(let j=0;j<this.customerArr.length;j++){
                      if(this.eventArr[i].customer == this.customerArr[j].uid || this.eventArr[i].customer == this.customerArr[j].id){
                          this.eventArr[i].c_name = this.customerArr[j].firstName + " " + this.customerArr[j].lastName;
                          this.eventArr[i].c_mobile = this.customerArr[j].phone;
                      }
                    }
                }
                this.eventArrSource.next(this.eventArr);
            }
        );

    }

    changeEventArray(eventArr: EventC[]) {
        this.eventArrSource.next(eventArr);
    }

    changeDate(date: NgbDateStruct) {
        this.dateSrc.next(date);
    }

    getTwoDigitValue(value){
        if(value <= 9)
          return "0"+value;
        else
          return value;   
    }

}
