import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { EventM } from '../_models/eventM';
import { CustomerM } from '../_models/customerM';
import { Services } from '@angular/core/src/view';
import { EventC } from '../_models/eventC';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StylistM } from '../_models/stylistM';
import { ServiceM } from '../_models/serviceM';

@Injectable()
export class FirestoreDataService {
    authS: any = null;
    stylistsCollection: AngularFirestoreCollection<StylistM>;
    stylists: Observable<StylistM[]>;
    currentStylistsCollection: AngularFirestoreCollection<StylistM>;
    currentStylist: Observable<StylistM[]>;
    stylist: Observable<StylistM>;
    stylistsDoc: AngularFirestoreDocument<StylistM>;

    eventsCollection: AngularFirestoreCollection<EventM>;
    events: Observable<EventM[]>;
    stylistsEventsCollection: AngularFirestoreCollection<EventM>;
    stylistsEvents: Observable<EventC[]>;
    eventDoc: AngularFirestoreDocument<EventM>;

    servicesMenCollection: AngularFirestoreCollection<ServiceM>;
    servicesMenAll: Observable<ServiceM[]>;
    servicesMenDoc: AngularFirestoreDocument<ServiceM>;
    serviceMen: Observable<ServiceM>;

    servicesWomenCollection: AngularFirestoreCollection<ServiceM>;
    servicesWomenAll: Observable<ServiceM[]>;
    servicesWomenDoc: AngularFirestoreDocument<ServiceM>;
    serviceWomen: Observable<ServiceM>;

    servicesChildrenCollection: AngularFirestoreCollection<ServiceM>;
    servicesChildrenAll: Observable<ServiceM[]>;
    servicesChildrenDoc: AngularFirestoreDocument<ServiceM>;
    serviceChildren: Observable<ServiceM>;
    services: Observable<ServiceM>;

    customerDoc: AngularFirestoreDocument<CustomerM>;
    customer: Observable<CustomerM>;
    customerSingleCollection: AngularFirestoreCollection<CustomerM>;
    customerSingle: Observable<CustomerM[]>;
    customersCollection: AngularFirestoreCollection<CustomerM>;
    customers: Observable<CustomerM[]>;

    response: string = "";

    calendarsCollection: AngularFirestoreCollection<any>;

    constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth, private http: HttpClient) {
        this.stylistsCollection = this.afs.collection('stylist');
        this.servicesMenCollection = this.afs.collection('services_men');
        this.servicesWomenCollection = this.afs.collection('services_women');
        this.servicesChildrenCollection = this.afs.collection('services_children');
        this.eventsCollection = this.afs.collection('bookings', x => x.orderBy('startDate'));

        this.calendarsCollection = this.afs.collection('calendars');

        this.afAuth.authState.subscribe((auth) => {
            if (auth) {
                this.authS = auth;
                //console.log(this.authS.uid);
                this.currentStylistsCollection = this.afs.collection<StylistM>('stylist', x => x.where('uid', '==', this.authS.uid));
                this.currentStylist = this.currentStylistsCollection.snapshotChanges().pipe(
                    map(actions => actions.map(a => {
                        const data = a.payload.doc.data() as StylistM;
                        data.id = a.payload.doc.id;
                        return data;
                    }))
                );

            }
        });

        this.events = this.eventsCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as EventM;
                        data.id = a.payload.doc.id;
                        return data;
                    });
            });


        //console.log(this.authS.uid);
        //this.stylistsCollection = this.afs.collection('stylists', x => x.where('uid','==',this.authS.uid)); 
    }


    //++++++++++++++++++++++++++++++++++++Stylist Http Methods++++++++++++++++++++++++++++++++++++++++++++ 
    addStylistToCollection(userData: StylistM) {
        return this.stylistsCollection.add(userData);
    }

    getStylistFromCollection() {
        return this.currentStylist;
    }

    getStylistById(id: string) {
        this.stylistsDoc = this.afs.doc(`stylist/` + id);
        this.stylist = this.stylistsDoc.valueChanges();

        return this.stylist;
    }

    getAllStylists() {
        //this.stylistsCollection = this.afs.collection('stylists');
        this.stylists = this.stylistsCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as StylistM;
                        const id = a.payload.doc.id;
                        return { id, ...data };
                    });
            });

        return this.stylists;
    }

    updateStylist(stylist: StylistM) {
        this.stylistsDoc = this.afs.doc(`stylist/${stylist.id}`);
        return this.stylistsDoc.update(stylist);
    }

    updateStylistColor(id, color) {
        //console.log(color+""+id);
        this.stylistsDoc = this.afs.doc(`stylist/${id}`);
        return this.stylistsDoc.update({
            color: color
        });
    }

    getStylistByUID(uid) {
        this.currentStylistsCollection = this.afs.collection('stylist', x => x.where('uid', '==', uid));
        this.currentStylist = this.currentStylistsCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as StylistM;
                        data.id = a.payload.doc.id;
                        return data;
                    });

            });
        return this.currentStylist;
    }

    updateStylistServices(services: string[], id: string, ) {
        this.stylistsDoc = this.afs.doc(`stylist/${id}`);
        return this.stylistsDoc.update(
            {
                services: services
            }
        );
    }


    updateStylistToken(id, fcmToken){
        this.stylistsDoc = this.afs.doc(`stylist/${id}`);
        return this.stylistsDoc.update(
            {
                fcmToken: fcmToken
            }
        );
    }

    deleteStylist(stylistId) {
        this.stylistsDoc = this.afs.doc(`stylist/${stylistId}`);
        return this.stylistsDoc.delete();
    }


    //++++++++++++++++++++++++++++++++++++Customer Http Methods++++++++++++++++++++++++++++++++++++++++++++
    addCustomer(customer) {
        return this.customersCollection.add(customer);
    }


    getCustomerByUid(id: string) {
        this.customerSingleCollection = this.afs.collection('Customers', x => x.where('uid', '==', id));
        this.customerSingle = this.customerSingleCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as CustomerM;
                        data.id = a.payload.doc.id;
                        return data;
                    });
            });
        return this.customerSingle;
    }

    getCustomerById(id: string) {
        this.customerDoc = this.afs.doc(`Customers/` + id);
        this.customer = this.customerDoc.valueChanges();
        return this.customer;
    }


    deleteCustomer(customerId) {
        this.customerDoc = this.afs.doc(`Customers/${customerId}`);
        return this.customerDoc.delete();
    }

    updateCustomer(customer) {
        this.customerDoc = this.afs.doc(`Customers/${customer.id}`);
        return this.customerDoc.update(customer);
    }


    getAllCustomers() {
        this.customersCollection = this.afs.collection('Customers');
        this.customers = this.customersCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as CustomerM;
                        const id = a.payload.doc.id;
                        return { id, ...data };
                    });
            });
        return this.customers;
    }


    //++++++++++++++++++++++++++++++++++++Services Men Http Methods++++++++++++++++++++++++++++++++++++++++++++
    getServiceMenById(id: string) {
        this.servicesMenDoc = this.afs.doc(`services_men/` + id);
        this.serviceMen = this.servicesMenDoc.valueChanges();

        return this.serviceMen;
    }

    getAllServicesMen() {
        //this.servicesMenCollection = this.afs.collection('service',x => x.orderBy('name'));
        this.servicesMenAll = this.servicesMenCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as ServiceM;
                        const id = a.payload.doc.id;
                        return { id, ...data };
                    });
            });
        return this.servicesMenAll;
    }

    addServiceMen(service: ServiceM) {
        //console.log(service);
        return this.servicesMenCollection.add(service);
    }

    addServiceMenForDoc(service, id) {
        return this.servicesMenCollection.doc(id).set(service);
    }

    deleteServiceMen(serviceId: string) {
        this.servicesMenDoc = this.afs.doc(`services_men/${serviceId}`);
        return this.servicesMenDoc.delete();
    }

    updateServiceMen(updatedServices, id) {
        //console.log(updatedServices);
        this.servicesMenDoc = this.afs.doc(`services_men/${id}`);
        return this.servicesMenDoc.update({
            services: updatedServices
        });
    }

    updateWholeServiceMen(updatedService) {
        this.servicesMenDoc = this.afs.doc(`services_men/${updatedService.id}`);
        return this.servicesMenDoc.update(updatedService);
    }


    //++++++++++++++++++++++++++++++++++++Services Women Http Methods++++++++++++++++++++++++++++++++++++++++++++
    getServiceWomenById(id: string) {
        this.servicesWomenDoc = this.afs.doc(`services_women/` + id);
        this.serviceWomen = this.servicesWomenDoc.valueChanges();

        return this.serviceWomen;
    }

    getAllServicesWomen() {
        //this.servicesMenCollection = this.afs.collection('service',x => x.orderBy('name'));
        this.servicesWomenAll = this.servicesWomenCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as ServiceM;
                        const id = a.payload.doc.id;
                        return { id, ...data };
                    });
            });
        return this.servicesWomenAll;
    }

    addServiceWomen(service: ServiceM) {
        //console.log(service);
        return this.servicesWomenCollection.add(service);
    }

    addServiceWomenForDoc(service, id) {
        return this.servicesWomenCollection.doc(id).set(service);
    }

    deleteServiceWomen(serviceId: string) {
        this.servicesWomenDoc = this.afs.doc(`services_women/${serviceId}`);
        return this.servicesWomenDoc.delete();
    }

    updateServiceWomen(updatedServices, id) {
        this.servicesWomenDoc = this.afs.doc(`services_women/${id}`);
        return this.servicesWomenDoc.update({
            services: updatedServices
        });
    }

    updateWholeServiceWomen(updatedService) {
        this.servicesWomenDoc = this.afs.doc(`services_women/${updatedService.id}`);
        return this.servicesWomenDoc.update(updatedService);
    }


    //++++++++++++++++++++++++++++++++++++Services Children Http Methods++++++++++++++++++++++++++++++++++++++++++++
    getServiceChildrenById(id: string) {
        this.servicesChildrenDoc = this.afs.doc(`services_children/` + id);
        this.serviceChildren = this.servicesChildrenDoc.valueChanges();

        return this.serviceChildren;
    }

    getAllServicesChildren() {
        //this.servicesMenCollection = this.afs.collection('service',x => x.orderBy('name'));
        this.servicesChildrenAll = this.servicesChildrenCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as ServiceM;
                        const id = a.payload.doc.id;
                        return { id, ...data };
                    });
            });
        return this.servicesChildrenAll;
    }

    addServiceChildren(service: ServiceM) {
        //console.log(service);
        return this.servicesChildrenCollection.add(service);
    }

    addServiceChildrenForDoc(service, id) {
        return this.servicesChildrenCollection.doc(id).set(service);
    }

    deleteServiceChildren(serviceId: string) {
        this.servicesChildrenDoc = this.afs.doc(`services_children/${serviceId}`);
        return this.servicesChildrenDoc.delete();
    }

    updateServiceChildren(updatedServices, id) {
        this.servicesChildrenDoc = this.afs.doc(`services_children/${id}`);
        return this.servicesChildrenDoc.update({
            services: updatedServices
        });
    }

    updateWholeServiceChildren(updatedService) {
        this.servicesChildrenDoc = this.afs.doc(`services_children/${updatedService.id}`);
        return this.servicesChildrenDoc.update(updatedService);
    }


    //++++++++++++++++++++++++++++++++++++Event Http Methods++++++++++++++++++++++++++++++++++++++++++++
    addEvent(event: EventM) {
        return this.eventsCollection.add(event);
    }

    updateEvent(booking: EventM) {
        this.eventDoc = this.afs.doc(`bookings/${booking.id}`);
        return this.eventDoc.update(booking);
    }

    getAllEvents() {
        //console.log(this.events);
        return this.events;
    }

    deleteEventFromCollection(id: string) {
        this.response = "";
        this.eventDoc = this.afs.doc(`bookings/${id}`);
        return this.eventDoc.delete();
    }

    getEventsForDate(date:string) {
        //console.log(date);
        //var d = new Date('2018-3-18');
        /* var d1 = new Date();

        d1.setMonth(date.getMonth());
        d1.setDate(date.getDate());
        d1.setFullYear(date.getFullYear());
        d1.setHours(23);
        d1.setMinutes(0);
        d1.setSeconds(0); */
        //console.log(date);
        //console.log(d1);
        this.stylistsEventsCollection = this.afs.collection('bookings', x => {
            return x.where('date', '==', date)
        });

        this.stylistsEvents = this.stylistsEventsCollection.snapshotChanges().map(
            changes => {
                return changes.map(
                    a => {
                        const data = a.payload.doc.data() as EventC;
                        data.id = a.payload.doc.id;
                        return data;
                    });

            });
        //console.log(this.stylistsEvents);
        return this.stylistsEvents;
    }



    createUserByEandP(email, password) {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    }


    getAllSlots(currentStylistID, startDate, endDate) {
        const params = new HttpParams()
            .set('currentStylistID', currentStylistID)
            .set('startDate', startDate)
            .set('endDate', endDate);

        return this.http.get('https://us-central1-meetdax4.cloudfunctions.net/freeBusyFlow1', { params });
    }

    addCalendar(calendar) {
        return this.calendarsCollection.add(calendar);
    }




}
