import { Injectable, ComponentFactoryResolver } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Router } from "@angular/router";
import "rxjs/add/operator/map";
import { AngularFireAuth } from "angularfire2/auth";
import { AlertService } from "./alert.service";
import { AlertComponent } from "../_directives/alert.component";


@Injectable()
export class AuthenticationService {
    loading = false;
    authState: any = null;
    constructor(private http: Http,
        private afAuth: AngularFireAuth,
        private _router: Router,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver) {

        this.afAuth.authState.subscribe((auth) => {
            this.authState = auth;
        });
    }



    /*  get authentication(): boolean{
         return this.authState !== null;
     } */

    login(email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    logout() {
        this.afAuth.auth.signOut();
        this._router.navigate(['/login']);
    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }
}