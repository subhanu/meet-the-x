export interface ServiceM {
    id?: string;
    name: string;
    price: number;
    description: string;
    imageUrl: string;
    services: any[];
    comments: string;
}