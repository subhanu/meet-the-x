import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

export interface EventAddM {
    id?: string;
    eid: string;
    customer: string;
    services: string[];
    stylist: string;
    date: NgbDateStruct;
    startTime: string;
    endTime: string;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
    comments: string;
}