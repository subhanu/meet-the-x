import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

export interface CustomerM {
    id?: string;
    uid: string;
    email: string;
    firstName: string;
    lastName: string;
    dob: Date;
    gender: string;
    phone: string;
    comments: string;
    imageUrl: string
}