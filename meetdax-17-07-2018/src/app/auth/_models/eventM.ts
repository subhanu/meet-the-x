export interface EventM {
    id?: string;
    eid: string;
    customer: string;
    imageUrl: string,
    services: string[];
    stylist: string;
    date: string;
    startTime: string;
    endTime: string;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
    comments: string;
}