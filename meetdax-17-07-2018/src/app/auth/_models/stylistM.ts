export interface StylistM {
    id?: string;
    uid: string,
    imageUrl: string,
    firstName: string,
    lastName: string,
    email: string,
    mobile: string,
    password: string,
    gender: string,
    dob: Date,
    services: string[],
    color: string,
    comments: string,
    fcmToken: string;
}