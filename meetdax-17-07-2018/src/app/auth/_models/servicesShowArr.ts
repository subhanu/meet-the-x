export interface ServicesShowArr {
    id?: string;
    name: string;
    price: number;
    description: string;
    imageUrl: string;
    stylists: string[];
    services: any[];
    comments: string;
    minMaxDur: string;
}