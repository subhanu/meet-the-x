export interface EventC {
    id?: string;
    eid: string;
    customer: string;
    c_name: string;
    c_mobile: string;
    services: string[];
    snames: string[];
    stylist: string;
    date: string;
    startTime: string;
    endTime: string;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
    comments: string;
}