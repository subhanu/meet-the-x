import { Component, OnInit, Injectable } from '@angular/core';
import { StylistM } from '../../../../auth/_models/stylistM';
import { AngularFirestore } from 'angularfire2/firestore';
import { FirestoreDataService } from '../../../../auth/_services/firestore-data.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { Helpers } from '../../../../helpers';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { CustomerM } from '../../../../auth/_models/index';

const now = new Date();

const I18N_VALUES = {
    'da': {
      weekdays: ['Ma', 'Ti', 'On', 'To', 'Fr', 'Lø', 'Sø'],
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
    }
    // other languages you would support
};
  

@Injectable()
export class I18n {
  language = 'da';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

    constructor(private _i18n: I18n) {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month: number): string {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month: number): string {
        return this.getMonthShortName(month);
    }

    getDayAriaLabel(date: NgbDateStruct): string {
        return `${date.day}-${date.month}-${date.year}`;
    }
}

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css'],
    providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class ProfileComponent implements OnInit {
    authS: any = null;
    ages: number[] = new Array(41);
    userData: StylistM;
    stylistDOB: NgbDateStruct = {year: 0, month: 0, day: 0};

    stylistArr: StylistM[] = [];
    stylistData: any = {
        uid: '',
        imageUrl: '',
        firstName: '',
        lastName: '',
        dob: null,
        email: '',
        mobile: '',
        gender: '',
        age: 0,
        services: []
    };

    selectedFile: File;
    private basePath: string = '/stylists';
    audio = new Audio();
    maxDate: NgbDateStruct = { year: now.getFullYear() - 16, month: 12, day: now.getDate() };
    isAlert: boolean = false;
    alertMessage: string = "";
    isProfileChanged: boolean = false;
    updateProfileForm: FormGroup;

    calendar: any = {
        stylist: "",
        calendar: ""
    }

    customerArr: CustomerM[] = [];
    newCustomerArr: CustomerM[] = [];

    constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth,
        private _firestoreService: FirestoreDataService, private _script: ScriptLoaderService, private _fb: FormBuilder) {

        this.afAuth.authState.subscribe((auth) => {
            if (auth) {
                this.authS = auth;
            }
        });

        this.updateProfileForm = this._fb.group({
           firstName: ["", [Validators.required, Validators.maxLength(16)]],
           lastName: ["", [Validators.required, Validators.maxLength(16)]],
           email: ["", [Validators.required, Validators.email]],
           gender: ["", Validators.required],
           dob: ["", Validators.required]

        });
        this.audio.src = "./assets/app/media/sound/to-the-point.mp3";
        this.audio.load();
        //console.log(this.stylistArr);
        //console.log(this.stylistData);
    }

    ngOnInit() {
        this._firestoreService.getStylistByUID(this.authS.uid).subscribe(
            (user: StylistM[]) => {
                //console.log(user);
                this.stylistArr = user;
                //console.log(this.stylistArr);
                
                this.stylistData = {
                    id: this.stylistArr[0].id,
                    uid: this.stylistArr[0].uid,
                    imageUrl: this.stylistArr[0].imageUrl,
                    firstName: this.stylistArr[0].firstName,
                    lastName: this.stylistArr[0].lastName,
                    email: this.stylistArr[0].email,
                    mobile: this.stylistArr[0].mobile,
                    gender: this.stylistArr[0].gender,
                    services: this.stylistArr[0].services,
                    dob: this.stylistArr[0].dob
                };

                var d = new Date(this.stylistData.dob);

                //d.setFullYear(this.stylistData.dob);
                //d.setFullYear(this.stylistData.dob);
                //d.setFullYear(this.stylistData.dob);

                this.stylistDOB = {year: d.getFullYear(),month: d.getMonth(),day: d.getDate()}
            }
        );

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {this.customerArr = data;}
        );
    }

    updateProfile() {
        Helpers.setLoading(true);
        let storageRef = firebase.storage().ref();
        this.stylistData.dob.setFullYear(this.stylistDOB.year);
        this.stylistData.dob.setMonth(this.stylistDOB.month);
        this.stylistData.dob.setDate(this.stylistDOB.day);

        if (this.isProfileChanged) {
            let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    // upload in progress
                },
                (error) => {
                    // upload failed
                    //console.log(error)
                },
                () => {
                    // upload success
                    this.stylistData.imageUrl = uploadTask.snapshot.downloadURL;
                    this._firestoreService.updateStylist(this.stylistData)
                    .then(() => {
                        Helpers.setLoading(false);
                        this.showToastr("Profile Updated Succesfully");
                    })
                    .catch((err) => {
                        //console.log(err);
                        Helpers.setLoading(false);
                        this.showToastr("Profile Updation Error");
                    });
                }
            );
        }
        else {
            this._firestoreService.updateStylist(this.stylistData)
            .then(() => {
                Helpers.setLoading(false);
                this.showToastr("Profile Updated Succesfully");
            })
            .catch((err) => {
                //console.log(err);
                Helpers.setLoading(false);
                this.showToastr("Profile Updation Error");
            });
        }

    }

    showToastr(msg) {
        //this.audio.play();
        this.isAlert = true;
        this.alertMessage = msg;
        setTimeout(() => this.isAlert = false, 5000);
    }

    imagePreview(file: FileList) {
        this.isProfileChanged = true;
        this.selectedFile = file.item(0);

        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.stylistData.imageUrl = event.target.result;
            //console.log(this.imageUrl);
        }
        reader.readAsDataURL(this.selectedFile);
    }

    onCalendarSubmit(){
        /* this._firestoreService.addCalendar(this.calendar)
        .then(()=>{
            this.showToastr("Added Calendar");
        })
        .catch((err)=>{
            this.showToastr(err);
        }); */

        this.showToastr("Hello");
    }


    /* onDoTheMagic(){
        //Helpers.setLoading(true);
        var booking = {
           comments: "", 
           customer: "",
           date: "",
           duration: 0,
           eid: "",
           endTime: "",
           how_often: "",
           imageUrl: "",
           price: 0,
           recurring: false,
           services: [],
           startTime: "",
           stylist: ""
        };

        var randomPhone = 10000140;
        var found = false;

        for(let k=0;k<this.sampleData.length;k++){
            if(this.sampleData[k].phone == "")
              this.sampleData[k].phone = "+45"+(randomPhone++).toString();
            else
              this.sampleData[k].phone = "+45"+(this.sampleData[k].phone).toString();
        }

        //console.log(this.sampleData);
        //console.log(this.customerArr);

        for(let i=0;i<this.sampleData.length;i++){
            Helpers.setLoading(true);
            found = false;
            booking = {
                comments: this.sampleData[i].comments,
                customer: "",
                date: this.sampleData[i].date,
                duration: this.calDuration(this.sampleData[i].startTime, this.sampleData[i].endTime),
                eid: "",
                endTime: this.sampleData[i].endTime,
                how_often: "",
                imageUrl: this.sampleData[i].imageUrl,
                price: 1,
                recurring: false,
                services: ["oejZHKvnS6g28wL40vqn"],
                startTime: this.sampleData[i].startTime,
                stylist: this.sampleData[i].stylist
            };

            //console.log(this.sampleData[i]);
            for(let j=0;j<this.customerArr.length;j++){
                //console.log(this.customerArr[j]);
                if(this.sampleData[i].phone == this.customerArr[j].phone){
                    booking.customer = this.customerArr[j].id;
                    this._firestoreService.addEvent(booking)
                    .then(()=>{
                        Helpers.setLoading(false);
                        console.log("success");
                    })
                    .catch((err)=>{
                        console.log(err);
                    });

                    break;
                }
            }  
        }
    }

    onAddCustomers(){
        var found = false;
        var randomPhone = 10000140;

        for(let k=0;k<this.sampleData.length;k++){
            this.sampleData[k].phone = "+45"+(this.sampleData[k].phone).toString();
        }

        for(let i=0;i<this.sampleData.length;i++){  
            Helpers.setLoading(true);
            found = false;
            if(this.sampleData[i].phone == "+45"){
                this.sampleData[i].phone = this.sampleData[i].phone+(randomPhone++).toString();

                var customer = {
                    comments: "",
                    dob: new Date(),
                    email: "",
                    firstName: this.sampleData[i].customer,
                    gender: "Male",
                    imageUrl: "https://firebasestorage.googleapis.com/v0/b/meetdax4.appspot.com/o/customers%2Fuser.png?alt=media&token=5120aab6-9f35-44ab-a764-8630258bd820",
                    lastName: "",
                    phone: (this.sampleData[i].phone).toString(),
                    uid: "" 
                 };
 
                 this.customerArr.push(customer);    
            }
            else{
                for(let j=0;j<this.customerArr.length;j++){
                    //console.log(this.sampleData[i].phone , this.customerArr[j].phone);
                    if(this.sampleData[i].phone == this.customerArr[j].phone){
                        found = true;
                        //console.log("found");
                        break;
                    }
                }

                if(found == false){
                    var customer = {
                        comments: "",
                        dob: new Date(),
                        email: "",
                        firstName: this.sampleData[i].customer,
                        gender: "Male",
                        imageUrl: "https://firebasestorage.googleapis.com/v0/b/meetdax4.appspot.com/o/customers%2Fuser.png?alt=media&token=5120aab6-9f35-44ab-a764-8630258bd820",
                        lastName: "",
                        phone: (this.sampleData[i].phone).toString(),
                        uid: "" 
                     };

                     this.customerArr.push(customer);
                }
            }
        }

        for(let k=397;k<this.customerArr.length;k++){
            Helpers.setLoading(true);
            this._firestoreService.addCustomer(this.customerArr[k])
            .then((docRef) =>{
                 Helpers.setLoading(false);
            })
            .catch((err)=>{
                console.log(err); 
            });  
        }
    } */


    calDuration(startTime, endTime){
        var sH = parseInt(startTime.substr(0,2));
        var eH = parseInt(endTime.substr(0,2));
        var sM = parseInt(startTime.substr(3,2));
        var eM = parseInt(endTime.substr(3,2));

        return (eH-sH)*60 + (eM-sM);
    }

}
