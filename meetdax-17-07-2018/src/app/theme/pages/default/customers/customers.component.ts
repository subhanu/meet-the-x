import { Component, OnInit, ViewChild, Injectable } from '@angular/core';
import { CustomerM } from '../../../../auth/_models/customerM';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import * as firebase from 'firebase';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Helpers } from '../../../../helpers';
import { FirestoreDataService } from '../../../../auth/_services/firestore-data.service';

const now = new Date();
const I18N_VALUES = {
  'da': {
    weekdays: ['MA', 'TI', 'ON', 'TO', 'FR', 'LØ', 'SØ'],
    months: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
  }
  // other languages you would support
};


@Injectable()
export class I18n {
  language = 'da';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private _i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
  providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class CustomersComponent implements OnInit {
  customersArr: CustomerM[] = [];
  customerAF: CustomerM = {
    uid: "",
    email: "",
    firstName: "",
    lastName: "",
    dob: null,
    gender: "",
    phone: "",
    comments: "",
    imageUrl: ""
  };

  customerToShow: any = {
    id: "",
    uid: "",
    email: "",
    firstName: "",
    lastName: "",
    age: "",
    dob: null,
    gender: "",
    phone: "",
    comments: "",
    imageUrl: ""
  }

  customerToShowArr: any[] = [];
  filteredCustomerArr: any[] = [];
  _listFilter: string;
  filterProperty: string = "firstName";

  customerUF: CustomerM = {
    uid: "",
    email: "",
    firstName: "",
    lastName: "",
    dob: null,
    gender: "",
    phone: "",
    comments: "",
    imageUrl: ""
  };
  customerDOB: NgbDateStruct;
  customerDOBU: NgbDateStruct = { year: 0, month: 0, day: 0 };
  deleteCustomerId: string;
  display = 'none';
  customerExists: boolean;

  isAlert: boolean = false;
  alertMessage: string = "";
  isSpinner: boolean = false;

  maxDate: NgbDateStruct = { year: now.getFullYear() - 12, month: 12, day: now.getDate() };

  selectedFile: File;
  private basePath: string = '/customers';

  addCustomerForm: FormGroup;
  updateCustomerForm: FormGroup;

  constructor(private _firestoreservices: FirestoreDataService, private _script: ScriptLoaderService,
    private _fb: FormBuilder) {
    this.customerAF.imageUrl = "assets/app/media/img/users/user.png";

    this.addCustomerForm = this._fb.group({
      firstName: ["", [Validators.required, Validators.maxLength(16)]],
      lastName: ["", [Validators.required, Validators.maxLength(16)]],
      imageUrl: ["", Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      dob: ["", Validators.required],
      phone: ["", Validators.compose([Validators.required, Validators.pattern('^[0-9]{8}$')])],
      gender: ["", Validators.required],
      comments: ["", [Validators.required, Validators.maxLength(100)]]
    });

    this.updateCustomerForm = this._fb.group({
      firstName: ["", [Validators.required, Validators.maxLength(16)]],
      lastName: ["", [Validators.required, Validators.maxLength(16)]],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      comments: ["", [Validators.required, Validators.maxLength(100)]],
      dob: ["", Validators.required]
    });
  }

  ngOnInit() {
    var d = new Date();
    var curYear = d.getFullYear();
    var curMon = d.getMonth() + 1;
    var curD = d.getDate();

    this._firestoreservices.getAllCustomers().subscribe(
      (data: CustomerM[]) => {
        this.customersArr = data;
        //console.log(this.customersArr);

        this.customerToShowArr = [];
        for (let i = 0; i < this.customersArr.length; i++) {
          this.customerToShow = {};

          this.customerToShow.id = this.customersArr[i].id;
          this.customerToShow.uid = this.customersArr[i].uid;
          this.customerToShow.firstName = this.customersArr[i].firstName;
          this.customerToShow.lastName = this.customersArr[i].lastName;
          this.customerToShow.dob = this.customersArr[i].dob;
          this.customerToShow.email = this.customersArr[i].email;
          this.customerToShow.gender = this.customersArr[i].gender;
          this.customerToShow.imageUrl = this.customersArr[i].imageUrl;
          this.customerToShow.phone = this.customersArr[i].phone;
          this.customerToShow.comments = this.customersArr[i].comments;

          var custd = new Date(this.customersArr[i].dob);
          var custYear = custd.getFullYear();
          var custMon = custd.getMonth() + 1;
          var custD = custd.getDate();

          //console.log(curYear,styYear,curMon,styMon,curD,styD,styd);
          if (curMon < custMon)
            this.customerToShow.age = curYear - (custYear + 1);
          else if (curMon == custMon) {
            if (curD >= custD)
              this.customerToShow.age = curYear - custYear;
            else
              this.customerToShow.age = curYear - (custYear + 1);
          }
          else if (curMon > custMon)
            this.customerToShow.age = curYear - (custYear);

          this.customerToShowArr.push(this.customerToShow);
        }

        //console.log(this.customerToShowArr);
        this.filteredCustomerArr = this.customerToShowArr;
      });
  }

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredCustomerArr = this.listFilter ? this.performFilter(this.listFilter) : this.customerToShowArr;
  }

  performFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    //console.log(this.filterProperty);
    var property = this.filterProperty;
    return this.customerToShowArr.filter((customer: any) =>
      customer.firstName.toLocaleLowerCase().indexOf(filterBy) != -1
      //customer[property].toLowerCase().indexOf(filterBy) != -1
    );
  }


  showAddCustomerModal() {
    this.display = 'block';
    $(".add-customer-modal").addClass("show");
  }

  hideAddCustomerModal() {
    this.display = 'none';
    $(".add-customer-modal").removeClass("show");

    this.addCustomerForm.reset({
      firstName: "",
      lastName: "",
      imageUrl: "",
      email: "",
      dob: "",
      phone: "",
      gender: "",
      comments: ""
    });

    this.customerAF.imageUrl = "assets/app/media/img/users/user.png";
  }

  showToastr(msg) {
    this.isAlert = true;
    this.alertMessage = msg;
    setTimeout(() => this.isAlert = false, 5000);
  }

  showConfirmDeleteModal() {
    this.display = 'block';
    $(".confirm-delete-modal").addClass("show");
  }

  hideConfirmDeleteModal() {
    this.display = 'none';
    $(".confirm-delete-modal").removeClass("show");
  }

  showUpdateCustomerModal(customer: CustomerM) {
    this.customerUF = JSON.parse(JSON.stringify(customer));
    //console.log(this.customerUF);
    //console.log(this.customerUF.dob.getFullYear());
    //console.log(this.customerDOBU);
    this.customerDOBU = { year: 0, month: 0, day: 0 };
    this.customerDOBU.year = customer.dob.getFullYear();
    this.customerDOBU.month = customer.dob.getMonth() + 1;
    this.customerDOBU.day = customer.dob.getDate();
    //console.log(this.customerDOBU);

    this.display = 'block';
    $(".update-customer-modal").addClass("show");
  }

  hideUpdateCustomerModal() {
    this.display = 'none';
    $(".update-customer-modal").removeClass("show");
  }

  onSubmitCustomer() {
    this.isSpinner = true;
    this.customerExists = false;
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);
    //this.hideAddCustomerModal();
    //console.log(this.customerAF);
    var d = new Date();
    //console.log(this.customerDOB);
    d.setFullYear(this.customerDOB.year);
    d.setMonth(this.customerDOB.month);
    d.setDate(this.customerDOB.day);
    this.customerAF.dob = d;
    //console.log(d);
    this.customerAF.phone = "+45"+this.customerAF.phone;

    for (let x = 0; x < this.customersArr.length; x++) {
      if (this.customersArr[x].phone == this.customerAF.phone){
        this.customerExists = true;
        break;
      }
    }

    if (!this.customerExists) {
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          // upload in progress
          //this.progressMessage = "Uploading Picture..."; 
        },
        (error) => {
          // upload failed
          console.log(error);
          this.showToastr("Fejl ved tilføjelse af kunde");

          //this.progressMessage = "Error Uploading Picture...";
        },
        () => {
          this.customerAF.imageUrl = uploadTask.snapshot.downloadURL;
          this._firestoreservices.addCustomer(this.customerAF)
            .then(() => {
              this.isSpinner = false;
              this.hideAddCustomerModal();
              this.showToastr("Kunde tilføjet succesfuldt");
            })
            .catch(() => {
              this.isSpinner = false;
              this.hideAddCustomerModal();
              this.showToastr("Fejl ved tilføjelse af kunde");
            });
        }
      );
    }
    else {
      this.isSpinner = false;
      this.hideAddCustomerModal();
      this.showToastr("Telefon findes allerede");
    }

    //this.addCustomerForm.reset();
  }

  onUpdateCustomer() {
    this.isSpinner = true;
    this.hideUpdateCustomerModal();
    var finalCustomerUpdate: any = {};
    var d = new Date();
    d.setFullYear(this.customerDOBU.year);
    d.setMonth(this.customerDOBU.month - 1);
    d.setDate(this.customerDOBU.day);

    this.customerUF.dob = d;
    //console.log(this.customerUF.dob);
    //console.log(this.customerUF);
    finalCustomerUpdate = {
      uid: this.customerUF.uid,
      id: this.customerUF.id,
      email: this.customerUF.email,
      firstName: this.customerUF.firstName,
      lastName: this.customerUF.lastName,
      dob: this.customerUF.dob,
      gender: this.customerUF.gender,
      phone: this.customerUF.phone,
      comments: this.customerUF.comments,
      imageUrl: this.customerUF.imageUrl
    };

    this._firestoreservices.updateCustomer(finalCustomerUpdate)
      .then(() => {
        this.isSpinner = false;
        this.showToastr("Kunde opdateret succesfuldt");
      })
      .catch((err) => {
        //console.log(err);
        this.isSpinner = false;
        this.showToastr("Fejl Opdatering af kunde");
      });
  }

  setDeleteId(customer: CustomerM) {
    this.deleteCustomerId = customer.id;
    this.showConfirmDeleteModal();
  }

  deleteCustomer() {
    Helpers.setLoading(true);
    this.hideConfirmDeleteModal();
    this._firestoreservices.deleteCustomer(this.deleteCustomerId)
      .then(() => {
        Helpers.setLoading(false);
        this.showToastr("Kunde slettet med succes");
      })
      .catch((err) => {
        Helpers.setLoading(false);
        this.showToastr("Fejl ved sletning af kunde");
      });
  }


  imagePreview(file: FileList) {
    this.selectedFile = file.item(0);
    //console.log(this.selectedFile);

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.customerAF.imageUrl = event.target.result;
      //console.log(this.imageUrl);
    }
    reader.readAsDataURL(this.selectedFile);
  }



}
