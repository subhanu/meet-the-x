import { Component, OnInit, Injectable } from '@angular/core';
import { StylistM } from '../../../../auth/_models/stylistM';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import * as firebase from 'firebase';
import { ServiceM } from '../../../../auth/_models/serviceM';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServicesStylist } from '../../../../auth/_models/index';
import { Helpers } from '../../../../helpers';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirestoreDataService } from '../../../../auth/_services/firestore-data.service';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

const now = new Date();
const I18N_VALUES = {
    'da': {
      weekdays: ['MA', 'TI', 'ON', 'TO', 'FR', 'LØ', 'SØ'],
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
    }
    // other languages you would support
  };
  
  
  @Injectable()
  export class I18n {
    language = 'da';
  }
  
  @Injectable()
  export class CustomDatepickerI18n extends NgbDatepickerI18n {
  
    constructor(private _i18n: I18n) {
      super();
    }
  
    getWeekdayShortName(weekday: number): string {
      return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month: number): string {
      return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month: number): string {
      return this.getMonthShortName(month);
    }
  
    getDayAriaLabel(date: NgbDateStruct): string {
      return `${date.day}-${date.month}-${date.year}`;
    }
  }
  
@Component({
    selector: 'app-stylists',
    templateUrl: './stylists.component.html',
    styleUrls: ['stylists.component.css'],
    providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})

export class StylistsComponent implements OnInit {
    stylistArr: StylistM[] = [];
    stylistToShowArr: any[] = [];
    filteredStylistArr: any[] = [];
    servicesMenArr: ServiceM[] = [];
    servicesWomenArr: ServiceM[] = [];
    servicesChildrenArr: ServiceM[] = [];

    _stylistFilter: string;

    stylistAddF: StylistM = {
        uid: '',
        imageUrl: '',
        firstName: '',
        lastName: '',
        email: '',
        mobile: '',
        password: '',
        gender: '',
        dob: null,
        services: [],
        color: '',
        comments: '',
        fcmToken: ''
    };

    stylistUpdateF: StylistM = {
        uid: '',
        imageUrl: '',
        firstName: '',
        lastName: '',
        email: '',
        mobile: '',
        password: '',
        gender: '',
        dob: null,
        services: [],
        color: '',
        comments: '',
        fcmToken: ''
    };

    serviceAddF: ServiceM = {
        name: "",
        price: 0,
        imageUrl: "",
        description: "",
        services: [],
        comments: ""
    }

    stylistToShow: any = {
        imageUrl: "",
        firstName: "",
        lastName: "",
        email: "",
        mobile: "",
        gender: "",
        age: 0,
        dob: null,
        services: [],
        servicesNames: [],
        serviceFor: [],
        color: "",
        comments: ""
    }

    selectedFile: File;
    selectedFileU: File;
    isAlert: boolean = false;
    alertMessage: string;
    alertClass: string;
    serviceForArr: any[] = [];
    colorsArr: any[] = [];
    stylistDOB: NgbDateStruct;
    stylistDOBU: NgbDateStruct = { year: 0, month: 0, day: 0 };
    stylistDocRefId: string;
    imageChanged: boolean;
    stylistExits: boolean;
    authS: any = null;
    isSpinner: boolean = false;

    deleteStylistId: string;
    deleteServices: string[] = [];

    services_men: ServicesStylist[] = [];
    services_women: ServicesStylist[] = [];
    services_children: ServicesStylist[] = [];

    services_men_u: ServicesStylist[] = [];
    services_women_u: ServicesStylist[] = [];
    services_children_u: ServicesStylist[] = [];

    originalServicesU: string[] = [];

    private basePath: string = '/stylists';
    maxDate: NgbDateStruct = { year: now.getFullYear() - 16, month: 12, day: now.getDate() };
    display = 'none';

    stylistAddForm: FormGroup;
    stylistUpdateForm: FormGroup;

    constructor(private _firestoreService: FirestoreDataService, private _script: ScriptLoaderService,
        private _fb: FormBuilder, private afAuth: AngularFireAuth) {
        this.stylistAddF.imageUrl = "assets/app/media/img/users/user.png";

        this.serviceForArr = [
            {
                name: 'Men',
                selected: false
            },
            {
                name: 'Women',
                selected: false
            },
            {
                name: 'Children',
                selected: false
            }
        ];

        this.colorsArr = [
            {
                name: 'Rød',
                code: '#ffebee'
            },
            {
                name: 'Grøn',
                code: '#f1f8e9'
            },
            {
                name: 'Blå',
                code: '#e1f5fe'
            },
            {
                name: 'Grå',
                code: '#eceff1'
            },
            {
                name: 'Orange',
                code: '#fff3e0'
            },
            {
                name: 'Lilla',
                code: '#f3e5f5'
            },
            {
                name: 'Lyserød',
                code: '#fce4ec'
            },
            {
                name: 'Standard',
                code: '#ffffff'
            },
        ];

        this.stylistAddForm = this._fb.group({
            imageUrl: ["", Validators.required],
            firstName: ["", [Validators.required, Validators.maxLength(16)]],
            lastName: ["", [Validators.required, Validators.maxLength(16)]],
            email: ["", Validators.compose([Validators.required, Validators.email])],
            mobile: ["", Validators.compose([Validators.required, Validators.pattern('^[0-9]{8}$')])],
            dob: ["", Validators.required],
            gender: ["", Validators.required],
            color: ["", Validators.required],
            comments: ["", [Validators.required, Validators.maxLength(100)]],
        });

        this.stylistUpdateForm = this._fb.group({
            firstName: ["", [Validators.required, Validators.maxLength(16)]],
            lastName: ["", [Validators.required, Validators.maxLength(16)]],
            email: ["", Validators.compose([Validators.required, Validators.email])],
            dob: ["", Validators.required],
            gender: ["", Validators.required],
            color: ["", Validators.required],
            comments: ["", [Validators.required, Validators.maxLength(100)]],
        });
    }


    ngOnInit() {
        var d = new Date();
        var curYear = d.getFullYear();
        var curMon = d.getMonth() + 1;
        var curD = d.getDate();

        this.afAuth.authState.subscribe((auth) => {
            if (auth) {
                this.authS = auth;
                //console.log(this.authS);
            }
        });

        this._firestoreService.getAllServicesMen().subscribe(
            (data) => {
                this.servicesMenArr = data;
                //console.log(this.servicesMenArr);
            }
        );

        this._firestoreService.getAllServicesWomen().subscribe(
            (data) => {
                this.servicesWomenArr = data;
                //console.log(this.servicesWomenArr);
            }
        );

        this._firestoreService.getAllServicesChildren().subscribe(
            (data) => {
                this.servicesChildrenArr = data;
                //console.log(this.servicesChildrenArr);
            }
        );

        this._firestoreService.getAllStylists().subscribe(
            (user: StylistM[]) => {
                this.stylistArr = user;
                //console.log(this.stylistArr);
                this.stylistToShowArr = [];
                for (let i = 0; i < this.stylistArr.length; i++) {
                    this.stylistToShow = {};
                    this.stylistToShow.servicesNames = [];
                    this.stylistToShow.id = this.stylistArr[i].id;
                    this.stylistToShow.firstName = this.stylistArr[i].firstName;
                    this.stylistToShow.lastName = this.stylistArr[i].lastName;
                    this.stylistToShow.email = this.stylistArr[i].email;
                    this.stylistToShow.mobile = this.stylistArr[i].mobile;
                    this.stylistToShow.gender = this.stylistArr[i].gender;
                    this.stylistToShow.imageUrl = this.stylistArr[i].imageUrl;
                    this.stylistToShow.comments = this.stylistArr[i].comments;
                    this.stylistToShow.services = this.stylistArr[i].services;
                    this.stylistToShow.dob = this.stylistArr[i].dob;
                    this.stylistToShow.color = this.stylistArr[i].color;
                    this.stylistToShow.uid = this.stylistArr[i].uid;

                    var styd = new Date(this.stylistArr[i].dob);
                    var styYear = styd.getFullYear();
                    var styMon = styd.getMonth() + 1;
                    var styD = styd.getDate();
                    //console.log(curYear,styYear,curMon,styMon,curD,styD,styd);

                    if(curMon < styMon)
                        this.stylistToShow.age = curYear - (styYear+1);
                    else if(curMon == styMon){
                        if(curD >= styD)
                           this.stylistToShow.age = curYear - styYear;
                        else
                           this.stylistToShow.age = curYear - (styYear+1); 
                    }  
                    else if(curMon > styMon) 
                        this.stylistToShow.age = curYear - (styYear);    


                    for (let j = 0; j < this.servicesMenArr.length; j++) {
                        for (let k = 0; k < this.stylistArr[i].services.length; k++) {
                            if (this.stylistArr[i].services[k] == this.servicesMenArr[j].id) {
                                this.stylistToShow.servicesNames.push(this.servicesMenArr[j].name + " (Herrer)");
                            }
                        }
                    }

                    for (let j = 0; j < this.servicesWomenArr.length; j++) {
                        for (let k = 0; k < this.stylistArr[i].services.length; k++) {
                            //console.log(this.stylistArr[i].services[k],this.servicesWomenArr[j].id);
                            if (this.stylistArr[i].services[k] == this.servicesWomenArr[j].id) {
                                this.stylistToShow.servicesNames.push(this.servicesWomenArr[j].name + " (Kvinder)");
                            }
                        }
                    }

                    for (let j = 0; j < this.servicesChildrenArr.length; j++) {
                        for (let k = 0; k < this.stylistArr[i].services.length; k++) {
                            if (this.stylistArr[i].services[k] == this.servicesChildrenArr[j].id) {
                                this.stylistToShow.servicesNames.push(this.servicesChildrenArr[j].name + " (Børn)");
                            }
                        }
                    }

                    //console.log(this.stylistToShow);
                    this.stylistToShowArr.push(this.stylistToShow);
                }
                //console.log(this.stylistToShowArr);
                this.filteredStylistArr = this.stylistToShowArr;
            });
    }

    get stylistFilter(): string {
      return this._stylistFilter;
    }
  
    set stylistFilter(value: string) {
      this._stylistFilter = value;
      this.filteredStylistArr = this.stylistFilter ? this.performFilter(this.stylistFilter) : this.stylistToShowArr;
    }
  
    performFilter(filterBy: string) {
      filterBy = filterBy.toLocaleLowerCase();
      return this.stylistToShowArr.filter((stylist: any) =>
        stylist.firstName.toLocaleLowerCase().indexOf(filterBy) != -1
      );
    }
  
    showAddStylistModal() {
        this.display = 'block';
        $(".add-stylist-modal").addClass("show");
    }

    hideAddStylistModal() {
        this.display = 'none';
        $(".add-stylist-modal").removeClass("show");

       
        /* console.log(this.services_men);
        console.log(this.services_women);
        console.log(this.services_children);
        console.log(this.stylistAddF); */
    }

    resetAddStylist(){
        this.stylistAddForm.reset({
            sname: "",
            simageUrl: "",
            soffered: "",
            sdescription: "",
            sprice: 0,
            scomments: ""
        });

        for (let z = 0; z < this.servicesMenArr.length; z++) {
            $("#" + z + "ism").prop('checked', false);
            $("#" + z + "iSM").val("");
        }

        for (let z = 0; z < this.servicesWomenArr.length; z++) {
            $("#" + z + "isf").prop('checked', false);
            $("#" + z + "iSF").val("");
        }

        for (let z = 0; z < this.servicesChildrenArr.length; z++) {
            $("#" + z + "isc").prop('checked', false);
            $("#" + z + "iSC").val("");
        }

        this.services_men = [];
        this.services_women = [];
        this.services_children = [];
        this.stylistAddF.services = [];
        this.stylistAddF.imageUrl = "assets/app/media/img/users/user.png";
    }


    showUpdateStylistModal() {
        this.display = 'block';
        $(".update-stylist-modal").addClass("show");
    }

    hideUpdateStylistModal() {
        this.display = 'none';
        $(".update-stylist-modal").removeClass("show");
    }

    showConfirmDeleteModal() {
        this.display = 'block';
        $(".confirm-delete-modal").addClass("show");
    }

    hideConfirmDeleteModal() {
        this.display = 'none';
        $(".confirm-delete-modal").removeClass("show");
    }

    imagePreview(file: FileList) {
        this.selectedFile = file.item(0);

        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.stylistAddF.imageUrl = event.target.result;
            //console.log(this.imageUrl);
        }
        reader.readAsDataURL(this.selectedFile);
    }


    selectService(service, i, event) {
        if (event.target.checked == false) {
            //console.log(this.eventAddF.duration);
            var index = this.stylistAddF.services.indexOf(service.id);
            this.stylistAddF.services.splice(index, 1);


            if (i.indexOf('iSM') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_men.length; i++) {
                    if (this.services_men[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_men.splice(index, 1);
            }
            else if (i.indexOf('iSF') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_women.length; i++) {
                    if (this.services_women[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_women.splice(index, 1);
            }
            else if (i.indexOf('iSC') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_children.length; i++) {
                    if (this.services_children[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_children.splice(index, 1);
            }
        }
        else {
            this.stylistAddF.services.push(service.id);

            if (i.indexOf('iSM') > -1) {
                this.services_men.push(
                    {
                        duration: $("#" + i).val(),
                        service: service.id,
                        stylist: ""
                    }
                );
            }
            else if (i.indexOf('iSF') > -1) {
                this.services_women.push(
                    {
                        duration: $("#" + i).val(),
                        service: service.id,
                        stylist: ""
                    }
                );
            }
            else if (i.indexOf('iSC') > -1) {
                this.services_children.push(
                    {
                        duration: $("#" + i).val(),
                        service: service.id,
                        stylist: ""
                    }
                );
            }
        }

        /*  console.log(this.services_men);
         console.log(this.services_women);
         console.log(this.services_children);
         console.log(this.stylistAddF.services); */
    }

    onChangeDuration(service, ind, id) {
        //console.log(ind, id);
        var duration = $("#" + id).val();
        if ($("#" + ind + "ism").prop("checked")) {
            for (let i = 0; i < this.services_men.length; i++) {
                if (service.id == this.services_men[i].service) {
                    this.services_men[i].duration = duration;
                    break;
                }
            }
        }

        if ($("#" + ind + "isf").prop("checked")) {
            for (let i = 0; i < this.services_women.length; i++) {
                if (service.id == this.services_women[i].service) {
                    this.services_women[i].duration = duration;
                    break;
                }
            }
        }

        if ($("#" + ind + "isc").prop("checked")) {
            for (let i = 0; i < this.services_children.length; i++) {
                if (service.id == this.services_children[i].service) {
                    this.services_children[i].duration = duration;
                    break;
                }
            }
        }


        /*  console.log(this.services_men);
         console.log(this.services_women);
         console.log(this.services_children); */
    }

    onSubmitStylist() {
        //let stylistDocRefId: string;
        var sMenFlag: boolean = false;
        var sWomenFlag: boolean = false;
        var sChildrenFlag: boolean = false;

        this.hideAddStylistModal();
        Helpers.setLoading(true);
        this.stylistExits = false;
        this.stylistAddF.dob = new Date();
        //console.log(this.stylistDOB);
        this.stylistAddF.dob.setFullYear(this.stylistDOB.year);
        this.stylistAddF.dob.setMonth(this.stylistDOB.month - 1);
        this.stylistAddF.dob.setDate(this.stylistDOB.day);
        this.stylistAddF.mobile = "+45"+this.stylistAddF.mobile;
        //console.log(this.stylistAddF);
        //this.showProgressModal();
        let storageRef = firebase.storage().ref();
        let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);

        
        for(let x=0;x<this.stylistArr.length;x++){
            //console.log(this.stylistArr[x].mobile,this.stylistAddF.mobile);
            if(this.stylistArr[x].mobile == this.stylistAddF.mobile){
                this.stylistExits = true; 
                break;
            }
        }

        if(!this.stylistExits){
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    // upload in progress
                },
                (error) => {
                    // upload failed
                    //console.log(error);
                    this.showToastr("Fejl ved at tilføje stylist");
                    //this.hideAddStylistModal();
                },
                () => {
                    // upload success
                    //console.log(uploadTask.snapshot.downloadURL);
                    this.stylistAddF.imageUrl = uploadTask.snapshot.downloadURL;
                                    
                    this._firestoreService.addStylistToCollection(this.stylistAddF)
                        .then((docRef) => {
                            //console.log(this.services_men);
                            for (let m = 0; m < this.services_men.length; m++) {
                                for (let s = 0; s < this.servicesMenArr.length; s++) {
                                    //console.log(this.services_men[m].service+"/"+this.servicesMenArr[s].id);
                                    if (this.servicesMenArr[s].id == this.services_men[m].service) {
                                        var tempServices = [];
                                        this.services_men[m].stylist = docRef.id;
                                        tempServices = this.servicesMenArr[s].services;
                                        delete this.services_men[m].service;
                                        tempServices.push(this.services_men[m]);
    
                                        //console.log(this.services_men[m]);
                                        //console.log(this.servicesMenArr[s].services);
                                        this._firestoreService.updateServiceMen(tempServices, this.servicesMenArr[s].id)
                                            .then(() => {
                                                //console.log("Added Services Too for Men");
                                                sMenFlag = true;
                                                this.showToastr("Stylist tilføjet succesfuldt");
                                                Helpers.setLoading(false);
                                            })
                                            .catch((err) => {
                                                //console.log(err);
                                            });
                                    }
                                }
                            }
    
                            for (let m = 0; m < this.services_women.length; m++) {
                                for (let s = 0; s < this.servicesWomenArr.length; s++) {
                                    if (this.servicesWomenArr[s].id == this.services_women[m].service) {
                                        var tempServices = [];
                                        this.services_women[m].stylist = docRef.id;
                                        tempServices = this.servicesWomenArr[s].services;
                                        delete this.services_women[m].service;
                                        tempServices.push(this.services_women[m]);
    
                                        this._firestoreService.updateServiceWomen(tempServices, this.servicesWomenArr[s].id)
                                            .then(() => {
                                                //console.log("Added Services Too for Women");  
                                                sWomenFlag = true;
                                                this.showToastr("Stylist tilføjet succesfuldt");
                                                Helpers.setLoading(false);
                                            })
                                            .catch((err) => {
                                                //console.log(err);
                                            });
                                    }
                                }
                            }
    
                            for (let m = 0; m < this.services_children.length; m++) {
                                for (let s = 0; s < this.servicesChildrenArr.length; s++) {
                                    if (this.servicesChildrenArr[s].id == this.services_children[m].service) {
                                        var tempServices = [];
                                        this.services_children[m].stylist = docRef.id;
                                        tempServices = this.servicesChildrenArr[s].services;
                                        delete this.services_children[m].service;
                                        tempServices.push(this.services_children[m]);
                                        //console.log(this.servicesChildrenArr[s].services);
                                        this._firestoreService.updateServiceChildren(tempServices, this.servicesChildrenArr[s].id)
                                            .then(() => {
                                                //console.log("Added Services Too for Children");  
                                                sChildrenFlag = true;
                                                this.showToastr("Stylist tilføjet succesfuldt");
                                                Helpers.setLoading(false);
                                            })
                                            .catch((err) => {
                                                //console.log(err);
                                            });
                                    }
                                }
                            }
                        })
                        .catch((err) => {
                            //console.log(err);
                        });

                        /* if(sMenFlag == true && sWomenFlag == true && sChildrenFlag == true){
                            this.resetAddStylist();
                            this.showToastr("Stylist tilføjet succesfuldt");
                            Helpers.setLoading(false);
                        } */
                    //this.hideAddStylistModal();         
                }
            );
        }
        else{
            Helpers.setLoading(false);
            this.showToastr("Nummer findes allerede, brug forskelligt!!");
        }
    }


    showToastr(msg) {
        this.isAlert = true;
        this.alertMessage = msg;
        //console.log(this.isAlert+"/"+this.alertMessage);
        setTimeout(() => this.isAlert = false, 5000);
    }


    setDeleteId(stylist) {
        this.deleteStylistId = stylist.id;
        this.deleteServices = stylist.services;
        /* this.deleteServiceFor = service.serviceFor;
        console.log(this.deleteServiceFor);
        //console.log(service);
        for(let i=0;i<service.services.length;i++){
          this.deleteStylistArr.push(service.services[i].stylist);
        } */
        //console.log(this.deleteServiceId,this.deleteServiceFor);
        console.log(stylist.uid , this.authS.uid);
        if(stylist.uid != this.authS.uid)
           this.showConfirmDeleteModal();
    }


    deleteStylist() {
        this.hideConfirmDeleteModal();
        Helpers.setLoading(true);
        var index = -1;
        var services: any[] = [];
        this._firestoreService.deleteStylist(this.deleteStylistId)
            .then(() => {
                //console.log("Deleted Stylist");
            })
            .catch((err) => {
                //console.log(err);
            });

        //console.log(this.deleteServices);
        //console.log(this.servicesMenArr);
        for (let i = 0; i < this.deleteServices.length; i++) {
            for (let j = 0; j < this.servicesMenArr.length; j++) {
                if (this.deleteServices[i] == this.servicesMenArr[j].id) {
                    for (let k = 0; k < this.servicesMenArr[j].services.length; k++) {
                        if (this.servicesMenArr[j].services[k].stylist == this.deleteStylistId) {
                            services = this.servicesMenArr[j].services;

                            services.splice(k, 1);
                            //console.log(services);
                            this._firestoreService.updateServiceMen(services, this.servicesMenArr[j].id)
                                .then(() => {
                                    this.showToastr("Stylist slettet succesfuldt");
                                    Helpers.setLoading(false);
                                })
                                .catch((err) => {
                                    //console.log(err);
                                });
                            break;
                        }
                    }
                }
                services = [];
            }
            index = -1;

            for (let j = 0; j < this.servicesWomenArr.length; j++) {
                if (this.deleteServices[i] == this.servicesWomenArr[j].id) {
                    for (let k = 0; k < this.servicesWomenArr[j].services.length; k++) {
                        if (this.servicesWomenArr[j].services[k].stylist == this.deleteStylistId) {
                            services = this.servicesWomenArr[j].services;
                            services.splice(k, 1);
                            this._firestoreService.updateServiceWomen(services, this.servicesWomenArr[j].id)
                                .then(() => {
                                    this.showToastr("Stylist slettet succesfuldt");
                                    Helpers.setLoading(false);
                                })
                                .catch((err) => {
                                    //console.log(err);
                                });
                            break;
                        }
                    }
                }
                services = [];
            }
            index = -1;

            for (let j = 0; j < this.servicesChildrenArr.length; j++) {
                if (this.deleteServices[i] == this.servicesChildrenArr[j].id) {
                    for (let k = 0; k < this.servicesChildrenArr[j].services.length; k++) {
                        if (this.servicesChildrenArr[j].services[k].stylist == this.deleteStylistId) {
                            services = this.servicesChildrenArr[j].services;
                            services.splice(k, 1);
                            this._firestoreService.updateServiceChildren(services, this.servicesChildrenArr[j].id)
                                .then(() => {
                                    this.showToastr("Stylist slettet succesfuldt");
                                    Helpers.setLoading(false);
                                })
                                .catch((err) => {
                                    //console.log(err);
                                });
                            break;
                        }
                    }
                }
                services = [];
            }
            index = -1;
        }
    }


    setStylistForUpdate(stylist: StylistM) {
        //console.log(stylist);
        this.stylistUpdateF = JSON.parse(JSON.stringify(stylist));
        this.originalServicesU = this.stylistUpdateF.services.slice(0);
        //console.log(this.stylistUpdateF);
        this.stylistDOBU = { year: 0, month: 0, day: 0 };
        this.stylistDOBU.year = stylist.dob.getFullYear();
        this.stylistDOBU.month = stylist.dob.getMonth() + 1;
        this.stylistDOBU.day = stylist.dob.getDate();
        //console.log(this.stylistDOBU);

        for (let z = 0; z < this.servicesMenArr.length; z++) {
            $("#" + z + "iSMS").prop('checked', false);
            $("#" + z + "iSMD").val("");
        }

        for (let z = 0; z < this.servicesWomenArr.length; z++) {
            $("#" + z + "iSFS").prop('checked', false);
            $("#" + z + "iSFD").val("");
        }

        for (let z = 0; z < this.servicesChildrenArr.length; z++) {
            $("#" + z + "iSCS").prop('checked', false);
            $("#" + z + "iSCD").val("");
        }

        this.services_men_u = [];
        this.services_women_u = [];
        this.services_children_u = [];


        for (let i = 0; i < stylist.services.length; i++) {
            for (let m = 0; m < this.servicesMenArr.length; m++) {
                if (stylist.services[i] == this.servicesMenArr[m].id) {
                    $('#' + m + 'iSMS').prop('checked', true);
                    for (let j = 0; j < this.servicesMenArr[m].services.length; j++) {
                        if (stylist.id == this.servicesMenArr[m].services[j].stylist) {
                            $('#' + m + 'iSMD').val(this.servicesMenArr[m].services[j].duration);
                            this.services_men_u.push({
                                duration: this.servicesMenArr[m].services[j].duration,
                                service: this.servicesMenArr[m].id,
                                stylist: stylist.id
                            });
                        }
                    }
                    //console.log(this.servicesMenArr[m]);
                }
            }

            for (let m = 0; m < this.servicesWomenArr.length; m++) {
                if (stylist.services[i] == this.servicesWomenArr[m].id) {
                    $('#' + m + 'iSFS').prop('checked', true);
                    for (let j = 0; j < this.servicesWomenArr[m].services.length; j++) {
                        if (stylist.id == this.servicesWomenArr[m].services[j].stylist) {
                            $('#' + m + 'iSFD').val(this.servicesWomenArr[m].services[j].duration);
                            this.services_women_u.push({
                                duration: this.servicesWomenArr[m].services[j].duration,
                                service: this.servicesWomenArr[m].id,
                                stylist: stylist.id
                            });
                        }
                    }
                    //console.log(this.servicesMenArr[m]);
                }
            }

            for (let m = 0; m < this.servicesChildrenArr.length; m++) {
                if (stylist.services[i] == this.servicesChildrenArr[m].id) {
                    $('#' + m + 'iSCS').prop('checked', true);
                    for (let j = 0; j < this.servicesChildrenArr[m].services.length; j++) {
                        if (stylist.id == this.servicesChildrenArr[m].services[j].stylist) {
                            $('#' + m + 'iSCD').val(this.servicesChildrenArr[m].services[j].duration);
                            this.services_children_u.push({
                                duration: this.servicesChildrenArr[m].services[j].duration,
                                service: this.servicesChildrenArr[m].id,
                                stylist: stylist.id
                            });
                        }
                    }
                    //console.log(this.servicesMenArr[m]);
                }
            }
        }
        //this.originalServices = this.serviceUpdateF.services.slice(0);
        /*  console.log(this.services_men_u);
         console.log(this.services_women_u);
         console.log(this.services_children_u); */
        this.showUpdateStylistModal();
    }


    selectServiceU(service, ind, id, event) {
        //console.log(service.id+"/"+$("#"+i).val()+"/"+i);
        if (event.target.checked == false) {
            //console.log(this.eventAddF.duration);
            var index = this.stylistUpdateF.services.indexOf(service.id);
            this.stylistUpdateF.services.splice(index, 1);

            if (id.indexOf('iSMS') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_men_u.length; i++) {
                    if (this.services_men_u[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_men_u.splice(index, 1);
            }
            else if (id.indexOf('iSFS') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_women_u.length; i++) {
                    if (this.services_women_u[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_women_u.splice(index, 1);
            }
            else if (id.indexOf('iSCS') > -1) {
                var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
                for (let i = 0; i < this.services_children_u.length; i++) {
                    if (this.services_children_u[i].service == service.id) {
                        index = i;
                        break;
                    }
                }
                this.services_children_u.splice(index, 1);
            }
        }
        else {
            this.stylistUpdateF.services.push(service.id);
            //console.log("#"+i);
            if (id.indexOf('iSMS') > -1) {
                this.services_men_u.push(
                    {
                        duration: $("#" + ind + "iSMD").val(),
                        service: service.id,
                        stylist: this.stylistUpdateF.id
                    }
                );
            }
            else if (id.indexOf('iSFS') > -1) {
                //console.log(id);
                this.services_women_u.push(
                    {
                        duration: $("#" + ind + "iSFD").val(),
                        service: service.id,
                        stylist: this.stylistUpdateF.id
                    }
                );
            }
            else if (id.indexOf('iSCS') > -1) {
                this.services_children_u.push(
                    {
                        duration: $("#" + ind + "iSCD").val(),
                        service: service.id,
                        stylist: this.stylistUpdateF.id
                    }
                );
            }
        }

        /*  console.log(this.services_men_u);
         console.log(this.services_women_u);
         console.log(this.services_children_u);
         console.log(this.stylistUpdateF.services); */
    }


    onChangeDurationU(service, ind, id) {
        //console.log(ind, id);
        var duration = $("#" + id).val();
        if ($("#" + ind + "iSMS").prop("checked")) {
            for (let i = 0; i < this.services_men_u.length; i++) {
                if (service.id == this.services_men_u[i].service) {
                    this.services_men_u[i].duration = duration;
                    break;
                }
            }
        }

        if ($("#" + ind + "iSFS").prop("checked")) {
            for (let i = 0; i < this.services_women_u.length; i++) {
                if (service.id == this.services_women_u[i].service) {
                    this.services_women_u[i].duration = duration;
                    break;
                }
            }
        }

        if ($("#" + ind + "iSCS").prop("checked")) {
            for (let i = 0; i < this.services_children_u.length; i++) {
                if (service.id == this.services_children_u[i].service) {
                    this.services_children_u[i].duration = duration;
                    break;
                }
            }
        }


        /*  console.log(this.services_men_u);
         console.log(this.services_women_u);
         console.log(this.services_children_u); */
    }

    imagePreviewU(file: FileList) {
        this.selectedFileU = file.item(0);
        this.imageChanged = false;

        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.stylistUpdateF.imageUrl = event.target.result;
            this.imageChanged = true;
            //console.log(this.imageUrl);
        }
        reader.readAsDataURL(this.selectedFileU);
    }

    onUpdateStylist() {
        this.isSpinner = true;
        if (this.imageChanged) {
            let storageRef = firebase.storage().ref();
            let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFileU.name}`).put(this.selectedFileU);
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {

                },
                (error) => {
                    // upload failed
                    //console.log(error);
                    this.isSpinner = false;
                    this.hideUpdateStylistModal();
                    this.showToastr("Fejl Opdatering af Stylist");
                },
                () => {
                    this.finalUpdateForStylist(uploadTask.snapshot.downloadURL);
                });
        }
        else {
            this.finalUpdateForStylist(this.stylistUpdateF.imageUrl);
        }
    }



    finalUpdateForStylist(imageUrl) {
        //console.log(this.stylistUpdateF);
        var finalStylistUpdate: any = {};
        this.stylistUpdateF.imageUrl = imageUrl;
        //console.log(this.stylistUpdateF.imageUrl);
        var copyServicesMen, copyServicesWomen, copyServicesChildren;
        copyServicesMen = copyServicesWomen = copyServicesChildren = [];
        copyServicesMen = JSON.parse(JSON.stringify(this.servicesMenArr));
        copyServicesWomen = JSON.parse(JSON.stringify(this.servicesWomenArr));
        copyServicesChildren = JSON.parse(JSON.stringify(this.servicesChildrenArr));
        //console.log(copyServicesMen);

        this.stylistUpdateF.dob = new Date();
        this.stylistUpdateF.dob.setFullYear(this.stylistDOBU.year);
        this.stylistUpdateF.dob.setMonth(this.stylistDOBU.month);
        this.stylistUpdateF.dob.setDate(this.stylistDOBU.day);

        finalStylistUpdate = {
            id: this.stylistUpdateF.id,
            color: this.stylistUpdateF.color,
            comments: this.stylistUpdateF.comments,
            dob: this.stylistUpdateF.dob,
            email: this.stylistUpdateF.email,
            firstName: this.stylistUpdateF.firstName,
            lastName: this.stylistUpdateF.lastName,
            gender: this.stylistUpdateF.gender,
            imageUrl: this.stylistUpdateF.imageUrl,
            mobile: this.stylistUpdateF.mobile,
            services: this.stylistUpdateF.services
        };

        //console.log(finalStylistUpdate);

        this._firestoreService.updateStylist(finalStylistUpdate)
            .then(() => {

                for (let m = 0; m < this.services_men_u.length; m++) {
                    for (let s = 0; s < copyServicesMen.length; s++) {
                        //console.log(this.services_men_u[m].service, copyServicesMen[s].id, s);
                        if (this.services_men_u[m].service == copyServicesMen[s].id) {
                            var index = -1;
                            var isFound = false;
                            for (let t = 0; t < copyServicesMen[s].services.length; t++) {
                                //console.log(copyServicesMen[s].services[t].stylist, this.services_men_u[m].stylist);
                                if (copyServicesMen[s].services[t].stylist == this.services_men_u[m].stylist) {
                                    index = t;
                                    isFound = true;
                                    break;
                                }
                            }

                            //console.log(copyServicesMen[s]);
                            if (isFound) {
                                copyServicesMen[s].services[index].duration = this.services_men_u[m].duration;
                                //console.log(copyServicesMen[s]);
                            }
                            else {
                                copyServicesMen[s].services.push({
                                    duration: this.services_men_u[m].duration,
                                    stylist: this.services_men_u[m].stylist
                                });
                                //console.log(copyServicesMen[s]);
                            }

                            //console.log(copyServicesMen[s].services);
                            this._firestoreService.updateServiceMen(copyServicesMen[s].services, copyServicesMen[s].id)
                                .then(() => {
                                    //console.log("Success");
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Stylist opdateret med succes");
                                })
                                .catch((err) => {
                                    //console.log(err);
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Fejl Opdatering af Stylist");
                                });
                        }
                    }
                }

                for (let m = 0; m < this.services_women_u.length; m++) {
                    for (let s = 0; s < copyServicesWomen.length; s++) {
                        //console.log(this.services_women_u[m].service, copyServicesWomen[s].id, s);
                        if (this.services_women_u[m].service == copyServicesWomen[s].id) {
                            var index = -1;
                            var isFound = false;
                            for (let t = 0; t < copyServicesWomen[s].services.length; t++) {
                                //console.log(copyServicesWomen[s].services[t].stylist, this.services_women_u[m].stylist);
                                if (copyServicesWomen[s].services[t].stylist == this.services_women_u[m].stylist) {
                                    index = t;
                                    isFound = true;
                                    break;
                                }
                            }

                            //console.log(copyServicesWomen[s]);
                            if (isFound) {
                                copyServicesWomen[s].services[index].duration = this.services_women_u[m].duration;
                                //console.log(copyServicesWomen[s]);
                            }
                            else {
                                copyServicesWomen[s].services.push({
                                    duration: this.services_women_u[m].duration,
                                    stylist: this.services_women_u[m].stylist
                                });
                                //console.log(copyServicesWomen[s]);
                            }

                            //console.log(copyServicesWomen[s].services);
                            this._firestoreService.updateServiceWomen(copyServicesWomen[s].services, copyServicesWomen[s].id)
                                .then(() => {
                                    //console.log("Success");
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Stylist opdateret med succes");
                                })
                                .catch((err) => {
                                    //console.log(err);
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Fejl Opdatering af Stylist");
                                });
                        }
                    }
                }

                for (let m = 0; m < this.services_children_u.length; m++) {
                    for (let s = 0; s < copyServicesChildren.length; s++) {
                        //console.log(this.services_children_u[m].service, copyServicesChildren[s].id, s);
                        if (this.services_children_u[m].service == copyServicesChildren[s].id) {
                            var index = -1;
                            var isFound = false;
                            for (let t = 0; t < copyServicesChildren[s].services.length; t++) {
                                //console.log(copyServicesChildren[s].services[t].stylist, this.services_children_u[m].stylist);
                                if (copyServicesChildren[s].services[t].stylist == this.services_children_u[m].stylist) {
                                    index = t;
                                    isFound = true;
                                    break;
                                }
                            }

                            //console.log(copyServicesChildren[s]);
                            if (isFound) {
                                copyServicesChildren[s].services[index].duration = this.services_children_u[m].duration;
                                //console.log(copyServicesChildren[s]);
                            }
                            else {
                                copyServicesChildren[s].services.push({
                                    duration: this.services_children_u[m].duration,
                                    stylist: this.services_children_u[m].stylist
                                });
                                //console.log(copyServicesChildren[s]);
                            }

                            //console.log(copyServicesChildren[s].services);
                            this._firestoreService.updateServiceChildren(copyServicesChildren[s].services, copyServicesChildren[s].id)
                                .then(() => {
                                    //console.log("Success");
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Stylist opdateret med succes");
                                })
                                .catch((err) => {
                                    //console.log(err);
                                    this.isSpinner = false;
                                    this.hideUpdateStylistModal();
                                    this.showToastr("Fejl Opdatering af Stylist");
                                });
                        }
                    }
                }

                for (let n = 0; n < this.originalServicesU.length; n++) {
                    var found = false;
                    var index = this.stylistUpdateF.services.indexOf(this.originalServicesU[n]);
                    //console.log(this.originalServicesU, this.stylistUpdateF.services, index); 
                    if (index == -1) {
                        for (let s = 0; s < copyServicesMen.length; s++) {
                            //console.log(copyServicesMen[s], this.originalServicesU[n]);
                            if (copyServicesMen[s].id == this.originalServicesU[n]) {
                                var index1 = -1;
                                for (let t = 0; t < copyServicesMen[s].services.length; t++) {
                                    if (copyServicesMen[s].services[t].stylist == this.stylistUpdateF.id) {
                                        index1 = t;
                                        break;
                                    }
                                }
                                copyServicesMen[s].services.splice(index1, 1);
                                //console.log(copyServicesMen[s].services);
                                this._firestoreService.updateServiceMen(copyServicesMen[s].services, copyServicesMen[s].id)
                                    .then(() => {
                                        //console.log("Success");
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Stylist opdateret med succes");
                                    })
                                    .catch((err) => {
                                        //console.log(err);
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Fejl Opdatering af Stylist");
                                    });
                            }
                        }

                        for (let s = 0; s < copyServicesWomen.length; s++) {
                            //console.log(copyServicesMen[s].services[t].stylist, this.services_men_u[m].stylist);
                            if (copyServicesWomen[s].id == this.originalServicesU[n]) {
                                var index1 = -1;
                                for (let t = 0; t < copyServicesWomen[s].services.length; t++) {
                                    if (copyServicesWomen[s].services[t].stylist == this.stylistUpdateF.id) {
                                        index1 = t;
                                        break;
                                    }
                                }
                                copyServicesWomen[s].services.splice(index1, 1);
                                //console.log(copyServicesWomen[s].services);
                                this._firestoreService.updateServiceWomen(copyServicesWomen[s].services, copyServicesWomen[s].id)
                                    .then(() => {
                                        //console.log("Success");
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Stylist opdateret med succes");
                                    })
                                    .catch((err) => {
                                        //console.log(err);
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Fejl Opdatering af Stylist");
                                    });
                            }
                        }

                        for (let s = 0; s < copyServicesChildren.length; s++) {
                            //console.log(copyServicesMen[s].services[t].stylist, this.services_men_u[m].stylist);
                            if (copyServicesChildren[s].id == this.originalServicesU[n]) {
                                var index1 = -1;
                                for (let t = 0; t < copyServicesChildren[s].services.length; t++) {
                                    if (copyServicesChildren[s].services[t].stylist == this.stylistUpdateF.id) {
                                        index1 = t;
                                        break;
                                    }
                                }
                                copyServicesChildren[s].services.splice(index1, 1);
                                //console.log(copyServicesChildren[s].services);
                                this._firestoreService.updateServiceChildren(copyServicesChildren[s].services, copyServicesChildren[s].id)
                                    .then(() => {
                                        //console.log("Success");
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Stylist opdateret med succes");
                                    })
                                    .catch((err) => {
                                        //console.log(err);
                                        this.isSpinner = false;
                                        this.hideUpdateStylistModal();
                                        this.showToastr("Fejl Opdatering af Stylist");
                                    });
                            }
                        }
                    }
                }

            })
            .catch((err) => {
                //console.log(err);
                this.isSpinner = false;
                this.hideUpdateStylistModal();
                this.showToastr("Fejl Opdatering af Stylist");
            });
    }



}
