import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ServiceM } from '../../../../auth/_models/serviceM';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { Observable } from 'rxjs/Observable';
import { StylistM } from '../../../../auth/_models/index';
import { AnonymousSubject } from 'rxjs/Subject';
import { ServicesShowArr } from '../../../../auth/_models/index';
import * as firebase from 'firebase/app';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Helpers } from '../../../../helpers';
import { FirestoreDataService } from '../../../../auth/_services/firestore-data.service';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit {
    menToShow: any = {
        id: "",
        name: "",
        price: 0,
        imageUrl: "",
        description: "",
        comments: "",
        stylists: [],
        services: [],
        serviceFor: "",
        minMaxDur: ""
    };
    womenToShow: any = {
        id: "",
        name: "",
        price: 0,
        imageUrl: "",
        description: "",
        comments: "",
        stylists: [],
        services: [],
        serviceFor: "",
        minMaxDur: ""
    };
    childrenToShow: any = {
        id: "",
        name: "",
        price: 0,
        imageUrl: "",
        description: "",
        comments: "",
        stylists: [],
        services: [],
        serviceFor: "",
        minMaxDur: ""
    };

    menArr: ServicesShowArr[] = [];
    womenArr: ServicesShowArr[] = [];
    childrenArr: ServicesShowArr[] = [];

    servicesMenArr: ServiceM[] = [];
    servicesWomenArr: ServiceM[] = [];
    servicesChildrenArr: ServiceM[] = [];

    stylistsArr: any[] = [];
    stylistTA: any = [];
    stylistD: StylistM = null;
    stylistDocRef: string;
    serviceNamesDocRefId: string;
    newServiceDocRef: string;
    deleteServiceId: string;
    deleteServiceFor: string;
    deleteStylistId: string;
    deleteStylistArr: string[] = [];
    stylistId: string;
    display = 'none';
    serviceNamesId: string;

    serviceFor: string;
    serviceForU: string;
    originalServiceForU: string;

    isAlert: boolean = false;
    alertMessage: string = "";    
    isSpinner: boolean = false;

    isServiceMenPresent: boolean = false;
    isServiceWomenPresent: boolean = false;
    isServiceChildrenPresent: boolean = false;

    selectedFile: File;
    private basePath: string = '/services';

    serviceAddF: ServiceM = {
        name: "",
        price: 0,
        description: "",
        imageUrl: "",
        services: [],
        comments: ""
    };

    serviceUpdateF: ServiceM = {
        name: "",
        price: 0,
        description: "",
        imageUrl: "",
        services: [],
        comments: ""
    };

    finalServiceUpdateF: ServiceM = {
        name: "",
        price: 0,
        description: "",
        imageUrl: "",
        services: [],
        comments: ""
    };

    allStylists: string[];
    originalServices: any[] = [];

    serviceAddForm: FormGroup;
    serviceUpdateForm: FormGroup;


    constructor(private _firestoreservices: FirestoreDataService, private _script: ScriptLoaderService,
        private _fb: FormBuilder) {
        this.serviceAddForm = this._fb.group({
            sname: ["", [Validators.required, Validators.maxLength(16)]],
            simageUrl: ["", Validators.required],
            soffered: ["", Validators.required],
            sdescription: ["", [Validators.required, Validators.maxLength(100)]],
            sprice: ["", [Validators.required, Validators.min(1)]],
            scomments: ["", [Validators.required, Validators.maxLength(100)]]
        });

        this.serviceUpdateForm = this._fb.group({
            snameU: ["", [Validators.required, Validators.maxLength(16)]],
            sdescriptionU: ["", [Validators.required, Validators.maxLength(100)]],
            spriceU: ["", [Validators.required, Validators.min(1)]],
            scommentsU: ["", [Validators.required, Validators.maxLength(100)]]
        });

        this.serviceAddF.imageUrl = "assets/app/media/img/misc/service.png";
    }


    ngOnInit() {
        this._firestoreservices.getAllStylists().subscribe(
            (data) => {
                this.stylistsArr = data;
                //console.log(this.stylistsArr);
            }
        );

        this._firestoreservices.getAllServicesMen().subscribe(
            (data) => {
                this.servicesMenArr = data;
                //console.log(this.servicesMenArr);

                if (this.servicesMenArr.length == 0) {
                    this.menArr = [];
                }
                else {
                    for (let i = 0; i < this.servicesMenArr.length; i++) {
                        this.menToShow.id = this.servicesMenArr[i].id;
                        this.menToShow.name = this.servicesMenArr[i].name;
                        this.menToShow.price = this.servicesMenArr[i].price;
                        this.menToShow.description = this.servicesMenArr[i].description;
                        this.menToShow.comments = this.servicesMenArr[i].comments;
                        this.menToShow.imageUrl = this.servicesMenArr[i].imageUrl;
                        this.menToShow.services = this.servicesMenArr[i].services;
                        this.menToShow.serviceFor = "Men";
                        this.menToShow.comments = this.servicesMenArr[i].comments;
                        this.menToShow.stylists = this.getStylistNames(this.servicesMenArr[i].services);
                        this.menToShow.minMaxDur = this.getMinMaxDuration(this.servicesMenArr[i].services);
                        //console.log(this.menToShow);            

                        this.menArr[i] = this.menToShow;
                        this.menToShow = {};
                    }
                }
            }
        );

        this._firestoreservices.getAllServicesWomen().subscribe(
            (data) => {
                this.servicesWomenArr = data;
                //console.log(this.servicesWomenArr);
                if (this.servicesWomenArr.length == 0) {
                    this.womenArr = [];
                }
                else {
                    for (let i = 0; i < this.servicesWomenArr.length; i++) {
                        this.womenToShow.id = this.servicesWomenArr[i].id;
                        this.womenToShow.name = this.servicesWomenArr[i].name;
                        this.womenToShow.price = this.servicesWomenArr[i].price;
                        this.womenToShow.description = this.servicesWomenArr[i].description;
                        this.womenToShow.comments = this.servicesWomenArr[i].comments;
                        this.womenToShow.imageUrl = this.servicesWomenArr[i].imageUrl;
                        this.womenToShow.services = this.servicesWomenArr[i].services;
                        this.womenToShow.serviceFor = "Women";
                        this.womenToShow.comments = this.servicesWomenArr[i].comments;
                        this.womenToShow.stylists = this.getStylistNames(this.servicesWomenArr[i].services);
                        this.womenToShow.minMaxDur = this.getMinMaxDuration(this.servicesWomenArr[i].services);
                        //console.log(this.womenToShow);            

                        this.womenArr[i] = this.womenToShow;
                        this.womenToShow = {};
                    }
                }
                //console.log(this.womenArr);
            }
        );

        this._firestoreservices.getAllServicesChildren().subscribe(
            (data) => {
                this.servicesChildrenArr = data;
                //console.log(this.servicesChildrenArr);
                if (this.servicesChildrenArr.length == 0) {
                    this.childrenArr = [];
                }
                else {
                    for (let i = 0; i < this.servicesChildrenArr.length; i++) {
                        this.childrenToShow.id = this.servicesChildrenArr[i].id;
                        this.childrenToShow.name = this.servicesChildrenArr[i].name;
                        this.childrenToShow.price = this.servicesChildrenArr[i].price;
                        this.childrenToShow.description = this.servicesChildrenArr[i].description;
                        this.childrenToShow.comments = this.servicesChildrenArr[i].comments;
                        this.childrenToShow.imageUrl = this.servicesChildrenArr[i].imageUrl;
                        this.childrenToShow.services = this.servicesChildrenArr[i].services;
                        this.childrenToShow.serviceFor = "Children";
                        this.childrenToShow.comments = this.servicesChildrenArr[i].comments;
                        this.childrenToShow.stylists = this.getStylistNames(this.servicesChildrenArr[i].services);
                        this.childrenToShow.minMaxDur = this.getMinMaxDuration(this.servicesChildrenArr[i].services);
                        //console.log(this.childrenToShow);            

                        this.childrenArr[i] = this.childrenToShow;
                        this.childrenToShow = {};
                    }
                }

                //console.log(this.childrenArr);
            }
        );
    }


    showConfirmDeleteModal() {
        this.display = 'block';
        $(".confirm-delete-modal").addClass("show");
    }

    hideConfirmDeleteModal() {
        this.display = 'none';
        $(".confirm-delete-modal").removeClass("show");
    }

    showAddServiceModal() {
        this.display = 'block';
        $(".add-service-modal").addClass("show");
    }

    hideAddServiceModal() {
        this.display = 'none';
        $(".add-service-modal").removeClass("show");

        this.serviceAddForm.reset({
            sname: "",
            simageUrl: "",
            soffered: "",
            sdescription: "",
            sprice: 0,
            scomments: ""
        });

        for (let i = 0; i < this.stylistsArr.length; i++) {
            $("#" + i + "sty").prop("checked", false);
            $("#" + i + "dur").val(0);
        }

        this.serviceAddF.imageUrl = "assets/app/media/img/misc/service.png";
        this.serviceAddF.services = [];
        //console.log(this.serviceAddF);
    }

    showUpdateServiceModal() {
        this.display = 'block';
        $(".update-service-modal").addClass("show");
    }

    hideUpdateServiceModal() {
        this.display = 'none';
        $(".update-service-modal").removeClass("show");
        //this.serviceUpdateForm.reset();
    }

    showToastr(msg) {
        this.isAlert = true;
        this.alertMessage = msg;
        setTimeout(() => this.isAlert = false, 5000);
    }


    selectStylist(stylistId, i, event) {
        //console.log(stylistId+"/"+$("#"+i).val());
        if (event.target.checked == false) {
            //console.log(this.eventAddF.duration);
            var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
            for (let i = 0; i < this.serviceAddF.services.length; i++) {
                if (this.serviceAddF.services[i].stylist == stylistId) {
                    index = i;
                    break;
                }
            }

            this.serviceAddF.services.splice(index, 1);
            //console.log(this.serviceAddF.services);   
        }
        else {
            this.serviceAddF.services.push(
                {
                    duration: $("#" + i + "dur").val(),
                    stylist: stylistId,
                }
            );
            //console.log(this.serviceAddF.services);
        }
    }

    setStylistDuration(stylistId, i) {
        //console.log($("#"+i+"sty").prop("checked"),$("#"+i+"upd").val());
        var duration = $("#" + i + "dur").val();
        if ($("#" + i + "sty").prop("checked")) {
            for (let i = 0; i < this.serviceAddF.services.length; i++) {
                //console.log(stylistId ,this.serviceUpdateF.services[i].stylist);
                if (stylistId == this.serviceAddF.services[i].stylist) {
                    //console.log($("#"+i+"upd").val(),this.serviceUpdateF.services[i].duration);
                    this.serviceAddF.services[i].duration = duration;
                    //console.log($("#"+i+"upd").val(),this.serviceUpdateF.services[i].duration);
                }
            }
        }
        //console.log(this.serviceAddF);
    }

    onSubmitService() {
        this.isSpinner = true;
        let storageRef = firebase.storage().ref();
        let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                // upload in progress
            },
            (error) => {
                // upload failed
                //console.log(error)
                this.isSpinner = false;
            },
            () => {
                // upload success
                this.serviceAddF.imageUrl = uploadTask.snapshot.downloadURL;

                if (this.serviceFor == "Men") {
                    this.isServiceMenPresent = false;
                    var pos;

                    for (let i = 0; i < this.servicesMenArr.length; i++) {
                        pos = i;
                        //console.log("Here");
                        //console.log(this.servicesMenArr[i].name+"/"+this.serviceAddF.name);
                        if (this.servicesMenArr[i].name == this.serviceAddF.name) {
                            this.isServiceMenPresent = true;
                            break;
                        }
                    }

                    if (this.isServiceMenPresent) {
                        this.servicesMenArr[pos].services.push(this.serviceAddF.services);
                        //console.log(this.serviceAddF.services);
                        //console.log(this.servicesMenArr[pos].services);

                        this._firestoreservices.updateServiceMen(this.servicesMenArr[pos].services, this.servicesMenArr[pos].id)
                            .then(() => {
                                this.updateStylistServices(this.servicesMenArr[pos].id);
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                    else {
                        this._firestoreservices.addServiceMen(this.serviceAddF)
                            .then((docRef) => {
                                this.updateStylistServices(docRef.id);
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                }

                //++++++++++++++++++++++++++ServicesFor Women Logic++++++++++++++++++++++++++++++++++
                else if (this.serviceFor == "Women") {
                    this.isServiceWomenPresent = false;
                    var pos;

                    for (let i = 0; i < this.servicesWomenArr.length; i++) {
                        pos = i;
                        //console.log("Here");
                        //console.log(this.servicesWomenArr[i].name+"/"+this.serviceAddF.name);
                        if (this.servicesWomenArr[i].name == this.serviceAddF.name) {
                            this.isServiceWomenPresent = true;
                            break;
                        }
                    }

                    if (this.isServiceWomenPresent) {
                        //console.log(this.serviceAddF.services);
                        this.servicesWomenArr[pos].services.push(this.serviceAddF.services);

                        //console.log(this.servicesWomenArr[pos].services);
                        this._firestoreservices.updateServiceWomen(this.servicesWomenArr[pos].services, this.servicesWomenArr[pos].id)
                            .then(() => {
                                this.updateStylistServices(this.servicesWomenArr[pos].id);
                                //this.showToastr("New Service Added");
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                    else {
                        this._firestoreservices.addServiceWomen(this.serviceAddF)
                            .then((docRef) => {
                                this.updateStylistServices(docRef.id);
                                //this.showToastr("New Service Added");
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                }

                //+++++++++++++++++++++++++ServicesFor Children Logic+++++++++++++++++++++++++++++++++++++++++++
                else if (this.serviceFor == "Children") {
                    this.isServiceChildrenPresent = false;
                    var pos;

                    for (let i = 0; i < this.servicesChildrenArr.length; i++) {
                        pos = i;
                        //console.log("Here");
                        //console.log(this.servicesChildrenArr[i].name+"/"+this.serviceAddF.name);
                        if (this.servicesChildrenArr[i].name == this.serviceAddF.name) {
                            this.isServiceChildrenPresent = true;
                            break;
                        }
                    }

                    if (this.isServiceChildrenPresent) {
                        //console.log(this.serviceAddF.services);
                        this.servicesChildrenArr[pos].services.push(this.serviceAddF.services);

                        //console.log(this.servicesChildrenArr[pos].services);
                        this._firestoreservices.updateServiceChildren(this.servicesChildrenArr[pos].services, this.servicesChildrenArr[pos].id)
                            .then(() => {
                                this.updateStylistServices(this.servicesChildrenArr[pos].id);
                                //this.showToastr("New Service Added");
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                    else {
                        this._firestoreservices.addServiceChildren(this.serviceAddF)
                            .then((docRef) => {
                                this.updateStylistServices(docRef.id);
                                //this.showToastr("New Service Added");
                            })
                            .catch((err) => {
                                //console.log(err);
                                this.isSpinner = false;
                            });
                    }
                }
            }
        );
    }

    updateStylistServices(id) {
        for (let i = 0; i < this.serviceAddF.services.length; i++) {
            for (let j = 0; j < this.stylistsArr.length; j++) {
                if (this.serviceAddF.services[i].stylist == this.stylistsArr[j].id) {
                    this.stylistsArr[j].services.push(id);
                    this._firestoreservices.updateStylistServices(this.stylistsArr[j].services, this.serviceAddF.services[i].stylist)
                        .then(() => {
                            this.isSpinner = false;
                            this.hideAddServiceModal();
                            this.showToastr("Ny Service Tilføjet");
                            //this.isServiceMenPresent = false;
                        })
                        .catch((err) => {
                            //console.log(err);
                            this.isSpinner = false;
                            this.hideAddServiceModal();
                            this.showToastr("Ny service tilføjelsesfejl");
                        })
                }
            }
        }
    }

    setDeleteId(service) {
        this.deleteServiceId = service.id;
        this.deleteServiceFor = service.serviceFor;
        //console.log(this.deleteServiceFor);
        //console.log(service);
        for (let i = 0; i < service.services.length; i++) {
            this.deleteStylistArr.push(service.services[i].stylist);
        }
        //console.log(this.deleteServiceId,this.deleteServiceFor);
        this.showConfirmDeleteModal();
    }

    deleteService() {
        Helpers.setLoading(true);
        this.hideConfirmDeleteModal();

        if (this.deleteServiceFor == "Men") {
            this._firestoreservices.deleteServiceMen(this.deleteServiceId)
                .then(() => {
                    this.deleteServicesFromStylists();
                    //this.showToastr("Service Slettet");
                })
                .catch((err) => {
                    this.showToastr("Fejl Ved At Slette Tjenesten");
                    Helpers.setLoading(false);
                });
        }
        else if (this.deleteServiceFor == "Women") {
            this._firestoreservices.deleteServiceWomen(this.deleteServiceId)
                .then(() => {
                    this.deleteServicesFromStylists();
                    //this.showToastr("Service Slettet");
                })
                .catch((err) => {
                    this.showToastr("Fejl Ved At Slette Tjenesten");
                    Helpers.setLoading(false);
                });
        }
        else if (this.deleteServiceFor == "Children") {
            this._firestoreservices.deleteServiceChildren(this.deleteServiceId)
                .then(() => {
                    //this.deleteServicesFromStylists();
                    //this.showToastr("Service Slettet");
                })
                .catch((err) => {
                    this.showToastr("Fejl Ved At Slette Tjenesten");
                    Helpers.setLoading(false);
                });
        }
    }


    deleteServicesFromStylists() {
        for (let i = 0; i < this.deleteStylistArr.length; i++) {
            //console.log(this.deleteStylistArr[i]);
            for (let j = 0; j < this.stylistsArr.length; j++) {
                //console.log(this.stylistsArr[j].id,this.deleteStylistArr[i]);
                if (this.stylistsArr[j].id == this.deleteStylistArr[i]) {
                    let stylistServices = this.stylistsArr[j].services;
                    let index = stylistServices.indexOf(this.deleteServiceId);
                    stylistServices.splice(index, 1);
                    //console.log(stylistServices,this.deleteStylistArr[i]);
                    this._firestoreservices.updateStylistServices(stylistServices, this.deleteStylistArr[i])
                        .then(() => {
                            this.isSpinner = false;
                            this.showToastr("Service Slettet");
                        })
                        .catch((err) => {
                            this.isSpinner = false;
                            this.showToastr("Fejl Ved At Slette Tjenesten");
                        })
                }
            }
        }
    }


    setServiceForUpdate(service) {
        //console.log(service);
        this.serviceUpdateF = {
            name: "",
            price: 0,
            description: "",
            imageUrl: "",
            services: [],
            comments: ""
        };

        this.serviceUpdateF = JSON.parse(JSON.stringify(service));
        this.originalServices = this.serviceUpdateF.services.slice(0);

        //console.log(this.originalServices);
        //console.log(this.serviceUpdateF.services);

        for (let i = 0; i < this.stylistsArr.length; i++) {
            $("#" + i + "styUpd").prop('checked', false);
            $("#" + i + "durUpd").val("");
        }

        if ($("#MenU").val() == service.serviceFor) {
            this.serviceForU = "Men";
            this.originalServiceForU = "Men";
            $("#MenU").prop('checked', true);
        }
        else if ($("#WomenU").val() == service.serviceFor) {
            this.serviceForU = "Women";
            this.originalServiceForU = "Women";
            $("#WomenU").prop('checked', true);
        }
        else if ($("#ChildrenU").val() == service.serviceFor) {
            this.serviceForU = "Children";
            this.originalServiceForU = "Children";
            $("#ChildrenU").prop('checked', true);
        }

        for (let i = 0; i < this.stylistsArr.length; i++) {
            for (let j = 0; j < service.services.length; j++) {
                if (this.stylistsArr[i].id == service.services[j].stylist) {
                    $("#" + i + "styUpd").prop('checked', true);
                    $("#" + i + "durUpd").val(service.services[j].duration);
                }
            }
        }

        this.showUpdateServiceModal();
    }


    selectStylistU(stylistId, i, event) {
        //console.log(stylistId+"/"+$("#"+i+"upd").val());
        if (event.target.checked == false) {
            //console.log(this.eventAddF.duration);
            var index = -1; //= this.serviceAddF.services.indexOf(stylistId);
            for (let i = 0; i < this.serviceUpdateF.services.length; i++) {
                if (this.serviceUpdateF.services[i].stylist == stylistId) {
                    index = i;
                    break;
                }
            }

            this.serviceUpdateF.services.splice(index, 1);
            //console.log(index);   
        }
        else {
            this.serviceUpdateF.services.push(
                {
                    duration: $("#" + i + "durUpd").val(),
                    stylist: stylistId
                }
            );
        }
        //console.log(this.originalServices);
        //console.log(this.serviceUpdateF.services);
    }


    setStylistDurationU(stylistId, i) {
        //console.log($("#"+i+"sty").prop("checked"),$("#"+i+"upd").val());
        var duration = $("#" + i + "durUpd").val();
        if ($("#" + i + "styUpd").prop("checked")) {
            for (let i = 0; i < this.serviceUpdateF.services.length; i++) {
                //console.log(stylistId ,this.serviceUpdateF.services[i].stylist);
                if (stylistId == this.serviceUpdateF.services[i].stylist) {
                    //console.log($("#"+i+"upd").val(),this.serviceUpdateF.services[i].duration);
                    this.serviceUpdateF.services[i].duration = duration;
                    //console.log($("#"+i+"upd").val(),this.serviceUpdateF.services[i].duration);
                }
            }
        }
        //console.log(this.serviceUpdateF);
    }


    getMinMaxDuration(services) {
        var min, max;
        if (services.length > 0)
            min = max = services[0].duration;
        else
            min = max = 0;

        for (let i = 0; i < services.length; i++) {
            if (parseInt(services[i].duration) < min)
                min = parseInt(services[i].duration);
            if (parseInt(services[i].duration) > max)
                max = parseInt(services[i].duration);
        }

        if (min == max)
            return min;
        else
            return min + " - " + max;
    }


    getStylistNames(services) {
        var stylists: string[] = [];
        for (let i = 0; i < services.length; i++) {
            for (let j = 0; j < this.stylistsArr.length; j++) {
                if (services[i].stylist == this.stylistsArr[j].id)
                    stylists.push(this.stylistsArr[j].firstName + " " + this.stylistsArr[j].lastName);
            }
        }
        return stylists;
    }

    onUpdateService() {
        //console.log(this.serviceUpdateF);
        //console.log(this.originalServiceForU);
        //console.log(this.serviceForU);
        this.isSpinner = true;
        this.finalServiceUpdateF.id = this.serviceUpdateF.id;
        this.finalServiceUpdateF.name = this.serviceUpdateF.name;
        this.finalServiceUpdateF.description = this.serviceUpdateF.description;
        this.finalServiceUpdateF.imageUrl = this.serviceUpdateF.imageUrl;
        this.finalServiceUpdateF.comments = this.serviceUpdateF.comments;
        this.finalServiceUpdateF.price = this.serviceUpdateF.price;
        //this.finalServiceUpdateF.services = this.serviceUpdateF.services;

        for (let i = 0; i < this.originalServices.length; i++) {
            var index = -1;
            var isFound = false;
            for (let j = 0; j < this.serviceUpdateF.services.length; j++) {
                //console.log(this.originalServices[i].stylist,this.serviceUpdateF.services[j].stylist);
                if (this.originalServices[i].stylist == this.serviceUpdateF.services[j].stylist) {
                    //index = j;
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                var tempServices = [];
                for (let k = 0; k < this.stylistsArr.length; k++) {
                    var ind = -1;
                    //tempServices = [];
                    //console.log(this.stylistsArr[k].id, this.originalServices[i].stylist);
                    if (this.stylistsArr[k].id == this.originalServices[i].stylist) {
                        ind = this.stylistsArr[k].services.indexOf(this.serviceUpdateF.id);
                        tempServices = this.stylistsArr[k].services.slice(0);
                        //console.log(ind);
                        tempServices.splice(ind, 1);
                    }
                }
                //console.log(tempServices, this.originalServices[i].stylist);
                this._firestoreservices.updateStylistServices(tempServices, this.originalServices[i].stylist)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
        }

        for (let i = 0; i < this.serviceUpdateF.services.length; i++) {
            isFound = false;
            for (let j = 0; j < this.originalServices.length; j++) {
                if (this.serviceUpdateF.services[i].stylist == this.originalServices[j].stylist) {
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                var tempServices = [];
                for (let k = 0; k < this.stylistsArr.length; k++) {
                    if (this.stylistsArr[k].id == this.serviceUpdateF.services[i].stylist) {
                        tempServices = this.stylistsArr[k].services.slice(0);
                        tempServices.push(this.serviceUpdateF.id);
                    }
                }
                //console.log(tempServices, this.serviceUpdateF.services[i].stylist);
                this._firestoreservices.updateStylistServices(tempServices, this.serviceUpdateF.services[i].stylist)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
        }

        this.finalServiceUpdateF.services = this.serviceUpdateF.services;
        //console.log(this.serviceForU);
        //console.log(this.serviceUpdateF.serviceFor);
        //+++++++++++++++++++++++++++++++++++++++++++++++Men++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (this.serviceForU == "Men" && this.serviceForU == this.originalServiceForU) {
            this._firestoreservices.updateWholeServiceMen(this.finalServiceUpdateF)
                .then(() => {
                    //console.log("Success");
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Tjenesten opdateres med succes");
                })
                .catch((err) => {
                    //console.log(err);
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Fejlopdatering af service");
                });
        }
        else if (this.serviceForU != this.originalServiceForU) {
            var id = this.serviceUpdateF.id;

            if (this.serviceForU == "Women") {
                this._firestoreservices.deleteServiceMen(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceWomenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

            }
            else if (this.serviceForU == "Children") {
                this._firestoreservices.deleteServiceMen(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceChildrenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
        }

        //++++++++++++++++++++++++++++++++++++++++++++Women++++++++++++++++++++++++++++++++++++++++++
        if (this.serviceForU == "Women" && this.serviceForU == this.originalServiceForU) {
            this._firestoreservices.updateWholeServiceWomen(this.finalServiceUpdateF)
                .then(() => {
                    //console.log("Success");
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Tjenesten opdateres med succes");
                })
                .catch((err) => {
                    //console.log(err);
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Fejlopdatering af service");
                });
        }
        else if (this.serviceForU != this.originalServiceForU) {
            var id = this.serviceUpdateF.id;

            if (this.serviceForU == "Men") {
                this._firestoreservices.deleteServiceWomen(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceMenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
            else if (this.serviceForU == "Children") {
                this._firestoreservices.deleteServiceWomen(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceChildrenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
        }

        //+++++++++++++++++++++++++++++++++++++++++Children++++++++++++++++++++++++++++++++++++++++++++++ 
        if (this.serviceForU == "Children" && this.serviceForU == this.originalServiceForU) {
            this._firestoreservices.updateWholeServiceChildren(this.finalServiceUpdateF)
                .then(() => {
                    //console.log("Success");
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Tjenesten opdateres med succes");
                })
                .catch((err) => {
                    //console.log(err);
                    this.isSpinner = false;
                    this.hideUpdateServiceModal();
                    this.showToastr("Fejlopdatering af service");
                });
        }
        else if (this.serviceForU != this.originalServiceForU) {
            var id = this.serviceUpdateF.id;

            if (this.serviceForU == "Men") {
                this._firestoreservices.deleteServiceChildren(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceMenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

            }
            else if (this.serviceForU == "Women") {
                this._firestoreservices.deleteServiceChildren(id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });

                this._firestoreservices.addServiceWomenForDoc(this.finalServiceUpdateF, id)
                    .then(() => {
                        //console.log("Success");
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Tjenesten opdateres med succes");
                    })
                    .catch((err) => {
                        //console.log(err);
                        this.isSpinner = false;
                        this.hideUpdateServiceModal();
                        this.showToastr("Fejlopdatering af service");
                    });
            }
        }


        /* console.log(this.serviceUpdateF);
        console.log(this.originalServices);
        console.log(this.serviceUpdateF.services);
        console.log(this.finalServiceUpdateF); */
    }


    imagePreview(file: FileList) {
        this.selectedFile = file.item(0);

        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.serviceAddF.imageUrl = event.target.result;
        }
        reader.readAsDataURL(this.selectedFile);
    }

    imagePreviewU(file: FileList) {
        this.selectedFile = file.item(0);

        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.serviceUpdateF.imageUrl = event.target.result;
        }
        reader.readAsDataURL(this.selectedFile);
    }

}



