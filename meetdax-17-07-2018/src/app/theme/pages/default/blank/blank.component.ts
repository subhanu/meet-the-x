import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, Injectable } from '@angular/core';
import { EventM } from '../../../../auth/_models/eventM';
import { StylistM } from '../../../../auth/_models/stylistM';
import { ServiceM } from '../../../../auth/_models/serviceM';
import { CustomerM } from '../../../../auth/_models/customerM';
import { EventFullCal } from '../../../../auth/_models/eventFullCal';
import { EventAddM } from '../../../../auth/_models/eventAddM';
import { Observable } from 'rxjs/Observable';
import { FirestoreDataService } from '../../../../auth/_services/firestore-data.service';
import { EventForDateService } from '../../../../auth/_services/event-for-date.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { EventC } from '../../../../auth/_models/index';
import { NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Helpers } from '../../../../helpers';
import { AngularFireStorage } from 'angularfire2/storage';

const now = new Date();
const I18N_VALUES = {
    'da': {
      weekdays: ['MA', 'TI', 'ON', 'TO', 'FR', 'LØ', 'SØ'],
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
    }
    // other languages you would support
  };
  
  
  @Injectable()
  export class I18n {
    language = 'da';
  }
  
  @Injectable()
  export class CustomDatepickerI18n extends NgbDatepickerI18n {
  
    constructor(private _i18n: I18n) {
      super();
    }
  
    getWeekdayShortName(weekday: number): string {
      return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month: number): string {
      return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month: number): string {
      return this.getMonthShortName(month);
    }
  
    getDayAriaLabel(date: NgbDateStruct): string {
      return `${date.day}-${date.month}-${date.year}`;
    }
  }

@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.css'],
    encapsulation: ViewEncapsulation.None,
    providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class BlankComponent implements OnInit {
    eventArr: EventC[] = [];
    eventArrCopy: EventC[] = [];
    allStylistsArr: StylistM[] = [];
    service: ServiceM;
    servicesArr: string[] = new Array();
    closeResult: string;

    eventToShowArr: EventFullCal[] = [];

    servicesMenArr: ServiceM[] = [];
    servicesWomenArr: ServiceM[] = [];
    servicesChildrenArr: ServiceM[] = [];

    stylistServicesMenArr: ServiceM[] = [];
    stylistServicesWomenArr: ServiceM[] = [];
    stylistServicesChildrenArr: ServiceM[] = [];

    stylistServicesMenArrU: ServiceM[] = [];
    stylistServicesWomenArrU: ServiceM[] = [];
    stylistServicesChildrenArrU: ServiceM[] = [];

    updateCustomer: string;
    updateMobile: string;
    serviceArr: ServiceM[] = [];
    //serviceArr1: any[] = [];
    customerArr: CustomerM[] = [];
    services: any = [];
    errorMessage: string = "";

    customerTA: any = [];
    isAlert: boolean = false;
    alertMessage: string;
    isLoading: boolean = false;
    isSpinner: boolean = false;
    audio = new Audio();
    date: Date;

    slotArr: boolean[] = new Array(9);

    display = 'none';
    minDate: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    timeline$: Observable<string>;
    private basePath: string = '/customers';
    selectedFile: File;

    event: EventM = {
        id: "",
        eid: "",
        date: "",
        startTime: "",
        endTime: "",
        customer: "",
        imageUrl: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0,
        comments: ""
    };

    eventU: EventM = {
        id: "",
        eid: "",
        date: "",
        startTime: "",
        endTime: "",
        customer: "",
        imageUrl: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0,
        comments: ""
    };

    eventAddF: EventAddM = {
        id: "",
        eid: "",
        date: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0,
        comments: ""
    }

    eventUpdateF: EventAddM = {
        id: "",
        eid: "",
        date: { year: 0, month: 0, day: 0 },
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0,
        comments: ""
    }

    copyEventUpdateF: EventAddM = {
        id: "",
        eid: "",
        date: { year: 0, month: 0, day: 0 },
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0,
        comments: ""
    }

    eventToShow: any = {
        id: "",
        eid: "",
        customer: "",
        services: [],
        stylist: "",
        start: "",
        end: "",
        day: "",
        weekDay: "",
        month: "",
        how_often: "",
        recurring: false
    }

    someServices: string[] = [];

    model: any = {
        name: "",
        phone: ""
    };

    customerAdd: CustomerM = {
        uid: "",
        email: "",
        firstName: "",
        lastName: "",
        dob: null,
        gender: "",
        phone: "",
        comments: "",
        imageUrl: ""
    };

    timeSlots: any = {
        9: true,
        10: true,
        11: true,
        12: true,
        13: true,
        14: true,
        15: true,
        16: true,
        17: true,
        18: true,
    };

    timeSlotsU: any = {
        9: true,
        10: true,
        11: true,
        12: true,
        13: true,
        14: true,
        15: true,
        16: true,
        17: true,
        18: true,
    };

    serFlag: number = 0;
    servicesU_validation: boolean = false;
    services_validation: boolean = true;
    popOverServices: string[] = [];

    timeSlotStartArr: string[] = [];
    timeSlotEndArr: string[] = [];
    timeSlotStartArrU: string[] = [];
    timeSlotEndArrU: string[] = [];

    colorsArr = [
        {
            name: 'Rød',
            code: '#ffebee'
        },
        {
            name: 'Grøn',
            code: '#f1f8e9'
        },
        {
            name: 'Blå',
            code: '#e1f5fe'
        },
        {
            name: 'Grå',
            code: '#eceff1'
        },
        {
            name: 'Orange',
            code: '#fff3e0'
        },
        {
            name: 'Lilla',
            code: '#f3e5f5'
        },
        {
            name: 'Lyserød',
            code: '#fce4ec'
        },
        {
            name: 'Standard',
            code: '#ffffff'
        },
    ];

    search = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .map(term => term === '' ? []
                : this.customerTA.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

    formatter = (x: { name: string }) => x.name;

    eventAddForm: FormGroup;
    eventUpdateForm: FormGroup;

    constructor(private _firestoreService: FirestoreDataService, private _dateService: EventForDateService,
                private _script: ScriptLoaderService, private _fb: FormBuilder, private storage: AngularFireStorage) {
        this.audio.src = "./assets/app/media/sound/to-the-point.mp3";
        this.audio.load();

        this.timeline$ = Observable
            .interval(1000)
            .map(val => this.getTimeline());

        this.eventAddForm = this._fb.group({
            cname: ["", [Validators.required, Validators.maxLength(16)]],
            mobile: ["", [Validators.required, Validators.pattern('^[0-9]{8}$')]],
            stylist: ["", Validators.required],
            date: ["", Validators.required],
            startTime: ["", Validators.required],
            endTime: ["", Validators.required]
        });

        this.eventUpdateForm = this._fb.group({
            stylist: ["", Validators.required],
            startTime: ["", Validators.required],
            endTime: ["", Validators.required]
        });

    }

    checkServices(s: AbstractControl): {[key: string]: boolean} | null {
        //console.log(s);
        //console.log(s.get('services').);
        if($('.servicesAddF:checked').length == 0){
          return {'services' : true};
        }
        return null;
    }

    checkServicesU(s: AbstractControl): {[key: string]: boolean} | null {
        //console.log(s.get('services').value);
        //console.log("Length:"+$('.servicesUpdate:checked').length );
        if($('.servicesUpdate:checked').length == 0){
          return {'services' : true};
        }
        return null;
    }


    ngOnInit() {
        var i = 0;
        this._dateService.currentEventArr.subscribe(
            (data) => {
                this.eventArr = data;
                this.eventArrCopy = this.eventArr;
                //this.showToastr("New Booking");
                //console.log(this.eventArr);
            }
        );

        //console.log(this.eventArr);
        this._firestoreService.getAllStylists().subscribe(
            (data) => {
                this.allStylistsArr = data;
            }
        );

        this._firestoreService.getAllServicesMen().subscribe(
            (data) => {
                this.servicesMenArr = data;
                //console.log(this.servicesMenArr);
            }
        );

        this._firestoreService.getAllServicesWomen().subscribe(
            (data) => {
                this.servicesWomenArr = data;
                //console.log(this.servicesWomenArr);
            }
        );

        this._firestoreService.getAllServicesChildren().subscribe(
            (data) => {
                this.servicesChildrenArr = data;
                //console.log(this.servicesChildrenArr);
            }
        );

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
                this.customerArr = data;
            }
        );
        //console.log(this.slotArr);
    }

    setStartTimeArr(){
        var date = new Date();
        var hours = date.getHours();
        var min = date.getMinutes();

        var h, min1, min2,start,finalMin;
        min1 = min / 10;
        min2 = min % 10;

        this.timeSlotStartArr =[
            "08:00", "08:05", "08:10", "08:15", "08:20", "08:25",
            "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
            "09:00", "09:05", "09:10", "09:15", "09:20", "09:25",
            "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
            "10:00", "10:05", "10:10", "10:15", "10:20", "10:25",
            "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
            "11:00", "11:05", "11:10", "11:15", "11:20", "11:25",
            "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
            "12:00", "12:05", "12:10", "12:15", "12:20", "12:25",
            "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
            "13:00", "13:05", "13:10", "13:15", "13:20", "13:25",
            "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
            "14:00", "14:05", "14:10", "14:15", "14:20", "14:25",
            "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
            "15:00", "15:05", "15:10", "15:15", "15:20", "15:25",
            "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
            "16:00", "16:05", "16:10", "16:15", "16:20", "16:25",
            "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
            "17:00", "17:05", "17:10", "17:15", "17:20", "17:25",
            "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
            "18:00", "18:05", "18:10", "18:15", "18:20", "18:25",
            "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
            "19:00", "19:05", "19:10", "19:15", "19:20", "19:25",
            "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
            "20:00", "20:05", "20:10", "20:15", "20:20", "20:25",
            "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
        ];

        this.timeSlotEndArr = [];
        //console.log(date.getMonth() + 1 , this.eventAddF.date.month , date.getDate() , this.eventAddF.date.day); 
        if (date.getMonth() + 1 != this.eventAddF.date.month || date.getDate() != this.eventAddF.date.day) {
            //console.log(this.timeSlotStartArr);
        }
        else {
            //if (date.getHours() >= hours) {
                //console.log(hours);
                var index = -1;
                if (date.getHours() >= 8 && date.getHours()<= 20) {
                    if (min2 <= 5) {
                        //console.log(hours, (parseInt(min1) * 10) + 5);
                        finalMin = this.getTwoDigitValue((parseInt(min1) * 10) + 5);
                        start = this.getTwoDigitValue(hours) + ":" + finalMin;
                    }
                    else {
                        if (min > 55) {
                            //console.log(hours + 1, "00");
                            start = this.getTwoDigitValue(hours + 1) + ":" + "00";
                        }
                        else {
                            //console.log(hours, (parseInt(min1) * 10) + 10);
                            finalMin = this.getTwoDigitValue((parseInt(min1) * 10) + 10);
                            start = this.getTwoDigitValue(hours) + ":" + finalMin;
                        }
                    }
                }
                else if(date.getHours()>20){
                    this.timeSlotStartArr = [];
                }

                //console.log(start);
                for (let i = 0; i < this.timeSlotStartArr.length; i++) {
                    if (this.timeSlotStartArr[i] == start) {
                        index = i;
                        this.timeSlotStartArr = this.timeSlotStartArr.slice(index, 156);
                        break;
                    }
                }

                /* if(index > -1)
                  this.timeSlotStartArr = this.timeSlotStartArr.slice(index, 144);
                else
                  this.timeSlotStartArr =[]; */  
            //}
        }
    }

    setEndTimeArr(val) {
        this.timeSlotEndArr = this.timeSlotStartArr.slice(this.timeSlotStartArr.indexOf(val) + 1, 156);
    }

    setStartTimeArrU(){
        //this.eventUpdateF.startTime = "";
        //this.eventUpdateF.endTime = "";
        var date = new Date();
        var hours = date.getHours();
        var min = date.getMinutes();

        var h, min1, min2,start,finalMin;
        min1 = min / 10;
        min2 = min % 10;

        this.timeSlotStartArrU =[
            "08:00", "08:05", "08:10", "08:15", "08:20", "08:25",
            "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
            "09:00", "09:05", "09:10", "09:15", "09:20", "09:25",
            "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
            "10:00", "10:05", "10:10", "10:15", "10:20", "10:25",
            "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
            "11:00", "11:05", "11:10", "11:15", "11:20", "11:25",
            "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
            "12:00", "12:05", "12:10", "12:15", "12:20", "12:25",
            "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
            "13:00", "13:05", "13:10", "13:15", "13:20", "13:25",
            "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
            "14:00", "14:05", "14:10", "14:15", "14:20", "14:25",
            "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
            "15:00", "15:05", "15:10", "15:15", "15:20", "15:25",
            "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
            "16:00", "16:05", "16:10", "16:15", "16:20", "16:25",
            "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
            "17:00", "17:05", "17:10", "17:15", "17:20", "17:25",
            "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
            "18:00", "18:05", "18:10", "18:15", "18:20", "18:25",
            "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
            "19:00", "19:05", "19:10", "19:15", "19:20", "19:25",
            "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
            "20:00", "20:05", "20:10", "20:15", "20:20", "20:25",
            "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
        ];

        this.timeSlotEndArrU = [];
        //console.log(date.getMonth() + 1 , this.eventUpdateF.date.month , date.getDate() , this.eventUpdateF.date.day); 
        if (date.getMonth() + 1 != this.eventUpdateF.date.month || date.getDate() != this.eventUpdateF.date.day) {
            //console.log(this.timeSlotStartArr);
        }
        else {
            //if (date.getHours() >= hours) {
                //console.log(hours);
                var index = -1;
                if (date.getHours() >= 8 && date.getHours()<= 20) {
                    if (min2 <= 5) {
                        //console.log(hours, (parseInt(min1) * 10) + 5);
                        finalMin = this.getTwoDigitValue((parseInt(min1) * 10) + 5);
                        start = this.getTwoDigitValue(hours) + ":" + finalMin;
                    }
                    else {
                        if (min > 55) {
                            //console.log(hours + 1, "00");
                            start = this.getTwoDigitValue(hours + 1) + ":" + "00";
                        }
                        else {
                            //console.log(hours, (parseInt(min1) * 10) + 10);
                            finalMin = this.getTwoDigitValue((parseInt(min1) * 10) + 10);
                            start = this.getTwoDigitValue(hours) + ":" + finalMin;
                        }
                    }
                }
                else if(date.getHours()>20){
                  this.timeSlotStartArrU = [];
                }

                //console.log(start);
                for (let i = 0; i < this.timeSlotStartArrU.length; i++) {
                    if (this.timeSlotStartArrU[i] == start) {
                        index = i;
                        this.timeSlotStartArrU = this.timeSlotStartArrU.slice(index, 156);
                        break;
                    }
                }

                /* if(index > -1)
                  this.timeSlotStartArrU = this.timeSlotStartArrU.slice(index, 156);
                else
                  this.timeSlotStartArrU = [];   */
            //}
        }
    }

    setEndTimeArrU(val) {
        this.timeSlotEndArrU = this.timeSlotStartArrU.slice(this.timeSlotStartArrU.indexOf(val) + 1, 156);
    }

    getPosition(startTime) {
        var hours = parseInt(startTime.substr(0,2));
        var mins = parseInt(startTime.substr(3,2));

        return ((hours - 8)* 60 + mins) * 2;
    }

    getHeight(duration) {
        return duration * 2 + "px";
    }


    getWidth(event,i) {
        var cEventSTP = this.getPosition(event.startTime);
        var cEventETP = this.getPosition(event.endTime);
        //var hoursE = parseInt(event.endTime.substr(0,2));
        //var minsE = parseInt(event.endTime.substr(3,2));
        var width = 240;
        var newArr = this.eventArr.slice(0);
        newArr.splice(i,1);

        for (let i = 0; i < newArr.length; i++) {
            var eventSTP =  this.getPosition(newArr[i].startTime);
            var eventETP =  this.getPosition(newArr[i].endTime);
            //var eventEH =  parseInt(newArr[i].endTime.substr(0,2));
            //var eventEM =  parseInt(newArr[i].endTime.substr(3,2)); 

            //console.log(i); 
            if (event.stylist == newArr[i].stylist) {
                /* if (hours <= eventEH && hours >= eventSH && mins >= eventSM && mins <= eventEM) {
                //console.log(eventSH, hours, eventEH,eventSM,mins,eventEM);
                width = 135;
                break;
                } 
                else if (hoursE <= eventEH && hoursE >= eventSH && minsE >= eventSM && minsE <= eventEM){
                //console.log(eventSH, hoursE, eventEH,eventSM,minsE,eventEM);
                width = 135;
                break;
                }
                else if(hours >= eventSH && hoursE >= eventEH){
                width = 135;
                break;
                }
                else{
                //console.log(eventSH, hoursE, eventEH,eventSM,minsE,eventEM);
                width = 270;
                } */
                if (cEventSTP > eventSTP && cEventSTP < eventETP) {
                    width = 120;
                }
                else if (cEventETP > eventSTP && cEventETP < eventETP) {
                    width = 120;
                }
                else if (cEventSTP >= eventSTP && cEventETP <= eventETP) {
                    width = 120;
                }
                else if (cEventSTP <= eventSTP && cEventETP >= eventETP) {
                    width = 120;
                }
                
            }
        /* else {
            //console.log(eventSH, hoursE, eventEH,eventSM,minsE,eventEM);
            width = 270;
        } */
        //console.log(width);

        }
        return width;
    }

    getLeft(event,i) {
        var cEventSTP = this.getPosition(event.startTime);
        var cEventETP = this.getPosition(event.endTime);

        var left = 30;
        var newArr = this.eventArr.slice(0);
        newArr.splice(i,1);
        
        for (let i = 0; i < newArr.length; i++) {
            var eventSTP =  this.getPosition(newArr[i].startTime);
            var eventETP =  this.getPosition(newArr[i].endTime);
        
            if (event.stylist == newArr[i].stylist) {
                if (cEventSTP > eventSTP && cEventSTP < eventETP) {
                    left = 150;
                }
                else if (cEventETP > eventSTP && cEventETP < eventETP) {
                    left = 30;
                }
                else if (cEventSTP >= eventSTP && cEventETP <= eventETP) {
                    left = 150;
                }
                else if (cEventSTP <= eventSTP && cEventETP >= eventETP) {
                    left = 30;
                }
            }
        }

        return left;
    }

    showServices(services, time){
        //console.log(this.getPosition(time));
        this.popOverServices = [];
        for (let i = 0; i < services.length; i++) {
            for (let j = 0; j < this.servicesMenArr.length; j++) {
                if (services[i] == this.servicesMenArr[j].id)
                    this.popOverServices.push(this.servicesMenArr[j].name);
            }

            for (let k = 0; k < this.servicesWomenArr.length; k++) {
                if (services[i] == this.servicesWomenArr[k].id)
                    this.popOverServices.push(this.servicesWomenArr[k].name);
            }

            for (let l = 0; l < this.servicesChildrenArr.length; l++) {
                if (services[i] == this.servicesChildrenArr[l].id)
                    this.popOverServices.push(this.servicesChildrenArr[l].name);
            }
        }
    }

    //method called when event in fullCalendar is clicked
    eventClick(eventData: any) {
        this.openDetailModal();
        this.servicesU_validation = false;
        //console.log(eventData);
        var months = ["JAN", "FEB", "MAR", "APR", "MAJ", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC"];
        var weekdays = ["SØNDAG", "MANDAG", "TIRSDAG", "ONSDAG", "TORSDAG", "FREDAG", "LØRDAG"];

        this.eventToShowArr = [];
        this.eventToShow = {
            id: "",
            eid: "",
            customer: "",
            cimageUrl: "",
            cmobile: "",
            services: [],
            stylist: "",
            start: "",
            end: "",
            day: "",
            weekDay: "",
            month: "",
            how_often: "",
            recurring: false
        }

        this.eventUpdateF = {
            id: eventData.id,
            eid: eventData.eid,
            date: { year: parseInt(eventData.date.substr(0,4)), month: parseInt(eventData.date.substr(5,2)), day: parseInt(eventData.date.substr(8,2)) },
            startTime: eventData.startTime,
            endTime: eventData.endTime,
            customer: eventData.customer,
            services: eventData.services,
            stylist: eventData.stylist,
            duration: eventData.duration,
            price: eventData.price,
            how_often: eventData.how_often,
            recurring: eventData.recurring,
            comments: eventData.comments
        };

        //console.log(this.eventUpdateF);
        //this.eventUpdate.services = eventData.services;

        this.copyEventUpdateF = this.eventUpdateF;
        this.onSelectStylistU(this.eventUpdateF.stylist);
        this.someServices = eventData.services.slice(0);
        this.eventUpdateF.price  = eventData.price;
        this.eventUpdateF.duration  = eventData.duration;

        /* console.log(this.eventUpdateF);
        console.log(this.copyEventUpdateF);
        console.log(eventData); */
        //console.log(this.eventArrCopy);
        this.setServicesToTrue();
        this.setStartTimeArrU();
        this.setEndTimeArrU(this.eventUpdateF.startTime);
        //this.getSlotsStatusU();

        //console.log(this.eventArr);

        //console.log(this.eventUpdateF.startTime);
        //var startTimeU = new Date(this.eventUpdateF.startTime);

        /*Recent var time = this.eventUpdateF.startTime.getHours();
        switch (time) {
            case 9: $('.time-slot .btn-time #nineU').prop('checked', true); break;
            case 10: $('.time-slot .btn-time #tenU').prop('checked', true); break;
            case 11: $('.time-slot .btn-time #elevenU').prop('checked', true); break;
            case 12: $('.time-slot .btn-time #twelveU').prop('checked', true); break;
            case 13: $('.time-slot .btn-time #thirteenU').prop('checked', true); break;
            case 14: $('.time-slot .btn-time #fourteenU').prop('checked', true); break;
            case 15: $('.time-slot .btn-time #fifteenU').prop('checked', true); break;
            case 16: $('.time-slot .btn-time #sixteenU').prop('checked', true); break;
            case 17: $('.time-slot .btn-time #seventeenU').prop('checked', true); break;
        } */

        //console.log(this.eventUpdateF);
        //console.log(this.eventArr);
        //var d = new Date();
        //d.setDate(eventData.startDate.getDate());
        //d.setMonth(eventData.startDate.getMonth());
        //d.setFullYear(eventData.startDate.getFullYear());

        //eventData.date = { year: parseInt(eventData.date.substr(0,4)), month: parseInt(eventData.date.substr(5,2)), day: parseInt(eventData.date.substr(8,2)) };

        //this.event.startDate = model.startDate;
        //this.event.endDate = model.endDate;
        //this.event.services = model.services;
        //console.log(this.eventUpdate);
        //console.log(this.event);
        //console.log(model.services);

        for (let i = 0; i < this.allStylistsArr.length; i++) {
            if (eventData.stylist == this.allStylistsArr[i].id)
                this.eventToShow.stylist = this.allStylistsArr[i].firstName + " " + this.allStylistsArr[i].lastName;
        }

        for (let i = 0; i < this.customerArr.length; i++) {
            //console.log(eventData.customer , this.customerArr[i].uid);
            if (eventData.customer == this.customerArr[i].uid) {
                this.eventToShow.customer = this.customerArr[i].firstName + " " + this.customerArr[i].lastName;
                this.updateCustomer = this.customerArr[i].firstName + " " + this.customerArr[i].lastName;
                this.eventToShow.cmobile = this.customerArr[i].phone;
                this.updateMobile = this.customerArr[i].phone;
                this.eventToShow.cimageUrl = this.customerArr[i].imageUrl;

                //console.log(this.customerArr[i].phone+","+this.updateMobile);
                //console.log(this.customerArr[i].imageUrl);
                if (this.customerArr[i].imageUrl == "")
                    this.eventToShow.cimageUrl = "assets/app/media/img/users/user.png";
                break;
            }
            else if(eventData.customer == this.customerArr[i].id){
                this.eventToShow.customer = this.customerArr[i].firstName + " " + this.customerArr[i].lastName;
                this.updateCustomer = this.customerArr[i].firstName + " " + this.customerArr[i].lastName;
                this.eventToShow.cmobile = this.customerArr[i].phone;
                this.updateMobile = this.customerArr[i].phone;
                this.eventToShow.cimageUrl = this.customerArr[i].imageUrl;

                break;
            }
        }

        for (let i = 0; i < eventData.services.length; i++) {
            for (let j = 0; j < this.servicesMenArr.length; j++) {
                if (eventData.services[i] == this.servicesMenArr[j].id)
                    this.eventToShow.services.push(this.servicesMenArr[j].name);
            }

            for (let k = 0; k < this.servicesWomenArr.length; k++) {
                if (eventData.services[i] == this.servicesWomenArr[k].id)
                    this.eventToShow.services.push(this.servicesWomenArr[k].name);
            }

            for (let l = 0; l < this.servicesChildrenArr.length; l++) {
                if (eventData.services[i] == this.servicesChildrenArr[l].id)
                    this.eventToShow.services.push(this.servicesChildrenArr[l].name);
            }
        }

        //console.log(ds);   
        var ds = new Date(eventData.date.substr(0,4)+"-"+eventData.date.substr(5,2)+"-"+eventData.date.substr(8,2));
        //var de = new Date(eventData.endDate);

        this.eventToShow.id = eventData.id;
        this.eventToShow.start = eventData.startTime;
        this.eventToShow.end = eventData.endTime;
        this.eventToShow.day = ds.getDate();
        this.eventToShow.weekDay = weekdays[ds.getDay()];
        this.eventToShow.month = months[ds.getMonth()];
        this.eventToShow.how_often = eventData.how_often;

        //console.log(this.eventToShow);
        this.eventToShowArr.push(this.eventToShow);

        if(this.someServices.length > 0)
           this.servicesU_validation = false;
        else   
           this.servicesU_validation = true;
        //console.log(this.eventToShowArr);
    }//event click ends


    //method called when empty space is clicked, for adding event 
    addEventClick(stylistId, time) {
        this._dateService.changedDate.subscribe(
        (data) => {
            //console.log(data);
            this.eventAddF.date = data;
        });

        //this.onSelectTime(time);
        this.eventAddF.stylist = stylistId;
        //this.getSlotsStatus();
        this.setStartTimeArr();

        //console.log(this.customerArr);
        for (var i = 0; i < this.customerArr.length; i++) {
            var custTA = {
                name: this.customerArr[i].firstName + " " + this.customerArr[i].lastName,
                phone: (this.customerArr[i].phone).substr(3,10)
            }
            //console.log(custTA);
            this.customerTA[i] = custTA;
        }

       /*  this.stylistServicesMenArr = [];
        this.stylistServicesWomenArr = [];
        this.stylistServicesChildrenArr = []; */
        this.onSelectStylist(stylistId);
        //console.log(date.getMonth() + 1,this.eventAddF.date.month ,date.getDate(),this.eventAddF.date.day);
        this.openAddModal();
    }


    addEventFloatClick() {
        this._dateService.changedDate.subscribe(
        (data) => {
            //console.log(data);
            this.eventAddF.date = data;
        });

        this.stylistServicesMenArr = [];
        this.stylistServicesWomenArr = [];
        this.stylistServicesChildrenArr = [];

        this.setStartTimeArr();

        for (var i = 0; i < this.customerArr.length; i++) {
            var custTA = {
                name: this.customerArr[i].firstName + " " + this.customerArr[i].lastName,
                phone: (this.customerArr[i].phone).substr(3,10)
            }
            //console.log(custTA);
            this.customerTA[i] = custTA;
        }

       /*  this.stylistServicesMenArr = [];
        this.stylistServicesWomenArr = [];
        this.stylistServicesChildrenArr = []; */
        //console.log(date.getMonth() + 1,this.eventAddF.date.month ,date.getDate(),this.eventAddF.date.day);
        this.openAddModal();
    }


    onSelectStylist(id: string) {
        this.eventAddF.services = [];
        this.eventAddF.price = 0;
        this.eventAddF.duration = 0;
        this.stylistServicesMenArr = [];
        this.stylistServicesWomenArr = [];
        this.stylistServicesChildrenArr = [];
        this.services_validation = true;

       /*  console.log(this.stylistServicesMenArr);
        console.log(this.stylistServicesWomenArr);
        console.log(this.stylistServicesChildrenArr);
        console.log("before"); */
        for (let j = 0; j < this.allStylistsArr.length; j++) {
            if (id == this.allStylistsArr[j].id) {
                this.event.imageUrl = this.allStylistsArr[j].imageUrl;
                break;
            }
        }

        for (let i = 0; i < this.servicesMenArr.length; i++) {
            for (let j = 0; j < this.servicesMenArr[i].services.length; j++) {
                if (id == this.servicesMenArr[i].services[j].stylist) {
                    this.stylistServicesMenArr.push(this.servicesMenArr[i]);
                    break;
                }
            }
        }
       /*  console.log(this.stylistServicesMenArr);
        console.log(this.servicesWomenArr); */
        for (let i = 0; i < this.servicesWomenArr.length; i++) {
            for (let j = 0; j < this.servicesWomenArr[i].services.length; j++) {
                if (id == this.servicesWomenArr[i].services[j].stylist) {
                    this.stylistServicesWomenArr.push(this.servicesWomenArr[i]);
                    break;
                }
            }
        }
        /* console.log(this.stylistServicesWomenArr);
        console.log(this.servicesChildrenArr); */
        for (let i = 0; i < this.servicesChildrenArr.length; i++) {
            for (let j = 0; j < this.servicesChildrenArr[i].services.length; j++) {
                if (id == this.servicesChildrenArr[i].services[j].stylist) {
                    this.stylistServicesChildrenArr.push(this.servicesChildrenArr[i]);
                    break;
                }
            }
        }

        //this.getSlotsStatus();
        $('.servicesAddF').prop('checked', false);
        //$('tab-pane .m-checkbox-inline .m-checkbox .servicesAddF').attr("checked","false");
        //console.log( $('tab-pane .m-checkbox-inline .m-checkbox .servicesAddF').prop("checked"));

       /*  console.log(this.stylistServicesChildrenArr);
        for (let i = 0; i < this.stylistServicesMenArr.length; i++) {
            console.log("#" + i + "ismSAF");
            console.log($("#" + i + "ismSAF").is(':checked'));
            $("#" + i + "ismSAF").prop('checked', false);
            console.log($("#" + i + "ismSAF").is(':checked'));
        }
        for (let i = 0; i < this.stylistServicesWomenArr.length; i++) {
            console.log("#" + i + "isfSAF");
            console.log($("#" + i + "isfSAF").is(':checked'));
            $("#" + i + "isfSAF").prop('checked', false);
            console.log($("#" + i + "isfSAF").is(':checked'));
        }
        for (let i = 0; i < this.stylistServicesChildrenArr.length; i++) {
            console.log("#" + i + "iscSAF");
            console.log($("#" + i + "iscSAF").is(':checked'));
            $("#" + i + "iscSAF").prop('checked', false);
            console.log($("#" + i + "iscSAF").is(':checked'));
        } */
        //console.log(this.stylistServicesChildrenArr); 
    }


    onSelectService(service, event) {
        //console.log(service);
        //this.services_validation = true;
        if ($('.servicesAddF:checked').length > 2) {
            event.target.checked = false;
        }
        else if (event.target.checked == false) {
            //console.log(this.eventAddF.duration);
            var index = this.eventAddF.services.indexOf(service.id);
            this.eventAddF.services.splice(index, 1);
            this.serFlag--;

            for (let i = 0; i < service.services.length; i++) {
                if (this.eventAddF.stylist == service.services[i].stylist) {
                    this.eventAddF.duration -= parseInt(service.services[i].duration);
                    break;
                }
            }

            this.eventAddF.price -= service.price;
            //console.log(this.eventAddF.price);
            //console.log(this.eventAddF.duration);
        }
        else {
            //console.log(this.eventAddF.duration);
            this.eventAddF.services.push(service.id);
            for (let i = 0; i < service.services.length; i++) {
                if (this.eventAddF.stylist == service.services[i].stylist) {
                    this.eventAddF.duration += parseInt(service.services[i].duration);
                    break;
                }
            }
            this.eventAddF.price += service.price;
            //console.log(this.eventAddF.price);
            //console.log(this.eventAddF.duration);
        }

        if(this.eventAddF.services.length > 0)
           this.services_validation = false;
        else   
           this.services_validation = true;

        //console.log(this.eventAddF.services);
    }


    openEditModal() {
        this.closeDetailModal();
        this.display = "block";
        $('#edit_modal').addClass('show');
    }

    onSelectStylistU(id: string) {
        //this.eventAddF.services = [];
        //console.log("Entered");
        this.someServices = [];
        this.eventUpdateF.price = 0;
        this.eventUpdateF.duration = 0;
        this.servicesU_validation = true;
        //this.getSlotsStatusU();

        //console.log(this.someServices);
        for (let j = 0; j < this.allStylistsArr.length; j++) {
            if (id == this.allStylistsArr[j].id) {
                this.eventU.imageUrl = this.allStylistsArr[j].imageUrl;
                break;
            }
        }

        //console.log(id);
        this.stylistServicesMenArr = [];
        for (let i = 0; i < this.servicesMenArr.length; i++) {
            for (let j = 0; j < this.servicesMenArr[i].services.length; j++) {
                if (id == this.servicesMenArr[i].services[j].stylist) {
                    this.stylistServicesMenArr.push(this.servicesMenArr[i]);
                    break;
                }
            }
        }
        //console.log(this.stylistServicesMenArr);
        this.stylistServicesWomenArr = []
        //console.log(this.servicesWomenArr);
        for (let i = 0; i < this.servicesWomenArr.length; i++) {
            for (let j = 0; j < this.servicesWomenArr[i].services.length; j++) {
                if (id == this.servicesWomenArr[i].services[j].stylist) {
                    this.stylistServicesWomenArr.push(this.servicesWomenArr[i]);
                    break;
                }
            }
        }
        //console.log(this.stylistServicesWomenArr);

        this.stylistServicesChildrenArr = [];
        //console.log(this.servicesChildrenArr);
        for (let i = 0; i < this.servicesChildrenArr.length; i++) {
            for (let j = 0; j < this.servicesChildrenArr[i].services.length; j++) {
                if (id == this.servicesChildrenArr[i].services[j].stylist) {
                    this.stylistServicesChildrenArr.push(this.servicesChildrenArr[i]);
                    break;
                }
            }
        }
        //console.log(this.stylistServicesChildrenArr);  

        $('.servicesUpdate').prop('checked',false);
    }

    setServicesToTrue() {
        //console.log("Enterd here also");
        this.serFlag = 0;
        $('.servicesUpdate').prop('checked',false);
        
        //console.log(this.someServices);
        for (let k = 0; k < this.someServices.length; k++) {
            for (let x = 0; x < this.stylistServicesMenArr.length; x++) {
                //console.log(this.someServices[k], this.stylistServicesMenArr[x].id, "#"+x+"ism");
                if (this.someServices[k] == this.stylistServicesMenArr[x].id) {
                    //this.eventUpdateForm.get('services').status;
                    $("#" + x + "ism").prop('checked', true);
                    this.serFlag++;
                    break;
                }
            }
        }

        //console.log($('.servicesUpdate:checked').length);
        for (let k = 0; k < this.someServices.length; k++) {
            for (let y = 0; y < this.stylistServicesWomenArr.length; y++) {
                //console.log(this.someServices[k], this.stylistServicesWomenArr[y].id, "#"+y+"isw");
                if (this.someServices[k] == this.stylistServicesWomenArr[y].id) {
                    $("#" + y + "isw").prop('checked', true);
                    this.serFlag++;
                    break;
                }
            }
        }
        //console.log($('.servicesUpdate:checked').length);
        for (let k = 0; k < this.someServices.length; k++) {
            for (let z = 0; z < this.stylistServicesChildrenArr.length; z++) {
                //console.log(this.someServices[k], this.stylistServicesChildrenArr[z].id, "#"+z+"isc");
                if (this.someServices[k] == this.stylistServicesChildrenArr[z].id) {
                    $("#" + z + "isc").prop('checked', true);
                    this.serFlag++;
                    break;
                }
            }
        }
        //console.log($('.servicesUpdate:checked').length);
    }

    onSelectServiceU(service, event) {
        if ($('.servicesUpdate:checked').length > 2) {
            event.target.checked = false;
        }
        else if (event.target.checked == false) {
            //console.log(this.eventUpdateF.duration);
            var index = this.someServices.indexOf(service.id);
            this.someServices.splice(index, 1);

            //console.log(this.eventUpdateF.duration);
            for (let i = 0; i < service.services.length; i++) {
                if (this.eventUpdateF.stylist == service.services[i].stylist) {
                    this.eventUpdateF.duration -= parseInt(service.services[i].duration);
                    break;
                }
            }

            this.eventUpdateF.price -= service.price;
            //console.log(this.eventUpdateF.price);
            //console.log(this.eventUpdateF.duration);
        }
        else {
            //console.log(this.eventUpdateF.duration);
            this.someServices.push(service.id);
            for (let i = 0; i < service.services.length; i++) {
                if (this.eventUpdateF.stylist == service.services[i].stylist) {
                    this.eventUpdateF.duration += parseInt(service.services[i].duration);
                    break;
                }
            }
            this.eventUpdateF.price += service.price;
            //console.log(this.eventUpdateF.price);
            //console.log(this.eventUpdateF.duration);
        }

        //Recent this.onSelectTimeU(this.eventUpdateF.startTime.getHours());
        //console.log(this.someServices);
        /* console.log(this.eventToShowArr);
        console.log(this.eventArr);
        console.log(this.eventArrCopy);  */
        if(this.someServices.length > 0)
           this.servicesU_validation = false;
        else   
           this.servicesU_validation = true;
    }


    onSelectTimeU(value) {
        var d = new Date();
        d.setFullYear(this.eventUpdateF.date.year);
        d.setMonth(this.eventUpdateF.date.month - 1);
        d.setDate(this.eventUpdateF.date.day);

        /*Recentthis.eventUpdateF.startTime = new Date(d);
        this.eventUpdateF.startTime.setHours(parseInt(value));
        this.eventUpdateF.startTime.setMinutes(0);

        this.eventUpdateF.endTime = this.formatEndDate(d, this.eventUpdateF.duration, value);*/
        //console.log(this.eventUpdateF.startTime);
        //console.log(this.eventUpdateF.endTime);
    }

    getSlotsStatusU() {
        //this.isLoading = true;
        //Recent this.onSelectTimeU(this.eventUpdateF.startTime.getHours());
        this.timeSlotsU = {};
        //console.log(this.eventAddF.date);
        var month = "";
        var day = "";

        if (this.eventUpdateF.date.month <= 9) {
            month = "0" + this.eventUpdateF.date.month;
            //this.slotsProgressU = "30%";
        }
        else {
            month = "" + this.eventUpdateF.date.month;
            //this.slotsProgressU = "30%";
        }

        if (this.eventUpdateF.date.day <= 9) {
            day = "0" + this.eventUpdateF.date.day;
            //this.slotsProgressU = "65%";
        }
        else {
            day = "" + this.eventUpdateF.date.day;
            //this.slotsProgressU = "65%";
        }

        var startDate = this.eventUpdateF.date.year + "-" + month + "-" + day + "T00:00:00";
        var endDate = this.eventUpdateF.date.year + "-" + month + "-" + day + "T23:00:00";
        //console.log(endDate);

        //this.slotsProgress = "65%";
        this._firestoreService.getAllSlots(this.eventUpdateF.stylist, startDate, endDate)
            .toPromise()
            .then((res) => {
                this.timeSlotsU = res;
                this.isLoading = false;
                //console.log(this.timeSlotsU);
            })
            .catch((err) => {
                //console.log(err);
                this.isLoading = false;
            });

    }

    onUpdateSubmit() {
        //console.log(this.eventUpdate.services);
        //console.log(this.event);
        this.isSpinner = true;
        this.eventU.id = this.eventUpdateF.id;
        this.eventU.recurring = false;

        //console.log(this.event.startDate);
        //console.log(this.event.endDate);
        //console.log(this.customerArr);
        this.eventU.stylist = this.eventUpdateF.stylist;
        this.eventU.services = this.someServices;
        this.eventU.price = this.eventUpdateF.price;
        this.eventU.customer = this.eventUpdateF.customer;
        this.eventU.date = this.eventUpdateF.date.year+"-"+this.getTwoDigitValue(this.eventUpdateF.date.month)+"-"+this.getTwoDigitValue(this.eventUpdateF.date.day);
        this.eventU.startTime = this.eventUpdateF.startTime;
        this.eventU.endTime = this.eventUpdateF.endTime;
        this.eventU.duration = this.calDuration(this.eventUpdateF.startTime, this.eventUpdateF.endTime);
        this.eventU.comments = this.eventUpdateF.comments;
         
        
        /* for (var i = 0; i < this.customerArr.length; i++) {
            console.log(this.eventU.customer, this.customerArr[i].uid);
            if (this.customerArr[i].email == this.eventU.customer) {
                this.eventU.customer = this.customerArr[i].uid;
            }
        } */

        //console.log(this.eventU);
        this._firestoreService.updateEvent(this.eventU)
        .then(() => {
            this.closeEditModal();
            this.isSpinner = false;
            this.showToastr("Booking opdateret succesfuldt");
        })
        .catch((err) => {
            this.closeEditModal();
            this.isSpinner = false;
            this.showToastr("Booking opdateringsfejl");
        });
    }


    //method for event deletion
    deleteEvent() {
        //console.log("Inside Booking Deletion");
        this.isSpinner = true;
        this._firestoreService.deleteEventFromCollection(this.eventToShow.id)
            .then(() => {
                this.closeEditModal();
                this.isSpinner = false;
                this.showToastr("Bestilling slettet korrekt");
            })
            .catch((err) => {
                this.closeEditModal();
                this.isSpinner = false;
                this.showToastr("Booking sletning fejl");
            });
    }

    updateColor(color: string, id: string) {
        //console.log(color+""+id);
        this.isSpinner = true;
        this._firestoreService.updateStylistColor(id, color)
        .then(() => {
            this.isSpinner = false;
            this.showToastr("Farve opdateret med succes");
        })
        .catch((err) => {
            this.isSpinner = false;
            this.showToastr("Farveopdateringsfejl");
        });;
    }


    onSelectTime(value) {
        this.eventAddF.startTime = value;
        //console.log(value); 
        //console.log(this.eventAddF.startTime);
        //console.log($("#eight").prop('checked'));
    }


    onSubmit() {
        //console.log(this.eventAddF.date+" "+this.eventAddF.startTime);
        this.isSpinner = true;
        var custIndex = -1;
        /* var d = new Date();
        d.setFullYear(this.eventAddF.date.year);
        d.setMonth(this.eventAddF.date.month - 1);
        d.setDate(this.eventAddF.date.day);

        //console.log(d);
        this.event.startDate = new Date(d);
        this.event.startDate.setHours(parseInt(this.eventAddF.startTime));
        this.event.startDate.setMinutes(0); */
        //console.log(start);

        //this.event.endDate = this.formatEndDate(d, this.eventAddF.duration, this.eventAddF.startTime);
        //console.log(this.event.startDate);
        //console.log(this.event.endDate);
        //console.log(this.eventAddF.services);

        this.event.startTime = this.eventAddF.startTime;
        this.event.date = this.eventAddF.date.year+"-"+this.getTwoDigitValue(this.eventAddF.date.month)+"-"+this.getTwoDigitValue(this.eventAddF.date.day);
        this.event.endTime = this.eventAddF.endTime;
        this.event.stylist = this.eventAddF.stylist;
        this.event.services = this.eventAddF.services;
        this.event.price = this.eventAddF.price;
        this.event.comments = this.eventAddF.comments;

        //console.log(this.eventAddF.startTime, this.eventAddF.endTime);
        this.event.duration = this.calDuration(this.eventAddF.startTime, this.eventAddF.endTime);
        //this.event.customer = this.eventAddF.customer;

        //console.log(this.event.customer);
        for (var i = 0; i < this.customerArr.length; i++) {
            if (this.customerArr[i].phone == "+45"+this.eventAddF.customer.toString().trim()) {
                //this.event.customer = this.customerArr[i].uid;
                custIndex = i;
                break;
            }
        }

        if(custIndex > -1){
            if(this.customerArr[custIndex].uid != "")
               this.event.customer = this.customerArr[custIndex].uid;
            else
               this.event.customer = this.customerArr[custIndex].id;
             
            //console.log(this.event);   

            this._firestoreService.addEvent(this.event)
            .then(() => {
                this.closeAddModal();
                this.isSpinner = false;
                this.audio.play();
                this.showToastr("Bestilling udført succesfuldt");
            })
            .catch((err) => {
                this.closeAddModal();
                console.log(err);
                this.isSpinner = false;
                this.audio.play();
                this.showToastr("Bookingfejl");
            });
        }
        else{
            var newCustomer = {
                uid: "", 
                firstName: this.model.name,
                lastName: "",
                dob: new Date(),
                phone: "+45"+this.eventAddF.customer,
                gender: "",
                imageUrl: null,
                email: "",
                comments: ""
            }; 

            newCustomer.imageUrl = "https://firebasestorage.googleapis.com/v0/b/meetdax2.appspot.com/o/customers%2Fuser.png?alt=media&token=5ea7c9b4-e1bd-4c1e-9f29-94995a431c10";
            this._firestoreService.addCustomer(newCustomer)
            .then((docRef)=>{
                this.event.customer = docRef.id;
                //console.log(this.event);
                this._firestoreService.addEvent(this.event)
                .then(() => {
                    this.closeAddModal();
                    this.isSpinner = false;
                    this.audio.play();
                    this.showToastr("Bestilling udført succesfuldt");
                })
                .catch((err) => {
                    this.closeAddModal();
                    console.log(err);
                    this.isSpinner = false;
                    this.audio.play();
                    this.showToastr("Bookingfejl");
                });
            })
            .catch((err)=>{
                this.closeAddModal();
                this.isSpinner = false;
                this.audio.play();
                this.showToastr("Bookingfejl");
            });
        }

        //console.log(this.eventAddF.services);
        //console.log(this.event.services);
        //console.log(this.event);
    }

    formatStartDate(start: Date, startTime: string) {
        start.setHours(parseInt(startTime));
        start.setMinutes(0);
        //console.log(this.event.startDate);
        return start;
    }

    formatEndDate(end: Date, duration: number, startTime: string) {
        var hrs = parseInt(startTime) + (duration / 60);
        var min = duration % 60;

        end.setHours(hrs);
        end.setMinutes(min);
        //console.log(end);
        //console.log(this.event.endDate);
        return end;
    }

    calDuration(startTime, endTime){
        var sH = parseInt(startTime.substr(0,2));
        var eH = parseInt(endTime.substr(0,2));
        var sM = parseInt(startTime.substr(3,2));
        var eM = parseInt(endTime.substr(3,2));

        return (eH-sH)*60 + (eM-sM);
    }

    getTwoDigitValue(value){
        if(value <= 9)
          return "0"+value;
        else
          return value;   
    }

    showToastr(msg) {
        this.audio.play();
        this.isAlert = true;
        this.alertMessage = msg;
        setTimeout(() => this.isAlert = false, 5000);
    }


    closeEditModal() {
        //this.closeDetailModal();
        this.display = 'none';
        $('#edit_modal').removeClass('show');
        this.eventUpdateF = this.copyEventUpdateF;
        this.eventArr = this.eventArrCopy;
        //console.log(this.eventUpdateF);
        //console.log(this.eventArrCopy);
        //console.log(this.eventArr);

        this.eventUpdateForm.reset();
    }

    openDetailModal() {
        this.display = 'block';
        $('#detail_modal').addClass('show');
    }

    closeDetailModal() {
        this.display = 'none';
        $('#detail_modal').removeClass('show');
    }

    closeAddModal() {
        this.display = 'none';
        $('#add_event_modal').removeClass('show');

        this.eventAddForm.reset({
            cname: "",
            mobile: "",
            stylist: "",
            services: [],
            date: { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() },
        });

        //console.log(this.eventAddF);
        this.eventAddF = {
            id: "",
            eid: "",
            date: { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() },
            startTime: "",
            endTime: "",
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0,
            comments: "" 
        };

        this.event = {
            id: "",
            eid: "",
            date: "",
            startTime: "",
            endTime: "",
            customer: "",
            imageUrl: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0,
            comments: ""
        };
        //console.log(this.eventAddF);
        //console.log(this.event);
    }

    openAddModal() {
        this.display = 'block';
        $('#add_event_modal').addClass('show');
    }

    getMin(min) {
        if (min < 10)
            return '0' + min;
        else
            return min;
    }

    setCustomer() {
        console.log(this.eventAddF.customer);
        this.eventAddF.customer = this.model.phone;
        this.eventUpdateF.customer = this.model.phone;
        console.log(this.model, this.eventAddF.customer);
    }

    /* showCustomer(){
        console.log(this.model, this.eventAddF.customer);
    } */


    getTimeline() {
        var timeline_local: string;
        var date = new Date();
        var hrs = date.getHours();
        var mins = date.getMinutes();

        if(hrs <= 18)
           timeline_local = 0 + ((hrs - 8) * 60 + mins) * 2 + 'px';
        else
           timeline_local = -15+'px';
    
        return timeline_local;
    }

    getSlotsStatus() {
        //this.isLoading = true;
        this.timeSlots = {};
        //console.log(this.eventAddF.date);
        var month = "";
        var day = "";

        if (this.eventAddF.date.month <= 9)
            month = "0" + this.eventAddF.date.month;
        else 
            month = "" + this.eventAddF.date.month;
        
        if (this.eventAddF.date.day <= 9)
            day = "0" + this.eventAddF.date.day;
        else 
            day = "" + this.eventAddF.date.day;
            
        var startDate = this.eventAddF.date.year + "-" + month + "-" + day + "T00:00:00";
        var endDate = this.eventAddF.date.year + "-" + month + "-" + day + "T23:00:00";
        //console.log(endDate);

        this._firestoreService.getAllSlots(this.eventAddF.stylist, startDate, endDate)
        .toPromise()
        .then((res) => {
            this.timeSlots = res;
            this.isLoading = false;
            //console.log(this.timeSlots);
            
            $('.time-slot .btn-time #nine').prop('disabled', false);
            $('.time-slot #nineLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #ten').prop('disabled', false); 
            $('.time-slot #tenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #eleven').prop('disabled', false);
            $('.time-slot #elevenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #twelve').prop('disabled', false);
            $('.time-slot #twelveLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #thirteen').prop('disabled', false);
            $('.time-slot #thirteenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #fourteen').prop('disabled', false);
            $('.time-slot #fourteenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #fifteen').prop('disabled', false);
            $('.time-slot #fifteenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #sixteen').prop('disabled', false);
            $('.time-slot #sixteenLabel').removeClass('m-radio--disabled');
            $('.time-slot .btn-time #seventeen').prop('disabled', false);
            $('.time-slot #seventeenLabel').removeClass('m-radio--disabled');
                    
            for(let i=9;i<18;i++){
                var slot: boolean = false;
                slot = this.setSlots(i, this.timeSlots);
    
                //console.log(slot);
                if(slot == true){
                    switch (i) {
                        case 9:$('.time-slot .btn-time #nine').prop('disabled', true); 
                                $('.time-slot #nineLabel').addClass('m-radio--disabled');
                                break;
                        case 10:$('.time-slot .btn-time #ten').prop('disabled', true);
                                $('.time-slot #tenLabel').addClass('m-radio--disabled');
                                break;
                        case 11:$('.time-slot .btn-time #eleven').prop('disabled', true); 
                                $('.time-slot #elevenLabel').addClass('m-radio--disabled');
                                break;
                        case 12: $('.time-slot .btn-time #twelve').prop('disabled', true);
                                $('.time-slot #twelveLabel').addClass('m-radio--disabled');
                                break;
                        case 13: $('.time-slot .btn-time #thirteen').prop('disabled', true);
                                $('.time-slot #thirteenLabel').addClass('m-radio--disabled');
                                break;
                        case 14: $('.time-slot .btn-time #fourteen').prop('disabled', true);
                                $('.time-slot #fourteenLabel').addClass('m-radio--disabled');
                                break;
                        case 15: $('.time-slot .btn-time #fifteen').prop('disabled', true);
                                $('.time-slot #fifteenLabel').addClass('m-radio--disabled');
                                break;
                        case 16: $('.time-slot .btn-time #sixteen').prop('disabled', true); 
                                $('.time-slot #sixteenLabel').addClass('m-radio--disabled');
                                break;
                        case 17: $('.time-slot .btn-time #seventeen').prop('disabled', true);
                                $('.time-slot #seventeenLabel').addClass('m-radio--disabled');
                                break;
                    }
                }
            }   
        })
        .catch((err) => {
            //console.log(err);
            this.isLoading = false;
        });

        //console.log(this.timeSlots);    
    }

    setSlots(id, slots): boolean {
        var d = new Date();
        //console.log(this.eventAddF.date.month+1);
        var timeSlots = slots;
        //console.log(this.timeSlots[id]);
        //console.log(d.getMonth() + 1 , this.eventAddF.date.month , d.getDate() , this.eventAddF.date.day);
        if (d.getMonth() + 1 != this.eventAddF.date.month || d.getDate() != this.eventAddF.date.day) {
            //console.log("Yes");
            if (timeSlots[id] == false) {
                //this.resetTimeSlot(id);
                return true;
            }
            else{
                return false;
            }
        }
        else {
            if (d.getHours() >= id) {
                //this.resetTimeSlot(id);
                return true;
            }
            else {
                if (timeSlots[id] == false) {
                    //this.resetTimeSlot(id);
                    return true;
                }
            }
        }
    }

    resetTimeSlot(time) {
        switch (time) {
            case 9: $('.time-slot .btn-time #nine').prop('checked', false); break;
            case 10: $('.time-slot .btn-time #ten').prop('checked', false); break;
            case 11: $('.time-slot .btn-time #eleven').prop('checked', false); break;
            case 12: $('.time-slot .btn-time #twelve').prop('checked', false); break;
            case 13: $('.time-slot .btn-time #thirteen').prop('checked', false); break;
            case 14: $('.time-slot .btn-time #fourteen').prop('checked', false); break;
            case 15: $('.time-slot .btn-time #fifteen').prop('checked', false); break;
            case 16: $('.time-slot .btn-time #sixteen').prop('checked', false); break;
            case 17: $('.time-slot .btn-time #seventeen').prop('checked', false); break;
        }
    }

    setSlotsU(id) {
        var d = new Date();
        if (d.getMonth() + 1 != this.eventUpdateF.date.month || d.getDate() != this.eventUpdateF.date.day) {
            //console.log("Yes");
            if (this.timeSlotsU[id] == false) {
                //this.resetTimeSlotU(id);
                return true;
            }
        }
        else {
            if (d.getHours() >= id) {
                //console.log(id);
                //this.resetTimeSlotU(id);
                return true;
            }
            else {
                if (this.timeSlotsU[id] == false) {
                    //this.resetTimeSlotU(id);
                    return true;
                }

            }
        }
    }


    resetTimeSlotU(time) {
        switch (time) {
            case 9: $('.time-slot .btn-time #nineU').prop('checked', false); break;
            case 10: $('.time-slot .btn-time #tenU').prop('checked', false); break;
            case 11: $('.time-slot .btn-time #elevenU').prop('checked', false); break;
            case 12: $('.time-slot .btn-time #twelveU').prop('checked', false); break;
            case 13: $('.time-slot .btn-time #thirteenU').prop('checked', false); break;
            case 14: $('.time-slot .btn-time #fourteenU').prop('checked', false); break;
            case 15: $('.time-slot .btn-time #fifteenU').prop('checked', false); break;
            case 16: $('.time-slot .btn-time #sixteenU').prop('checked', false); break;
            case 17: $('.time-slot .btn-time #seventeenU').prop('checked', false); break;
        }
    }




}