import { Component, OnInit, ViewEncapsulation, AfterViewInit, Injectable } from '@angular/core';
import { Helpers } from '../../../helpers';
import { EventM } from '../../../auth/_models/eventM';
import { NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { BlankComponent } from '../../pages/default/blank/blank.component';
import { EventC } from '../../../auth/_models/eventC';
import { ServiceM, CustomerM } from '../../../auth/_models/index';
import { FirestoreDataService } from '../../../auth/_services/firestore-data.service';
import { EventForDateService } from '../../../auth/_services/index';

declare let mLayout: any;
const now = new Date();

const I18N_VALUES = {
    'da': {
      weekdays: ['Ma', 'Ti', 'On', 'To', 'Fr', 'Lø', 'Sø'],
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
    }
    // other languages you would support
};
  

@Injectable()
export class I18n {
  language = 'da';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

    constructor(private _i18n: I18n) {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month: number): string {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month: number): string {
        return this.getMonthShortName(month);
    }

    getDayAriaLabel(date: NgbDateStruct): string {
        return `${date.day}-${date.month}-${date.year}`;
    }
}

@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    styleUrls: ["./aside-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
    providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    next: { year: number, month: number };
    next2next: { year: number, month: number };
    current: { year: number, month: number };

    model: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    date: { year: number, month: number, day: number };
    dateT: Date;
    location: any;
    navigation = "none";
    outsideDays = "collapsed";
    showWeekNumbers = true;

    latitude: number = 0;
    longitude: number = 0;
    weatherProperties: any;
    currentDate: any = {
        day: "",
        month: "",
        date: ""
    };

    servicesArr: string[] = [];
    eventArr: EventC[] = [];
    customerArr: CustomerM[] = [];

    constructor(private _firestoreService: FirestoreDataService, private _dateService: EventForDateService,
        private http: HttpClient, private _script: ScriptLoaderService) {

    }

    ngOnInit() {
        this.current = { year: now.getFullYear(), month: now.getMonth() + 1 };
        this.next = { year: now.getFullYear(), month: now.getMonth() + 1 };
        this.next2next = { year: now.getFullYear(), month: now.getMonth() + 1 };

        //console.log(this.current);
        if (this.current.month == 12) {
            //console.log(this.next);
            this.next.year = this.current.year + 1;
            this.next.month = 1;
            this.next2next.year = this.current.year + 1;
            this.next2next.month = 2;
        }
        else {
            //console.log(this.current);
            this.next.year = this.current.year;
            this.next.month = this.current.month + 1;
            this.next2next.month = this.current.month + 2;
            //console.log(this.next);
        }

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
                this.customerArr = data;
            }
        );
    }

    selectToday() {
        this.model = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        this.onDateChange(this.model);
        /* this.current = {year: now.getFullYear(), month: now.getMonth() + 1};
        if(this.current.month == 12){
            //console.log(this.next);
            this.next.year = this.current.year + 1;
            this.next.month = 1;
            this.next2next.year = this.current.year + 1;
            this.next2next.month = 2;
        }
        else{
            //console.log(this.current);
            this.next.year = this.current.year;
            this.next.month = this.current.month+1;
            this.next2next.month = this.current.month+2;
            //console.log(this.next);
        } */
    }

    showDate(model) {
        var months = ["JAN", "FEB", "MAR", "APR", "MAJ", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC"];
        var days = ["SØNDAG", "MANDAG", "TIRSDAG", "ONSDAG", "TORSDAG", "FREDAG", "LØRDAG"];

        var d = new Date();
        d.setMonth(model.month - 1);
        d.setDate(model.day);
        this.currentDate.day = days[d.getDay()];
        this.currentDate.month = months[d.getMonth()];
        //console.log(this.currentDate);

        if (d.getDate() <= 9)
            this.currentDate.date = "0" + d.getDate();
        else
            this.currentDate.date = d.getDate();
    }


    selectTomorrow() {
        this.model = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 };
        this.onDateChange(this.model);
    }


    onDateChange(date: NgbDateStruct) {
        this.showDate(this.model);
        this._dateService.changeDate(this.model);

        var d = new Date();
        d.setMonth(date.month - 1);
        d.setDate(date.day);
        d.setFullYear(date.year);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);

        //var i = 0;
        //console.log(this.customerArr);
        this._firestoreService.getEventsForDate(date.year+"-"+this.getTwoDigitValue(date.month)+"-"+this.getTwoDigitValue(date.day)).subscribe(
            (data: any) => {
                this.eventArr = data;
                /* for (let i=0; i<this.eventArr.length; i++) {
                    //console.log(this.eventArr[i].customer);
                    this.saveCustomerName(i, this.eventArr[i].customer, this.eventArr[i].services);
                    //console.log(this.customerArr);
                } */
                
                for(let i=0;i<this.eventArr.length;i++){
                    for(let j=0;j<this.customerArr.length;j++){
                      if(this.eventArr[i].customer == this.customerArr[j].uid || this.eventArr[i].customer == this.customerArr[j].id){
                          this.eventArr[i].c_name = this.customerArr[j].firstName + " " + this.customerArr[j].lastName;
                          this.eventArr[i].c_mobile = this.customerArr[j].phone;
                      }
                    }
                }
                this._dateService.changeEventArray(this.eventArr);
            }
        );
    }


    saveCustomerName(i: number, cId: string, services: string[]) {
        this._firestoreService.getCustomerByUid(cId).subscribe(
            (data1) => {
                if(data1.length > 0){
                    this.eventArr[i].c_name = data1[0].firstName + " " + data1[0].lastName;
                    this.eventArr[i].c_mobile = data1[0].phone;
                }   
                else{
                    this._firestoreService.getCustomerById(cId).subscribe(
                        (data2) => {
                            this.eventArr[i].c_name = data2.firstName + " " + data2.lastName;
                            this.eventArr[i].c_mobile = data2.phone;
                        }
                    );
                } 
            }
        );

        

        //console.log(this.customerArr);
       /*  for(let j=0;j<this.customerArr.length;j++){
            if(cId == this.customerArr[j].uid)
              this.eventArr[i].c_name = this.customerArr[j].firstName + " " + this.customerArr[j].lastName;
            else if(this.eventArr[i].customer == this.customerArr[i].id)
              this.eventArr[i].c_name = this.customerArr[j].firstName + " " + this.customerArr[j].lastName;   
        } */

        /* console.log(services.length+" "+services);
        var arr : string[] = [];
        var snameIndex: number = 0;
        for(var j=0;j<services.length;j++){
            console.log("j:"+j);
            this._firestoreService.getServiceById(services[j]).subscribe(
                (data: ServicesM) => {
                    console.log("j1:"+j);
                arr[snameIndex] = data.name;

                console.log(arr[snameIndex]);
                console.log(snameIndex+"/"+data.name);
                //this.eventArr[i].snames[j] = data.name;
                }
            );
            snameIndex++;
            console.log("snameIndex: "+snameIndex);
        }
     */
        //this.eventArr[i].snames = arr;
        //console.log(this.eventArr[i].snames);
        //console.log("i: "+i+"sevices names: "+this.eventArr[i].snames);
    }


    gotoPrev() {
        //console.log(this.current);console.log(this.next);
        if (this.current.month == 1) {
            this.current.month = 12;
            this.current.year--;
            this.next.month--;
            this.next2next.month--;
        }
        else if (this.next.month == 1) {
            this.next.month = 12;
            this.next.year--;
            this.current.month--;
            this.next2next.month--;
        }
        else if (this.next2next.month == 1) {
            this.next2next.month = 12;
            this.next2next.year--;
            this.next.month--;
            this.current.month--;
        }
        else {
            this.next.month--;
            this.current.month--;
            this.next2next.month--;
        }

        //console.log(this.current);console.log(this.next);
    }


    gotoNext() {
        if (this.next.month == 12) {
            this.next.month = 1;
            this.next.year++;
            this.current.month++;
            this.next2next.month++;
        }
        else if (this.current.month == 12) {
            this.current.month = 1;
            this.current.year++;
            this.next.month++;
            this.next2next.month++;
        }
        else if (this.next2next.month == 12) {
            this.next2next.month = 1;
            this.next2next.year++;
            this.next.month++;
            this.current.month++;
        }
        else {
            this.current.month++;
            this.next.month++;
            this.next2next.month++;
        }
    }

    getTwoDigitValue(value){
        if(value <= 9)
          return "0"+value;
        else
          return value;   
    }

    

    isToday(date: NgbDateStruct){
        var d = new Date();
   
        if(date.year == d.getFullYear() && date.month == d.getMonth()+1 && date.day == d.getDate())
           return true;
   
        return false;
    }



    ngAfterViewInit() {
        mLayout.initAside();
    }

}