import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { FirestoreServicesService } from '../../../auth/_services/firestore-services.service';
import { EventM } from '../../../auth/_models/eventM';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateService } from '../../../auth/_services/date.service';
import { HttpClient } from '@angular/common/http';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { BlankComponent } from '../../pages/default/blank/blank.component';
import { EventC } from '../../../auth/_models/eventC';
import { ServicesM } from '../../../auth/_models/index';

declare let mLayout: any;
const now = new Date();

@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    styleUrls: ["./aside-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    next: {year: number, month: number};
    next2next: {year: number, month: number};
    current: {year: number, month: number};

    model: NgbDateStruct = {year:now.getFullYear(), month:now.getMonth()+1, day: now.getDate()};
    
    date: {year: number, month: number, day:number};
    dateT: Date;
    location: any;
    navigation = "none";
    outsideDays = "collapsed";
    showWeekNumbers = true;

    latitude: number = 0;
    longitude: number = 0;
    weatherProperties : any;
    currentDate: any = {
        day: "",
        month: "",
        date: ""
    };

    servicesArr: string[] = [];
    eventArr: EventC[] = [];
    
    constructor(private _firestoreService: FirestoreServicesService, private _dateService: DateService,
         private http: HttpClient, private _script: ScriptLoaderService) {
            
    }

    ngOnInit() {
        this.current = {year: now.getFullYear(), month: now.getMonth() + 1};
        this.next = {year: now.getFullYear(), month: now.getMonth() + 1};
        this.next2next = {year: now.getFullYear(), month: now.getMonth() + 1};

        var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
        var days = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY", "SATURDAY"];

        var d = new Date();
        this.currentDate.day = days[d.getDay()];
        this.currentDate.month = months[d.getMonth()];

        if(d.getDate()<=9)
           this.currentDate.date = "0"+d.getDate();
        else  
           this.currentDate.date = d.getDate();

        //console.log(this.current);
        if(this.current.month == 12){
            //console.log(this.next);
            this.next.year = this.current.year + 1;
            this.next.month = 1;
            this.next2next.year = this.current.year + 1;
            this.next2next.month = 2;
        }
        else{
            //console.log(this.current);
            this.next.year = this.current.year;
            this.next.month = this.current.month+1;
            this.next2next.month = this.current.month+2;
            //console.log(this.next);
        }
    }

    selectToday() {
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        this.onDateChange(this.model);
    }


    selectTomorrow(){
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()+1};
        this.onDateChange(this.model);
    }


    onDateChange(date: NgbDateStruct){
        this._dateService.changeDate(this.model);

        var d = new Date();
        d.setMonth(date.month-1);
        d.setDate(date.day);
        d.setFullYear(date.year);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);

        //var i = 0;
        this._firestoreService.getEventsForDate(d).subscribe(
            (data:any) =>{
                console.log(data);
                this.eventArr = data;
                
                //i++;
                for(var i=0; i<this.eventArr.length;i++){
                    //console.log(this.eventArr[i].customer);
                    this.saveCustomerName(i, this.eventArr[i].customer, this.eventArr[i].services);
                }
                //console.log(this.eventArr);
                this._dateService.changeEventArray(this.eventArr);
            }
        );  
    }


    saveCustomerName(i: number, cId: string, services: string[]){
        this._firestoreService.getCustomerById(cId).subscribe(
            (data1)=> {
                //console.log(data1[0].firstName+" "+data1[0].lastName);
                this.eventArr[i].c_name = data1[0].firstName+" "+data1[0].lastName;
            }
        );

        /* console.log(services.length+" "+services);
        var arr : string[] = [];
        var snameIndex: number = 0;
        for(var j=0;j<services.length;j++){
            console.log("j:"+j);
            this._firestoreService.getServiceById(services[j]).subscribe(
                (data: ServicesM) => {
                    console.log("j1:"+j);
                arr[snameIndex] = data.name;

                console.log(arr[snameIndex]);
                console.log(snameIndex+"/"+data.name);
                //this.eventArr[i].snames[j] = data.name;
                }
            );
            snameIndex++;
            console.log("snameIndex: "+snameIndex);
        }
     */
        //this.eventArr[i].snames = arr;
        //console.log(this.eventArr[i].snames);
        //console.log("i: "+i+"sevices names: "+this.eventArr[i].snames);
    }


    gotoPrev(){
        //console.log(this.current);console.log(this.next);
        if(this.current.month==1){
          this.current.month = 12;
          this.current.year--;
          this.next.month--;
          this.next2next.month--; 
        }
        else if(this.next.month==1){
          this.next.month = 12;
          this.next.year--;
          this.current.month--;
          this.next2next.month--; 
        }
        else if(this.next2next.month==1){
          this.next2next.month = 12;
          this.next2next.year--;
          this.next.month--;
          this.current.month--;
        }
        else{
          this.next.month--;
          this.current.month--; 
          this.next2next.month--; 
        }
      
        //console.log(this.current);console.log(this.next);
    }

  
    gotoNext(){
        if(this.next.month==12){
          this.next.month = 1;
          this.next.year++;
          this.current.month++;
          this.next2next.month++;
        }
        else if(this.current.month==12){
          this.current.month = 1;
          this.current.year++;
          this.next.month++;
          this.next2next.month++;
        }
        else if(this.next2next.month==12){
          this.next2next.month = 1;
          this.next2next.year++;
          this.next.month++;
          this.current.month++;
        }
        else{
          this.current.month++;
          this.next.month++;
          this.next2next.month++;
        }
    }

    

    ngAfterViewInit() {
        mLayout.initAside();  
    }

}