import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirestoreServicesService } from '../../../auth/_services/firestore-services.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Stylist } from '../../../auth/_models/stylist';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    styleUrls: ["./header-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    firstName: string;
    lastName: string;
    email: string;
    photoUrl: string;
    color: string;
    authS: any;
    stylistCollection: AngularFirestoreCollection<Stylist>;
    stylists: Observable<Stylist[]>;
    stylistArr: Stylist[] = []; 

    latitude: number = 0;
    longitude: number = 0;
    weatherProperties : any;

    constructor(private af: AngularFireAuth, private afs: AngularFirestore, 
        private _firestoreService: FirestoreServicesService, private http: HttpClient) {

            this.af.authState.subscribe(auth => {
            if(auth) {
                this.authS = auth;  
                /* this.name = auth.displayName;
                this.email = auth.email;
                this.photoUrl = auth.photoURL; */
            }
            });

    }

    ngOnInit() {
        this._firestoreService.getStylistByUID(this.authS.uid).subscribe(
            (data: Stylist[]) => {
              this.stylistArr = data;
              //console.log(this.stylistArr);
              this.photoUrl = this.stylistArr[0].imageUrl;
                this.firstName = this.stylistArr[0].firstName;
                this.lastName = this.stylistArr[0].lastName;
                this.email = this.stylistArr[0].email;
                this.color = this.stylistArr[0].color;
            }
        );
        
        if(navigator.geolocation){
            navigator.geolocation.watchPosition((position) => {
               this.setPosition(position);
            });
        }
        else{
           alert("Geolocation is not supported by this browser.");
        } 
    }

    setPosition(position){
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;

        //console.log(this.latitude+" "+this.longitude);

        var apiKey = '6ea35821d2fcd1244eddf8a7467a1b1e';
            var url = 'https://api.openweathermap.org/data/2.5/weather?&units=metric&lat='+this.latitude+'&lon='+this.longitude;
            this.http.get(url+'&appid='+apiKey).subscribe(
                  data =>{ 
                  //console.log(data);
                  this.weatherProperties = data;
            });
    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }

}