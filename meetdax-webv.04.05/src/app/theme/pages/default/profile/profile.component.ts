import { Component, OnInit } from '@angular/core';
import { Stylist } from '../../../../auth/_models/stylist';
import { AngularFirestore } from 'angularfire2/firestore';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  authS: any = null;
  ages: number[] = new Array(41);
  userData: Stylist;
  
  stylistArr: Stylist[] = []; 
  stylistData: any = {
    uid: '',
    imageFile: null,
    imageUrl: '',
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    gender: '',
    age: 0,
    services: []
  };
  
  selectedFile: File;
  private basePath:string = '/stylists';

  audio = new Audio();

  isAlert: boolean = false;
  alertMessage: string = "";

  isProfileChanged: boolean = false;

  
  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth, 
    private _firestoreService: FirestoreServicesService, private _script: ScriptLoaderService) {
    
    this.afAuth.authState.subscribe((auth) => {
      if(auth) {
        this.authS = auth;  
      }
    });

    var lastAge = 20;
    for(var i=0;i<=40;i++){
        this.ages[i] = lastAge++;
    }
    
    this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
    this.audio.load(); 
    //console.log(this.stylistArr);
    //console.log(this.stylistData);
  }

  ngOnInit() {
    this._firestoreService.getStylistByUID(this.authS.uid).subscribe(
      (user: Stylist[]) => {
        //console.log(user);
        this.stylistArr = user;
        //console.log(this.stylistArr);
        this.stylistData = {
          id: this.stylistArr[0].id,
          uid: this.stylistArr[0].uid,
          imageFile: null,
          imageUrl: this.stylistArr[0].imageUrl,
          firstName: this.stylistArr[0].firstName,
          lastName: this.stylistArr[0].lastName,
          email: this.stylistArr[0].email,
          mobile: this.stylistArr[0].mobile,
          gender: this.stylistArr[0].gender,
          age: this.stylistArr[0].age,
          services: this.stylistArr[0].services
        };
      }
    );
  }

  updateProfile(){
        let storageRef = firebase.storage().ref();
        
        if(this.isProfileChanged){
            let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) =>  {
                // upload in progress
                },
                (error) => {
                // upload failed
                console.log(error)
                },
                () => {
                // upload success
                  this.stylistData.imageUrl = uploadTask.snapshot.downloadURL;
                                      
                  this._firestoreService.updateStylist(this.stylistData)
                  .then(()=>{
                    this.showToastr("Profile Updated Succesfully");
                  })
                  .catch((err)=>{
                    console.log(err);
                    this.showToastr("Profile Updation Error");
                  });
                }
            );
        }
        else{
            this._firestoreService.updateStylist(this.stylistData)
            .then(()=>{
              this.showToastr("Profile Updated Succesfully");
            })
            .catch((err)=>{
              console.log(err);
              this.showToastr("Profile Updation Error");
            });
        }
     
  }

  showToastr(msg){
    this.audio.play();
    this.isAlert = true;
    this.alertMessage = msg;
    setTimeout(() => this.isAlert= false, 5000);
  }

  imagePreview(file:FileList){
    this.isProfileChanged = true;
    this.selectedFile = file.item(0);

      var reader = new FileReader();
      reader.onload = (event:any) => {
          this.stylistData.imageUrl = event.target.result;  
          //console.log(this.imageUrl);
      }
      reader.readAsDataURL(this.selectedFile);
  }

}
