import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { EventM } from '../../../../auth/_models/eventM';
import { Stylist } from '../../../../auth/_models/stylist';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { CustomerM } from '../../../../auth/_models/customerM';
import { EventFullCal } from '../../../../auth/_models/eventFullCal';
import { EventAddM } from '../../../../auth/_models/eventAddM';
import { Observable } from 'rxjs/Observable';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { DateService } from '../../../../auth/_services/date.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { EventC } from '../../../../auth/_models/index';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import swal from 'sweetalert';

const now = new Date();
@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class BlankComponent implements OnInit, AfterViewInit {
    eventArr:EventC[] = []; 
    allStylistsArr: Stylist[] = []; 
    service: ServicesM; 
    servicesArr:string[] = new Array();
    closeResult: string;
    
    eventToShowArr: EventFullCal[] = [];

    serviceArr: ServicesM[] = [];
    //serviceArr1: any[] = [];
    customerArr: CustomerM[] = [];
    services: any = [];
    errorMessage: string = "";
    
    customerTA: any = [];
    isAlert: boolean = false;
    alertMessage: string;

    audio = new Audio();
    date: Date;

    slotArr: boolean[] = new Array(9);

    priceArr: number[] = new Array(0,0);
    durationArr: number[] = new Array(0,0);

    display = 'none';

    minDate: NgbDateStruct = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};

    timeline$: Observable<string>;

    event: EventM = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        imageUrl: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    };

    eventU: EventM = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        imageUrl: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    };

    eventAddF: EventAddM = {
        id: "",
        eid: "",
        date: null,
        startTime: null,
        endTime: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventUpdate: EventAddM = {
        id: "",
        eid: "",
        date: null,
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventToShow: any = {
        id: "",
        eid: "",
        customer: "",
        services: [],
        stylist: "",
        start: "",
        end: "",
        day: "",
        weekDay: "",
        month: "",
        how_often: "",
        recurring: false
    }

    model: any = {
        name: "",
        phone: ""
    };

    customerAdd: CustomerM = {
        uid: "",
        email: "",
        firstName: "",
        lastName: "",
        dob: null,
        gender: "", 
        phone: "",
        comments: ""
    };
 
    search = (text$: Observable<string>) =>
       text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.customerTA.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
     
    formatter = (x: {name: string}) => x.name; 

    @ViewChild('eventAddForm') eventAddForm: any;
    
    constructor(private _firestoreService:FirestoreServicesService, private _dateService: DateService
               ,private _script: ScriptLoaderService   ) {
           this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
           this.audio.load();
           
           this.timeline$ = Observable
                            .interval(1000)
                            .map(val => this.getTimeline());
    }

   

    ngOnInit() {
        var i = 0;
        this._dateService.currentEventArr.subscribe(
            (data) => {
                this.eventArr = data;
                //console.log(data.length);
                
                /* if(this.eventArr==null){
                    this._firestoreService.getCustomerById(this.eventArr[i].customer).subscribe(
                        data =>
                        this.eventArr[i].c_name = data[i].firstName+" "+data[i].lastName
                    ); 
                    
                    console.log(this.eventArr[i]);
                    i++;
                }  */
                
                }    
                
                /* if(this.eventArr.length>0){
                    console.log(this.eventArr.length);
                } */

                //console.log(this.eventArr.length);     
        );

        console.log(this.eventArr);
    
        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;               
            }
        );

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
              this.customerArr = data;
            }
        );
        //console.log(this.slotArr);
    }

    

    ngAfterViewInit() {
        this._script.loadScripts('app-blank',
            ['assets/demo/default/custom/components/forms/widgets/bootstrap-select.js',
            'assets/demo/default/custom/components/forms/validation/form-controls.js']);

    }

    

    getPosition(startDate){
        var hours = startDate.getHours();
        
        switch(hours){
            case 8: return '0px';
            case 9: return '100px';   
            case 10: return '200px';
            case 11: return '300px';
            case 12: return '400px';
            case 13: return '500px';
            case 14: return '600px';
            case 15: return '700px';
            case 16: return '800px';
        }
    }

    getHeight(duration){
        return duration * 1.66 + "px";
    }

    //method called when event in fullCalendar is clicked
    eventClick(model: any) {
        this.openDetailModal();
        
        var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
        var weekdays = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"];
                
        var ds = new Date(model.startDate);
        var de = new Date(model.endDate);
        //console.log(ds);

        console.log(model);
        this.eventUpdate.services = model.services;
        console.log(this.eventUpdate);
        var d = new Date();
        d.setDate(model.startDate.getDate());
        d.setMonth(model.startDate.getMonth());
        d.setFullYear(model.startDate.getFullYear());
        
        model.date = {year: d.getFullYear(), month: d.getMonth()+1,day: d.getDate()};
        
        this.eventUpdate = {
            id: model.id,
            eid: model.eid,
            date: model.date,
            startTime: model.startDate,
            endTime: model.endDate,
            how_often: model.how_often,
            customer: model.customer,
            services: [],
            stylist: model.stylist,
            recurring: model.recurring,
            price: model.price,
            duration: model.duration
        }; 

        this.onSelectStylist(this.eventUpdate.stylist);
        this.eventUpdate.services = [];
        console.log(this.eventUpdate);

        this.event.startDate = model.startDate;
        this.event.endDate = model.endDate;
        this.event.services = model.services;

        //console.log(this.eventUpdate);
        
        //console.log(this.event);
        this.eventToShowArr = [];
        this.eventToShow = {
            id: "",
            eid: "",
            customer: "",
            services: [],
            stylist: "",
            start: "",
            end: "",
            day: "",
            weekDay: "",
            month: "",
            how_often: "",
            recurring: false
        }
        
        for(var j=0;j<model.services.length;j++){
            this._firestoreService.getServiceById(model.services[j]).subscribe(
                (data: ServicesM) => {
                  this.servicesArr[j] = data.name;
                  //console.log(this.servicesArr[j]+"++++"+data.name);
                  this.eventToShow.services.push(data.name);
                }
            );
        }
        
        this._firestoreService.getStylistById(model.stylist).subscribe(
            (data) => {
              var stylist_name = data.firstName+" "+data.lastName;  
              //console.log(stylist_name);
              this.eventToShow.stylist = stylist_name;
            }
        );

        this._firestoreService.getCustomerById(model.customer).subscribe(
            (data) => {
              var customer_name = data[0].firstName+" "+data[0].lastName;  
              //console.log(customer_name);
              this.eventToShow.customer = customer_name;
              
            }
        );
        //console.log(ds);   
        this.eventToShow.id = model.id;     
        this.eventToShow.start = this.getMin(ds.getHours())+" : "+this.getMin(ds.getMinutes());
        this.eventToShow.end = this.getMin(de.getHours())+" : "+this.getMin(de.getMinutes());
        this.eventToShow.day = ds.getDate();
        this.eventToShow.weekDay = weekdays[de.getDay()];
        this.eventToShow.month = months[ds.getMonth()];
        this.eventToShow.how_often = model.how_often;
        //console.log(this.eventToShow);
        this.eventToShowArr.push( this.eventToShow);
        
        console.log(this.eventToShowArr);
    }//event click ends

    //method called when empty space is clicked, for adding event 
    addEventClick(stylistId, time){
        console.log("Clicked/"+time);
        //this.openModal();
        this.eventAddF.stylist = stylistId;
        this.onSelectStylist(stylistId);

        console.log($('#eight').val());
        switch(time){
           case 8: console.log(time); $("input[name='"+time+"'][value='"+8+"']").prop('checked', true); break;
           case 9: $('#nine').prop('checked', 'true'); break;
           case 10: $('#ten').attr('checked', 'true'); break;
           case 11: $('#eleven').attr('checked', 'true'); break;
           case 12: $('#twelve').attr('checked', 'true'); break;
           case 13: $('#thirteen').attr('checked', 'true'); break;
           case 14: $('#fourteen').attr('checked', 'true'); break;
           case 15: $('#fifteen').attr('checked', 'true'); break;
           case 16: $('#sixteen').attr('checked', 'true'); break;
        }

        this._dateService.changedDate.subscribe(
            (data) => {
                //console.log(data);
                this.eventAddF.date = data;
        });

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
              this.customerArr = data;
            }
        );

        console.log(this.customerArr);
        for(var i=0;i< this.customerArr.length;i++){
            var custTA = {
                name : this.customerArr[i].firstName +" " +this.customerArr[i].lastName,
                phone : this.customerArr[i].phone
            }
            //console.log(custTA);
            this.customerTA[i] = custTA;
        }

        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;
            }
        );

        //console.log(this.allStylistsArr);
        this.openAddModal();
    }

    onUpdateSubmit(){
        console.log("Inside Booking Updation");
       //console.log(this.eventUpdate.services);
       //console.log(this.event);
       this.eventU.id = this.eventUpdate.id;
       this.eventU.recurring = false;
       
       //console.log(this.event.startDate);
       //console.log(this.event.endDate);
       console.log(this.customerArr);
       this.eventU.stylist = this.eventUpdate.stylist;
       this.eventU.services = this.eventUpdate.services;
       this.eventU.price = this.eventUpdate.price;
       this.eventU.duration = this.eventUpdate.duration;
       this.eventU.customer = this.eventUpdate.customer;

       
       /* for(var i=0; i<this.customerArr.length; i++){
           if(this.customerArr[i].email == this.eventU.customer){
               this.eventU.customer = this.customerArr[i].uid;
           }
       }     */

       console.log(this.eventU);
       this._firestoreService.updateEvent(this.eventU)
       .then(() => {
           this.showToastr("Booking Updated Successfully");
       })
       .catch((err) => {
           this.showToastr("Booking Updation Error");
       });

       //this.event.services = [];
       /* this.eventU = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        imageUrl: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,  
        price: 0,
        duration: 0
        }; */

        this.closeEditModal();        
        
    }


    //method for event deletion
    deleteEvent(){
        console.log("Inside Booking Deletion");
        this._firestoreService.deleteEventFromCollection(this.eventToShow.id)
        .then(() => {
             this.showToastr("Booking Deleted Succesfully");
        })
        .catch((err) => {
             this.showToastr("Booking Deletion Error"); 
        }); 
        this.closeEditModal();
    }

    updateColor(color: string, id: string){
        console.log(color+""+id);
        this._firestoreService.updateStylistColor(id, color);
    }


    onSelectStylist(id:string){
        //console.log(id);
        this.serviceArr = []; 
        this.event.services[0] = "";
        this.event.services[1] = "";
        this.eventAddF.price = 0;
        this.eventAddF.duration = 0;
        this.eventUpdate.price = 0;
        this.eventUpdate.duration = 0;
        this.eventUpdate.services = [];

        this._firestoreService.getStylistById(id).subscribe(
            (data) => {
                this.event.imageUrl = data.imageUrl;
                this.eventU.imageUrl = data.imageUrl;
            }    
        );

        
        for(var i=0;i<this.allStylistsArr.length;i++){
            if(id == this.allStylistsArr[i].id){
                this.services = this.allStylistsArr[i].services;
            }
        }
        
        var k = 0;
        for(var j=0;j<this.services.length;j++){
            this._firestoreService.getServiceById(this.services[j]).subscribe(
                (data : ServicesM) => {
                  //console.log("Service: "+this.services[k]+", Price: "+data.price);
                  
                  data.id = this.services[k];
                  this.serviceArr.push(data);
                  k++;
                }
            );
        }

        
        /* console.log(this.serviceArr);

        for(var k=0;k<this.eventUpdate.services.length;k++){
            console.log("first"+this.serviceArr.length);
            for(var l=0;l<this.serviceArr.length;l++){
                console.log("second");
                console.log(this.eventUpdate.services[k]+"/"+this.serviceArr[l].id);
                if(this.eventUpdate.services[k] == this.serviceArr[l].id){
                    $('#servicesUpdateF').prop('checked', true);
                    console.log("Matched");
                }
            }
        }

        $('#servicesUpdateF').prop('checked', 'true'); */


        
        console.log(this.serviceArr);
    }

    //+++++++++++++++++++++++++++++++++++Selecting ServiceTwo++++++++++++++++++++++++++++++++++
    onSelectServiceOne(id){
        if(this.eventUpdate.services[0] != id && this.eventUpdate.services[1] != id){
            this.eventUpdate.services[0] = id;
            console.log(this.eventUpdate.services);
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 //console.log(this.serviceArr[i].price);
     
                 this.priceArr[0] = this.serviceArr[i].price;
                 //console.log(this.priceArr[0]);
                 this.durationArr[0] = this.serviceArr[i].duration;
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }
            } 
         }

         /* if(this.eventAddF.services[0] != id && this.eventAddF.services[1] != id){
            this.eventAddF.services[0] = id;
            console.log(this.eventAddF.services);
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 //console.log(this.serviceArr[i].price);
     
                 this.priceArr[0] = this.serviceArr[i].price;
                 //console.log(this.priceArr[0]);
                 this.durationArr[0] = this.serviceArr[i].duration;
                 this.eventAddF.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAddF.duration= this.durationArr[0] + this.durationArr[1];
              }
            } 
         } */
        
        
        /* if(id == '0'){
            console.log(this.eventUpdate.services);
            this.eventUpdate.services.splice(0,1);
            this.eventAddF.services.splice(0,1);
            console.log(this.eventUpdate.services);
            console.log(this.eventAddF.services);
            this.priceArr[0] = 0;
            console.log(this.priceArr[0]+"/"+this.priceArr[1]);
            this.durationArr[0] = 0;
            this.eventAddF.price = this.priceArr[0] + this.priceArr[1];
            this.eventAddF.duration = this.durationArr[0] + this.durationArr[1];
            this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
            this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
        } */

        //console.log(this.eventAdd.services+"/"+this.eventAdd.price);
    }

    //+++++++++++++++++++++++++++++++++++Selecting ServiceTwo++++++++++++++++++++++++++++++++++
    onSelectServiceTwo(id){
        if(this.eventUpdate.services[1] != id && this.eventUpdate.services[0] != id){
            this.eventUpdate.services[1] = id;
            console.log(this.eventUpdate.services);
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 //console.log(this.serviceArr[i].price);
     
                 this.priceArr[1] = this.serviceArr[i].price;
                 //console.log(this.priceArr[1]);
                 this.durationArr[1] = this.serviceArr[i].duration;
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }
            }
         }

         /* if(this.eventAddF.services[1]!= id && this.eventAddF.services[0]!= id){
            this.eventAddF.services[1] = id;
            console.log(this.eventAddF.services);
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 //console.log(this.serviceArr[i].price);
     
                 this.priceArr[1] = this.serviceArr[i].price;
                 //console.log(this.priceArr[0]);
                 this.durationArr[1] = this.serviceArr[i].duration;
                 this.eventAddF.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAddF.duration= this.durationArr[0] + this.durationArr[1];
              }
            } 
         } */

         
        /* if(id == '0'){
            this.eventUpdate.services.splice(1,1);
            this.eventAddF.services.splice(1,1);
            console.log(this.eventUpdate.services);
            console.log(this.eventAddF.services);
            this.priceArr[1] = 0;
            console.log(this.priceArr[0]);
            this.durationArr[1] = 0;
            this.eventAddF.price = this.priceArr[0] + this.priceArr[1];
            this.eventAddF.duration= this.durationArr[0] + this.durationArr[1];
            this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
            this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
         } */
    }

    onSelectService(service, event){
        console.log(service);
        if($('#servicesAddF:checked').length > 2){
            event.target.checked = false;
        }
        else if(event.target.checked == false){
            console.log(this.eventAddF.duration);
            var index = this.eventAddF.services.indexOf(service.id);
            this.eventAddF.services.splice(index,1);
            this.eventAddF.duration-= service.duration;
            this.eventAddF.price-= service.price;
            console.log(this.eventAddF.duration);
        }
        else{
            console.log(this.eventAddF.duration);
            this.eventAddF.services.push(service.id);
            this.eventAddF.duration+= service.duration;
            this.eventAddF.price+= service.price;
            console.log(this.eventAddF.duration);
        }
    }

    onSelectServiceU(service, event){
        console.log(service);
        if($('#servicesUpdateF:checked').length > 2){
            event.target.checked = false;
        }
        else if(event.target.checked == false){
            console.log(this.eventUpdate.duration);
            var index = this.eventUpdate.services.indexOf(service.id);
            this.eventUpdate.services.splice(index,1);
            this.eventUpdate.duration-= service.duration;
            this.eventUpdate.price-= service.price;
            console.log(this.eventUpdate.duration);
            console.log(this.eventUpdate.services);
        }
        else{
            console.log(this.eventUpdate.duration);
            this.eventUpdate.services.push(service.id);
            this.eventUpdate.duration+= service.duration;
            this.eventUpdate.price+= service.price;
            console.log(this.eventUpdate.duration);
            console.log(this.eventUpdate.services);
        }
        
    }

    onSelectTime(value){
        this.eventAddF.startTime = value;    
    }

    onSelectTimeU(value){
        this.eventUpdate.startTime = value;

        var d = new Date();
        d.setFullYear(this.eventUpdate.date.year);
        d.setMonth(this.eventUpdate.date.month-1);
        d.setDate(this.eventUpdate.date.day);

        this.eventU.startDate = new Date(d);
        this.eventU.startDate.setHours(parseInt(this.eventUpdate.startTime));
        this.eventU.startDate.setMinutes(0);

        this.eventU.endDate = this.formatEndDate(d, this.eventUpdate.duration, this.eventUpdate.startTime);
        console.log(this.eventU.startDate);
        console.log(this.eventU.endDate);
    }

    onSubmit(){
        console.log(this.eventAddF.date+" "+this.eventAddF.startTime);
        
        var d = new Date();
        d.setFullYear(this.eventAddF.date.year);
        d.setMonth(this.eventAddF.date.month-1);
        d.setDate(this.eventAddF.date.day);

        console.log(d);

        this.event.startDate = new Date(d);
        this.event.startDate.setHours(parseInt(this.eventAddF.startTime));
        this.event.startDate.setMinutes(0);
        //console.log(start);
    
        this.event.endDate = this.formatEndDate(d, this.eventAddF.duration, this.eventAddF.startTime);
        
        console.log(this.event.startDate);
        console.log(this.event.endDate);

        console.log(this.eventAddF.services);
        this.event.stylist = this.eventAddF.stylist;
        this.event.services = this.eventAddF.services;
        this.event.price = this.eventAddF.price;
        this.event.duration = this.eventAddF.duration;
        //this.event.customer = this.eventAddF.customer;
        
        for(var i=0; i<this.customerArr.length; i++){
            if(this.customerArr[i].phone == this.eventAddF.customer.trim()){
                this.event.customer = this.customerArr[i].uid;
            }
        }
        
        /* if(this.event.customer==""){
            this.customerAdd.firstName = this.model;
            this.customerAdd.phone = this.eventAddF.customer;
            
            console.log(this.customerAdd);
            this._firestoreService.addCustomer(this.customerAdd)
            .then((docRef)=>{
                this.event.customer = docRef.id;
                
                this._firestoreService.addEvent(this.event)
                .then(() => {
                    this.showToastr("Booking Done Successfully");
                })
                .catch((err) => {
                    console.log(err);
                    this.showToastr("Booking Error");
                });
            })
            .catch((err)=>{
                console.log(err);
                this.showToastr("Error Adding New Customer");
            });
        }
        else{
            this._firestoreService.addEvent(this.event)
            .then(() => {
                this.showToastr("Booking Done Successfully");
            })
            .catch((err) => {
                console.log(err);
                this.showToastr("Booking Error");
            });
        } */

        console.log(this.eventAddF.services);
        console.log(this.event.services);
        console.log(this.event);

        this._firestoreService.addEvent(this.event)
                    .then(() => {
                        this.showToastr("Booking Done Successfully");
                    })
                    .catch((err) => {
                        console.log(err);
                        this.showToastr("Booking Error");
                    });

        this.eventAddF = {
            id: "",
            eid: "",
            date: null,
            startTime: null,
            endTime: null,
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0  
        }

        this.event.services = [];
        this.event = {
                id: "",
                eid: "",
                startDate: null,
                endDate: null,
                customer: "",
                imageUrl: "",
                stylist: "",
                services: [],
                how_often: "",
                recurring: false,  
                price: 0,
                duration: 0
        };
        this.closeAddModal();
    }

    formatStartDate(start: Date,startTime:string){
        //console.log(start+" "+startTime);
        //var start: Date = new Date(date);
        start.setHours(parseInt(startTime));
        start.setMinutes(0);
        
        console.log(this.event.startDate);
        return start;
    }

    formatEndDate(end:Date, duration: number, startTime: string){
        //console.log(end+" "+duration+" "+startTime);
        var hrs = parseInt(startTime)+(duration/60);
        var min = duration%60;

        //var end = new Date(date);
        end.setHours(hrs);
        end.setMinutes(min);
        //console.log(end);

        console.log(this.event.endDate);
        return end;
    }

    showToastr(msg){
        this.audio.play();
        this.isAlert = true;
        this.alertMessage = msg;
        setTimeout(() => this.isAlert = false, 5000);
    }

    openEditModal(){
        this.closeDetailModal();
        this.display = "block";
        $('#edit_modal').addClass('show');  
    }

    closeEditModal(){
        //this.closeDetailModal();
        this.display = 'none';
        $('#edit_modal').removeClass('show');
       /*  this.eventUpdate = {
            id: "",
            eid: "",
            date: null,
            startTime: null,
            endTime: null,
            how_often: "",
            customer: "",
            services: null,
            stylist: "",
            recurring: false,
            price: 0,
            duration: 0
        };  */
        console.log(this.eventUpdate);
    }

    openDetailModal(){
        this.display = 'block';
        $('#detail_modal').addClass('show');
    }

    closeDetailModal(){
        this.display = 'none';
        $('#detail_modal').removeClass('show');
    }

    closeAddModal(){
        this.display = 'none';
        $('#add_event_modal').removeClass('show');

        this.eventAddForm.reset();

        this.eventAddF = {
            id: "",
            eid: "",
            date: null,
            startTime: null,
            endTime: null,
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0 
        };

        this.event = {
            id: "",
            eid: "",
            startDate: null,
            endDate: null,
            customer: "",
            imageUrl: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,  
            price: 0,
            duration: 0
        };
    }

    openAddModal(){
        this.display = 'block';
        $('#add_event_modal').addClass('show');
    }

    getMin(min){
        if(min<10)
           return '0'+min;
        else
           return min;   
    }

    setCustomer(){
        //console.log("Inside SetCustomer");
        this.eventAddF.customer = this.model.phone;
        this.eventUpdate.customer = this.model.phone;
    }

    /* onSelectDate(){
        //console.log(this.eventAdd.date+"/"+this.eventAdd.stylist);
        this.slotArr = [false,false,false,false,false,false,false,false,false];
        var d = new Date();
        d.set
        d.setMinutes(0);
        
        for(var i=0;i<9;i++){
            d.setHours(i+8);
            //console.log(d);
            this.getSlot(i+8,d);
        } */
        
        
        /* d.setHours(16);
        this._firestoreService.getEventsForStylistDate(d, this.eventAdd.stylist).subscribe(
            (data) => {
                if(data){
                    this.slotArr[16] = true;
                }
                  
                else
                  console.log("Slot Available"+ data);  
            }
        ); */

        //console.log(this.slotArr);
    //}

    getSlot(i:number ,d:Date){
        //console.log(i+8+" "+d);
        this._firestoreService.getEventsForStylistDate(d, this.eventAddF.stylist).subscribe(
            (data) => {
                if(data != null){
                  this.slotArr[i-8] = true; 
                  
                  //console.log("True");
                }  
            }
        );
    }

    changeColor(color, event){
         $(event.path[12]).css('background-color', color); 
         console.log(event);
    }

    checkService(id){
        //console.log(id);
          if(id=="sr7geiInNklO2TTzmNPN")
             return true;
    }

    getTimeline() {
        var timeline_local: string;
        var date = new Date();
        var hrs = date.getHours();
        var mins = date.getMinutes();

        //this.timeline = 40 + (1 * 60 *1.66) + (mins * 1.66) + 'px';
        switch(hrs){
            case 8: timeline_local = (((0*60) + (mins))*1.66)-14+'px'; 
                    break;
            case 9: timeline_local = (((1*60) + (mins))*1.66)-14+'px';
                    break;
            case 10: timeline_local = (((2*60) + (mins))*1.66)-14+'px';
                    break;
            case 11: timeline_local = (((3*60) + (mins))*1.66)-14+'px';
                    break;
            case 12: timeline_local = (((4*60) + (mins))*1.66)-14+'px';
                    break;
            case 13: timeline_local = (((5*60) + (mins))*1.66)-14+'px';
                    break;
            case 14: timeline_local = (((6*60) + (mins))*1.66)-14+'px';
                    break;
            case 15: timeline_local = (((7*60) + (mins))*1.66)-14+'px';
                    break;
            case 16: timeline_local = (((8*60) + (mins))*1.66)-13+'px';
                     break; 
            case 17: timeline_local = (((9*60) + (mins))*1.66)-12+'px';
                     break;  
            case 18: timeline_local = (((10*60) + (mins))*1.66)-12+'px';
                     break;                 
            default: timeline_local = -20+'px';
                     break;          
        }
               
        return timeline_local;
    }

    
}