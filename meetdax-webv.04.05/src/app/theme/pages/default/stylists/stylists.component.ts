import { Component, OnInit } from '@angular/core';
import { Stylist } from '../../../../auth/_models/stylist';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { WindowService } from '../../../../auth/_services/index';
import * as firebase from 'firebase'; 
import { ServicesM } from '../../../../auth/_models/servicesM';

declare let Dropzone: any;
@Component({
  selector: 'app-stylists',
  templateUrl: './stylists.component.html',
  styleUrls: ['stylists.component.css']
})

export class StylistsComponent implements OnInit, AfterViewInit {
  stylistArr: Stylist[] = [];

  stylistAddF: Stylist = {
    uid: '',
    imageFile: null,
    imageUrl: '',
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    password: '',
    gender: '',
    age: 0,
    services: [],
    serviceFor: [],
    color: ''
  };

  serviceAddF: ServicesM = {
    name: "",
    price: 0,
    duration: 0,
    stylist: "",
    description: ""
  }

  formData: any = {
    imageFile: null,
    imageUrl: '',
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    password: '',
    gender: '',
    age: 0,
    services: [],
    serviceFor: [],
    color: '',
    name: "",
    price: 0,
    duration: 0,
    stylist: "",
    description: ""
  }
  
  windowRef: any;
  selectedFile: File;
  ages: number[] = new Array(30);
  verificationCode: string;
  isAlert: boolean = false;
  alertMessage: string;
  isProgressAlert: boolean = false;
  progressMessage: string;
  alertClass: string;
  serviceForArr: any[] = [];
  colorsArr: any[] = [];

  audio = new Audio();
  private basePath:string = '/stylists';

  display = 'none';

  constructor(private _firestoreService: FirestoreServicesService, private _script: ScriptLoaderService,
              private win: WindowService) {
      this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
      this.audio.load();      

      this.stylistAddF.imageUrl = "assets/app/media/img/users/user.png";  

      var lastAge = 18;
      for(var i=0;i<=63;i++){
          this.ages[i] = lastAge++;
      } 

      this.serviceForArr = [
        {
          name: 'Male',
          selected: false
        },
        {
          name: 'Female',
          selected: false
        },
        {
          name: 'Children',
          selected: false
        }
      ];

      this.colorsArr = [
        {
          name: 'Red',
          code: '#ffebee'
        },
        {
          name: 'Green',
          code: '#f1f8e9'
        },
        {
          name: 'Blue',
          code: '#e1f5fe'
        },
        {
          name: 'Grey',
          code: '#eceff1'
        },
        {
          name: 'Orange',
          code: '#fff3e0'
        },
        {
          name: 'Purple',
          code: '#f3e5f5'
        },
        {
          name: 'Pink',
          code: '#fce4ec'
        },
        {
          name: 'Default',
          code: '#ffffff'
        },

      ];
  }
  

  ngOnInit() {
    this._firestoreService.getAllStylists().subscribe(
      (user: Stylist[]) => {
        this.stylistArr = user;
        console.log(this.stylistArr);
      }
    );
    this.progressMessage = "Uploading Picture...";
    /* this.windowRef = this.win.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    this.windowRef.recaptchaVerifier.render(); */
    //this.alertClass = "alert-success";
  }

  ngAfterViewInit(){
    this._script.loadScripts('app-stylists',
            ['assets/demo/default/custom/components/forms/wizard/wizard.js','assets/demo/default/custom/components/forms/widgets/dropzone.js']);
    Dropzone._autoDiscoverFunction();        
  }

  imagePreview(file:FileList){
      this.selectedFile = file.item(0);

      var reader = new FileReader();
      reader.onload = (event:any) => {
          this.stylistAddF.imageUrl = event.target.result;  
          //console.log(this.imageUrl);
      }
      reader.readAsDataURL(this.selectedFile);
  }


  setupVerification(){
    console.log("Inside setupVerification()");
   /*  this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        console.log(response);
         this.sendVerificationCode();
         console.log("Inside Inside setupVerification()");
      }
    }); */
  }

  sendVerificationCode(){
      /* const appVerifier = this.windowRef.recaptchaVerifier;
      const num = this.stylistAddF.mobile.toString();
      console.log("Inside sendVerificationCode()");
      firebase.auth().signInWithPhoneNumber(num, appVerifier)
          .then(result => {
              this.windowRef.confirmationResult = result;
              this.isProgressAlert = true;
              this.alertMessage = "Verification Code Sent";
              this.alertClass = "alert-success";
              console.log("Verification Code Sent");
          })
          .catch( error => console.log(error) ); */
  }

  verifyLoginCode() {
      this.windowRef.confirmationResult
          .confirm(this.verificationCode)
          .then( result => {
              console.log(result.user.id);
              this.stylistAddF.uid = result.user.uid;
              this.isAlert = true;
              this.alertMessage = "Stylist Verified Successfully";
              this.alertClass = "alert-success";
          })
          .catch( (error) => {
              console.log(error, "Incorrect code entered?");
              this.isAlert = true;
              this.alertMessage = "Incorrect Verrification Code";
              this.alertClass = "alert-danger";
          });
  }

  setServicesFor(sf){
      if(sf.selected == true){
        this.stylistAddF.serviceFor.push(sf.name);
      }
      else{
        var index = this.stylistAddF.serviceFor.indexOf(sf.name);
        console.log(index);
        this.stylistAddF.serviceFor.splice(index,1);
      }
  }

  onSubmitStylistForm(){
    console.log("Submit Stylist form");
    
    /* this.stylistAddF.firstName = this.formData.firstName;
    this.stylistAddF.lastName = this.formData.lastName;
    this.stylistAddF.mobile = this.formData.mobile;
    this.stylistAddF.email = this.formData.email;
    this.stylistAddF.gender = this.formData.gender;
    this.stylistAddF.age = this.formData.age;
    this.stylistAddF.serviceFor = this.formData.serviceFor;
    this.stylistAddF.color = this.formData.color;
    this.serviceAddF.name = this.formData.name;
    this.serviceAddF.price = this.formData.price;
    this.serviceAddF.duration = this.formData.duration;
    this.serviceAddF.description = this.formData.description; */
    
    this.showProgressModal();
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
          (snapshot) =>  {
          // upload in progress
            this.progressMessage = "Uploading Picture..."; 
          },
          (error) => {
          // upload failed
            console.log(error);
            this.progressMessage = "Error Uploading Picture...";
          },
          () => {
          // upload success
              this.stylistAddF.imageUrl = uploadTask.snapshot.downloadURL;
              //this.serviceAddF.stylist="abc";
              //this.progressMessage = "Adding Stylist...";              
          }
    );

    this.progressMessage = "Adding Stylist...";
    this._firestoreService.addStylistToCollection(this.stylistAddF)
        .then((styDocRef)=>{
            this.serviceAddF.stylist = styDocRef.id;
            
        })
        .catch((err)=>{
            this.showToastr("Error Adding Stylist");
        }); 

    this._firestoreService.getAllServices()
    .subscribe(

    );
        
    this.progressMessage = "Adding Service...";    
    this._firestoreService.addService(this.serviceAddF)
        .then((servDocRef)=>{
          console.log("iNSIDE sERVICE");
          this.stylistAddF.services.push(servDocRef.id);
          
          this._firestoreService.updateStylist(this.stylistAddF);
          this.showToastr("Stylist Added Successfully");
          this.hideProgressModal();
        })
        .catch((err)=>{
          console.log(err);
          this.progressMessage = "Error Adding Service..."; 
          this.showToastr("Error Adding Service");
          
        });
  }

  showToastr(msg){
    this.audio.play();
    this.isAlert = true;
    this.alertMessage = msg;
    console.log(this.isAlert+"/"+this.alertMessage);
    setTimeout(() => this.isAlert= false, 5000);
  }

  showProgressModal(){
    console.log("ShowProgressMOdal");
    //this.display = 'block';
    //$('.progressModal').css("display","block");
    this.isProgressAlert = true;
    
  }

  hideProgressModal(){
    //this.display = 'none';
    //$('.progressModal').removeClass('showPM');
    this.isProgressAlert = false;
  }

}
