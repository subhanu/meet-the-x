import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreServicesService } from '../../../../auth/_services/index';
import { CustomerM } from '../../../../auth/_models/customerM';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
})
export class CustomersComponent implements OnInit {
  customersArr: CustomerM[] = [];
  customerAF: CustomerM = {
      uid: "",
      email: "",
      firstName: "",
      lastName: "",
      dob: null,
      gender: "", 
      phone: "",
      comments: ""
  };

  customerUF: CustomerM = {
      uid: "",
      email: "",
      firstName: "",
      lastName: "",
      dob: null,
      gender: "", 
      phone: "",
      comments: ""
  };
  customerDOB: NgbDateStruct;
  deleteCustomerId: string;
  display = 'none';

  audio = new Audio();

  isAlert: boolean = false;
  alertMessage: string = "";

  @ViewChild('addCustomerForm') addCustomerForm: any;

  constructor(private _firestoreservices: FirestoreServicesService, private _script: ScriptLoaderService) { }

  ngOnInit() {
    this._firestoreservices.getAllCustomers().subscribe(
      (data: CustomerM[]) => {
        this.customersArr = data;
        console.log(this.customersArr);
      } 
    );

    this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
    this.audio.load();
  }

  ngAfterViewInit() {
    
  }

  showAddCustomerModal(){
    this.display = 'block';
    $(".add-customer-modal").addClass("show");
  }

  hideAddCustomerModal(){
    this.display = 'none';
    $(".add-customer-modal").removeClass("show");
  }

  showToastr(msg){
    this.audio.play();
    this.isAlert = true;
    this.alertMessage = msg;
    setTimeout(() => this.isAlert= false, 5000);
  }

  showConfirmDeleteModal(){
    this.display = 'block';
    $(".confirm-delete-modal").addClass("show");
  }

  hideConfirmDeleteModal(){
    this.display = 'none';
    $(".confirm-delete-modal").removeClass("show");
  }

  showUpdateCustomerModal(customer: CustomerM){
     this.customerUF = customer;
     this.display = 'block';
     $(".update-customer-modal").addClass("show");
  }

  hideUpdateCustomerModal(){
    this.display = 'none';
    $(".update-customer-modal").removeClass("show");
  }

  onSubmitCustomer(){
    this.hideAddCustomerModal();
    console.log(this.customerAF);
    var d = new Date();
    console.log(this.customerDOB);
    d.setFullYear(this.customerDOB.year);
    d.setMonth(this.customerDOB.month);
    d.setDate(this.customerDOB.day);

    this.customerAF.dob = d;
    
    console.log(d);
    this._firestoreservices.addCustomer(this.customerAF)
    .then(()=>{
        this.audio.play();
        this.showToastr("Customer Added Succesfully");
    })
    .catch(()=>{
        this.audio.play();
        this.showToastr("Error Adding Customer");
    });

    this.addCustomerForm.reset();
  }

  onUpdateCustomer(){
    this.hideUpdateCustomerModal();
    var d = new Date();
    console.log(this.customerDOB);
    d.setFullYear(this.customerDOB.year);
    d.setMonth(this.customerDOB.month);
    d.setDate(this.customerDOB.day);

    this.customerUF.dob = d;
    this._firestoreservices.updateCustomer(this.customerUF)
    .then(()=>{
        this.audio.play();
        this.showToastr("Customer Updated Succesfully");
    })
    .catch(()=>{
        this.audio.play();
        this.showToastr("Error Updating Customer");
    });
  }

  setDeleteId(customer: CustomerM){
    this.deleteCustomerId = customer.id;
    this.showConfirmDeleteModal();
  }

  deleteCustomer(){
    this.hideConfirmDeleteModal();
    this._firestoreservices.deleteCustomer(this.deleteCustomerId)
    .then(()=>{
        this.audio.play();
        this.showToastr("Customer Deleted Succesfully");
    })
    .catch(()=>{
        this.audio.play();
        this.showToastr("Error Deleting Customer");
    });
  }

  

}
