// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyC-E_PJf3ZaH6MVmdSUBi7tEQCBnw3UgCA",
        authDomain: "meetdax2.firebaseapp.com",
        databaseURL: "https://meetdax2.firebaseio.com",
        projectId: "meetdax2",
        storageBucket: "meetdax2.appspot.com",
        messagingSenderId: "405541611569"
    }
};
