import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { FirestoreServicesService } from '../../../auth/_services/firestore-services.service';
import { EventM } from '../../../auth/_models/eventM';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateService } from '../../../auth/_services/date.service';
import { HttpClient } from '@angular/common/http';

declare let mLayout: any;
const now = new Date();

@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    styleUrls: ["./aside-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    model: NgbDateStruct;
    date: {year: number, month: number, day:number};
    dateT: Date;
    location: any;
    navigation = "arrows";

    weatherProperties : any = {
        'name' : '',
        'sys' : {
            'country' : ''
        },
        'weather' : [
            {
                'description' : '',
                'icon' : ''
            }
        ],
        'main' : {
            'temp' : ''
        }
    };

    eventArr: EventM[] = [];
    constructor(private _firestoreService: FirestoreServicesService, private _dateService: DateService,
         private http: HttpClient) {

    }

    ngOnInit() {
        navigator.geolocation.getCurrentPosition(this.setLocation);
        var apiKey = '6ea35821d2fcd1244eddf8a7467a1b1e';
        var url = 'https://api.openweathermap.org/data/2.5/weather?&units=metric&q=Bangalore';
        this.http.get(url+'&appid='+apiKey).subscribe(
            data =>{ 
                //console.log(data);
                this.weatherProperties = data;
            }
        );
    }

    setLocation(position){
        
    }

    selectToday() {
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        this.onDateChange(this.model);
    }

    selectTomorrow(){
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()+1};
        this.onDateChange(this.model);
    }

    onDateChange(date: NgbDateStruct){
        console.log(date);
        var d = new Date();
        d.setDate(date.day);
        d.setMonth(date.month-1);
        d.setFullYear(date.year);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        console.log(d);

        
        this._firestoreService.getEventsForDate(d).subscribe(
            (data:any) =>{
                console.log(data);
                this.eventArr = data;
                this._dateService.changeEventArray(this.eventArr);
            }
        );  
    }

    ngAfterViewInit() {
        mLayout.initAside();
    }

}