import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirestoreServicesService } from '../../../auth/_services/firestore-services.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Stylist } from '../../../auth/_models/stylist';
import { Observable } from 'rxjs/Observable';

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    styleUrls: ["./header-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    firstName: string;
    lastName: string;
    email: string;
    photoUrl: string;
    authS: any;
    stylistCollection: AngularFirestoreCollection<Stylist>;
    stylists: Observable<Stylist[]>;
    stylistArr: Stylist[] = []; 
    constructor(private af: AngularFireAuth, private afs: AngularFirestore, 
        private _firestoreService: FirestoreServicesService) {

            this.af.authState.subscribe(auth => {
            if(auth) {
                this.authS = auth;  
                /* this.name = auth.displayName;
                this.email = auth.email;
                this.photoUrl = auth.photoURL; */
            }
            });

    }

    ngOnInit() {
        this._firestoreService.getStylistByUID(this.authS.uid).subscribe(
            (data: Stylist[]) => {
              this.stylistArr = data;
              //console.log(this.stylistArr);
              this.photoUrl = this.stylistArr[0].imageUrl;
                this.firstName = this.stylistArr[0].firstName;
                this.lastName = this.stylistArr[0].lastName;
                this.email = this.stylistArr[0].email;
            }
        );
        /*  */
    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }

}