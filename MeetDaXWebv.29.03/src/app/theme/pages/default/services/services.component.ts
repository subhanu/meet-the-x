import { Component, OnInit } from '@angular/core';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { FirestoreServicesService } from '../../../../auth/_services/index';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit {
  servicesArr: ServicesM[] = [];

  constructor(private _firestoreservices: FirestoreServicesService) { }

  ngOnInit() {
    this._firestoreservices.getAllServices().subscribe(
       data => this.servicesArr = data
    );
  }

}
