import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { EventM } from '../../../../auth/_models/eventM';
import { Stylist } from '../../../../auth/_models/stylist';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { CustomerM } from '../../../../auth/_models/customerM';
import { EventFullCal } from '../../../../auth/_models/eventFullCal';
import { EventAddM } from '../../../../auth/_models/eventAddM';
import { Observable } from 'rxjs/Observable';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { DateService } from '../../../../auth/_services/date.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class BlankComponent implements OnInit {
    eventArr:EventM[] = []; 
    allStylistsArr: Stylist[] = []; 
    service: ServicesM; 
    servicesArr:string[] = new Array();
    closeResult: string;
    
    eventToShowArr: EventFullCal[] = [];

    serviceArr: ServicesM[] = [];
    customerArr: CustomerM[] = [];
    services: any = [];
    errorMessage: string = "";
    
    customerTA: any = [];
    message: boolean = false;
    deletionMessage: string;

    audio = new Audio();
    date: Date;

    slotArr: boolean[] = new Array(9);

    priceArr: number[] = new Array(0,0);
    durationArr: number[] = new Array(0,0);

    display = 'none';

    event: EventM = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    };

    eventAdd: EventAddM = {
        id: "",
        eid: "",
        date: "",
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventUpdate: EventAddM = {
        id: "",
        eid: "",
        date: "",
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventToShow: any = {
        id: "",
        eid: "",
        customer: "",
        services: [],
        stylist: "",
        start: "",
        end: "",
        day: "",
        weekDay: "",
        month: "",
        how_often: "",
        recurring: false
    }

    public model: any;

    search = (text$: Observable<string>) =>
       text$
      .debounceTime(200)
      .map(term => term.length < 2 ? []
        : this.customerTA.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
     
    formatter = (x: {name: string}) => x.name; 

    
    constructor(private _firestoreService:FirestoreServicesService, private _dateService: DateService) {
           
        this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
           this.audio.load(); 
                   
   }

    ngOnInit() {
        this._dateService.currentEventArr.subscribe(
            data => {
                this.eventArr = data;
            }
        );
        //console.log(this.eventArr);

        for(var i=0;i<this.eventArr.length;i++){
            this._firestoreService.getCustomerById(this.eventArr[i].customer).subscribe(
                data =>
                this.eventArr[i].customer = data.firstName+" "+data.lastName
            );

            
        }
        

        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;               
            }
        );
        //console.log(this.slotArr);
    }

    //method called when event in fullCalendar is clicked
    eventClick(model: any) {
        this.openDetailModal();
        
        var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
        var weekdays = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"];
                
        var ds = new Date(model.startDate);
        var de = new Date(model.endDate);
        //console.log(ds);

        //console.log(model);
        
        var d = new Date();
        d.setDate(model.startDate.getDate());
        d.setMonth(model.startDate.getMonth());
        d.setFullYear(model.startDate.getFullYear());
        
        model.date = d;
        
        this.eventUpdate = {
            id: model.id,
            eid: model.eid,
            date: model.date,
            startTime: model.startDate,
            endTime: model.endDate,
            how_often: model.how_often,
            customer: "",
            services: model.services,
            stylist: model.stylist,
            recurring: model.recurring,
            price: model.price,
            duration: model.duration
        }; 

        console.log(this.eventUpdate);

        this.event.startDate = model.startDate;
        this.event.endDate = model.endDate;
        this.event.services = model.services;

        //console.log(this.eventUpdate);
        
        //console.log(this.event);
        this.eventToShowArr = [];
        this.eventToShow = {
            id: "",
            eid: "",
            customer: "",
            services: [],
            stylist: "",
            start: "",
            end: "",
            day: "",
            weekDay: "",
            month: "",
            how_often: "",
            recurring: false
        }
        
        for(var j=0;j<model.services.length;j++){
            this._firestoreService.getServiceById(model.services[j]).subscribe(
                (data: ServicesM) => {
                  this.servicesArr[j] = data.name;
                  //console.log(this.servicesArr[j]+"++++"+data.name);
                  this.eventToShow.services.push(data.name);
                }
            );
        }
        
        this._firestoreService.getStylistById(model.stylist).subscribe(
            (data) => {
              var stylist_name = data.firstName+" "+data.lastName;  
              //console.log(stylist_name);
              this.eventToShow.stylist = stylist_name;
            }
        );

        this._firestoreService.getCustomerById(model.customer).subscribe(
            (data) => {
              var customer_name = data.firstName+" "+data.lastName;  
              //console.log(customer_name);
              this.eventToShow.customer = customer_name;
              
            }
        );
        //console.log(ds);   
        this.eventToShow.id = model.id;     
        this.eventToShow.start = this.getMin(ds.getHours())+" : "+this.getMin(ds.getMinutes());
        this.eventToShow.end = this.getMin(de.getHours())+" : "+this.getMin(de.getMinutes());
        this.eventToShow.day = ds.getDate();
        this.eventToShow.weekDay = weekdays[de.getDay()];
        this.eventToShow.month = months[ds.getMonth()];
        this.eventToShow.how_often = model.how_often;
        //console.log(this.eventToShow);
        this.eventToShowArr.push( this.eventToShow);
        
        console.log(this.eventToShowArr);
    }//event click ends

    //method called when empty space is clicked, for adding event 
    addEventClick(){
        console.log("Clicked");
        //this.openModal();
        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
              this.customerArr = data;
            }
        );

        //console.log(this.customerArr);
        for(var i=0;i< this.customerArr.length;i++){
            var custTA = {
                name : this.customerArr[i].firstName +" " +this.customerArr[i].lastName,
                email : this.customerArr[i].email
            }
            //console.log(custTA);
            this.customerTA[i] = custTA;
        }
        //console.log(this.customerTA);  

        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;
            }
        );

        //console.log(this.allStylistsArr);
        this.openAddModal();
    }

    onUpdateSubmit(){
       //console.log(this.eventUpdate.services);
       //console.log(this.event);
       this.event.id = this.eventUpdate.id;
       this.event.recurring = false;
       
       //console.log(this.event.startDate);
       //console.log(this.event.endDate);

       this.event.stylist = this.eventUpdate.stylist;
       this.event.services = this.eventUpdate.services;
       this.event.price = this.eventUpdate.price;
       this.event.duration = this.eventUpdate.duration;
       this.event.customer = this.eventUpdate.customer;

       
       for(var i=0; i<this.customerArr.length; i++){
           if(this.customerArr[i].email == this.event.customer){
               this.event.customer = this.customerArr[i].id;
           }
       }    

       console.log(this.event);
       this._firestoreService.updateEvent(this.event);
              
       //this.event.services = [];
       this.event = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,  
        price: 0,
        duration: 0
        };

        this.closeEditModal();        
        this.showToastr("Booking Updated Successfully");
    }

    //method for event deletion
    deleteEvent(){
        //console.log("Event to be deleted: "+this.eventToShow.id);
        this._firestoreService.deleteEventFromCollection(this.eventToShow.id);
        this.closeEditModal();
         
        this.showToastr("Deletion Done Successfully"); 
        /* console.log(response);
        if(response == 'success'){
            this.closeEditModal();
            audio.play(); 
            this.showToastr("Deletion Done Successfully"); 
        }
        else{
            this.closeEditModal();
            audio.play(); 
            this.showToastr("Deletion Error");
        }   */  
    }

    onSelectStylist(id:string){
        //console.log(id);
        this.serviceArr = []; 
        this.event.services[0] = "";
        this.event.services[1] = "";
        this.eventAdd.price = 0;
        this.eventAdd.duration = 0;
        this.eventUpdate.price = 0;
        this.eventUpdate.duration = 0;
        this.eventUpdate.services = [];

        for(var i=0;i<this.allStylistsArr.length;i++){
            if(id == this.allStylistsArr[i].id){
                this.services = this.allStylistsArr[i].services;
            }
        }
        
        var k = 0;
        
        for(var j=0 ; j<this.services.length ; j++){
            this._firestoreService.getServiceById(this.services[j]).subscribe(
                (data : ServicesM) => {
                  //console.log("Service: "+this.services[k]+", Price: "+data.price);
                  
                  data.id = this.services[k];
                  this.serviceArr.push(data);
                  k++;
                }
            );
        }
        //console.log(this.serviceArr);
    }

    onSelectServiceOne(id){
        if(id == '0'){
             this.priceArr[0] = 0;
             console.log(this.priceArr[0]+"/"+this.priceArr[1]);
             this.durationArr[0] = 0;
             this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
             this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
             this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
             this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
        }

        if(this.eventUpdate.services[0] != id && this.eventUpdate.services[1] != id){
            this.eventUpdate.services[0] = id;
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 console.log(this.serviceArr[i].price);
     
                 this.priceArr[0] = this.serviceArr[i].price;
                 console.log(this.priceArr[0]);
                 this.durationArr[0] = this.serviceArr[i].duration;
                 this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }
            } 
         }
        
        
        //console.log(this.eventAdd.services+"/"+this.eventAdd.price);
    }

    onSelectServiceTwo(id){
        if(id == '0'){
          this.priceArr[1] = 0;
           console.log(this.priceArr[0]);
           this.durationArr[1] = 0;
           this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
           this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
           this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
           this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
        }

        if(this.eventUpdate.services[1] != id && this.eventUpdate.services[0] != id){
            this.eventUpdate.services[1] = id;
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 console.log(this.serviceArr[i].price);
     
                 this.priceArr[1] = this.serviceArr[i].price;
                 console.log(this.priceArr[1]);
                 this.durationArr[1] = this.serviceArr[i].duration;
                 this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }

            }
             
         }
    }

    onSelectTime(value){
        this.eventAdd.startTime = value;
        this.eventUpdate.startTime = value;

        this.event.startDate = this.formatStartDate(this.eventUpdate.date, this.eventUpdate.startTime);
        this.event.endDate = this.formatEndDate(this.eventUpdate.date, this.eventUpdate.duration, this.eventUpdate.startTime);
        //console.log(this.eventUpdate.startTime);
    }

    onSubmit(){
        //console.log(this.eventAdd);
        
        this.event.startDate = this.formatStartDate(this.eventAdd.date, this.eventAdd.startTime);
        this.event.endDate = this.formatEndDate(this.eventAdd.date, this.eventAdd.duration, this.eventAdd.startTime);
        //console.log(this.event.startDate);
        //console.log(this.event.endDate);

        this.event.stylist = this.eventAdd.stylist;
        this.event.services = this.eventAdd.services;
        this.event.price = this.eventAdd.price;
        this.event.duration = this.eventAdd.duration;
        this.event.customer = this.eventAdd.customer;
        
        for(var i=0; i<this.customerArr.length; i++){
            if(this.customerArr[i].email == this.event.customer){
                this.event.customer = this.customerArr[i].id;
            }
        }        

        this._firestoreService.addEvent(this.event);

        this.eventAdd = {
            id: "",
            eid: "",
            date: "",
            startTime: "",
            endTime: "",
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0  
        }

        this.event.services = [];
        this.event = {
                id: "",
                eid: "",
                startDate: null,
                endDate: null,
                customer: "",
                stylist: "",
                services: [],
                how_often: "",
                recurring: false,  
                price: 0,
                duration: 0
        };
        this.closeAddModal();
    }

    formatStartDate(date:string, startTime:string){
        //console.log(date+" "+startTime);
        var start: Date = new Date(date);
        start.setHours(parseInt(startTime));
        start.setMinutes(0);

        return start;
    }

    formatEndDate(date:string, duration: number, startTime: string){
        //console.log(date+" "+duration+" "+startTime);
        var hrs = parseInt(startTime)+(duration/60);
        var min = duration%60;

        var end = new Date(date);
        end.setHours(hrs);
        end.setMinutes(min);

        return end;
    }

    showToastr(msg){
        this.audio.play();
        this.message = true;
        this.deletionMessage = msg;
        setTimeout(() => this.message = false, 5000);
    }

    openEditModal(){
        this.closeDetailModal();
        this.display = "block";
        $('#edit_modal').addClass('show');  
    }

    closeEditModal(){
        //this.closeDetailModal();
        this.display = 'none';
        $('#edit_modal').removeClass('show');
        this.eventUpdate = {
            id: "",
            eid: "",
            date: null,
            startTime: null,
            endTime: null,
            how_often: "",
            customer: "",
            services: null,
            stylist: "",
            recurring: false,
            price: 0,
            duration: 0
        }; 
        console.log(this.eventUpdate);
    }

    openDetailModal(){
        this.display = 'block';
        $('#detail_modal').addClass('show');
    }

    closeDetailModal(){
        this.display = 'none';
        $('#detail_modal').removeClass('show');
    }

    closeAddModal(){
        this.display = 'none';
        $('#add_event_modal').removeClass('show');
    }

    openAddModal(){
        this.display = 'block';
        $('#add_event_modal').addClass('show');
    }

    getMin(min){
        if(min<10)
           return '0'+min;
        else
           return min;   
    }

    onSelectDate(){
        //console.log(this.eventAdd.date+"/"+this.eventAdd.stylist);
        this.slotArr = [false,false,false,false,false,false,false,false,false];
        var d = new Date(this.eventAdd.date);
        d.setMinutes(0);
        
        for(var i=0;i<9;i++){
            d.setHours(i+8);
            //console.log(d);
            this.getSlot(i+8,d);
        }
        
        
        /* d.setHours(16);
        this._firestoreService.getEventsForStylistDate(d, this.eventAdd.stylist).subscribe(
            (data) => {
                if(data){
                    this.slotArr[16] = true;
                }
                  
                else
                  console.log("Slot Available"+ data);  
            }
        ); */

        //console.log(this.slotArr);
    }

    getSlot(i:number ,d:Date){
        //console.log(i+8+" "+d);
        this._firestoreService.getEventsForStylistDate(d, this.eventAdd.stylist).subscribe(
            (data) => {
                if(data != null){
                  this.slotArr[i-8] = true; 
                  
                  //console.log("True");
                }
                
            }
        );
    }

    changeColor(color, event){
         $(event.path[12]).css('background-color', color); 
         console.log(event);
    }
}