import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EventM } from '../_models/eventM';
import { FirestoreServicesService } from './index';

@Injectable()
export class DateService {
  private eventArrSource = new BehaviorSubject<EventM[]>(null);
  currentEventArr = this.eventArrSource.asObservable();

  eventArr:EventM[] = []; 

  constructor(private _firestoreservices: FirestoreServicesService) { 
    var d = new Date();
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    _firestoreservices.getEventsForDate(d).subscribe(
      data => {
        this.eventArr = data
        //console.log(this.eventArr);
        this.eventArrSource.next(this.eventArr);
      }
    ) 
  }

  changeEventArray(eventArr: EventM[]){
    console.log(eventArr);
    this.eventArrSource.next(eventArr);
  }

  

}
