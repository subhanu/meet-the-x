export interface EventM{
    id? : string;
    eid: string;
    customer: string;
    services: string[];
    stylist: string;
    startDate: any;
    endDate: any;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
}