export interface CustomerM{
    id?: string;
    uid: string;
    email: string;
    firstName: string;
    lastName: string;
    age: number;
    gender: string; 
    phone: string;
}