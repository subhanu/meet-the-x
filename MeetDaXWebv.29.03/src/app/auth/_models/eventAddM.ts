export interface EventAddM{
    id? : string;
    eid: string;
    customer: string;
    services: string[];
    stylist: string;
    date: string;
    startTime: string;
    endTime: string;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
}