import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { FirestoreServicesService } from '../../../auth/_services/firestore-services.service';
import { EventM } from '../../../auth/_models/eventM';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateService } from '../../../auth/_services/date.service';
import { HttpClient } from '@angular/common/http';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { BlankComponent } from '../../pages/default/blank/blank.component';
import { EventC } from '../../../auth/_models/eventC';
import { ServicesM } from '../../../auth/_models/index';

declare let mLayout: any;
const now = new Date();

@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    styleUrls: ["./aside-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    model: NgbDateStruct;
    date: {year: number, month: number, day:number};
    dateT: Date;
    location: any;
    navigation = "arrows";

    weatherProperties : any = {
        'name' : '',
        'sys' : {
            'country' : ''
        },
        'weather' : [
            {
                'description' : '',
                'icon' : ''
            }
        ],
        'main' : {
            'temp' : ''
        }
    };

    servicesArr: string[] = [];

    eventArr: EventC[] = [];
    /* eventArr: any = {
        c_name: "",
        customer: "",
        services: "",
        stylist: "",
        startDate: null,
        endDate: null,
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0
    } */

    
    constructor(private _firestoreService: FirestoreServicesService, private _dateService: DateService,
         private http: HttpClient, private _script: ScriptLoaderService) {

    }

    ngOnInit() {
        //navigator.geolocation.getCurrentPosition(this.setLocation);
        var apiKey = '6ea35821d2fcd1244eddf8a7467a1b1e';
        var url = 'https://api.openweathermap.org/data/2.5/weather?&units=metric&q=Bangalore';
        this.http.get(url+'&appid='+apiKey).subscribe(
            data =>{ 
                //console.log(data);
                this.weatherProperties = data;
            }
        );
    }

    setLocation(position){
        
    }

    selectToday() {
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        this.onDateChange(this.model);
    }

    selectTomorrow(){
        this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()+1};
        this.onDateChange(this.model);
    }

    onDateChange(date: NgbDateStruct){
        //console.log(date);
        var d = new Date();
        
        //console.log(d);
        d.setMonth(date.month-1);
        d.setDate(date.day);
        d.setFullYear(date.year);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        //console.log(d);

        //var i = 0;
        this._firestoreService.getEventsForDate(d).subscribe(
            (data:any) =>{
                //console.log(data);
                this.eventArr = data;
                
                //i++;
                for(var i=0; i<this.eventArr.length;i++){
                    //console.log(this.eventArr[i].customer);
                    this.saveCustomerName(i, this.eventArr[i].customer, this.eventArr[i].services);

                }
                console.log(this.eventArr);
                this._dateService.changeEventArray(this.eventArr);
            }
        );
        
        
    }

    saveCustomerName(i: number, cId: string, services: string[]){
        this._firestoreService.getCustomerById(cId).subscribe(
            (data1)=> {
                //console.log(data1[0].firstName+" "+data1[0].lastName);
                this.eventArr[i].c_name = data1[0].firstName+" "+data1[0].lastName;
            }
        );

        console.log(services.length+" "+services);
        var arr : string[] = [];
        var snameIndex: number = 0;
        for(var j=0;j<services.length;j++){
            console.log("j:"+j);
            /* this._firestoreService.getServiceById(services[j]).subscribe(
                (data: ServicesM) => {
                    console.log("j1:"+j);
                arr[snameIndex] = data.name;

                console.log(arr[snameIndex]);
                console.log(snameIndex+"/"+data.name);
                //this.eventArr[i].snames[j] = data.name;
                }
            ); */
            snameIndex++;
            console.log("snameIndex: "+snameIndex);
        }

        this.eventArr[i].snames = arr;
        console.log(this.eventArr[i].snames);
        console.log("i: "+i+"sevices names: "+this.eventArr[i].snames);
    }

    

    ngAfterViewInit() {
        mLayout.initAside();
        
    }

}