import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { FirestoreServicesService } from '../../../../auth/_services/index';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit, AfterViewInit {
  servicesArr: ServicesM[] = [];

  constructor(private _firestoreservices: FirestoreServicesService, private _script: ScriptLoaderService) { }

  ngOnInit() {
    this._firestoreservices.getAllServices().subscribe(
       data => this.servicesArr = data
    );
  }

  ngAfterViewInit(){
    this._script.loadScripts('app-services',
    ['assets/demo/default/custom/components/forms/validation/form-controls.js','assets/demo/default/custom/components/base/sweetalert2.js']);
  }

  open(){
    $('#m_form_2_msg').removeClass('m--hide');
  }

}
