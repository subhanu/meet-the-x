import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { EventM } from '../../../../auth/_models/eventM';
import { Stylist } from '../../../../auth/_models/stylist';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { CustomerM } from '../../../../auth/_models/customerM';
import { EventFullCal } from '../../../../auth/_models/eventFullCal';
import { EventAddM } from '../../../../auth/_models/eventAddM';
import { Observable } from 'rxjs/Observable';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { DateService } from '../../../../auth/_services/date.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { EventC } from '../../../../auth/_models/index';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';



const now = new Date();
@Component({
    selector: 'app-blank',
    templateUrl: './blank.component.html',
    styleUrls: ['./blank.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class BlankComponent implements OnInit, AfterViewInit {
    eventArr:EventC[] = []; 
    allStylistsArr: Stylist[] = []; 
    service: ServicesM; 
    servicesArr:string[] = new Array();
    closeResult: string;
    
    eventToShowArr: EventFullCal[] = [];

    serviceArr: ServicesM[] = [];
    //serviceArr1: any[] = [];
    customerArr: CustomerM[] = [];
    services: any = [];
    errorMessage: string = "";
    
    customerTA: any = [];
    message: boolean = false;
    deletionMessage: string;

    audio = new Audio();
    date: Date;

    slotArr: boolean[] = new Array(9);

    priceArr: number[] = new Array(0,0);
    durationArr: number[] = new Array(0,0);

    display = 'none';

    minDate: NgbDateStruct = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()}

    randomArr: any = [
        "One","Two","Three","Four","Five"
    ];

    event: EventM = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    };

    eventU: EventM = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    };

    eventAdd: EventAddM = {
        id: "",
        eid: "",
        date: null,
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventUpdate: EventAddM = {
        id: "",
        eid: "",
        date: null,
        startTime: "",
        endTime: "",
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,
        price: 0,
        duration: 0 
    }

    eventToShow: any = {
        id: "",
        eid: "",
        customer: "",
        services: [],
        stylist: "",
        start: "",
        end: "",
        day: "",
        weekDay: "",
        month: "",
        how_often: "",
        recurring: false
    }

    public model: any;

    search = (text$: Observable<string>) =>
       text$
      .debounceTime(200)
      .map(term => term.length < 2 ? []
        : this.customerTA.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
     
    formatter = (x: {name: string}) => x.name; 

    
    constructor(private _firestoreService:FirestoreServicesService, private _dateService: DateService
    ,private _script: ScriptLoaderService   ) {
           
        this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
           this.audio.load(); 
                   
   }

   

    ngOnInit() {
        //$('#datetimepicker').datetimepicker({  minDate:new Date()});
        var i = 0;
        this._dateService.currentEventArr.subscribe(
            (data) => {
                this.eventArr = data;
                //console.log(data.length);
                
                /* if(this.eventArr==null){
                    this._firestoreService.getCustomerById(this.eventArr[i].customer).subscribe(
                        data =>
                        this.eventArr[i].c_name = data[i].firstName+" "+data[i].lastName
                    ); 
                    
                    console.log(this.eventArr[i]);
                    i++;
                }  */
                
                }    
                
                /* if(this.eventArr.length>0){
                    console.log(this.eventArr.length);
                } */

                //console.log(this.eventArr.length);
                
            
            
        );

        console.log(this.eventArr);
    

        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;               
            }
        );

        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
              this.customerArr = data;
            }
        );
        //console.log(this.slotArr);
    }

    

    ngAfterViewInit() {
        this._script.loadScripts('app-blank',
            ['assets/demo/default/custom/components/forms/widgets/bootstrap-select.js',
            'assets/demo/default/custom/components/forms/validation/form-controls.js']);

    }

    

    getPosition(startDate){
          var hours = startDate.getHours();
          
          switch(hours){
              case 8: return '0px';
              case 9: return '65px';
              case 10: return '130px';
              case 11: return '195px';
              case 12: return '260px';
              case 13: return '325px';
              case 14: return '390px';
              case 15: return '455px';
              case 16: return '520px';
          }
    }

    //method called when event in fullCalendar is clicked
    eventClick(model: any) {
        this.openDetailModal();
        
        var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
        var weekdays = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"];
                
        var ds = new Date(model.startDate);
        var de = new Date(model.endDate);
        //console.log(ds);

        //console.log(model);
        
        var d = new Date();
        d.setDate(model.startDate.getDate());
        d.setMonth(model.startDate.getMonth());
        d.setFullYear(model.startDate.getFullYear());
        
        model.date = d;
        
        this.eventUpdate = {
            id: model.id,
            eid: model.eid,
            date: model.date,
            startTime: model.startDate,
            endTime: model.endDate,
            how_often: model.how_often,
            customer: "",
            services: model.services,
            stylist: model.stylist,
            recurring: model.recurring,
            price: model.price,
            duration: model.duration
        }; 

        console.log(this.eventUpdate);

        this.event.startDate = model.startDate;
        this.event.endDate = model.endDate;
        this.event.services = model.services;

        //console.log(this.eventUpdate);
        
        //console.log(this.event);
        this.eventToShowArr = [];
        this.eventToShow = {
            id: "",
            eid: "",
            customer: "",
            services: [],
            stylist: "",
            start: "",
            end: "",
            day: "",
            weekDay: "",
            month: "",
            how_often: "",
            recurring: false
        }
        
        for(var j=0;j<model.services.length;j++){
            this._firestoreService.getServiceById(model.services[j]).subscribe(
                (data: ServicesM) => {
                  this.servicesArr[j] = data.name;
                  //console.log(this.servicesArr[j]+"++++"+data.name);
                  this.eventToShow.services.push(data.name);
                }
            );
        }
        
        this._firestoreService.getStylistById(model.stylist).subscribe(
            (data) => {
              var stylist_name = data.firstName+" "+data.lastName;  
              //console.log(stylist_name);
              this.eventToShow.stylist = stylist_name;
            }
        );

        this._firestoreService.getCustomerById(model.customer).subscribe(
            (data) => {
              var customer_name = data[0].firstName+" "+data[0].lastName;  
              //console.log(customer_name);
              this.eventToShow.customer = customer_name;
              
            }
        );
        //console.log(ds);   
        this.eventToShow.id = model.id;     
        this.eventToShow.start = this.getMin(ds.getHours())+" : "+this.getMin(ds.getMinutes());
        this.eventToShow.end = this.getMin(de.getHours())+" : "+this.getMin(de.getMinutes());
        this.eventToShow.day = ds.getDate();
        this.eventToShow.weekDay = weekdays[de.getDay()];
        this.eventToShow.month = months[ds.getMonth()];
        this.eventToShow.how_often = model.how_often;
        //console.log(this.eventToShow);
        this.eventToShowArr.push( this.eventToShow);
        
        console.log(this.eventToShowArr);
    }//event click ends

    //method called when empty space is clicked, for adding event 
    addEventClick(){
        console.log("Clicked");
        //this.openModal();
        this._firestoreService.getAllCustomers().subscribe(
            (data) => {
              this.customerArr = data;
            }
        );

        console.log(this.customerArr);
        for(var i=0;i< this.customerArr.length;i++){
            var custTA = {
                name : this.customerArr[i].firstName +" " +this.customerArr[i].lastName,
                email : this.customerArr[i].email
            }
            //console.log(custTA);
            this.customerTA[i] = custTA;
        }
        //console.log(this.customerTA);  

        this._firestoreService.getAllStylists().subscribe(
            (data) => {
               this.allStylistsArr = data;
            }
        );

        //console.log(this.allStylistsArr);
        this.openAddModal();
    }

    onUpdateSubmit(){
       //console.log(this.eventUpdate.services);
       //console.log(this.event);
       this.eventU.id = this.eventUpdate.id;
       this.eventU.recurring = false;
       
       //console.log(this.event.startDate);
       //console.log(this.event.endDate);
       console.log(this.customerArr);
       this.eventU.stylist = this.eventUpdate.stylist;
       this.eventU.services = this.eventUpdate.services;
       this.eventU.price = this.eventUpdate.price;
       this.eventU.duration = this.eventUpdate.duration;
       this.eventU.customer = this.eventUpdate.customer;

       
       for(var i=0; i<this.customerArr.length; i++){
           if(this.customerArr[i].email == this.eventU.customer){
               this.eventU.customer = this.customerArr[i].uid;
           }
       }    

       console.log(this.eventU);
       this._firestoreService.updateEvent(this.eventU);
              
       //this.event.services = [];
       this.eventU = {
        id: "",
        eid: "",
        startDate: null,
        endDate: null,
        customer: "",
        stylist: "",
        services: [],
        how_often: "",
        recurring: false,  
        price: 0,
        duration: 0
        };

        this.closeEditModal();        
        this.showToastr("Booking Updated Successfully");
    }

    //method for event deletion
    deleteEvent(){
        //console.log("Event to be deleted: "+this.eventToShow.id);
        this._firestoreService.deleteEventFromCollection(this.eventToShow.id);
        this.closeEditModal();
         
        this.showToastr("Deletion Done Successfully"); 
        /* console.log(response);
        if(response == 'success'){
            this.closeEditModal();
            audio.play(); 
            this.showToastr("Deletion Done Successfully"); 
        }
        else{
            this.closeEditModal();
            audio.play(); 
            this.showToastr("Deletion Error");
        }   */  
    }

    onSelectStylist(id:string){
        //console.log(id);
        this.serviceArr = []; 
        this.event.services[0] = "";
        this.event.services[1] = "";
        this.eventAdd.price = 0;
        this.eventAdd.duration = 0;
        this.eventUpdate.price = 0;
        this.eventUpdate.duration = 0;
        this.eventUpdate.services = [];

        for(var i=0;i<this.allStylistsArr.length;i++){
            if(id == this.allStylistsArr[i].id){
                this.services = this.allStylistsArr[i].services;
            }
        }
        
        var k = 0;
        
        for(var j=0 ; j<this.services.length ; j++){
            this._firestoreService.getServiceById(this.services[j]).subscribe(
                (data : ServicesM) => {
                  //console.log("Service: "+this.services[k]+", Price: "+data.price);
                  
                  data.id = this.services[k];
                  this.serviceArr.push(data);
                  k++;
                }
            );
        }

        console.log(this.serviceArr);
    }

    onSelectServiceOne(id){
        if(id == '0'){
            this.eventUpdate.services.splice(0,1);
             this.priceArr[0] = 0;
             console.log(this.priceArr[0]+"/"+this.priceArr[1]);
             this.durationArr[0] = 0;
             this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
             this.eventAdd.duration = this.durationArr[0] + this.durationArr[1];
             this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
             this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
        }

        if(this.eventUpdate.services[0] != id && this.eventUpdate.services[1] != id){
            this.eventUpdate.services[0] = id;
            this.eventAdd.services[0] = id;
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 console.log(this.serviceArr[i].price);
     
                 this.priceArr[0] = this.serviceArr[i].price;
                 console.log(this.priceArr[0]);
                 this.durationArr[0] = this.serviceArr[i].duration;
                 this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }
            } 
         }
        
        
        //console.log(this.eventAdd.services+"/"+this.eventAdd.price);
    }

    onSelectServiceTwo(id){
        if(id == '0'){
           this.eventUpdate.services.splice(1,1);
           this.priceArr[1] = 0;
           console.log(this.priceArr[0]);
           this.durationArr[1] = 0;
           this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
           this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
           this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
           this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
        }

        if(this.eventUpdate.services[1] != id && this.eventUpdate.services[0] != id){
            this.eventUpdate.services[1] = id;
            this.eventAdd.services[1] = id;
            for(var i=0;i<this.serviceArr.length;i++){
              if(id == this.serviceArr[i].id){
                 console.log(this.serviceArr[i].price);
     
                 this.priceArr[1] = this.serviceArr[i].price;
                 console.log(this.priceArr[1]);
                 this.durationArr[1] = this.serviceArr[i].duration;
                 this.eventAdd.price = this.priceArr[0] + this.priceArr[1];
                 this.eventAdd.duration= this.durationArr[0] + this.durationArr[1];
                 this.eventUpdate.price = this.priceArr[0] + this.priceArr[1];
                 this.eventUpdate.duration= this.durationArr[0] + this.durationArr[1];
              }
            }
         }
    }

    onSelectTime(value){
        this.eventAdd.startTime = value;
        

        
        
    }

    onSelectTimeU(value){
        this.eventUpdate.startTime = value;

        var d = new Date();
        d.setFullYear(this.eventUpdate.date.year);
        d.setMonth(this.eventUpdate.date.month-1);
        d.setDate(this.eventUpdate.date.day);

        this.eventU.startDate = new Date();
        this.eventU.startDate.setHours(parseInt(this.eventUpdate.startTime));
        this.eventU.startDate.setMinutes(0);

        this.eventU.endDate = this.formatEndDate(d, this.eventUpdate.duration, this.eventUpdate.startTime);
    }

    onSubmit(){
        console.log(this.eventAdd.date);
        
        var d = new Date();
        d.setFullYear(this.eventAdd.date.year);
        d.setMonth(this.eventAdd.date.month-1);
        d.setDate(this.eventAdd.date.day);
        //d.setFullYear(this.eventAdd.date.year);
        //d.setFullYear(this.eventAdd.date.year);
        //this.formatStartDate(d, this.eventAdd.startTime);

        this.event.startDate = new Date();
        this.event.startDate.setHours(parseInt(this.eventAdd.startTime));
        this.event.startDate.setMinutes(0);
        //console.log(start);
    
        

        this.event.endDate = this.formatEndDate(d, this.eventAdd.duration, this.eventAdd.startTime);
        
        console.log(this.event.endDate);
        console.log(this.event.startDate);

        this.event.stylist = this.eventAdd.stylist;
        this.event.services = this.eventAdd.services;
        this.event.price = this.eventAdd.price;
        this.event.duration = this.eventAdd.duration;
        this.event.customer = this.eventAdd.customer;
        
        for(var i=0; i<this.customerArr.length; i++){
            if(this.customerArr[i].email == this.event.customer){
                this.event.customer = this.customerArr[i].uid;
            }
        }        


        this._firestoreService.addEvent(this.event);

        this.eventAdd = {
            id: "",
            eid: "",
            date: null,
            startTime: "",
            endTime: "",
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0  
        }

        this.event.services = [];
        this.event = {
                id: "",
                eid: "",
                startDate: null,
                endDate: null,
                customer: "",
                stylist: "",
                services: [],
                how_often: "",
                recurring: false,  
                price: 0,
                duration: 0
        };
        this.closeAddModal();
    }

    formatStartDate(start: Date,startTime:string){
        //console.log(start+" "+startTime);
        //var start: Date = new Date(date);
        start.setHours(parseInt(startTime));
        start.setMinutes(0);
        
        console.log(this.event.startDate);
        return start;
    }

    formatEndDate(end:Date, duration: number, startTime: string){
        //console.log(end+" "+duration+" "+startTime);
        var hrs = parseInt(startTime)+(duration/60);
        var min = duration%60;

        //var end = new Date(date);
        end.setHours(hrs);
        end.setMinutes(min);
        //console.log(end);

        console.log(this.event.endDate);
        return end;
    }

    showToastr(msg){
        this.audio.play();
        this.message = true;
        this.deletionMessage = msg;
        setTimeout(() => this.message = false, 5000);
    }

    openEditModal(){
        this.closeDetailModal();
        this.display = "block";
        $('#edit_modal').addClass('show');  
    }

    closeEditModal(){
        //this.closeDetailModal();
        this.display = 'none';
        $('#edit_modal').removeClass('show');
        this.eventUpdate = {
            id: "",
            eid: "",
            date: null,
            startTime: null,
            endTime: null,
            how_often: "",
            customer: "",
            services: null,
            stylist: "",
            recurring: false,
            price: 0,
            duration: 0
        }; 
        console.log(this.eventUpdate);
    }

    openDetailModal(){
        this.display = 'block';
        $('#detail_modal').addClass('show');
    }

    closeDetailModal(){
        this.display = 'none';
        $('#detail_modal').removeClass('show');
    }

    closeAddModal(){
        this.display = 'none';
        $('#add_event_modal').removeClass('show');
        this.eventAdd = {
            id: "",
            eid: "",
            date: null,
            startTime: "",
            endTime: "",
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,
            price: 0,
            duration: 0  
        };

        this.event = {
            id: "",
            eid: "",
            startDate: null,
            endDate: null,
            customer: "",
            stylist: "",
            services: [],
            how_often: "",
            recurring: false,  
            price: 0,
            duration: 0
        };
    }

    openAddModal(){
        this.display = 'block';
        $('#add_event_modal').addClass('show');
    }

    getMin(min){
        if(min<10)
           return '0'+min;
        else
           return min;   
    }

    /* onSelectDate(){
        //console.log(this.eventAdd.date+"/"+this.eventAdd.stylist);
        this.slotArr = [false,false,false,false,false,false,false,false,false];
        var d = new Date();
        d.set
        d.setMinutes(0);
        
        for(var i=0;i<9;i++){
            d.setHours(i+8);
            //console.log(d);
            this.getSlot(i+8,d);
        } */
        
        
        /* d.setHours(16);
        this._firestoreService.getEventsForStylistDate(d, this.eventAdd.stylist).subscribe(
            (data) => {
                if(data){
                    this.slotArr[16] = true;
                }
                  
                else
                  console.log("Slot Available"+ data);  
            }
        ); */

        //console.log(this.slotArr);
    //}

    getSlot(i:number ,d:Date){
        //console.log(i+8+" "+d);
        this._firestoreService.getEventsForStylistDate(d, this.eventAdd.stylist).subscribe(
            (data) => {
                if(data != null){
                  this.slotArr[i-8] = true; 
                  
                  //console.log("True");
                }
                
            }
        );
    }

    changeColor(color, event){
         $(event.path[12]).css('background-color', color); 
         console.log(event);
    }

    
}