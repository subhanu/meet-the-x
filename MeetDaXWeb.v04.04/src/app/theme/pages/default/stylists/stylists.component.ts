import { Component, OnInit } from '@angular/core';
import { Stylist } from '../../../../auth/_models/stylist';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';

@Component({
  selector: 'app-stylists',
  templateUrl: './stylists.component.html',
  styleUrls: ['stylists.component.css']
})
export class StylistsComponent implements OnInit {
  stylistArr: Stylist[] = [];
  constructor(private _firestoreService: FirestoreServicesService) { }
  

  ngOnInit() {
    this._firestoreService.getAllStylists().subscribe(
      (user: Stylist[]) => {
        this.stylistArr = user;
        console.log(this.stylistArr);
      }
    );
  }

}
