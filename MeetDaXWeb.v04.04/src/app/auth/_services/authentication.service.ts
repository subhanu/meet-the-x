import { Injectable, ComponentFactoryResolver } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/operator/map";

import { AngularFirestoreCollection, AngularFirestore } from "angularfire2/firestore";
import { AngularFireAuth } from "angularfire2/auth";
import { AlertService } from "./alert.service";
import { Router } from "@angular/router";
import { FirestoreServicesService } from "./firestore-services.service";
import { Stylist } from "../_models/index";

@Injectable()
export class AuthenticationService {
    userData: Stylist = {
        uid: '',
        imageFile: null,
        imageUrl: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        mobile: '',
        gender: '',
        age: 0,
        services: []
    };
    loading = false;
    authS: any = null;
    userExist: boolean = false;
    stylistArr: Stylist[] = [];
    
    constructor(private afAuth: AngularFireAuth,
        private router:Router,private _alertService: AlertService,
        private cfr: ComponentFactoryResolver,private afs: AngularFirestore,
        private _firestoreServices: FirestoreServicesService) {
 
            this.afAuth.authState.subscribe((auth) => {
              this.authS = auth
            });

            this._firestoreServices.getAllStylists().subscribe(
                (data)=>{
                    this.stylistArr = data;
                }
            );
    }

    socialSignIn(provider) {
        return this.afAuth.auth.signInWithPopup(provider)
          .then((credential) =>  {
              this.authS = credential.user;
              this.userData.firstName = credential.user.displayName;
              this.userData.email = credential.user.email;
              this.userData.uid = credential.user.uid;
              this.userData.imageUrl = credential.user.photoURL;

              for(var i=0; i<this.stylistArr.length;i++){
                  if(this.stylistArr[i].uid == this.userData.uid){
                     this.userExist = true;
                     break;
                  }
              }

              if(this.userExist){
                this.router.navigate(['/index']);  
              }
              else{ 
                this._firestoreServices.addStylistToCollection(this.userData);
                this.router.navigate(['/index']); 
              }
              
          })
          .catch(error => console.log(error));
    }

    logout(){
        this.afAuth.auth.signOut();
        this.router.navigate(['/login']);
    }
}