export interface EventC{
    id? : string;
    eid: string;
    customer: string;
    c_name: string;
    services: string[];
    snames: string[];
    stylist: string;
    startDate: any;
    endDate: any;
    how_often: string;
    recurring: boolean;
    price: number;
    duration: number;
}