export interface ServicesM{
    id? :string;
    name: string;
    price: number;
    description: string;
    stylists: string;
    duration: number;
}