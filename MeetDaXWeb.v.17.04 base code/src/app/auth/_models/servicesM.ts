export interface ServicesM{
    id? :string;
    name: string;
    price: number;
    description: string;
    stylist: string;
    duration: number;
}