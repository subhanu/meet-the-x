export interface Stylist{
    id?: string;
    uid: string;
    imageFile: File;
    imageUrl: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    mobile: string;
    gender: string;
    age: number;
    services: string[],
    serviceFor: string[],
    color: string
}