export interface EventFullCal{
    id?: string;
    eid: string;
    start: Date;
    end: Date;
    customer: string;
    recurring: boolean;
    services: string[];
    stylist: string;
    how_often: string;
}