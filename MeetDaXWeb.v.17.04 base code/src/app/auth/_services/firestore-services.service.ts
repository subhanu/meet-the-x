import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Stylist } from '../_models/stylist';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { EventM } from '../_models/eventM';
import { ServicesM } from '../_models/servicesM';
import { CustomerM } from '../_models/customerM';
import { Services } from '@angular/core/src/view';
import { EventC } from '../_models/eventC';

@Injectable()
export class FirestoreServicesService {
  authS: any = null;
  stylistsCollection: AngularFirestoreCollection<Stylist>;
  stylists: Observable<Stylist[]>;
  currentStylistsCollection: AngularFirestoreCollection<Stylist>;
  currentStylist: Observable<Stylist[]>;
  stylist: Observable<Stylist>;
  eventsCollection: AngularFirestoreCollection<EventM>;
  events: Observable<EventM[]>;
  stylistsEventsCollection: AngularFirestoreCollection<EventM>;
  stylistsEvents: Observable<EventC[]>;
  eventDoc: AngularFirestoreDocument<EventM>;
  stylistsDoc: AngularFirestoreDocument<Stylist>;
  servicesCollection: AngularFirestoreCollection<ServicesM>;
  servicesAll: Observable<ServicesM[]>;
  services: Observable<ServicesM>;
  servicesDoc: AngularFirestoreDocument<ServicesM>;
  service: Observable<ServicesM>;
  /* customerDoc: AngularFirestoreDocument<CustomerM>;
  customer: Observable<CustomerM>; */
  customerSingleCollection: AngularFirestoreCollection<CustomerM>;
  customerSingle: Observable<CustomerM[]>;
  customersCollection: AngularFirestoreCollection<CustomerM>;
  customers: Observable<CustomerM[]>;
  response: string = "";

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
      this.stylistsCollection = this.afs.collection('stylists');
      this.eventsCollection = this.afs.collection('bookings', x => x.orderBy('startDate')); 
      
          
      this.afAuth.authState.subscribe((auth) => {
        if(auth){
        this.authS = auth;
        //console.log(this.authS.uid);
        this.currentStylistsCollection = this.afs.collection('stylists', x => x.where('uid','==',this.authS.uid));
        this.currentStylist = this.currentStylistsCollection.snapshotChanges().map(
          changes => {
            return changes.map(
              a => {
                const data = a.payload.doc.data() as Stylist;
                data.id = a.payload.doc.id;
                return data;
              });
    
          });
        }  
      });

      this.events = this.eventsCollection.snapshotChanges().map(
        changes => {
          return changes.map(
            a => {
              const data = a.payload.doc.data() as EventM;
              data.id = a.payload.doc.id;
              return data;
            });
  
        });

        
      //console.log(this.authS.uid);
      //this.stylistsCollection = this.afs.collection('stylists', x => x.where('uid','==',this.authS.uid));
      
  }

  addStylistToCollection(userData: Stylist){
    //console.log(userData);
    return this.stylistsCollection.add(userData);
  }

  getStylistFromCollection(){
    return this.currentStylist;
  }

  updateStylist(stylist: Stylist){
    //console.log(stylist.id);
    this.stylistsDoc = this.afs.doc(`stylists/${stylist.id}`);
    return this.stylistsDoc.update(stylist);
  }
  

  getServiceById(id: string){
    this.servicesDoc = this.afs.doc(`services/`+id);
    this.service = this.servicesDoc.valueChanges();
    /* this.service = this.servicesDoc.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as Services;
            const id = a.payload.doc.id;
            return {id,...data};
          });

    }); */
    
    return this.service;
  }

  
  getCustomerById(id: string){
    this.customerSingleCollection = this.afs.collection('Customers', x => x.where('uid','==',id));
    this.customerSingle = this.customerSingleCollection.snapshotChanges().map(
          changes => {
            return changes.map(
              a => {
                const data = a.payload.doc.data() as CustomerM;
                data.id = a.payload.doc.id;
                return data;
              });
    
          });
    
    return this.customerSingle;
  }

  getStylistById(id: string){
    this.stylistsDoc = this.afs.doc(`stylists/`+id);
    this.stylist = this.stylistsDoc.valueChanges();
    
    return this.stylist;
  }

  getAllStylists(){
    this.stylistsCollection = this.afs.collection('stylists');
    this.stylists = this.stylistsCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as Stylist;
            const id = a.payload.doc.id;
            return {id,...data};
          });

    });

    return this.stylists;
  }

  updateStylistColor(id, color){
    //console.log(color+""+id);
    this.stylistsDoc = this.afs.doc(`stylists/${id}`);
    return this.stylistsDoc.update({
       color: color
    });
  }

  getAllCustomers(){
    this.customersCollection = this.afs.collection('Customers');
    this.customers = this.customersCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as CustomerM;
            const id = a.payload.doc.id;
            return {id,...data};
          });
    });

    return this.customers;
  }

  getAllServices(){
    this.servicesCollection = this.afs.collection('services',x => x.orderBy('name'));
    this.servicesAll = this.servicesCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as ServicesM;
            const id = a.payload.doc.id;
            return {id,...data};
          });

    });

    return this.servicesAll;
  }

  addService(service: ServicesM){
    return this.servicesCollection.add(service);
  }

  deleteService(serviceId: string){
    this.servicesDoc = this.afs.doc(`services/${serviceId}`);
    return this.servicesDoc.delete();
  }

  updateService(service: ServicesM){
    this.servicesDoc = this.afs.doc(`services/${service.id}`);
    return this.servicesDoc.update(service);
  }

  addEvent(event: EventM){
    return this.eventsCollection.add(event);  
  }

  updateEvent(booking: EventM){
    this.eventDoc = this.afs.doc(`bookings/${booking.id}`);
    return this.eventDoc.update(booking);
  }

  getAllEvents(){
    //console.log(this.events);
    return this.events;
  }

  deleteEventFromCollection(id: string){
    this.response = "";
    this.eventDoc = this.afs.doc(`bookings/${id}`);
    return this.eventDoc.delete();
  }

  getEventsForDate(date: Date){
    //var d = new Date('2018-3-18');
    var d1 = new Date();
    
    d1.setMonth(date.getMonth());
    d1.setDate(date.getDate());
    d1.setFullYear(date.getFullYear());
    d1.setHours(23);
    d1.setMinutes(0);
    d1.setSeconds(0);
    //console.log(date);
    //console.log(d1);
    this.stylistsEventsCollection = this.afs.collection('bookings', x => {
      return x.where('startDate','>=',date).where('startDate','<',d1)
    }); 
    this.stylistsEvents = this.stylistsEventsCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as EventC;
            data.id = a.payload.doc.id;
            return data;
          });

      });
      //console.log(this.stylistsEvents);
      return this.stylistsEvents;
  }

  getEventsForStylistDate(date : Date, stylistId: string){
    //date.setHours(13);
    var d1 = new Date();
    d1.setDate(date.getDate());
    d1.setMonth(date.getMonth());
    d1.setFullYear(date.getFullYear());
    d1.setHours(date.getHours()+1);
    d1.setMinutes(0);
    d1.setSeconds(0);
    //console.log(date);
    //console.log(d1);
    this.stylistsEventsCollection = this.afs.collection('bookings', x => {
      return x.where('stylist','==',stylistId).where('startDate','<=',date).where('endDate','>=',d1)
    }); 

    this.stylistsEvents = this.stylistsEventsCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as EventC;
            data.id = a.payload.doc.id;
            return data;
          });

    });
 
    //console.log(this.stylistsEvents);
    return this.stylistsEvents;
  }

  getStylistByUID(uid){
    this.currentStylistsCollection = this.afs.collection('stylists', x => x.where('uid','==',uid));
    this.currentStylist = this.currentStylistsCollection.snapshotChanges().map(
      changes => {
        return changes.map(
          a => {
            const data = a.payload.doc.data() as Stylist;
            data.id = a.payload.doc.id;
            return data;
          });

      });
      return this.currentStylist;
       
  }

  updateStylistServices(id:string, services:string[]){
    this.stylistsDoc = this.afs.doc(`stylists/${id}`);
    return this.stylistsDoc.update(
      {
        services: services
      }
    );
  }

  createUserByEandP(email, password){
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }




}
