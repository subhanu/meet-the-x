import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AuthenticationService } from './_services/authentication.service';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { AlertComponent } from './_directives/alert.component';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';

import { Stylist } from './_models/index';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirestoreServicesService } from './_services/firestore-services.service';
import * as firebase from 'firebase/app';
import { NgForm } from '@angular/forms';

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-4.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [AngularFireAuth]
})

export class AuthComponent implements OnInit {
    userData: Stylist;
    loading = false;
    returnUrl: string;
    ages: number[] = new Array(30);
    stylistData: Stylist = {
        uid: '',
        imageFile: null,
        imageUrl: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        mobile: '',
        gender: '',
        age: 0,
        services: [],
        serviceFor: [],
        color: ""
    };
    authS:any = null; 
    stylistsCollection: AngularFirestoreCollection<Stylist>; 
    selectedFile: File;
    private basePath:string = '/stylists';

    @ViewChild('alertSignin',
        { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertSignup',
        { read: ViewContainerRef }) alertSignup: ViewContainerRef;
    @ViewChild('alertForgotPass',
        { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;

    constructor(
        private _router: Router,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _authService: AuthenticationService,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver,
        private af: AngularFireAuth,
        private afs: AngularFirestore,
        private _fireStoreServices: FirestoreServicesService) {

            this.stylistsCollection = this.afs.collection('stylists'); 
            this.af.authState.subscribe((auth) => {
                this.authS = auth
              });
              var lastAge = 18;
              for(var i=0;i<=63;i++){
                  this.ages[i] = lastAge++;
              } 

              this.stylistData.imageUrl = "assets/app/media/img/users/user.png";  
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._router.navigate([this.returnUrl]);

        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/default/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });
    }

    loginGoogle(){
        const provider = new firebase.auth.GoogleAuthProvider()
        return this._authService.socialSignIn(provider);
    }

    loginFb(){
        const provider = new firebase.auth.FacebookAuthProvider()
        return this._authService.socialSignIn(provider);
    }
   
    signup(f : NgForm) {
        this.loading = true;
        this.userData = f.value;
        return this.af.auth.createUserWithEmailAndPassword(this.userData.email, this.userData.password)
        .then((user) => {
                let storageRef = firebase.storage().ref();
                let uploadTask = storageRef.child(`${this.basePath}/${this.selectedFile.name}`).put(this.selectedFile);

                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                    (snapshot) =>  {
                    // upload in progress
                    },
                    (error) => {
                    // upload failed
                    console.log(error)
                    },
                    () => {
                    // upload success
                    this.stylistData.imageUrl = uploadTask.snapshot.downloadURL;
                    this.stylistData.uid = user.uid;
                    this.stylistData.services = [];
                    this.stylistData.password = "";
                    //console.log("Inside  "+userData.imageUrl);
                    
                    this._fireStoreServices.addStylistToCollection(this.stylistData)
                            .then(() =>{
                                this.showAlert('alertSignin');
                                this._alertService.success(
                                'Thank you for Registration!!',
                                true);

                                this.loading = false;
                                LoginCustom.displaySignInForm();    
                            }).catch((error) => {
                                console.log(error);
                            }); 
                    }
                );
         })
         .catch(error => {
          console.log(error);
          this.showAlert('alertSignup');
          this._alertService.error(error);
          this.loading = false;
        });
      
    }

    signin(f:NgForm){
        this.userData = f.value;
        this.loading = true;
        return this.af.auth.signInWithEmailAndPassword(this.userData.email, this.userData.password)
        .then((user) => {
           this.authS = user;
           this._router.navigate(['/index']);           
        })
      .catch(error => {
          console.log(error);
          this.showAlert('alertSignin');
          this._alertService.error(error);
          this.loading = false;
        });
    }

    forgotPass(f: NgForm){
        this.userData = f.value;
        this.loading = true;
        var auth = firebase.auth();

        return auth.sendPasswordResetEmail(this.userData.email)
           .then(() => {
                this.showAlert('alertSignin');
                this._alertService.success(
                    'Cool! Password recovery instruction has been sent to your email.',
                    true);
                this.loading = false;
                LoginCustom.displaySignInForm();
                
            })
           .catch((error) =>{
            this.showAlert('alertForgotPass');
            this._alertService.error(error);
            this.loading = false;
            console.log(error);
           } ) 
    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }

    
    imagePreview(file: FileList){
        this.selectedFile = file.item(0);

        var reader = new FileReader();
        reader.onload = (event:any) => {
            this.stylistData.imageUrl = event.target.result;  
        }
        reader.readAsDataURL(this.selectedFile);
    }
}