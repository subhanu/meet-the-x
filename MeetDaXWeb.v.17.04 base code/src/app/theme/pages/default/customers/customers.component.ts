import { Component, OnInit } from '@angular/core';
import { FirestoreServicesService } from '../../../../auth/_services/index';
import { CustomerM } from '../../../../auth/_models/customerM';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
})
export class CustomersComponent implements OnInit {
  customersArr: CustomerM[] = [];
  constructor(private _firestoreservices: FirestoreServicesService) { }

  ngOnInit() {
    this._firestoreservices.getAllCustomers().subscribe(
      (data: CustomerM[]) => {
        this.customersArr = data;
        console.log(this.customersArr);
      } 
   );
   
  }

}
