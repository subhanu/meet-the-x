import { Component, OnInit } from '@angular/core';
import { Stylist } from '../../../../auth/_models/stylist';
import { FirestoreServicesService } from '../../../../auth/_services/firestore-services.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

declare let Dropzone: any;
@Component({
  selector: 'app-stylists',
  templateUrl: './stylists.component.html',
  styleUrls: ['stylists.component.css']
})

export class StylistsComponent implements OnInit, AfterViewInit {
  stylistArr: Stylist[] = [];

  stylistData: any = {
    uid: '',
    imageFile: null,
    imageUrl: '',
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    gender: '',
    age: 0,
    services: []
  };

  ages: number[] = new Array(30);
  
  constructor(private _firestoreService: FirestoreServicesService, private _script: ScriptLoaderService) {
      this.stylistData.imageUrl = "assets/app/media/img/users/user.png";  

      var lastAge = 18;
      for(var i=0;i<=63;i++){
          this.ages[i] = lastAge++;
      } 
  }
  

  ngOnInit() {
    this._firestoreService.getAllStylists().subscribe(
      (user: Stylist[]) => {
        this.stylistArr = user;
        console.log(this.stylistArr);
      }
    );
  }

  ngAfterViewInit(){
    this._script.loadScripts('app-stylists',
            ['assets/demo/default/custom/components/forms/wizard/wizard.js','assets/demo/default/custom/components/forms/widgets/dropzone.js']);
    Dropzone._autoDiscoverFunction();        
  }

}
