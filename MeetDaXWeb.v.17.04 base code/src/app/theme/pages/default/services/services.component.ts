import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ServicesM } from '../../../../auth/_models/servicesM';
import { FirestoreServicesService } from '../../../../auth/_services/index';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { Observable } from 'rxjs/Observable';
import { Stylist } from '../../../../auth/_models/index';
import swal from 'sweetalert';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit, AfterViewInit {
  servicesArr: ServicesM[] = [];
  stylistsArr: any[] = [];
  stylistTA: any = [];
  stylistD: Stylist = null;
  stylistDocRef: string;
  deleteServiceId: string;
  stylistId: string;
  display = 'none';

  audio = new Audio();

  isAlert: boolean = false;
  alertMessage: string = "";

  public model: any = {
    name: "",
    id: ""
  };

  serviceAddF: ServicesM = {
    name: "",
    price: 0,
    description: "",
    stylist: "",
    duration: 0
  };

  serviceUpdateF: ServicesM = {
    name: "",
    price: 0,
    description: "",
    stylist: "",
    duration: 0
  };

  @ViewChild('serviceUpdateForm') serviceUpdateForm: any;
  @ViewChild('serviceAddForm') serviceAddForm: any;
  
  search = (text$: Observable<string>) =>
    text$
    .debounceTime(200)
    .map(term => term === '' ? []
      : this.stylistTA.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
 
  formatter = (x: {name: string}) => x.name; 

  constructor(private _firestoreservices: FirestoreServicesService, private _script: ScriptLoaderService) { 
    this.audio.src= "./assets/app/media/sound/to-the-point.mp3";
    this.audio.load(); 
  }


  ngOnInit() {
        this._firestoreservices.getAllServices().subscribe(
           data => this.servicesArr = data
        );
        console.log(this.servicesArr);
  }

  showConfirmDeleteModal(){
    this.display = 'block';
    $(".confirm-delete-modal").addClass("show");
  }

  hideConfirmDeleteModal(){
    this.display = 'none';
    $(".confirm-delete-modal").removeClass("show");
  }

  showUpdateServiceModal(){
    this.display = 'block';
    $(".edit-service-modal").addClass("show");
  }

  hideUpdateServiceModal(){
    this.display = 'none';
    $(".edit-service-modal").removeClass("show");
    this.serviceAddForm.reset();
  }

  showToastr(msg){
    this.audio.play();
    this.isAlert = true;
    this.alertMessage = msg;
    setTimeout(() => this.isAlert= false, 5000);
  }

  setStylist(){
      console.log("Inside SetCustomer");

      this._firestoreservices.getAllStylists().subscribe(
          (data) => {
            this.stylistsArr = data
            //console.log(this.stylistsArr)
          }
      );
      //console.log(this.stylistsArr);

      for(var i=0;i< this.stylistsArr.length;i++){
        var stylistTA = {
            name : this.stylistsArr[i].firstName +" " +this.stylistsArr[i].lastName,
            id : this.stylistsArr[i].id
        }
        //console.log(custTA);
        this.stylistTA[i] = stylistTA;
        //console.log(stylistTA);
      }

      this.serviceAddF.stylist = this.model.id; 
      this.serviceUpdateF.stylist = this.model.id; 
      console.log(this.serviceAddF.stylist);
  }


  onSubmitService(){
     console.log(this.serviceAddF);
 
     this._firestoreservices.getStylistById(this.serviceAddF.stylist).subscribe(
      (data) => {
        this.stylistD = data;
        console.log(this.stylistD);
      }
     );

     this._firestoreservices.addService(this.serviceAddF)
     .then((docRef) => {
         console.log(docRef.id);
         this.stylistD.services.push(docRef.id);

         this._firestoreservices.updateStylistServices(this.serviceAddF.stylist, this.stylistD.services)
         .then(() => {
            this.showToastr("Service Added Successfully");
         })
         .catch(() => {
            this.showToastr("Service Addiiton Error");
         });
     })
     .catch((err) => {
        console.log(err);
        this.showToastr("Service Addiiton Error");
     });

     this.serviceAddForm.reset();
  }


  deleteService(){
     //console.log(service);
     this._firestoreservices.getStylistById(this.stylistId).subscribe(
      (data) => {
        this.stylistD = data;
        console.log(this.stylistD);
      }
     );

     this._firestoreservices.deleteService(this.deleteServiceId)
     .then((docRef) => {
         //this.showToastr("Service Deleted Successfully");
         var index = this.stylistD.services.indexOf(this.deleteServiceId);
         this.stylistD.services.splice(index, 1);

         this._firestoreservices.updateStylistServices(this.stylistId, this.stylistD.services)
         .then(() => {
            this.showToastr("Service Deleted Successfully");
         })
         .catch(() => {
            this.showToastr("Service Deletion Error");
         });
     })
     .catch((err) => {
         this.showToastr("Service Deletion Error");
         console.log(err);
     });
     
     this.hideConfirmDeleteModal();
  }


  setDeleteId(service: ServicesM){
     this.deleteServiceId = service.id;
     this.stylistId = service.stylist;
     this.showConfirmDeleteModal();
  }


  setServiceForUpdate(service: ServicesM){
     this.showUpdateServiceModal();
     this.serviceUpdateF = service;
     console.log(service);

     this._firestoreservices.getStylistById(service.stylist).subscribe(
       (data) => {
          console.log(data);
          this.model = {
            name: data.firstName+" "+data.lastName,
            id: data.id
          }
       }
     );
  }

  onUpdateService(){
     this._firestoreservices.updateService(this.serviceUpdateF)
     .then(() => {
        this.showToastr("Service Updated Successfully"); 
     })
     .catch(() => {
        this.showToastr("Service Updation Error");
     });

     this.hideUpdateServiceModal();
     this.serviceAddForm.reset();
     //this.serviceUpdateForm.reset();
  }


  ngAfterViewInit(){
    this._script.loadScripts('app-services',
    ['assets/demo/default/custom/components/forms/validation/form-controls.js',
     'assets/demo/default/custom/components/base/sweetalert2.js']);
  }

  open(){
    $('#m_form_2_msg').removeClass('m--hide');
  }

}
