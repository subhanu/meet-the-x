//
//  DatePickViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 07/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase
import DatePickerDialog

class DatePickViewController: UIViewController  {
  let delegate = UIApplication.shared.delegate as! AppDelegate
    let db = Firestore.firestore()
    let datePicker = UIDatePicker()
    // @IBOutlet weak var myDatePicker: UIDatePicker!
    var date = String()
    var time = String()
    @IBOutlet weak var barbernLabel: UILabel!
    @IBOutlet weak var barberName: UILabel!
    @IBOutlet weak var selectedDate: UILabel!
    @IBOutlet weak var selectedDt: UITextField!
    @IBOutlet weak var slctdtime: UILabel!
    @IBOutlet weak var serv1: UILabel!
    @IBOutlet weak var serv2: UILabel!
    @IBOutlet weak var barberImage: UIImageView!
    @IBOutlet weak var booknowBtn: UIButton!
    @IBOutlet weak var datePickButton: UIButton!
   
    //slot button action
    @IBOutlet weak var slot1: UIButton!
    @IBOutlet weak var slot2: UIButton!
    @IBOutlet weak var slot3: UIButton!
    @IBOutlet weak var slot4: UIButton!
    @IBOutlet weak var slot5: UIButton!
    @IBOutlet weak var slot6: UIButton!
    @IBOutlet weak var slot7: UIButton!
    @IBOutlet weak var slot8: UIButton!
    @IBOutlet weak var slot9: UIButton!
    @IBOutlet weak var slot10: UIButton!
    
    
    @IBAction func slotAct1(_ sender: Any) {
       self.time = "9:00 AM"
        slot1.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot1.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 9:00")
       setupDate()
    }
    
    @IBAction func slotAct2(_ sender: Any) {
        self.time = "10:00 AM"
        slot2.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot2.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 10:00")
        setupDate()
    }
    @IBAction func slotAct3(_ sender: Any) {
       self.time = "11:00 AM"
        slot3.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot3.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 11:00")
       setupDate()
    }
    
    @IBAction func slotAct4(_ sender: Any) {
       self.time = "12:00 PM"
        slot4.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot4.backgroundColor = UIColor(hexString: "#20BFC6ff")
       slctdtime.text = ("kl. 12:00")
        setupDate()
    }
    @IBAction func slotAct5(_ sender: Any) {
       self.time = "1:00 PM"
        slot5.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot5.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 13:00")
        setupDate()
    }
    
    @IBAction func slotAct6(_ sender: Any) {
       self.time = "2:00 PM"
        slot6.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot6.backgroundColor = UIColor(hexString: "#20BFC6ff")
       slctdtime.text = ("kl. 14:00")
        setupDate()
    }
    @IBAction func slotAct7(_ sender: Any) {
        self.time = "3:00 PM"
        slot7.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot7.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 15:00")
        setupDate()
    }
    @IBAction func slotAct8(_ sender: Any) {
       self.time = "4:00 PM"
        slot8.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot8.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 16:00")
        setupDate()
    }
    @IBAction func slotAct9(_ sender: Any) {
       self.time = "5:00 PM"
        slot9.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot9.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 17:00")
        setupDate()
    }
    @IBAction func slotAct10(_ sender: Any) {
       self.time = "6:00 PM"
        slot10.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        slot10.backgroundColor = UIColor(hexString: "#20BFC6ff")
        slctdtime.text = ("kl. 18:00")
        setupDate()
    }
    
   
    
     override func viewDidAppear(_ animated: Bool) {
        print("cust data : \(self.delegate.customer_id)")
        for i in self.delegate.services{
            print("hello: \(i)")
        }
        
        barberImage.image = self.delegate.stylistPic["selected"]
        if((delegate.services).count == 2){
            serv1.text = delegate.services[0]
            serv2.text = delegate.services[1]
        }else if((delegate.services).count == 1){
            serv1.text = delegate.services[0]
            serv2.text = " "
        }else if((delegate.services).count == 0){
            serv1.text = " "
            serv2.text = " "
        }
    
    }
    
    @IBAction func BookNowBtn(_ sender: Any) {
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.barbernLabel.text = delegate.stylist
       //  self.barberImage.layer.cornerRadius = 6
       //  self.bookNowbtn.layer.cornerRadius = 6
        setupLayout()
        createDatePicker()
        self.barberName.text = self.delegate.stylist
        // Do any additional setup after loading the view.
        
        
        slot1.layer.shadowColor = UIColor.black.cgColor
        slot1.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot1.layer.shadowRadius = 3
        slot1.layer.shadowOpacity = 0.4
        
        slot2.layer.shadowColor = UIColor.black.cgColor
        slot2.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot2.layer.shadowRadius = 3
        slot2.layer.shadowOpacity = 0.4
        
        slot3.layer.shadowColor = UIColor.black.cgColor
        slot3.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot3.layer.shadowRadius = 3
        slot3.layer.shadowOpacity = 0.4
        
        slot4.layer.shadowColor = UIColor.black.cgColor
        slot4.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot4.layer.shadowRadius = 3
        slot4.layer.shadowOpacity = 0.4
        
        slot5.layer.shadowColor = UIColor.black.cgColor
        slot5.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot5.layer.shadowRadius = 3
        slot5.layer.shadowOpacity = 0.4
        
        slot6.layer.shadowColor = UIColor.black.cgColor
        slot6.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot6.layer.shadowRadius = 3
        slot6.layer.shadowOpacity = 0.4
        
        slot7.layer.shadowColor = UIColor.black.cgColor
        slot7.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot7.layer.shadowRadius = 3
        slot7.layer.shadowOpacity = 0.4
        
        slot8.layer.shadowColor = UIColor.black.cgColor
        slot8.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot8.layer.shadowRadius = 3
        slot8.layer.shadowOpacity = 0.4
        
        slot9.layer.shadowColor = UIColor.black.cgColor
        slot9.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot9.layer.shadowRadius = 3
        slot9.layer.shadowOpacity = 0.4
        
        slot10.layer.shadowColor = UIColor.black.cgColor
        slot10.layer.shadowOffset = CGSize(width: 3, height: 3)
        slot10.layer.shadowRadius = 3
        slot10.layer.shadowOpacity = 0.4
        
        datePickButton.layer.shadowColor = UIColor.black.cgColor
        datePickButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        datePickButton.layer.shadowRadius = 3
        datePickButton.layer.shadowOpacity = 0.4
        
        
        //slot1.frame  = CGRect(x:0,y:0, width: view.frame.width/2,height: 0)
        //booknowBtn.backgroundColor = UIColor(hexString: "#ffffffff")
        booknowBtn.layer.shadowColor = UIColor.black.cgColor
        booknowBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        booknowBtn.layer.shadowRadius = 6
        booknowBtn.layer.shadowOpacity = 0.4
        booknowBtn.layer.cornerRadius = 6
        booknowBtn.addTarget(self, action: #selector(pickDate),for: .touchUpInside)
        booknowBtn.addTarget(self, action: #selector(buttonAction),for: .touchUpInside)
    }
    
    @objc func buttonAction(){
        var ref:DocumentReference? = nil
        var user = Auth.auth().currentUser;
        
        if let user = user{
            let mail = user.email
        }
        var duration = Int()
        for i in self.delegate.servDuration{
            duration = duration + i
        }
        var price = Int()
        for i in self.delegate.servPrice{
            price = price + i
        }
        
        //let endDate = calendar.date(byAdding: .minute, value: duration, to: self.delegate.aptDate)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MMMM dd, yyyy 'at' h:mm a"
        let string = self.date + " at " + self.time  // "March 24, 2017 at 7:00 AM"
        
        
        let finalDate = dateFormatter.date(from: string)!
        print("DateString \(finalDate )")
        delegate.aptDate = finalDate
        let calendar = finalDate
        let Later = TimeInterval(duration.minutes)
         let endDate = calendar.addingTimeInterval(Later)

        
        
        print("later: \(Later)")
        print("duration: \(duration)")
        print("calendar \(calendar)")
        print("endDate \(endDate)")
        
        
        
        
        if (self.delegate.update_id == nil){
            ref = db.collection("bookings").addDocument(data: [
                "stylist" : self.delegate.stylist_id,
                "customer" : self.delegate.customer_id,
                "services" : self.delegate.servicesId,
                "startDate": self.delegate.aptDate,
                "imageUrl" : self.delegate.stylist_pic_url,
                "duration" : duration,
                "endDate" : endDate,
                "price" : price,
                "eid" : "",
                "how_often" : "",
                "id" : ""
            ]){ err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
            }
        }else{
            db.collection("bookings").document(self.delegate.update_id!).updateData([
                "stylist"  : self.delegate.stylist_id,
                "services" : self.delegate.servicesId,
                "startDate": self.delegate.aptDate,
                "imageUrl" : self.delegate.stylist_pic_url,
                "duration" : duration,
                "endDate"  : endDate,
                "price" : price
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    self.delegate.update_id = nil
                    print("Document successfully updated!")
                    print(self.delegate.update_id)
                }
            }
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
        self.present(vc!, animated: true, completion: nil)
        print("\(endDate) : \(duration)")
        
    }
    @objc func pickDate(){

//        let toolbar = UIToolbar()
//        toolbar.sizeToFit()
//        datePicker.datePickerMode = .date
//        //add a done button on this toolbar
//        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
//
//        toolbar.setItems([doneButton], animated: true)

 
    }

    func createDatePicker(){
        
        //assign datepicker to our textfield
         selectedDt.inputView = datePicker

        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        datePicker.datePickerMode = .date
        //add a done button on this toolbar
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))

        toolbar.setItems([doneButton], animated: true)

        selectedDt.inputAccessoryView = toolbar


    }
    
    // combine date and time to convert into datetime
    private func setupDate(){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MMMM dd, yyyy 'at' h:mm a"
      
//        (finalDate?.description(with: .current))
        //print(finalDate)
    }
    
    @objc func doneClicked(){
        
        selectedDt.text = " "
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        datePickButton.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
        datePickButton.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        datePickButton.backgroundColor = UIColor(hexString: "#20BFC6ff")
        //selectedDt.text = dateFormatter.string(from: datePicker.date)
        self.date = dateFormatter.string(from: datePicker.date)
        //delegate.dateTime = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        selectedDate.text = ("- \(self.date)")
     
    }
    
    private func setupLayout(){
       // dayBtn.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.33)
       // monthBtn.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.33)
      //  yearBtn.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.33)
        
        //monthBtn.frame = CGRect(x:view.frame.width/3 , y:0 , width : view.frame.width/3 , height: 40)
        
        
        //yearBtn.frame = CGRect(x:2*(view.frame.width/3) , y:0 , width : view.frame.width/3 , height: 40)
        
    }
    
//    @IBAction func atePickerAction(_ sender: Any) {
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//        let strDate = dateFormatter.string(from: myDatePicker.date)
//        self.selectedDate.text = strDate
//        delegate.dateTime = strDate
//        self.selectedTime.text = strDate
//        print(delegate.dateTime)
//    }
    
//    @IBAction func submitButtonAction(_ sender: Any) {
//        var ref:DocumentReference? = nil
//        var user = Auth.auth().currentUser;
//
//        if let user = user{
//            let mail = user.email
//        }
//
//        if (self.delegate.update_id == nil){
//        ref = db.collection("events").addDocument(data: [
//            "stylist" : delegate.stylist,
//            "customers" : user?.displayName,
//            "services" : delegate.services,
//            "startDate": delegate.aptDate,
//            "email" : user?.email,
//            "imgUrl" : delegate.stylist_pic_url
//        ]){ err in
//            if let err = err {
//                print("Error adding document: \(err)")
//            } else {
//                print("Document added with ID: \(ref!.documentID)")
//            }
//        }
//        }else{
//             db.collection("events").document(self.delegate.update_id!).updateData([
//                "stylist" : delegate.stylist,
//                "services" : delegate.services,
//                "startDate": delegate.aptDate
//            ]) { err in
//                if let err = err {
//                    print("Error updating document: \(err)")
//                } else {
//                     self.delegate.update_id = nil
//                    print("Document successfully updated!")
//                    print(self.delegate.update_id)
//                }
//            }
//        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
//        self.present(vc!, animated: true, completion: nil)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
extension Int {
    
    var seconds: Int {
        return self
    }
    
    var minutes: Int {
        return self.seconds * 60
    }
    
    var hours: Int {
        return self.minutes * 60
    }
    
    var days: Int {
        return self.hours * 24
    }
    
    var weeks: Int {
        return self.days * 7
    }
    
    var months: Int {
        return self.weeks * 4
    }
    
    var years: Int {
        return self.months * 12
    }
}
