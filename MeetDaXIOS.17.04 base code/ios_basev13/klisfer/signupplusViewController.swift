//
//  signupplusViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 05/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseCore
import FBSDKCoreKit
import FirebaseFirestore

class signupplusViewController: UIViewController {
     let delegate = UIApplication.shared.delegate as! AppDelegate
    let db = Firestore.firestore()
    @IBOutlet weak var card1: CardView!
    
    @IBOutlet weak var card2: CardView!
    var cover1 = UIView()
    var cover2 = UIView()
    var mask1 = UIView()
    var mask2 = UIView()
    @IBOutlet weak var promotinpic1: UIImageView!
    @IBOutlet weak var promotinpic2: UIImageView!
    @IBOutlet weak var offerserv1: UIView!
    @IBOutlet weak var offerserv2: UIView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var navbar: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        //print("customer data : \(self.delegate.customer_id)")
        //let textAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       // let textAttributes1 = [NSAttributedStringKey.font: UIFont(name: "Roboto" , size: 20)]
        //navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        
        let nav = self.navigationController?.navigationBar
        
        // 2
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow
        
        // 3
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (nav?.frame.size.width)!, height: (nav?.frame.size.height)!))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo-white")
        imageView.image = image
        
        // 5
        navigationItem.titleView = imageView
        
        
        
        db.collection("products").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    self.delegate.prod_desc.append(document.data()["description"] as! String)
                    self.delegate.prod_name.append(document.data()["productName"] as! String)
                    self.delegate.prod_price.append(document.data()["price"] as! String)
                    self.delegate.prod_img_url.append(document.data()["imageUrl"] as! String)
                }
            }
        }
        
        
        db.collection("promotions").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    
                    self.delegate.prom_name.append(document.data()["name"] as! String)
                    self.delegate.prom_img_url.append(document.data()["imageUrl"] as! String)
                    self.delegate.prom_serv1.append(document.data()["service1"] as! String)
                    self.delegate.prom_serv2.append(document.data()["service2"] as! String)
                    self.delegate.prom_serv3.append(document.data()["service3"] as! String)
                    self.delegate.prom_price.append(document.data()["price"] as! String)
                }
            }
        }
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        // OLD QUERIES
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let db = Firestore.firestore()

        
        var formatter = DateFormatter()
        var aptDate = Date()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        var someDateTime = formatter.string(from: aptDate)
        
        let userID = Auth.auth().currentUser!.uid
        
        
        
        db.collection("bookings").whereField("customer", isEqualTo: userID).getDocuments{ (snapshot, error) in
            
            if error != nil{
                print(error)
            }else{
                delegate.event_service.removeAll()
                delegate.event_service_name.removeAll()
                delegate.event_dateTime.removeAll()
                delegate.doc_id.removeAll()
                delegate.event_stylists_pic_url.removeAll()
                delegate.event_timestamp.removeAll()
                delegate.event_stylists_name.removeAll()
                
                for document in (snapshot!.documents){
                    
                   // print("\(document.documentID) => \(document.data())")
                    let doc_id =  document.documentID as! String
                    delegate.doc_id.append(doc_id)
                    
                    
                    let stylist_id = document.data()["stylist"] as? String
                    //fetching img url of event stylist
                    
                    let img_url = document.data()["imageUrl"] as? String
                    delegate.event_stylists_pic_url.append(img_url!)
                    
                    let index  = delegate.uid_stylists.index(of: stylist_id!)
                    
                    
                    let  barberName = (delegate.barberName[index!])
                    
                    delegate.event_stylists_name.append(barberName)
                    
                    // fetching services array and storing in delegate(global var)
                    let arr = document.data()["services"] as? NSArray
                    var objCArray = NSMutableArray(array: arr!)
                    // reading array from document
                    let swiftArray = (objCArray as NSArray as? [String])!
                    delegate.event_service.append(swiftArray)
                    
                    var arr1 = [String]()
                    
                    for i in swiftArray{
                        arr1.append(delegate.all_services[i]!)
                    }
                    delegate.event_service_name.append(arr1)
                    
                    
                    // adding startDate
                    aptDate =  document.data()["startDate"] as! Date
                    print("aptDate: \(aptDate)")
                    //self.delegate.event_timestamp.append(aptDate)
                    //someDateTime = formatter.date(from: aptDate)
                    delegate.event_dateTime.append(aptDate)
                    
                    
                    
                }
                
            }
        }

    }
    
    
    @IBAction func signout(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        // signout user of facebook
        FBSDKAccessToken.setCurrent(nil)
        
        //send user to login screen after signout
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : UIViewController =  mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
        
        
        self.present(viewController, animated: true, completion: nil)
    }
   
    
    private func setupLayout(){
        
        //card1.backgroundColor = UIColor(patternImage: UIImage(named: "Promotion2.png")!)
        //card2.backgroundColor = UIColor(patternImage: UIImage(named: "Promotion1.png")!)
     
        card1.addSubview(cover1)
        cover1.frame = CGRect(x: 0 ,y: 160 ,width: card1.frame.width  , height: 12)
        cover1.backgroundColor = .white
        card2.addSubview(cover2)
        cover2.frame = CGRect(x: 0 ,y: 160 ,width: card2.frame.width  , height: 12)
        cover2.backgroundColor = .white
        
        
        card1.addSubview(mask1)
        mask1.frame = CGRect(x:0, y:0, width:view.frame.width - 40, height:220)
        mask1.backgroundColor = UIColor(hexString: "#00000000")
        card2.addSubview(mask2)
        mask2.frame = CGRect(x:0, y: 0, width:view.frame.width - 40, height:220)
        mask2.backgroundColor = UIColor(hexString: "#00000000")
        
        
        //button on mask1
        let mon1 = UIButton()
        let mon11 = UIButton()
        mask1.addSubview(mon1)
        mask1.addSubview(mon11)
        mon1.frame = CGRect(x: 20 ,y: 125 ,width: 40,height: 40)
        mon11.frame = CGRect(x: 75 ,y: 125 ,width: 100,height: 40)
        mon1.backgroundColor = .white
        mon11.backgroundColor = UIColor(hexString: "#1C92AFff")
        mon1.layer.shadowColor = UIColor.black.cgColor
        mon11.layer.shadowColor = UIColor.black.cgColor
        mon1.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon11.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon1.layer.shadowRadius = 3
        mon11.layer.shadowRadius = 3
        mon1.layer.shadowOpacity = 0.7
        mon11.setTitle("500 dkk", for: UIControlState.normal)
        mon11.layer.shadowOpacity = 1.0
        mon1.layer.cornerRadius = 6
        mon11.layer.cornerRadius = 6
        
        //button on mask 2
        let mon2 = UIButton()
        let mon22 = UIButton()
        mask2.addSubview(mon2)
        mask2.addSubview(mon22)
        mon2.frame = CGRect(x: 20 ,y: 125 ,width: 40,height: 40)
        mon22.frame = CGRect(x: 75 ,y: 125 ,width: 100,height: 40)
        mon2.backgroundColor = .white
        mon22.backgroundColor = UIColor(hexString: "#1C92AFff")
        mon2.layer.shadowColor = UIColor.black.cgColor
        mon22.layer.shadowColor = UIColor.black.cgColor
        mon2.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon22.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon2.layer.shadowRadius = 3
        mon22.layer.shadowRadius = 3
        mon2.layer.shadowOpacity = 0.7
        mon22.setTitle("500 dkk", for: UIControlState.normal)
        mon22.layer.shadowOpacity = 1.0
        mon2.layer.cornerRadius = 6
        mon22.layer.cornerRadius = 6
        
        
        promotinpic1.layer.cornerRadius = 12;
        promotinpic1.clipsToBounds = true;
        promotinpic2.layer.cornerRadius = 12;
        promotinpic2.clipsToBounds = true;
        offerserv1.layer.cornerRadius = 12;
        offerserv1.clipsToBounds = true;
        offerserv2.layer.cornerRadius = 12;
        offerserv2.clipsToBounds = true;
        
        
        
        //call button
        callBtn.layer.shadowColor = UIColor.black.cgColor
        callBtn.layer.shadowOffset = CGSize(width: 6, height: 6)
        callBtn.layer.shadowRadius = 6
        callBtn.layer.shadowOpacity = 1
        callBtn.layer.cornerRadius = 6
        callBtn.backgroundColor = .darkGray
        
        
    }

}
