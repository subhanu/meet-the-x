//
//  ChooseServiceViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 07/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth

class ChooseServiceViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    let services = ["Dameklip", "Damer" , "Vandondulation" , "Harvask"]
    let prices = ["300kr", "500kr" , "100kr" , "200kr"]
    var checked : [Int] = []
    let db = Firestore.firestore()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var serv1: UILabel!
    @IBOutlet weak var serv2: UILabel!
    @IBOutlet weak var barberImage: UIImageView!
    @IBOutlet weak var barberName: UILabel!
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        return (self.delegate.servProv.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChooseServiceTableViewCell
        cell.service.text = self.delegate.servProv[indexPath.row]
        cell.price.text = prices[indexPath.row]
        // cross checking for checked rows
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white
        cell.selectedBackgroundView = backgroundView
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath)?.accessoryType == UITableViewCellAccessoryType.checkmark{
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.none
            let indx = checked.index(of:indexPath.row)
            if(checked.contains(indexPath.row)){
                if let index = checked.index(of:indexPath.row) {
                    checked.remove(at: index)
                }
            }
            //var indx = Int()
            
            print("indx:\(indx)")
            if(indx == 0){
                serv1.text = " "
                serv1.text = serv2.text
                serv2.text = " "
            }else if (indx == 1){
                serv2.text = " "
            }
        }else{
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
            print("check count: \(checked.count)")
            if(checked.count == 0){
                serv1.text = self.delegate.servProv[indexPath.row]
            }else if (checked.count == 1){
                serv2.text = self.delegate.servProv[indexPath.row]
            }
            
            if(!(checked.contains(indexPath.row))){
                checked.append(indexPath.row)
            }
            
            
            
        }
    }
    
    
    
    
    var user : String?
    var stylist : String?
    
    @IBOutlet weak var navBar: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = .red
        layoutSetup()
        setupNavigationBar()
        navBar.title = self.delegate.stylist
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        print("fetched services are: \(self.delegate.all_services) ")
    }
    
    override func viewDidLayoutSubviews() {
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }
    private func setupNavigationBar(){
        
        
        navigationItem.title = "Home"
        
    }
    
    private func layoutSetup(){
        //TOPVIEW
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        //        let  topView = UIView()
        //        view.addSubview(topView)
        //        topView.translatesAutoresizingMaskIntoConstraints = false
        //        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor , constant: 20).isActive = true
        //        topView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor).isActive = true
        //        topView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        //        //topView.backgroundColor = .gray
        //
        //        //barber image
        //        let barberImage : UIImageView = {
        //            let imageView = UIImageView(image: #imageLiteral(resourceName: "emptyprof"))
        //            imageView.translatesAutoresizingMaskIntoConstraints = false
        //            imageView.contentMode = .scaleAspectFit
        //            return imageView
        //        }()
        //
        let imgUrl = delegate.stylist_pic_url
        let url = URL(string: imgUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                self.barberImage.image = imahe
                //self.delegate.allBarberPic.append(imahe)
            }
            
        }).resume()
        barberName.text = self.delegate.stylist
        //
        //        barberImage.heightAnchor.constraint(equalToConstant: 128).isActive = true
        //        barberImage.widthAnchor.constraint(equalToConstant: 170).isActive = true
        //        topView.addSubview(barberImage)
        //        barberImage.topAnchor.constraint(equalTo: topView.topAnchor, constant: 10).isActive = true
        //        barberImage.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 10).isActive = true
        //
        //        //barbername
        //        let barberName: UITextView = {
        //            let textView = UITextView()
        //            textView.text = delegate.stylist
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //
        //        topView.addSubview(barberName)
        //        barberName.topAnchor.constraint(equalTo: topView.topAnchor, constant: 10).isActive = true
        //        barberName.leftAnchor.constraint(equalTo: barberImage.rightAnchor, constant: 10).isActive = true
        //
        //
        //        //barber services
        //        let barberServ: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Women & Children"
        //            textView.font = UIFont.boldSystemFont(ofSize: 16)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        topView.addSubview(barberServ)
        //        barberServ.topAnchor.constraint(equalTo: barberName.bottomAnchor).isActive = true
        //        barberServ.leftAnchor.constraint(equalTo: barberImage.rightAnchor, constant: 10).isActive = true
        
        
        
        
        
        
        
        
        
        
        //BOTTOMVIEW  BOTTOMVIEW  BOTTOMVIEW  BOTTOMVIEW
        
        //
        //        let  bottomView = UIView()
        //        view.addSubview(bottomView)
        //        bottomView.translatesAutoresizingMaskIntoConstraints = false
        //        bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        //        bottomView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        //        bottomView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        //        //bottomView.backgroundColor = .cyan
        //
        //        //row1
        //
        //        let serv1: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Dameklip"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(serv1)
        //        serv1.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 30 ).isActive = true
        //        serv1.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 10).isActive = true
        //        serv1.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //        let price1: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "500kr"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(price1)
        //        price1.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        price1.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 30 ).isActive = true
        //        price1.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -70).isActive = true
        //
        //
        //
        //        mySwitch1.translatesAutoresizingMaskIntoConstraints = false
        //        mySwitch1.setOn(false, animated: true)
        //        mySwitch1.tintColor = UIColor.lightGray
        //        mySwitch1.onTintColor = UIColor.green
        //        mySwitch1.thumbTintColor = UIColor.lightGray
        //        mySwitch1.addTarget(self, action: #selector(switchChanged1(sender:)), for: UIControlEvents.valueChanged)
        //        bottomView.addSubview(mySwitch1)
        //        mySwitch1.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch1.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch1.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 30).isActive = true
        //        mySwitch1.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        //
        //        //row2
        //
        //        let serv2: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Damer"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(serv2)
        //        serv2.topAnchor.constraint(equalTo: serv1.bottomAnchor, constant: 10 ).isActive = true
        //        serv2.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 10).isActive = true
        //        serv2.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //        let price2: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "500kr"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(price2)
        //        price2.topAnchor.constraint(equalTo: price1.bottomAnchor, constant: 10 ).isActive = true
        //        price2.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -70).isActive = true
        //        price2.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //
        //        mySwitch2.translatesAutoresizingMaskIntoConstraints = false
        //        mySwitch2.setOn(false, animated: true)
        //        mySwitch2.tintColor = UIColor.lightGray
        //        mySwitch2.onTintColor = UIColor.green
        //        mySwitch2.thumbTintColor = UIColor.lightGray
        //        mySwitch2.addTarget(self, action: #selector(switchChanged2(sender:)), for: UIControlEvents.valueChanged)
        //        bottomView.addSubview(mySwitch2)
        //        mySwitch2.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch2.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch2.topAnchor.constraint(equalTo: mySwitch1.bottomAnchor, constant: 10).isActive = true
        //        mySwitch2.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        //
        //
        //
        //        //3rd row
        //
        //        let serv3: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Hel og bundfarving"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(serv3)
        //        serv3.topAnchor.constraint(equalTo: serv2.bottomAnchor, constant: 10 ).isActive = true
        //        serv3.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 10).isActive = true
        //        serv3.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //        let price3: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "500kr"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(price3)
        //        price3.topAnchor.constraint(equalTo: price2.bottomAnchor, constant: 10 ).isActive = true
        //        price3.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -70).isActive = true
        //        price3.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //
        //        mySwitch3.translatesAutoresizingMaskIntoConstraints = false
        //        mySwitch3.setOn(false, animated: true)
        //        mySwitch3.tintColor = UIColor.lightGray
        //        mySwitch3.onTintColor = UIColor.green
        //        mySwitch3.thumbTintColor = UIColor.lightGray
        //        mySwitch3.addTarget(self, action: #selector(switchChanged3(sender:)), for: UIControlEvents.valueChanged)
        //        bottomView.addSubview(mySwitch3)
        //        mySwitch3.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch3.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch3.topAnchor.constraint(equalTo: mySwitch2.bottomAnchor, constant: 10).isActive = true
        //        mySwitch3.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        //
        //
        //        //4th row
        //        let serv4: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Harvask"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(serv4)
        //        serv4.topAnchor.constraint(equalTo: serv3.bottomAnchor, constant: 10 ).isActive = true
        //        serv4.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 10).isActive = true
        //        serv4.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //        let price4: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "500kr"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(price4)
        //        price4.topAnchor.constraint(equalTo: price3.bottomAnchor, constant: 10 ).isActive = true
        //        price4.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -70).isActive = true
        //        price4.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //
        //        mySwitch4.translatesAutoresizingMaskIntoConstraints = false
        //        mySwitch4.setOn(false, animated: true)
        //        mySwitch4.tintColor = UIColor.lightGray
        //        mySwitch4.onTintColor = UIColor.green
        //        mySwitch4.thumbTintColor = UIColor.lightGray
        //        mySwitch4.addTarget(self, action: #selector(switchChanged4(sender:)), for: UIControlEvents.valueChanged)
        //        bottomView.addSubview(mySwitch4)
        //        mySwitch4.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch4.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch4.topAnchor.constraint(equalTo: mySwitch3.bottomAnchor, constant: 10).isActive = true
        //        mySwitch4.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        //
        //        //row5
        //        let serv5: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "Vandondulation"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(serv5)
        //        serv5.topAnchor.constraint(equalTo: serv4.bottomAnchor, constant: 10 ).isActive = true
        //        serv5.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 10).isActive = true
        //        serv5.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //        let price5: UITextView = {
        //            let textView = UITextView()
        //            textView.text = "500kr"
        //            textView.font = UIFont.boldSystemFont(ofSize: 17)
        //            textView.translatesAutoresizingMaskIntoConstraints = false
        //            textView.textAlignment = .center
        //            textView.isEditable = false
        //            textView.isScrollEnabled = false
        //            textView.textColor = .black
        //            return textView
        //        } ()
        //        bottomView.addSubview(price5)
        //        price5.topAnchor.constraint(equalTo: price4.bottomAnchor, constant: 10 ).isActive = true
        //        price5.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -70).isActive = true
        //        price5.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        //
        //        mySwitch5.translatesAutoresizingMaskIntoConstraints = false
        //        mySwitch5.setOn(false, animated: true)
        //        mySwitch5.tintColor = UIColor.lightGray
        //        mySwitch5.onTintColor = UIColor.green
        //        mySwitch5.thumbTintColor = UIColor.lightGray
        //        mySwitch5.addTarget(self, action: #selector(switchChanged5(sender:)), for: UIControlEvents.valueChanged)
        //        bottomView.addSubview(mySwitch5)
        //        mySwitch5.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch5.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        mySwitch5.topAnchor.constraint(equalTo: mySwitch4.bottomAnchor, constant: 10).isActive = true
        //        mySwitch5.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        //
        //next button outside bottomview
        let button = UIButton()
        button.setTitle("NEXT", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(hexString: "#4B4D4Dff")
        //button.translatesAutoresizingMaskIntoConstraints = false
        //button.heightAnchor.constraint(equalToConstant: 100).isActive = true
        //button.widthAnchor.constraint(equalToConstant: 300).isActive = true
        button.frame = CGRect(x: 25, y: 310 + ((self.delegate.servProv.count)*50), width: Int(screenWidth-50), height: 50)
        button.addTarget(self, action: #selector(buttonAction),for: .touchUpInside)
        button.layer.shadowColor = UIColor.black.cgColor
        //button.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 20).isActive = true
        button.layer.shadowOffset = CGSize(width: 5, height: 5)
        button.layer.shadowRadius = 5
        button.layer.shadowOpacity = 1.0
        button.layer.cornerRadius = 6
        
        
        
        self.view.addSubview(button)
        
        
    }
    
    @objc private func buttonAction(sender: UIButton!){
        print(delegate.stylist + " " + delegate.customer)
        if (checked.count > 2 ) {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "You can select maximum of 2 services at a time", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if (checked.count == 0){
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Select atleast 1 service", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }else{
           
            self.delegate.services.removeAll()
            self.delegate.servicesId.removeAll()
            for i in checked{
                self.delegate.services.append(self.delegate.servProv[i])
                self.delegate.servicesId.append(self.delegate.servProvId[i])
            }
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "datePick")
            self.present(vc!, animated: true, completion: nil)
            
            delegate.servDuration.removeAll()
            delegate.servPrice.removeAll()
            for i in self.delegate.servicesId{
                db.collection("services").document(i).getDocument { (document, error) in
                    if let document = document {
                        print("Document data: \(document.data()!["duration"])")
                        self.delegate.servDuration.append(document.data()!["duration"] as! Int)
                        self.delegate.servPrice.append(document.data()!["price"] as! Int)
                    } else {
                        print("Document does not exist")
                    }
                }
            }
            
            print("checked:\(checked)")
            print("services:\(self.delegate.services)")
            print("services:\(self.delegate.servicesId)")
        }
    }
    
    
    
}
