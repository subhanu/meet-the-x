//
//  accountViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 27/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Firebase
import FirebaseAuth
import FirebaseFirestore
import ChameleonFramework
import FBSDKCoreKit
import GoogleSignIn

class accountViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //UINavigationBar.appearance().barTintColor = UIColor(red: 234.0/255.0, green: 46.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        //UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        setDetails()
        setupLayout()
        // Do any additional setup after loading the view.
    }
    let delegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var viewScroll: UIView!
    var fName = SkyFloatingLabelTextField()
    var lName = SkyFloatingLabelTextField()
    var email = SkyFloatingLabelTextField()
    var mobile = SkyFloatingLabelTextField()
   // var gender = SkyFloatingLabelTextField()
    
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var genderField: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
   
   
    
    @IBOutlet weak var icon1: UILabel!
    @IBOutlet weak var iconMail: UILabel!
    @IBOutlet weak var iconGender: UILabel!
    @IBOutlet weak var iconAge: UILabel!
    @IBOutlet weak var iconPhone: UILabel!
    @IBOutlet weak var logoutBtn: UIButton!
   
    
    
    private func setDetails(){
      self.profilePic.image = self.delegate.profile_image
       
        
    }
    
    
    
    
    private func setupLayout(){
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        

        icon1.font = UIFont.fontAwesome(ofSize: 30)
        icon1.text = String.fontAwesomeIcon(name: .user)
        
        iconMail.font = UIFont.fontAwesome(ofSize: 30)
        iconMail.text = String.fontAwesomeIcon(name: .envelope)
        
        iconPhone.font = UIFont.fontAwesome(ofSize: 30)
        iconPhone.text = String.fontAwesomeIcon(name: .phone)
        
        iconGender.font = UIFont.fontAwesome(ofSize: 30)
        if(genderField.text == "Male"){
         iconGender.text = String.fontAwesomeIcon(name: .male)
        }else {
         iconGender.text = String.fontAwesomeIcon(name: .female)
        }
        
        iconAge.font = UIFont.fontAwesome(ofSize: 30)
        iconAge.text = String.fontAwesomeIcon(name: .genderless)
        
        
        logoutBtn.layer.shadowColor = UIColor.black.cgColor
        logoutBtn.layer.shadowOffset = CGSize(width: 3, height: 3)
        logoutBtn.layer.shadowRadius = 3
        logoutBtn.layer.shadowOpacity = 0.4
        logoutBtn.layer.cornerRadius = 6
        logoutBtn.backgroundColor = UIColor.flatBlack
        logoutBtn.addTarget(self, action: #selector(logout),for: .touchUpInside)

        
    }
    
    @objc func logout(){
        //signout users from firebase
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        // signout user of facebook
        FBSDKAccessToken.setCurrent(nil)
        GIDSignIn.sharedInstance().signOut()
        
        //send user to login screen after signout
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : UIViewController =  mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
        
        
        self.present(viewController, animated: true, completion: nil)
    }

}
