//
//  BarberSelectViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 13/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//imageUrl

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import FirebaseDatabase

class BarberSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var barberName = [String]()
    //let barbers = [ "barber1" , "barber2" , "barber3"]
    //let barber_names = ["Dorothe Sorensen" , "Julia Ruser" , "Dorianne"]
    let db = Firestore.firestore()
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        return (delegate.barberName.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BarberSelectTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.barbername.text = delegate.barberName[indexPath.row]
        cell.chooseBtn.tag = indexPath.row
        cell.specialization.text = delegate.serviceFor[indexPath.row]
        cell.chooseBtn.addTarget(self, action: #selector(btnSelected), for: .touchUpInside)
        let imgUrl = delegate.barberPicUrl[indexPath.row]
        let url = URL(string: imgUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                cell.barberimg.image = imahe
                self.delegate.allBarberPic.append(imahe)
            }
            
        }).resume()
        
        return (cell)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("service_id:\(self.delegate.event_service) ")
        print("service_Name:\(self.delegate.event_service_name) ")
        print("doc_id:\(self.delegate.doc_id) ")
        print("datetime:\(self.delegate.event_dateTime) ")
        print("pic_url:\(self.delegate.event_stylists_pic_url) ")
    }
    @objc private func btnSelected(sender: UIButton){
        let sv = UIViewController.displaySpinner(onView: self.view)
        print("row no. \(sender.tag)")
        delegate.row_no = sender.tag
        delegate.stylist = delegate.barberName[sender.tag]
        delegate.stylist_id = delegate.uid_stylists[sender.tag]
        delegate.stylist_pic_url = delegate.barberPicUrl[sender.tag]
        
        let imgUrl = delegate.barberPicUrl[sender.tag]
        let url = URL(string: imgUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                self.delegate.stylistPic["selected"] = imahe
                
            }
            
        }).resume()
       
        
        var array : [String] = self.delegate.barberServices[sender.tag + 1]
        print("services provided by the barber: \(self.delegate.barberServices[sender.tag + 1])")
        self.delegate.servProv.removeAll()
        self.delegate.servProvId.removeAll()
        
        for i in array{
            db.collection("services").document(i).getDocument { (document, error) in
                if let document = document {
                    
                    self.delegate.servProv.append(document.data()!["name"] as! String)
                    self.delegate.servProvId.append(i)
                    print("Document name: \(document.data()!["name"])")
                } else {
                    print("Document does not exist")
                }
            }
        }
        
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIViewController.removeSpinner(spinner: sv)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "chooseService")
            self.present(vc!, animated: true, completion: nil)
            
        }
        
        print(delegate.stylist)
        
    }
    private func fetch(){
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "signup")
        //self.present(vc!, animated: true, completion: nil)
        
        let storage = Storage.storage().reference()
        let database = Database.database().reference()
        
        
        
        
        db.collection("stylists").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    // print("\(document.documentID) => \(document.data() )")
                    let fname = document.data()["firstName"] as! String
                    let lname = document.data()["lastName"] as! String
                    let funame = fname + " " + lname
                    let imageUrl = document.data()["imageUrl"] as! String
                    
                    
                    
                    print(imageUrl)
                }
            }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
