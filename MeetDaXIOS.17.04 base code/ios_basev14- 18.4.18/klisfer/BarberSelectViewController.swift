//
//  BarberSelectViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 13/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//imageUrl

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import FirebaseDatabase


class BarberSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var barberName = [String]()
       let db = Firestore.firestore()
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tabBar: UINavigationItem!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        return (delegate.barberName.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BarberSelectTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.barbername.text = delegate.barberName[indexPath.row]
        cell.chooseBtn.tag = indexPath.row
        cell.specialization.text = delegate.serviceFor[indexPath.row]
        cell.chooseBtn.addTarget(self, action: #selector(btnSelected), for: .touchUpInside)
        let imgUrl = delegate.barberPicUrl[indexPath.row]
        let url = URL(string: imgUrl)
        cell.barberimg.kf.setImage(with: url)
        

        return (cell)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: nil, action: "selector");
        UINavigationBar.appearance().barTintColor = .black
        
        
        let btn1 = UIButton(type: .custom)
        btn1.setTitle("Skip", for: .normal)
        btn1.setTitleColor(UIColor(hexString: "#ffffffff"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 44 , width: 30, height: 30)
        btn1.addTarget(self, action: #selector(skipAction),for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        if(self.delegate.update_id != nil){
            self.tabBar.rightBarButtonItem = item1;
            
        }
    }
    
    @objc private func skipAction(sender: UIButton){
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        delegate.row_no = sender.tag
        let update_id = delegate.update_id
        let index  = delegate.doc_id.index(of: update_id!)
        print("stylist_name: \(delegate.event_stylists_name)")
        print("stylist_id: \(delegate.event_stylists_id)")
        
        delegate.stylist = delegate.event_stylists_name[index!]
        delegate.stylist_id = delegate.event_stylists_id[index!]
        let stylist_id = delegate.event_stylists_id[index!]
        print("index: \(index!)")
        print("stylist_name: \(delegate.event_stylists_name[index!])")
        print("stylist_id: \(delegate.event_stylists_id[index!])")
        delegate.stylist_pic_url = delegate.event_stylists_pic_url[index!]

        let imgUrl = delegate.barberPicUrl[sender.tag]
        let url = URL(string: imgUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                self.delegate.stylistPic["selected"] = imahe
                
            }
            
        }).resume()
        
        var index2 = self.delegate.uid_stylists.index(of: stylist_id)
        var array : [String] = self.delegate.barberServices[index2!]
        print("services provided by the barber: \(self.delegate.barberServices[index2!])")
        self.delegate.servProv.removeAll()
        self.delegate.servProvId.removeAll()
        
        for i in array{
            db.collection("services").document(i).getDocument { (document, error) in
                if let document = document {
                    
                    self.delegate.servProv.append(document.data()!["name"] as! String)
                    self.delegate.servProvId.append(i)
                    print("Document name: \(document.data()!["name"])")
                } else {
                    print("Document does not exist")
                }
            }
        }
        print("services provided by the barber: \(self.delegate.servProv)")
        print("services provided by the barber: \(self.delegate.servProvId)")
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            UIViewController.removeSpinner(spinner: sv)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "chooseService")
            //self.present(vc!, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc!, animated:
                true)
            
        }
        
        print(delegate.stylist)
        fetchingDataSlots()
    
    }
    
    @objc private func btnSelected(sender: UIButton){
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        print("row no. \(sender.tag)")
        delegate.row_no = sender.tag
        delegate.stylist = delegate.barberName[sender.tag]
        delegate.stylist_id = delegate.uid_stylists[sender.tag]
        delegate.stylist_pic_url = delegate.barberPicUrl[sender.tag]
        
        let imgUrl = delegate.barberPicUrl[sender.tag]
        let url = URL(string: imgUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                self.delegate.stylistPic["selected"] = imahe
                
            }
            
        }).resume()
        
        
        var array : [String] = self.delegate.barberServices[sender.tag]
        print("services provided by the barber: \(self.delegate.barberServices[sender.tag])")
        self.delegate.servProv.removeAll()
        self.delegate.servProvId.removeAll()
        
        for i in array{
            db.collection("services").document(i).getDocument { (document, error) in
                if let document = document {
                    
                    self.delegate.servProv.append(document.data()!["name"] as! String)
                    self.delegate.servProvId.append(i)
                    print("Document name: \(document.data()!["name"])")
                } else {
                    print("Document does not exist")
                }
            }
        }
        
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            UIViewController.removeSpinner(spinner: sv)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "chooseService")
            //self.present(vc!, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc!, animated:
                true)
            
        }
        
        print(delegate.stylist)
        fetchingDataSlots()
    }
    private func fetch(){
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "signup")
        //self.present(vc!, animated: true, completion: nil)
        
        let storage = Storage.storage().reference()
        let database = Database.database().reference()
        
        
        
        
        db.collection("stylists").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    // print("\(document.documentID) => \(document.data() )")
                    let fname = document.data()["firstName"] as! String
                    let lname = document.data()["lastName"] as! String
                    let funame = fname + " " + lname
                    let imageUrl = document.data()["imageUrl"] as! String
                    
                    
                    
                    print(imageUrl)
                }
            }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
