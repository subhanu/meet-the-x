//
//  ViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 28/02/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FBSDKCoreKit
import FirebaseAuth
import GoogleSignIn
import FirebaseFirestore
import FontAwesome_swift
import Material
import SkyFloatingLabelTextField


class ViewController: UIViewController,FBSDKLoginButtonDelegate,GIDSignInUIDelegate,UITextFieldDelegate ,GIDSignInDelegate{
    
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var dbt : Firestore!
    var loginButton: FBSDKLoginButton = FBSDKLoginButton()
    var googleButton : GIDSignInButton = GIDSignInButton()
    var email = SkyFloatingLabelTextField()
    var password = SkyFloatingLabelTextField()
    var loginbtn = UIButton()
    @objc var signup = UIButton()
    var fbgogl = UITextView()
    var frgt = UITextView()
    var fbbtn = UIButton()
    var goglbtn = UIButton()
    var logo = UIImageView()
    
    @IBOutlet weak var aiLoadingSpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        Auth.auth().addStateDidChangeListener { (auth, user) in
//            if user != nil{
//                // move the user to homescreen
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let homeViewController : UIViewController =  mainStoryboard.instantiateViewController(withIdentifier: "TabBar")
//
//
//                self.present(homeViewController, animated: true, completion: nil)
//
//            }
//        }
        
        
        //self.setupFacebookButtons()
        //self.setupGoogleButtons()
        dbt = Firestore.firestore()
        setupLayout()
        print(view.frame.height)
        //        GIDSignIn.sharedInstance().delegate = self as! GIDSignInDelegate
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        //checks whether user is logged in or not
        //setupFacebookButtons()
        
        if let fb = FBSDKAccessToken.current()
        {
            fetchUserProfile()
        }
        
        
        
        
        
    }
    
    
    private func setupLayout(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        
        email.frame = CGRect(x: 20, y: screenHeight - 410 , width: screenWidth-40, height: 45)
        //email.placeholder = "Email"
        //email.font = UIFont.systemFont(ofSize: 15)
        //email.borderStyle = UITextBorderStyle.roundedRect
        //email.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hexString: "#95989Aff")])
        //email.autocorrectionType = UITextAutocorrectionType.no
        email.keyboardType = UIKeyboardType.default
        email.returnKeyType = UIReturnKeyType.done
        email.clearButtonMode = UITextFieldViewMode.whileEditing;
        email.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        email.tintColor = UIColor(hexString: "#20BFC6ff")
        email.lineHeight = 1.0 // bottom line height in points
        email.selectedLineHeight = 2.0
        email.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        email.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        email.textColor = UIColor.white
        email.title = "Email"
        view.addSubview(email)
        
        
        
        password.frame = CGRect(x: 20, y: screenHeight - 360 , width: screenWidth-40, height: 45)
        //password.placeholder = "Password"
        password.font = UIFont.systemFont(ofSize: 15)
        //password.borderStyle = UITextBorderStyle.roundedRect
        password.attributedPlaceholder = NSAttributedString(string: "Password",
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor(hexString: "#95989Aff")])
        password.textColor = UIColor.white
        password.autocorrectionType = UITextAutocorrectionType.no
        password.keyboardType = UIKeyboardType.default
        password.returnKeyType = UIReturnKeyType.done
        password.clearButtonMode = UITextFieldViewMode.whileEditing;
        password.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        password.tintColor = UIColor(hexString: "#20BFC6ff")
        //password.textColor = UIColor.mf_veryDarkGray()
        password.title = "Password"
        password.lineHeight = 1.0 // bottom line height in points
        password.selectedLineHeight = 2.0
        password.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        password.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        view.addSubview(password)
        
        
        loginbtn.frame = CGRect(x: 20, y: screenHeight - 280 , width: screenWidth-40, height: 50)
        loginbtn.backgroundColor = UIColor(hexString: "#1C92AFff")
        loginbtn.layer.shadowColor = UIColor.black.cgColor
        loginbtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        loginbtn.layer.shadowRadius = 5
        loginbtn.layer.shadowOpacity = 1.0
        loginbtn.setTitle("LOG IN", for: UIControlState.normal)
        loginbtn.tintColor = UIColor.black
        
        loginbtn.layer.cornerRadius = 6
        loginbtn.addTarget(self, action: #selector(loginfu),for: .touchUpInside)
        view.addSubview(loginbtn)
        
        
        
        
        signup.frame = CGRect(x: 20, y: screenHeight - 220 , width: screenWidth-40, height: 50)
        signup.backgroundColor = UIColor.clear
        signup.setTitle("Don't have an account? SignUp", for: UIControlState.normal)
        signup.tintColor = UIColor.black
        signup.addTarget(self, action: #selector(check),for: .touchUpInside)
        view.addSubview(signup)
        
        fbgogl.frame = CGRect(x: 20, y: screenHeight - 150 , width: screenWidth-40, height: 30)
        fbgogl.text = "Login/Signup with Facebook/Google+"
        fbgogl.textColor = .white
        fbgogl.textAlignment = .center
        fbgogl.backgroundColor = UIColor.clear
        fbgogl.font = UIFont.systemFont(ofSize: 17)
        view.addSubview(fbgogl)
        
        
        
        fbbtn.frame = CGRect(x: 20, y: view.frame.height-100, width: view.frame.width/2 - 40, height: 50)
        fbbtn.backgroundColor = UIColor(hexString: "#3B5998ff")
        fbbtn.layer.shadowColor = UIColor.black.cgColor
        fbbtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        fbbtn.layer.shadowRadius = 5
        fbbtn.layer.shadowOpacity = 1.0
        //fbbtn.setTitle("FACEBOOK", for: UIControlState.normal)
        fbbtn.tintColor = UIColor.black
        fbbtn.titleLabel?.font = UIFont.fontAwesome(ofSize: 15)
        fbbtn.setTitle(String.fontAwesomeIcon(name: .facebook) + "  " + "FACEBOOK", for: .normal)
        fbbtn.layer.cornerRadius = 6
        fbbtn.addTarget(self, action: #selector(fbBtnLogin),for: .touchUpInside)
        view.addSubview(fbbtn)
        
        goglbtn.frame = CGRect(x:view.frame.width/2 + 16 , y: view.frame.height-100, width: view.frame.width/2 - 40, height: 50)
        goglbtn.backgroundColor = UIColor(hexString: "#db3236ff")
        goglbtn.layer.shadowColor = UIColor.black.cgColor
        goglbtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        goglbtn.layer.shadowRadius = 5
        goglbtn.layer.shadowOpacity = 1.0
        
        goglbtn.tintColor = UIColor.black
        goglbtn.titleLabel?.font = UIFont.fontAwesome(ofSize: 15)
        goglbtn.setTitle(String.fontAwesomeIcon(name: .googlePlus) + " " + " GOOGLE" , for: .normal)
        goglbtn.layer.cornerRadius = 6
        goglbtn.addTarget(self, action: #selector(googleLogin),for: .touchUpInside)
        view.addSubview(goglbtn)
        
        var image: UIImage = UIImage(named: "logo-white-center")!
        logo = UIImageView(image: image)
        logo.frame = CGRect(x: (view.frame.width - (2/7)*view.frame.height)/2,y: (0.1125)*view.frame.height ,width: (2/7)*view.frame.height ,height: (1/7)*view.frame.height )
        
        view.addSubview(logo)
        
    }
    @objc func googleLogin(){
        //let spin = UIViewController.displaySpinner(onView: self.view)
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    //GID SIGN IN -- google signin
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error{
            print(error.localizedDescription)
            return
        }
        print(user.profile.email)
        let spin = UIViewController.displaySpinner(onView: self.view)
        //fetching user profile pic from google+ signin
        let url = (user.profile.imageURL(withDimension: 300))
        //let request = URLRequest(url: url!)
        let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
            if error != nil{
                print (error)
                return
            }
            DispatchQueue.main.async{
                let imahe : UIImage = UIImage(data: data!)!
                self.delegate.profile_image = imahe
            }
            
        }).resume()
        
        
        
        
        
        
        
        print("user signed in google+")
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }else{
                
                print("user got signed in Firebase")
                UIViewController.removeSpinner(spinner: spin)
                let mail = user?.email
                let fbRef = self.dbt.collection("Customers")
                let quer = fbRef.whereField("email", isEqualTo: mail!)
                
                quer.getDocuments{(querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        var count = 0;
                        
                        for document in querySnapshot!.documents {
                            count = count + 1
                            if(count == 1){
                                self.delegate.cust_name = (user?.displayName)!
                                self.delegate.cust_mail = mail!
                                self.delegate.cust_age = document.data()["age"] as! String
                                self.delegate.cust_phone = document.data()["phone"] as! String
                                self.delegate.cust_gender = document.data()["gender"] as! String
                                
                            }
                        }
                        if(count == 1){
                            UIViewController.removeSpinner(spinner: spin)
                            self.fetchingData()
                        }else if(count == 0){
                            UIViewController.removeSpinner(spinner: spin)
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeView")
                            self.present(vc!, animated: true, completion: nil)
                        }
                    }
                }
                
            }
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: NSError!) {
        if let error = error{
            print(error.localizedDescription)
            return
        }
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    
    
    
    
    
    
    // don't have an account button action
    @objc func check(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "signup")
        self.present(vc!, animated: true, completion: nil)
    }
    
    
    
    // fb login button action
    @objc func fbBtnLogin(){
        let spin2 = UIViewController.displaySpinner(onView: self.view)
        var fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email", "public_profile"], handler: { (result,  error) -> Void in
            
            
            if (error == nil){
                var fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if(fbloginresult.isCancelled) {
                    UIViewController.removeSpinner(spinner: spin2)
                    
                } else if(fbloginresult.grantedPermissions.contains("email")) {
                    print("fb result is:: \(result)")
                    let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                    Auth.auth().signIn(with: credential) { (user, err) in
                        if err != nil {
                            print("user login failed", err)
                            return
                        }else{
                            let mail = user?.email
                            let fbRef = self.dbt.collection("Customers")
                            let quer = fbRef.whereField("email", isEqualTo: mail!)
                            
                            quer.getDocuments{(querySnapshot, err) in
                                if let err = err {
                                    print("Error getting documents: \(err)")
                                } else {
                                    var count = 0;
                                    
                                    for document in querySnapshot!.documents {
                                        count = count + 1
                                        if(count == 1){
                                            self.delegate.cust_name = (user?.displayName)!
                                            self.delegate.cust_mail = mail!
                                            self.delegate.cust_age = document.data()["age"] as! String
                                            self.delegate.cust_phone = document.data()["phone"] as! String
                                            self.delegate.cust_gender = document.data()["gender"] as! String
                                        }
                                    }
                                    if(count == 1){
                                        UIViewController.removeSpinner(spinner: spin2)
                                        self.fetchingData()
                                    }else if(count == 0){
                                        UIViewController.removeSpinner(spinner: spin2)
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeView")
                                        self.present(vc!, animated: true, completion: nil)
                                    }
                                }
                            }
                            
                        }
                        
                    }
                    
                }
            }
        })
        
    }
    
    
    
    
    @objc private func loginfu(sender: Any){
        
        if self.email.text == "" || self.password.text == "" {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            Auth.auth().signIn(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
                let spin2 = UIViewController.displaySpinner(onView: self.view)
                if error == nil {
                    
                    //Print into the console if successfully logged in
                    print("You have successfully signed in")
                    let mail = user?.email
                    let fbRef = self.dbt.collection("Customers")
                    let quer = fbRef.whereField("email", isEqualTo: mail!)
                    
                    quer.getDocuments{(querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            var count = 0;
                            
                            for document in querySnapshot!.documents {
                                count = count + 1
                                if(count == 1){
                                    self.delegate.cust_name = (user?.displayName)!
                                    self.delegate.cust_mail = mail!
                                    self.delegate.cust_age = document.data()["age"] as! String
                                    self.delegate.cust_phone = document.data()["phone"] as! String
                                    self.delegate.cust_gender = document.data()["gender"] as! String
                                }
                            }
                            if(count == 1){
                                UIViewController.removeSpinner(spinner: spin2)
                                self.fetchingData()
                            }else if(count == 0){
                                UIViewController.removeSpinner(spinner: spin2)
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeView")
                                self.present(vc!, animated: true, completion: nil)
                            }
                        }
                    }
                    
                    
                    
                } else {
                    UIViewController.removeSpinner(spinner: spin2)
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if email.isFirstResponder == true {
            email.placeholder = nil
        }
    }
    
    //facebook button
    private func setupFacebookButtons(){
        
        self.loginButton.center = self.view.center
        self.loginButton.readPermissions = ["public_profile","email","user_friends"]
        self.loginButton.delegate = self
        self.loginButton.frame = CGRect(x: 20, y: view.frame.height-96, width: view.frame.width/2 - 40, height: 42)
        self.view!.addSubview(self.loginButton)
        self.loginButton.isHidden = false
        self.loginButton.layer.shadowColor = UIColor.black.cgColor
        self.loginButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.loginButton.layer.shadowRadius = 5
        self.loginButton.layer.shadowOpacity = 1.0
        self.loginButton.backgroundColor = UIColor(hexString : "#3B5998ff")
        
    }
    
    
    //google+ button
    private func setupGoogleButtons(){
        
        self.googleButton.frame = CGRect(x:view.frame.width/2 + 16 , y: view.frame.height-100, width: view.frame.width/2 - 40, height: 50)
        view.addSubview(googleButton)
        self.googleButton.layer.shadowColor = UIColor.black.cgColor
        self.googleButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.googleButton.layer.shadowRadius = 5
        self.googleButton.layer.shadowOpacity = 1.0
        self.googleButton.layer.cornerRadius = 16
        
        //GIDSignIn.sharedInstance().uiDelegate = self
        
        
        
        
    }
    
    //login with google is handled in AppDelegate
    /*@objc func buttonAction(){
     if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
     self.present(vc!, animated: true, completion: nil)
     }
     }*/
    
    
    
    
    // login with facebook into firebase
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result:FBSDKLoginManagerLoginResult!, error: Error! ){
        print("User Logged In")
        aiLoadingSpinner.startAnimating()
        
        if(result.isCancelled){
            //handle signin cancel event
            self.loginButton.isHidden = false
            aiLoadingSpinner.stopAnimating()
        }else{
            
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, err) in
                if err != nil {
                    print("user login failed")
                    return
                }else{
                    let mail = user?.email
                    let fbRef = self.dbt.collection("Customers")
                    let quer = fbRef.whereField("email", isEqualTo: mail!)
                    
                    quer.getDocuments{(querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            var count = 0;
                            
                            for document in querySnapshot!.documents {
                                count = count + 1
                            }
                            //let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
                            //self.present(vc!, animated: true, completion: nil)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    func fetchUserProfile()
    {
        print("fetched user profile")
        let parameters = ["fields":"id, email, name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me/picture" , parameters: ["height": 300 , "width" : 300 , "redirect": false], httpMethod: "GET" ).start{ (connection , result , error) -> Void in
            
            if error != nil{
                print(error)
                return
            }
            let dict = result as? NSDictionary
            let data = dict?.object(forKey: "data") as? NSDictionary
            let picUrl = (data?.object(forKey: "url") as? String)!
            let url = URL(string: picUrl)!
            //let data = pict?.object(forKey: "url") as? String
            let request = URLRequest(url: url)
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response , error) in
                if error != nil{
                    print (error)
                    return
                }
                DispatchQueue.main.async{
                    let imahe : UIImage = UIImage(data: data!)!
                    self.delegate.profile_image = imahe
                    
                }
                
            }).resume()
            
            
        }
        
    }
    
    func loginButtonDidLogOut(_ loginButton:FBSDKLoginButton!){
        print("User did LogOut")
    }
    
}



