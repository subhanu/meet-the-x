//
//  signViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 05/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SkyFloatingLabelTextField

class signViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate , UIPickerViewDelegate , UIPickerViewDataSource ,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderSelect.text = genders[row]
        self.view.endEditing(false)
    }
    
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var email = SkyFloatingLabelTextField()
    var password = SkyFloatingLabelTextField()
    var fullname = SkyFloatingLabelTextField()
    var mobile = SkyFloatingLabelTextField()
    var genderSelect = SkyFloatingLabelTextField()
    var gender = SkyFloatingLabelTextField()
    var age = SkyFloatingLabelTextField()
    var terms = UILabel()
    var SignUpPage = UILabel()
    let confirmpassword = SkyFloatingLabelTextField()
    let db = Firestore.firestore()
    var servc : [String] = []
    let picker = UIPickerView()
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //@IBOutlet weak var email: SkyFloatingLabelTextField!
    //
    //    @IBOutlet weak var password: SkyFloatingLabelTextField!
    //
    //    @IBOutlet weak var fullname: SkyFloatingLabelTextField!
    //
    //    @IBOutlet weak var mobile: SkyFloatingLabelTextField!
    //
    @IBOutlet weak var agree: UITextView!
    //    @IBOutlet weak var gender: SkyFloatingLabelTextField!
    //
    //    @IBOutlet weak var age: SkyFloatingLabelTextField!
    //    @IBOutlet weak var confirmpassword: SkyFloatingLabelTextField!
    @IBOutlet weak var viewScroll: UIView!
    //
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var myImage: UIImageView!
    //     @IBOutlet weak var genderSelect: UITextField!
    var genders = ["Male", "Female", "Others"]
    @IBAction func importImage(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source" , message: "choose a source" , preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera" , style: .default , handler: {(action: UIAlertAction) in image.sourceType = .camera
            self.present(image , animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library" , style: .default , handler: {(action: UIAlertAction) in image.sourceType = .photoLibrary
            self.present(image , animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel" , style: .cancel , handler: nil ))
        
        self.present(actionSheet , animated: true , completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nav = self.navigationController?.navigationBar
        
        // 2
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow
        setupLayout()
        // Do any additional setup after loading the view.
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        myImage.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //Frontent - Layout setup
    
    private func setupLayout(){
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        
        //view
        
        self.viewScroll.backgroundColor = UIColor.clear
        
        //Label
        //descriptionTextView.frame = CGRect(x: 20, y: 100, width: screenWidth-40, height: 40)
        
        descriptionTextView.text = "Create an Account"
        //descriptionTextView.font = UIFont(ofSize: 24)
        descriptionTextView.font = UIFont(name: "Helvetica", size: 24)
        descriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        descriptionTextView.textAlignment = .center
        descriptionTextView.isEditable = false
        descriptionTextView.isScrollEnabled = false
        descriptionTextView.textColor = UIColor(hexString: "#008F95ff")
        
        
        descriptionTextView.backgroundColor = UIColor.clear
        viewScroll.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: viewScroll.topAnchor, constant: 150).isActive = true
        descriptionTextView.centerXAnchor.constraint(equalTo: viewScroll.centerXAnchor).isActive = true
        
        
        //text Field email
        myImage.frame =  CGRect(x: (screenWidth - 0.15*screenHeight)/2, y: 200, width: 0.15*screenHeight , height: 0.15*screenHeight)
        myImage.contentMode = .scaleToFill
        
        uploadBtn.frame =  CGRect(x: (screenWidth - 0.15*screenHeight)/2 , y: 200 + 0.15*screenHeight , width: 0.15*screenHeight , height: 40)
        uploadBtn.layer.shadowColor = UIColor.black.cgColor
        uploadBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        uploadBtn.layer.shadowRadius = 5
        uploadBtn.layer.shadowOpacity = 1.0
        uploadBtn.layer.cornerRadius = 2
        uploadBtn.backgroundColor = UIColor(hexString: "#20BFC6ff")
        viewScroll.addSubview(uploadBtn)
        
        
        email.frame = CGRect(x: 20, y: 390, width: screenWidth-40, height: 40)
        
        //email.placeholder = "Email"
        email.font = UIFont.systemFont(ofSize: 15)
        //email.borderStyle = UITextBorderStyle.roundedRect
        //email.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hexString: "#95989Aff")])
        //email.autocorrectionType = UITextAutocorrectionType.no
        email.keyboardType = UIKeyboardType.default
        email.returnKeyType = UIReturnKeyType.done
        email.tintColor = .cyan
        email.clearButtonMode = UITextFieldViewMode.whileEditing;
        email.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(email)
        email.tintColor = UIColor(hexString: "#20BFC6ff")
        email.lineHeight = 1.0 // bottom line height in points
        email.selectedLineHeight = 2.0
        email.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        email.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        email.textColor = UIColor(hexString: "#000000ff")!
        email.title = "Email"
        
        
        
        //text Field password
        
        password.frame = CGRect(x: 20, y: 450, width: screenWidth-40, height: 40)
        //password.placeholder = "Password"
        password.font = UIFont.systemFont(ofSize: 15)
        // password.borderStyle = UITextBorderStyle.roundedRect
        password.autocorrectionType = UITextAutocorrectionType.no
        password.keyboardType = UIKeyboardType.default
        password.returnKeyType = UIReturnKeyType.done
        password.clearButtonMode = UITextFieldViewMode.whileEditing;
        password.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        // password.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        viewScroll.addSubview(password)
        password.tintColor = UIColor(hexString: "#20BFC6ff")
        password.lineHeight = 1.0 // bottom line height in points
        password.selectedLineHeight = 2.0
        password.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        password.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        password.textColor = UIColor(hexString: "#000000ff")!
        password.title = "PASSWORD"
        //text Field confirm password
        
        confirmpassword.frame = CGRect(x: 20, y: 510, width: screenWidth-40, height: 40)
        //confirmpassword.placeholder = "Confirm Password"
        confirmpassword.font = UIFont.systemFont(ofSize: 15)
        //confirmpassword.borderStyle = UITextBorderStyle.roundedRect
        confirmpassword.autocorrectionType = UITextAutocorrectionType.no
        confirmpassword.keyboardType = UIKeyboardType.default
        confirmpassword.returnKeyType = UIReturnKeyType.done
        confirmpassword.clearButtonMode = UITextFieldViewMode.whileEditing;
        confirmpassword.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(confirmpassword)
        // confirmpassword.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        confirmpassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        confirmpassword.tintColor = UIColor(hexString: "#20BFC6ff")
        confirmpassword.lineHeight = 1.0 // bottom line height in points
        confirmpassword.selectedLineHeight = 2.0
        confirmpassword.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        confirmpassword.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        confirmpassword.textColor = UIColor(hexString: "#000000ff")!
        confirmpassword.title = "Confirm Password"
        //text Field full name
        
        fullname.frame = CGRect(x: 20, y: 570, width: screenWidth-40, height: 40)
        //fullname.placeholder = "First Name"
        fullname.font = UIFont.systemFont(ofSize: 15)
        //fullname.borderStyle = UITextBorderStyle.roundedRect
        fullname.autocorrectionType = UITextAutocorrectionType.no
        fullname.keyboardType = UIKeyboardType.default
        fullname.returnKeyType = UIReturnKeyType.done
        fullname.clearButtonMode = UITextFieldViewMode.whileEditing;
        fullname.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(fullname)
        // fullname.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        fullname.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        //text Field Mobile
        fullname.tintColor = UIColor(hexString: "#20BFC6ff")
        fullname.lineHeight = 1.0 // bottom line height in points
        fullname.selectedLineHeight = 2.0
        fullname.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        fullname.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        fullname.textColor = UIColor(hexString: "#000000ff")!
        fullname.title = "First Name"
        
        
        
        
        mobile.frame = CGRect(x: 20, y: 630, width: screenWidth-40, height: 40)
        //mobile.placeholder = "Mobile Number"
        mobile.font = UIFont.systemFont(ofSize: 15)
        //mobile.borderStyle = UITextBorderStyle.roundedRect
        mobile.autocorrectionType = UITextAutocorrectionType.no
        mobile.keyboardType = UIKeyboardType.default
        mobile.returnKeyType = UIReturnKeyType.done
        mobile.clearButtonMode = UITextFieldViewMode.whileEditing;
        mobile.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(mobile)
        // mobile.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        mobile.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        mobile.tintColor = UIColor(hexString: "#20BFC6ff")
        mobile.lineHeight = 1.0 // bottom line height in points
        mobile.selectedLineHeight = 2.0
        mobile.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        mobile.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        mobile.textColor = UIColor(hexString: "#000000ff")!
        mobile.title = "Mobile Number"
        
        
        //text Field gender
        
        gender.frame = CGRect(x: 20, y: 690, width: screenWidth-40, height: 40)
        //gender.placeholder = "Last Name"
        gender.font = UIFont.systemFont(ofSize: 15)
        // gender.borderStyle = UITextBorderStyle.roundedRect
        gender.autocorrectionType = UITextAutocorrectionType.no
        gender.keyboardType = UIKeyboardType.default
        gender.returnKeyType = UIReturnKeyType.done
        gender.clearButtonMode = UITextFieldViewMode.whileEditing;
        gender.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(gender)
        //  gender.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        gender.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        gender.tintColor = UIColor(hexString: "#20BFC6ff")
        gender.lineHeight = 1.0 // bottom line height in points
        gender.selectedLineHeight = 2.0
        gender.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        gender.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        gender.textColor = UIColor(hexString: "#000000ff")!
        gender.title = "Last Name"
        
        
        //text Field age
        
        age.frame = CGRect(x: 20, y: 750, width: screenWidth-40, height: 40)
        //age.placeholder = "Age"
        age.font = UIFont.systemFont(ofSize: 15)
        //age.borderStyle = UITextBorderStyle.roundedRect
        age.autocorrectionType = UITextAutocorrectionType.no
        age.keyboardType = UIKeyboardType.default
        age.returnKeyType = UIReturnKeyType.done
        age.clearButtonMode = UITextFieldViewMode.whileEditing;
        age.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        viewScroll.addSubview(age)
        //  age.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        age.attributedPlaceholder = NSAttributedString(string: "Age", attributes: [NSAttributedStringKey.foregroundColor:  UIColor(hexString: "#95989Aff")])
        age.tintColor = UIColor(hexString: "#20BFC6ff")
        age.lineHeight = 1.0 // bottom line height in points
        age.selectedLineHeight = 2.0
        age.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        age.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        age.textColor = UIColor(hexString: "#000000ff")!
        age.title = "Age"
        //gender picker
        
        picker.delegate = self
        picker.dataSource = self
        genderSelect.inputView = picker
        genderSelect.frame = CGRect(x: 20, y: 810, width: screenWidth-40, height: 40)
        //genderSelect.placeholder = "Gender"
        genderSelect.font = UIFont.systemFont(ofSize: 15)
        genderSelect.borderStyle = UITextBorderStyle.none
        viewScroll.addSubview(genderSelect)
        genderSelect.textColor = UIColor(hexString: "#000000ff")
        //genderSelect.backgroundColor = UIColor(red: 0, green:0,  blue:0 ,alpha: 0.4)
        genderSelect.attributedPlaceholder = NSAttributedString(string: "Gender", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hexString: "#95989Aff")])
        genderSelect.tintColor = UIColor(hexString: "#20BFC6ff")
        genderSelect.lineHeight = 1.0 // bottom line height in points
        genderSelect.selectedLineHeight = 2.0
        genderSelect.selectedTitleColor = UIColor(hexString: "#20BFC6ff")!
        genderSelect.selectedLineColor = UIColor(hexString: "#20BFC6ff")!
        genderSelect.textColor = UIColor(hexString: "#000000ff")!
        genderSelect.title = "GENDER"
        genderSelect.layer.masksToBounds = true
        
        
        terms.frame = CGRect(x: 20, y: 850, width: screenWidth-40, height: 40)
        terms.text = "By creating a profile you agree to the terms and conditions of Barbershop"
        terms.textColor = UIColor.black
        terms.font = UIFont.systemFont(ofSize: 12)
        terms.backgroundColor = UIColor.clear
        terms.textAlignment = .left
        terms.numberOfLines = 0
        
        viewScroll.addSubview(terms)
        //checkbox
        //        let checkbox = Checkbox(frame: CGRect(x: 30, y: 780, width: 25, height: 25))
        //        checkbox.borderStyle = .square
        //        checkbox.checkmarkColor = .blue
        //        checkbox.checkmarkStyle = .circle
        //        checkbox.borderStyle = .circle
        //        viewScroll.addSubview(checkbox)
        
        
        //agree terms
        
        //agree.frame = CGRect(x: 65, y: 740, width: screenWidth-85, height: 60)
        agree.text = "I agree to receive news, exclusive offers & more from barbershop. You can unsubscribe any time"
        agree.font = UIFont.systemFont(ofSize: 12)
        agree.translatesAutoresizingMaskIntoConstraints = false
        agree.textAlignment = .center
        agree.isEditable = false
        agree.isScrollEnabled = false
        agree.textColor = .black
        agree.backgroundColor = UIColor.clear
        agree.textAlignment = .left
        viewScroll.addSubview(agree)
        
        
        //button for SignUp
        let signupbutton = UIButton(frame: CGRect(x: 20, y: 990 , width: screenWidth - 40 , height: 50))
        signupbutton.setTitle("SIGN UP", for: UIControlState.normal)
        signupbutton.tintColor = UIColor.black
        signupbutton.backgroundColor = UIColor(hexString: "#20BFC6ff")
        signupbutton.addTarget(self, action: #selector(buttonAction),for: .touchUpInside)
        signupbutton.layer.shadowColor = UIColor.black.cgColor
        signupbutton.layer.shadowOffset = CGSize(width: 5, height: 5)
        signupbutton.layer.shadowRadius = 6
        signupbutton.layer.shadowOpacity = 0.4
        signupbutton.layer.cornerRadius = 6
        viewScroll.addSubview(signupbutton)
        
        
        SignUpPage.frame = CGRect(x: 20, y: 1045, width: screenWidth-40, height: 40)
        SignUpPage.text = "Don't have an account? Sign Up"
        SignUpPage.textColor = UIColor.black
        SignUpPage.font = UIFont.systemFont(ofSize: 12)
        SignUpPage.backgroundColor = UIColor.clear
        SignUpPage.textAlignment = .center
        SignUpPage.numberOfLines = 0
        
        viewScroll.addSubview(SignUpPage)
        
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @objc func buttonAction(sender: UIButton!) {
        
        // delegate.customer = fullname.text! + " " + gender.text!
        
        fetchingData()
        
        //authenticate email signup users.
        if email.text == "" || password.text == "" || fullname.text == "" || mobile.text == "" || age.text == ""  {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "All Details are mandatory", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if  password.text != confirmpassword.text {
            
            
            
            let alertController = UIAlertController(title: "Error", message: "Passwords dont match", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }else {
            uploadMedia() { (url) in
                Auth.auth().createUser(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
                    
                    if error == nil {
                        
                        //Print into the console if successfully logged in
                        print("You have successfully signed in")
                        
                        //write to db
                        var ref: DocumentReference? = nil
                        ref = self.db.collection("Customers").addDocument(data: [
                            "email": self.email.text!,
                            "firstName": self.fullname.text!,
                            "lastName": self.gender.text!,
                            "phone" :  self.mobile.text!,
                            "age" : self.age.text!,
                            "imageUrl" : url!
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        //delegate.customer = self.fullname.text + " " + self.gender.text
                        
                        //Go to the HomeViewController if the login is sucessful
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
                        self.present(vc!, animated: true, completion: nil)
                    } else {
                        
                        //Tells the user that there is an error and then gets firebase to tell them the error
                        let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    func uploadMedia(completion: @escaping (_ url: String?) -> Void) {
        let storageRef = Storage.storage().reference().child("customers/\(Auth.auth().currentUser?.uid).png")
        if let uploadData = UIImagePNGRepresentation(self.myImage.image!) {
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error")
                    completion(nil)
                } else {
                    completion((metadata?.downloadURL()?.absoluteString)!)
                    // your uploaded photo url.
                }
            }
        }
        
    }
    
    
    
}

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

extension UIViewController {
    func fetchingData() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let user = Auth.auth().currentUser
        let mail = user?.email
        var cust_id = String()
        let sv = UIViewController.displaySpinner(onView: self.view)
        let db = Firestore.firestore()
        let myGroup = DispatchGroup()
        DispatchQueue.global(qos: .userInitiated).async {
            db.collection("Customers").whereField("email", isEqualTo: mail).getDocuments(){ (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    myGroup.enter()
                    for document in querySnapshot!.documents {
                        cust_id = document.data()["uid"] as! String
                        delegate.customer_id = cust_id
                        delegate.customer = mail!
                        
                        
                        
                    }
                }
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIViewController.removeSpinner(spinner: sv)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar")
                self.present(vc!, animated: true, completion: nil)
            }
            
            
            
            
            
        }
        
        
        // fetch details for Choose barber controller
        db.collection("stylists").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                delegate.barberName.removeAll()
                delegate.uid_stylists.removeAll()
                delegate.barberPicUrl.removeAll()
                delegate.allBarberPic.removeAll()
                delegate.barberServices.removeAll()
                delegate.serviceFor.removeAll()
                delegate.all_barbers.removeAll()
                
                for document in querySnapshot!.documents {
                    
                    let fname = document.data()["firstName"] as! String
                    let lname = document.data()["lastName"] as! String
                    let funame = fname + " " + lname
                    let uid = document.documentID as! String
                    let imgUrl = document.data()["imageUrl"] as! String
                    
                    
                    delegate.all_barbers[uid] = funame
                    print("all data stylists: \(document.data())")
                    //reading serviceFor array
                    let arr1 = document.data()["serviceFor"] as? NSArray
                    let objCArray1 = NSMutableArray(array: arr1!)
                    // reading array from document
                    let swiftArray1 = (objCArray1 as NSArray as? [String])!
                    print("service for \(swiftArray1)")
                    var serviceFor = String()
                    for i in swiftArray1{
                        serviceFor += " " + i
                    }
                    delegate.serviceFor.append(serviceFor)
                    
                    // reading service array
                    let arr = document.data()["services"] as? NSArray
                    var objCArray = NSMutableArray(array: arr!)
                    // reading array from document
                    let swiftArray = (objCArray as NSArray as? [String])!
                    delegate.barberServices.append(swiftArray)
                    
                    delegate.barberPicUrl.append(imgUrl)
                    delegate.barberName.append(funame)
                    delegate.uid_stylists.append(uid)
                    
                    
                }
            }
        }
        
        db.collection("services").getDocuments(){ (querySnapshot, err) in
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    
                    delegate.all_services[document.documentID] = document.data()["name"] as! String
                    
                }
                
            }
        }
    }
}

extension UIViewController {
    func fetchingData2() {
        var formatter = DateFormatter()
        var aptDate = Date()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        var someDateTime = formatter.string(from: aptDate)
        
        let userID = Auth.auth().currentUser!.uid
        
       let delegate = UIApplication.shared.delegate as! AppDelegate
    let db = Firestore.firestore()
        db.collection("bookings").whereField("customer", isEqualTo: userID).getDocuments{ (snapshot, error) in
            
            if error != nil{
                print(error)
            }else{
                delegate.event_service.removeAll()
                delegate.event_service_name.removeAll()
                delegate.event_dateTime.removeAll()
                delegate.doc_id.removeAll()
                delegate.event_stylists_pic_url.removeAll()
                delegate.event_timestamp.removeAll()
                delegate.event_stylists_name.removeAll()
                delegate.event_stylists_id.removeAll()
                for document in (snapshot!.documents){
                    
                    print("\(document.documentID) => \(document.data())")
                    let doc_id =  document.documentID as! String
                    delegate.doc_id.append(doc_id)
                    
                    
                    let stylist_id = document.data()["stylist"] as? String
                    delegate.event_stylists_id.append(stylist_id!)
                    //fetching img url of event stylist
                    print("stylist id: \(stylist_id!)")
                    
                    let img_url = document.data()["imageUrl"] as? String
                    delegate.event_stylists_pic_url.append(img_url!)
                    
                    // let index  = delegate.uid_stylists.index(of: stylist_id!)
                    print("index: \(delegate.all_barbers[stylist_id!] )")
                    
                    
                    let  barberName = (delegate.all_barbers[stylist_id!])
                    
                    delegate.event_stylists_name.append(barberName!)
                    
                    // fetching services array and storing in delegate(global var)
                    let arr = document.data()["services"] as? NSArray
                    var objCArray = NSMutableArray(array: arr!)
                    // reading array from document
                    let swiftArray = (objCArray as NSArray as? [String])!
                    delegate.event_service.append(swiftArray)
                    
                    var arr1 = [String]()
                    
                    for i in swiftArray{
                        arr1.append(delegate.all_services[i]!)
                    }
                    delegate.event_service_name.append(arr1)
                    
                    
                    // adding startDate
                    aptDate =  document.data()["startDate"] as! Date
                    print("aptDate: \(aptDate)")
                    //self.delegate.event_timestamp.append(aptDate)
                    //someDateTime = formatter.date(from: aptDate)
                    delegate.event_dateTime.append(aptDate)
                    
                    
                    
                }
                
            }
        }

    }
    
    
    
}
extension UIViewController {
    func fetchingDataSlots() {
    
    
        let userID = Auth.auth().currentUser!.uid
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let db = Firestore.firestore()
        
        db.collection("FreeBusy").document(delegate.stylist_id).getDocument(){ (document, err) in
            
            if let document = document, document.exists {
               
                print("Document data: \(document.data())" )
            } else {
                print("Document does not exist")
            }
        }
        
    }
    
}

