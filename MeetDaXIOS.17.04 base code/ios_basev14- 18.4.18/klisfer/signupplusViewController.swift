//
//  signupplusViewController.swift
//  klisfer
//
//  Created by aayush chaubey on 05/03/18.
//  Copyright © 2018 aayush chaubey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseCore
import FBSDKCoreKit
import FirebaseFirestore

class signupplusViewController: UIViewController {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let db = Firestore.firestore()
    @IBOutlet weak var card1: CardView!
    
    @IBOutlet weak var card2: CardView!
    var cover1 = UIView()
    var cover2 = UIView()
    var mask1 = UIView()
    var mask2 = UIView()
    @IBOutlet weak var promotinpic1: UIImageView!
    @IBOutlet weak var promotinpic2: UIImageView!
    @IBOutlet weak var offerserv1: UIView!
    @IBOutlet weak var offerserv2: UIView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var apt_btn: UIImageView!
    
    @IBOutlet weak var appt_btn: UIButton!
    @IBOutlet weak var navbar: UINavigationItem!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        //button to make appointmeent
        
       
        
        
        // or for Swift 3
        
        //fetching customer details
        db.collection("Customers").whereField("uid", isEqualTo: Auth.auth().currentUser?.uid).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let fname = document.data()["firstName"] as! String
                    let lname = document.data()["lastName"] as! String
                    let funame = fname + " " + lname
                    self.delegate.cust_name = (funame)
                    self.delegate.cust_gender = (document.data()["gender"] as! String)
                    self.delegate.cust_phone = (document.data()["phone"] as! String)
                    self.delegate.cust_mail = (document.data()["email"] as! String)
                    self.delegate.cust_age = (document.data()["age"] as! String)
                    
                    let imgUrl = document.data()["imageUrl"] as! String
                    let url = URL(string: imgUrl)
                    let session = URLSession.shared
                    let task = session.dataTask(with: url!, completionHandler: { (data, response , error) in
                        if error != nil{
                            print (error)
                            return
                        }
                        DispatchQueue.main.async{
                            let imahe : UIImage = UIImage(data: data!)!
                            self.delegate.cust_image["selected"] = imahe
                            
                        }
                        
                    }).resume()
                    
                }
            }
        }
        
        
        
        
        let nav = self.navigationController?.navigationBar
        
        // 2
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow
        
        // 3
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (nav?.frame.size.width)!, height: (nav?.frame.size.height)!))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo-white")
        imageView.image = image
        
        // 5
        navigationItem.titleView = imageView
        
        
        
        db.collection("products").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    
                    self.delegate.prod_desc.append(document.data()["description"] as! String)
                    self.delegate.prod_name.append(document.data()["productName"] as! String)
                    self.delegate.prod_price.append(document.data()["price"] as! String)
                    self.delegate.prod_img_url.append(document.data()["imageUrl"] as! String)
                }
            }
        }
        
        
        db.collection("promotions").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    
                    self.delegate.prom_name.append(document.data()["name"] as! String)
                    self.delegate.prom_img_url.append(document.data()["imageUrl"] as! String)
                    self.delegate.prom_serv1.append(document.data()["service1"] as! String)
                    self.delegate.prom_serv2.append(document.data()["service2"] as! String)
                    self.delegate.prom_serv3.append(document.data()["service3"] as! String)
                    self.delegate.prom_price.append(document.data()["price"] as! String)
                }
            }
        }
        
      
    }
    
    
    @objc  func bookApt(){
        self.tabBarController?.selectedIndex = 1;

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // OLD QUERIES
        //let delegate = UIApplication.shared.delegate as! AppDelegate
        //let db = Firestore.firestore()
        
        fetchingData2()
        
        
    }
    
    
    @IBAction func signout(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        // signout user of facebook
        FBSDKAccessToken.setCurrent(nil)
        
        //send user to login screen after signout
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : UIViewController =  mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
        
        
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    private func setupLayout(){
        
        //card1.backgroundColor = UIColor(patternImage: UIImage(named: "Promotion2.png")!)
        //card2.backgroundColor = UIColor(patternImage: UIImage(named: "Promotion1.png")!)
        appt_btn.addTarget(self, action: #selector(bookApt), for: .touchUpInside)
        
        
        card1.addSubview(cover1)
        cover1.frame = CGRect(x: 0 ,y: 160 ,width: card1.frame.width  , height: 12)
        cover1.backgroundColor = .white
        card2.addSubview(cover2)
        cover2.frame = CGRect(x: 0 ,y: 160 ,width: card2.frame.width  , height: 12)
        cover2.backgroundColor = .white
        
        
        card1.addSubview(mask1)
        mask1.frame = CGRect(x:0, y:0, width:view.frame.width - 40, height:220)
        mask1.backgroundColor = UIColor(hexString: "#00000000")
        card2.addSubview(mask2)
        mask2.frame = CGRect(x:0, y: 0, width:view.frame.width - 40, height:220)
        mask2.backgroundColor = UIColor(hexString: "#00000000")
        
        
        //button on mask1
        let mon1 = UIButton()
        let mon11 = UIButton()
        mask1.addSubview(mon1)
        mask1.addSubview(mon11)
        mon1.frame = CGRect(x: 20 ,y: 125 ,width: 40,height: 40)
        mon11.frame = CGRect(x: 75 ,y: 125 ,width: 100,height: 40)
        mon1.backgroundColor = .white
        mon11.backgroundColor = UIColor(hexString: "#1C92AFff")
        mon1.layer.shadowColor = UIColor.black.cgColor
        mon11.layer.shadowColor = UIColor.black.cgColor
        mon1.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon11.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon1.layer.shadowRadius = 3
        mon11.layer.shadowRadius = 3
        mon1.layer.shadowOpacity = 0.7
        mon11.setTitle("500 dkk", for: UIControlState.normal)
        mon11.layer.shadowOpacity = 1.0
        mon1.layer.cornerRadius = 6
        mon11.layer.cornerRadius = 6
        
        //button on mask 2
        let mon2 = UIButton()
        let mon22 = UIButton()
        mask2.addSubview(mon2)
        mask2.addSubview(mon22)
        mon2.frame = CGRect(x: 20 ,y: 125 ,width: 40,height: 40)
        mon22.frame = CGRect(x: 75 ,y: 125 ,width: 100,height: 40)
        mon2.backgroundColor = .white
        mon22.backgroundColor = UIColor(hexString: "#1C92AFff")
        mon2.layer.shadowColor = UIColor.black.cgColor
        mon22.layer.shadowColor = UIColor.black.cgColor
        mon2.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon22.layer.shadowOffset = CGSize(width: 3, height: 3)
        mon2.layer.shadowRadius = 3
        mon22.layer.shadowRadius = 3
        mon2.layer.shadowOpacity = 0.7
        mon22.setTitle("500 dkk", for: UIControlState.normal)
        mon22.layer.shadowOpacity = 1.0
        mon2.layer.cornerRadius = 6
        mon22.layer.cornerRadius = 6
        
        
        promotinpic1.layer.cornerRadius = 12;
        promotinpic1.clipsToBounds = true;
        promotinpic2.layer.cornerRadius = 12;
        promotinpic2.clipsToBounds = true;
        offerserv1.layer.cornerRadius = 12;
        offerserv1.clipsToBounds = true;
        offerserv2.layer.cornerRadius = 12;
        offerserv2.clipsToBounds = true;
        
        
        
        //call button
        callBtn.layer.shadowColor = UIColor.black.cgColor
        callBtn.layer.shadowOffset = CGSize(width: 6, height: 6)
        callBtn.layer.shadowRadius = 6
        callBtn.layer.shadowOpacity = 1
        callBtn.layer.cornerRadius = 6
        callBtn.backgroundColor = .darkGray
        
        
    }
    
}
