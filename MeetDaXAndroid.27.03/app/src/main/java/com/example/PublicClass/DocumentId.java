package com.example.PublicClass;

/**
 * Created by DIS015 on 3/16/2018.
 */

public class DocumentId {
    public String documentid;


    public DocumentId(String documentid) {
        this.documentid = documentid;
    }

    public String getDocumentid()
    {
        return documentid;
    }

    public void setDocumentid(String documentid)
    {
        this.documentid = documentid;
    }
}
