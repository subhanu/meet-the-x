package com.example.PublicClass;

import java.util.List;

/**
 * Created by DIS015 on 3/21/2018.
 */

public class StylistCollection {
    private String age;
    private String email;
    private String firstName;
    private String gender;
    private String imagefile;
    private String imageShowcase;
    private String imageUrl;
    private String lastName;
    private String mobile;
    private List<String>serviceFor;
    private List<String>services;
    private String uid;
    private String videoshowcase;

    public StylistCollection(String age, String email, String firstName, String gender, String imagefile, String imageShowcase, String imageUrl, String lastName, String mobile, List<String> serviceFor, List<String> services, String uid, String videoshowcase) {
        this.age = age;
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.imagefile = imagefile;
        this.imageShowcase = imageShowcase;
        this.imageUrl = imageUrl;
        this.lastName = lastName;
        this.mobile = mobile;
        this.serviceFor = serviceFor;
        this.services = services;
        this.uid = uid;
        this.videoshowcase = videoshowcase;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImagefile() {
        return imagefile;
    }

    public void setImagefile(String imagefile) {
        this.imagefile = imagefile;
    }

    public String getImageShowcase() {
        return imageShowcase;
    }

    public void setImageShowcase(String imageShowcase) {
        this.imageShowcase = imageShowcase;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<String> getServiceFor() {
        return serviceFor;
    }

    public void setServiceFor(List<String> serviceFor) {
        this.serviceFor = serviceFor;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVideoshowcase() {
        return videoshowcase;
    }

    public void setVideoshowcase(String videoshowcase) {
        this.videoshowcase = videoshowcase;
    }
}
