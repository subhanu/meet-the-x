package com.example.PublicClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by DIS015 on 3/14/2018.
 */

public class StylistDetails implements Parcelable {
    private ArrayList<String> strServiceArr;
    private String strServiceCost;
    private String strStylistname;
    private String strStylistImg;
    private String strStylistid;
    private String strduration;
    private ArrayList<String>services;

    public StylistDetails(ArrayList<String> strServiceArr, String strServiceCost, String strStylistname, String strStylistImg,String strStylistid,String strduration,ArrayList<String>services) {
        this.strServiceArr = strServiceArr;
        this.strServiceCost = strServiceCost;
        this.strStylistname = strStylistname;
        this.strStylistImg = strStylistImg;
        this.strStylistid=strStylistid;
        this.strduration=strduration;
        this.services=services;
    }

    protected StylistDetails(Parcel in) {
        strServiceArr = in.createStringArrayList();
        strServiceCost = in.readString();
        strStylistname = in.readString();
        strStylistImg = in.readString();
        strStylistid=in.readString();
        strduration=in.readString();
        services=in.createStringArrayList();
    }

    public static final Creator<StylistDetails> CREATOR = new Creator<StylistDetails>() {
        @Override
        public StylistDetails createFromParcel(Parcel in) {
            return new StylistDetails(in);
        }

        @Override
        public StylistDetails[] newArray(int size) {
            return new StylistDetails[size];
        }
    };

    public ArrayList<String> getStrServiceArr() {
        return strServiceArr;
    }

    public void setStrServiceArr(ArrayList<String> strServiceArr) {
        this.strServiceArr = strServiceArr;
    }

    public String getStrServiceCost() {
        return strServiceCost;
    }

    public void setStrServiceCost(String strServiceCost) {
        this.strServiceCost = strServiceCost;
    }

    public String getStrStylistname() {
        return strStylistname;
    }

    public void setStrStylistname(String strStylistname) {
        this.strStylistname = strStylistname;
    }

    public String getStrStylistImg() {
        return strStylistImg;
    }

    public void setStrStylistImg(String strStylistImg) {
        this.strStylistImg = strStylistImg;
    }


    public String getStrStylistid() {
        return strStylistid;
    }

    public void setStrStylistid(String strStylistid) {
        this.strStylistid = strStylistid;
    }
    public String getStrduration() {
        return strduration;
    }

    public void setStrduration(String strduration) {
        this.strduration = strduration;
    }

    public ArrayList<String> getServices() {
        return services;
    }

    public void setServices(ArrayList<String> services) {
        this.services = services;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(strServiceArr);
        parcel.writeString(strServiceCost);
        parcel.writeString(strStylistname);
        parcel.writeString(strStylistImg);
        parcel.writeString(strStylistid);
        parcel.writeString(strduration);
    }
}
