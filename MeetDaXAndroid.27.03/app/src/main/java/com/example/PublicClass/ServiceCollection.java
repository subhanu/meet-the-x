package com.example.PublicClass;

/**
 * Created by DIS015 on 3/20/2018.
 */

public class ServiceCollection {
    private String description;
    private String duration;
    private String name;
    private String price;
    private String stylistid;

    public ServiceCollection(String description, String duration, String name, String price, String stylistid) {
        this.description = description;
        this.duration = duration;
        this.name = name;
        this.price = price;
        this.stylistid = stylistid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStylist() {
        return stylistid;
    }

    public void setStylist(String stylist) {
        this.stylistid = stylist;
    }
}
