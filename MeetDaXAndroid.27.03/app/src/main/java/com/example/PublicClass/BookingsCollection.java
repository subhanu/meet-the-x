package com.example.PublicClass;


import java.util.ArrayList;
import java.util.List;

public class BookingsCollection {

    private String customer;
    private String eid;
    private String endDate;
    private String stylistImg;
    private String how_often;
    private String recurring;
    private ArrayList<String> services;
    private String startDate;
    private String stylist;

    public BookingsCollection(String customer, String eid, String endDate,String stylistImg,String how_often, String recurring, ArrayList<String> services, String startDate, String stylist) {
        this.customer = customer;
        this.eid = eid;
        this.endDate = endDate;
        this.stylistImg=stylistImg;
        this.how_often = how_often;
        this.recurring = recurring;
        this.services = services;
        this.startDate = startDate;
        this.stylist = stylist;
    }

    public String getCustomerid() {
        return customer;
    }

    public void setCustomerid(String customerid) {
        this.customer = customerid;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStylistImg() {
        return stylistImg;
    }

    public void setStylistImg(String stylistImg) {
        this.stylistImg = stylistImg;
    }

    public String getHow_often() {
        return how_often;
    }

    public void setHow_often(String how_often) {
        this.how_often = how_often;
    }

    public String getRecurring() {
        return recurring;
    }

    public void setRecurring(String recurring) {
        this.recurring = recurring;
    }

    public ArrayList<String> getServices() {
        return services;
    }

    public void setServices(ArrayList<String> services) {
        this.services = services;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStylist() {
        return stylist;
    }

    public void setStylist(String stylist) {
        this.stylist = stylist;
    }
}
