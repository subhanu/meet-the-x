package com.example.PublicClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by DIS015 on 3/15/2018.
 */

public class EventsCollection implements Parcelable {

    private String customers;
    private String email;
    private List<String> services;
    private String startDate;
    private String stylist;
    private String imgUrl;


    public EventsCollection(String customers, String email, String imgUrl, List<String> services, String startDate, String stylist) {
        this.customers = customers;
        this.email = email;
        this.services = services;
        this.startDate = startDate;
        this.stylist = stylist;
        this.imgUrl=imgUrl;
    }

    protected EventsCollection(Parcel in) {
        customers = in.readString();
        email = in.readString();
        services = in.createStringArrayList();
        startDate = in.readString();
        stylist = in.readString();
        imgUrl=in.readString();
    }

    public static final Creator<EventsCollection> CREATOR = new Creator<EventsCollection>() {
        @Override
        public EventsCollection createFromParcel(Parcel in) {
            return new EventsCollection(in);
        }

        @Override
        public EventsCollection[] newArray(int size) {
            return new EventsCollection[size];
        }
    };

    public String getCustomers() {
        return customers;
    }

    public void setCustomers(String customers) {
        this.customers = customers;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgUrl(String imgUrl) {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStylist() {
        return stylist;
    }

    public void setStylist(String stylist) {
        this.stylist = stylist;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(customers);
        parcel.writeString(email);
        parcel.writeStringList(services);
        parcel.writeString(startDate);
        parcel.writeString(stylist);
        parcel.writeString(imgUrl);
    }

}
