package com.example.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.aayushchaubey.meetdax.R;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by DIS015 on 2/28/2018.
 */

public class UserLogoutActivity extends AppCompatActivity {

    Button logout;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlogout);

        mAuth = FirebaseAuth.getInstance();


        logout=(Button)findViewById(R.id.logoutBtn);
    logout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           mAuth.signOut();
            LoginManager.getInstance().logOut();
            updateUI();
        }
    });
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser==null){
            updateUI();
        }
    }

    private void updateUI() {
        Toast.makeText(this,"You are logged out",Toast.LENGTH_LONG).show();
        Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
