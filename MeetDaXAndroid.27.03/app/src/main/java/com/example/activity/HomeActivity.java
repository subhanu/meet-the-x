package com.example.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.Fragment.AccountFragment;
import com.example.Fragment.HairtipsFragment;
import com.example.Fragment.HomeFragment;
import com.example.Fragment.MyaaptsFragment;
import com.example.Fragment.StylistsFragment;
import com.example.aayushchaubey.meetdax.R;

public class HomeActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    HomeFragment homeFragment=new HomeFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout,homeFragment).commit();
                    return true;

                case R.id.navigation_stylist:
                    StylistsFragment stylistsFragment=new StylistsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction1=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction1.replace(R.id.frameLayout,stylistsFragment,"FragmentName").commit();
                    return true;

                case R.id.navigation_apps:
                    MyaaptsFragment myaaptsFragment=new MyaaptsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction2=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.frameLayout,myaaptsFragment,"FragmentName").commit();
                    return true;

                case R.id.navigation_tips:
                    HairtipsFragment hairtipsFragment=new HairtipsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction3=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction3.replace(R.id.frameLayout,hairtipsFragment,"FragmentName").commit();

                    return true;

                case R.id.navigation_account:
                    AccountFragment accountFragment=new AccountFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction4=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction4.replace(R.id.frameLayout,accountFragment,"FragmentName").commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home3);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //open home fragment on first launch

        HomeFragment homeFragment=new HomeFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout,homeFragment).commit();

    }

}
