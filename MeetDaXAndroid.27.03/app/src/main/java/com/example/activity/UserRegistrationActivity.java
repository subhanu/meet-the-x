package com.example.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PublicClass.BookingsCollection;
import com.example.PublicClass.CustomersCollection;
import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DIS015 on 3/1/2018.
 */

public class UserRegistrationActivity extends AppCompatActivity {

    TextView agreeServices,agreeTv,terms;
    EditText genderEdt,emailEdt,passwordEdt,confirmpasEdt,firstnameEdt,LastnameEdt,mobilenoEdt,ageEdt;
    FirebaseAuth mAuth;
    RelativeLayout signInLayout,profileUpdate;
    Spinner genderSpinner;
    String gender[]={"Male","Female","Others"};
    Button registrationBtn,uploadBtn,chooseBtn;
    ProgressBar progressBar;
    CheckBox checkBox;
    private FirebaseAuth.AuthStateListener fAuthListener;
    public static final String STORAGE_PATH="images/";
    public static final String DATABASE_PATH="mainObject";
    private Uri imageUri;
    ImageView userImage;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;
    ProgressDialog progressDialog;
    private  final int CAMERA_REQUEST_CODE=71;
    String strEmail,strAge,strMobileno,strGender,strFirstname,strLastname,strUserid,strPassword,strConfirmpass;
    BookingsCollection bookingsCollection;
    ArrayList<String> services=new ArrayList<>();

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);

        agreeServices=(TextView)findViewById(R.id.acessServicesTV);
        agreeTv=(TextView)findViewById(R.id.agreeTv);
        terms=(TextView)findViewById(R.id.termsTv);
        genderSpinner=(Spinner)findViewById(R.id.genderSpinner);
        genderEdt=(EditText)findViewById(R.id.genderEdt) ;
        emailEdt=(EditText)findViewById(R.id.emailEdt) ;
        passwordEdt=(EditText)findViewById(R.id.pasEdt) ;
        confirmpasEdt=(EditText)findViewById(R.id.confirmPasEdt) ;
        firstnameEdt=(EditText)findViewById(R.id.firstNameEdt) ;
        LastnameEdt=(EditText)findViewById(R.id.lastNameEdt) ;
        mobilenoEdt=(EditText)findViewById(R.id.mobileNoEdt) ;
        ageEdt=(EditText)findViewById(R.id.ageEdt);
        uploadBtn=(Button)findViewById(R.id.uploadBtn);
        chooseBtn=(Button)findViewById(R.id.chooseBtn);
        profileUpdate=(RelativeLayout)findViewById(R.id.profileUpdate);
        checkBox=(CheckBox)findViewById(R.id.checkBox);
        registrationBtn=(Button)findViewById(R.id.registerBtn);
        progressBar=(ProgressBar)findViewById(R.id.Progressbar);
        userImage=(ImageView)findViewById(R.id.userimage);

        mAuth=mAuth.getInstance();
        firebaseFirestore=FirebaseFirestore.getInstance();
        firebaseStorage=FirebaseStorage.getInstance();
        storageReference= firebaseStorage.getReference();

        registerDetails();

        registrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

//                        progressBar.setVisibility(View.VISIBLE);
                    mAuth.createUserWithEmailAndPassword(emailEdt.getText().toString(), passwordEdt.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){
                                        strEmail=emailEdt.getText().toString().trim();
                                        strFirstname=firstnameEdt.getText().toString().trim();
                                        strLastname=LastnameEdt.getText().toString().trim();
                                        strAge=ageEdt.getText().toString().trim();
                                        strGender=genderEdt.getText().toString().trim();
                                        strMobileno=mobilenoEdt.getText().toString().trim();
                                        strPassword=passwordEdt.getText().toString().trim();
                                        strConfirmpass=confirmpasEdt.getText().toString().trim();

                                        strUserid=mAuth.getUid();

                                        validate();

                                        Map<String,String> userDetails=new HashMap<>();

                                        userDetails.put("age",strAge);
                                        userDetails.put("email",strEmail);
                                        userDetails.put("firstName",strFirstname);
                                        userDetails.put("gender",strGender);
                                        userDetails.put("lastName",strLastname);
                                        userDetails.put("phone",strMobileno);
                                        userDetails.put("uid",strUserid);

                                        CustomersCollection customers=new CustomersCollection(strAge,strEmail,strFirstname,strGender,strLastname,strMobileno,strUserid);

                                        firebaseFirestore.collection("Customers").add(customers).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                String error=e.getMessage();
                                               (Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG)).show();
                                            }
                                        });
                                    }
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    public boolean validate(){
        boolean temp=true;
        if(!strPassword.equals(strConfirmpass)){
            confirmpasEdt.setError("Password and Confirm password does not match");
            temp=false;
        }
        if(strAge.isEmpty()){
            ageEdt.setError("Enter your age");
            temp=false;
        }
        if(strGender.isEmpty()){
            genderEdt.setError("Enter your gender");
            temp=false;
        }
        if(strMobileno.length()<6||strMobileno.length()>13){
            mobilenoEdt.setError("Enter valid Number");
            temp=false;
        }
        return temp;
    }

    @SuppressLint("ResourceAsColor")
    private void registerDetails() {
        agreeServices.setText(R.string.agreeText);
        agreeTv.setText(R.string.agreeTerms);
        terms.setText(R.string.termsconditions);
        terms.setTextColor(R.color.font_color);

        signInLayout=(RelativeLayout)findViewById(R.id.accountSigninLayout);
        signInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
            }
        });

        try {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gender);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            genderSpinner.setAdapter(dataAdapter);
            genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                    genderEdt.setText(item);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setType("stylists/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Image"),CAMERA_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CAMERA_REQUEST_CODE & resultCode==RESULT_OK && data != null && data.getData() != null){
            //Progress bar to show
            imageUri=data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                userImage.setImageBitmap(bitmap);
                upload();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void upload(){
        if(imageUri!=null){
            StorageReference reference=storageReference.child(STORAGE_PATH+System.currentTimeMillis()+"."+imageUri);
            reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(getApplicationContext(),"File Uploaded",Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double totalProgress=(100*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                    Toast.makeText(getApplicationContext(),"File uploaded"+(int)totalProgress,Toast.LENGTH_LONG).show();
                }
            });
        }

    }
}
