package com.example.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.aayushchaubey.meetdax.R;
import com.facebook.Profile;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DIS015 on 3/11/2018.
 */

public class FbUserRegistration extends AppCompatActivity{
    private FirebaseAuth mAuth;
    EditText emailEdt,firstnameEdt,lastnameEdt,genderEdt,ageEdt,mobilenoEdt;
    Button registrationBtn;
    String strEmail,strFirstname,strLastname,strGender,strAge,strMobileno,strUserid;
    ImageView userProfileImg;
    Button fbUserRegistrationBtn;
    private FirebaseFirestore firebaseFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fb_registration);
        mAuth = FirebaseAuth.getInstance();

        firebaseFirestore=FirebaseFirestore.getInstance();


        emailEdt=(EditText)findViewById(R.id.emailEdt);
        firstnameEdt=(EditText)findViewById(R.id.firstNameEdt);
        lastnameEdt=(EditText)findViewById(R.id.lastNameEdt);
        genderEdt=(EditText)findViewById(R.id.genderEdt);
        ageEdt=(EditText)findViewById(R.id.ageEdt);
        mobilenoEdt=(EditText)findViewById(R.id.mobileNoEdt);
        userProfileImg=(ImageView)findViewById(R.id.userProfilePic);

        registrationBtn=(Button)findViewById(R.id.registerBtn);


        Profile profile=Profile.getCurrentProfile();

        strFirstname=profile.getFirstName();
        strLastname=profile.getLastName();
        strGender=genderEdt.getText().toString().trim();
        strAge=ageEdt.getText().toString().trim();
        strEmail=emailEdt.getText().toString().trim();
        strMobileno=mobilenoEdt.getText().toString().trim();
//                userProfileImg.setImageResource(Profile.getCurrentProfile().getProfilePictureUri(200,200));

        firstnameEdt.setText(strFirstname);
        lastnameEdt.setText(strLastname);

        details();
    }

   public void details(){
        registrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String,String> userDetails=new HashMap<>();

                strUserid=mAuth.getUid();


                userDetails.put("age",strAge);
                userDetails.put("email",strEmail);
                userDetails.put("firstName",strFirstname);
                userDetails.put("gender",strGender);
                userDetails.put("lastName",strLastname);
                userDetails.put("phone",strMobileno);
                userDetails.put("uid",strUserid);


                firebaseFirestore.collection("Customers").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error=e.getMessage();
                    }
                });

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);

            }
        });

   }

}
