package com.example.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.Fragment.BookAppointmentFragment;
import com.example.PublicClass.StylistDetails;
import com.example.aayushchaubey.meetdax.R;
import com.example.adapter.ListAdapter;
import com.example.adapter.ServicesAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.Transaction;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.GraphRequest.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {
    String stylistname, stylistid,serviceid;
    CircleImageView imageView;
    TextView stylistNameTv, servicesTv;
    Button serviceButton;
    RecyclerView recyclerView;

    Transaction strStlistName, strSpecalist;
    private Bundle bundle;
    private ServicesAdapter servicesAdapter;
    ArrayList<String>serviceidArray=new ArrayList<>();

    public ServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.servicesfrag, container, false);

        stylistNameTv = (TextView) view.findViewById(R.id.stylistNameTv);
        servicesTv = (TextView) view.findViewById(R.id.serviceTv);
        imageView = (CircleImageView) view.findViewById(R.id.stylistImg);
        serviceButton = (Button) view.findViewById(R.id.servicesBtn);

        bundle = getArguments();

        if (bundle != null) {
            stylistname = bundle.getString("stylistname", "");
            serviceid=bundle.getString("serviceid","");

            serviceidArray.add(serviceid);

        }
        stylistNameTv.setText(stylistname);
        Picasso.get().load(bundle.getString("stylistImg")).into(imageView);

        recyclerView = (RecyclerView) view.findViewById(R.id.stylistRecycler);
        try {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("services")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                servicesAdapter = new ServicesAdapter(task,serviceid,servicesTv);
                                recyclerView.setAdapter(servicesAdapter);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);


                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //store data inside datastorage class
//                DataStorageClass data = new DataStorageClass(new String[10],new String[10],bundle.getString("stlistname", ""), bundle.getInt("stylistImg"));

                StylistDetails stylistDetails=new StylistDetails(servicesAdapter.getSelectedItems(),new String(),bundle.getString("stylistname", ""),bundle.getString("stylistImg"),servicesAdapter.getStrstylistid(),servicesAdapter.getDuration(),serviceidArray);
                BookAppointmentFragment bookAppointmentFragment = new BookAppointmentFragment();
                Bundle bundle = getArguments();
                bundle.putParcelable("data", stylistDetails);
                bookAppointmentFragment.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.frameLayout, bookAppointmentFragment).addToBackStack("").commit();

            }
        });


        return view;
    }


}
