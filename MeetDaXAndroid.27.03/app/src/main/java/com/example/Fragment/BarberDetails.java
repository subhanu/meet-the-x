package com.example.Fragment;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DIS015 on 3/13/2018.
 */

public class BarberDetails extends Throwable implements Parcelable {
    private String barbername;
    private String barberid;
    private String barberImg;

    protected BarberDetails(Parcel in) {
        barbername = in.readString();
        barberid = in.readString();
        barberImg = in.readString();
    }

    public static final Creator<BarberDetails> CREATOR = new Creator<BarberDetails>() {
        @Override
        public BarberDetails createFromParcel(Parcel in) {
            return new BarberDetails(in);
        }

        @Override
        public BarberDetails[] newArray(int size) {
            return new BarberDetails[size];
        }
    };

    public BarberDetails(String barbername, String barberid, String barberImg) {
        this.barbername = barbername;
        this.barberid = barberid;
        this.barberImg = barberImg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(barbername);
        parcel.writeString(barberid);
        parcel.writeString(barberImg);
    }

    public String getBarbername() {
        return barbername;
    }

    public void setBarbername(String barbername) {
        this.barbername = barbername;
    }

    public String getBarberid() {
        return barberid;
    }

    public void setBarberid(String barberid) {
        this.barberid = barberid;
    }

    public String getBarberImg() {
        return barberImg;
    }

    public void setBarberImg(String barberImg) {
        this.barberImg = barberImg;
    }

    public static Creator<BarberDetails> getCREATOR() {
        return CREATOR;
    }
}
