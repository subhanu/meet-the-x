package com.example.Fragment;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.PublicClass.EventsCollection;
import com.example.PublicClass.StylistDetails;
import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeFragment extends Fragment {

     Button timePickerBtn,datePickerBtn,nextBtn;
     EditText dateTime;
    Calendar myCalendar;
    DatePickerDialog datePickerDialog;
    String strDate,strTime,strUserName,strUserEmail;
    String strdateTime;
    StylistDetails stylistDetails;
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth firebaseAuth;

    String[] serviceArray={"Dameklip","Harvask","Vandondulation"};
    private StylistDetails data;

    public TimeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data=(StylistDetails)getArguments().getParcelable("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_time, container, false);
        timePickerBtn=(Button)view.findViewById(R.id.timeBtn);
        dateTime=(EditText)view.findViewById(R.id.dateTimeEdt);

        datePickerBtn=(Button)view.findViewById(R.id.dateBtn);
        nextBtn=(Button)view.findViewById(R.id.nextBtn);

        firebaseAuth=firebaseAuth.getInstance();
        firebaseFirestore=FirebaseFirestore.getInstance();

        timePickerBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        dateTime.setText( selectedHour + ":" + selectedMinute);
//                        strTime=(selectedHour+":"+selectedMinute).toString().trim();
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
//        myCalendar = Calendar.getInstance();
//
//        datePickerBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//                final Calendar calendar = Calendar.getInstance();
//                int yy = calendar.get(Calendar.YEAR);
//                int mm = calendar.get(Calendar.MONTH);
//                int dd = calendar.get(Calendar.DAY_OF_MONTH);
//                DatePickerDialog datePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear+1)
//                                + "/" + String.valueOf(year);
//                        strDate=date;
//                        getDateFromString(strDate);
//                        dateTime.setText(strDate+" "+strTime);
//                    }
//                }, yy, mm, dd);
//                datePicker.show();
//            }
//        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                BookAppointmentFragment bookAppointmentFragment = new BookAppointmentFragment();

                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, bookAppointmentFragment).addToBackStack(null).commit();


                strUserName=firebaseAuth.getCurrentUser().getDisplayName();
                strUserEmail=firebaseAuth.getCurrentUser().getEmail();
                EventsCollection userDetails=new EventsCollection(strUserName,strUserEmail,stylistDetails.getStrStylistImg(),data.getStrServiceArr(),""+getDateFromString(dateTime.getText().toString()),data.getStrStylistname());

                firebaseFirestore.collection("events").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getContext(),"UPLOADED ",Toast.LENGTH_LONG).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error=e.getMessage();
                    }
                });


            }
        });
        return view;
    }

    static final SimpleDateFormat format = new SimpleDateFormat("MMMM dd,yyyy 'at' hh:mm:ss a Z");
    SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public String getDateFromString(String datetoSaved){
        try {
            Date date = format1.parse(datetoSaved);
            return format.format(date) ;
        } catch (ParseException e){
            return null ;
        }
    }

}
