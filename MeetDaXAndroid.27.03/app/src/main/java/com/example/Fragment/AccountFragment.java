package com.example.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aayushchaubey.meetdax.R;
import com.example.activity.LoginActivity;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

//    Button logout;
    Context context;
    private FirebaseAuth mAuth;
    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        try {
            Button logout;

            mAuth = FirebaseAuth.getInstance();
            logout = (Button) view.findViewById(R.id.logoutBtn);

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        mAuth.signOut();
                        LoginManager.getInstance().logOut();
                        updateUI();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            // Inflate the layout for this fragment
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        try {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) {
                updateUI();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateUI() {
        try {
//            Toast.makeText(context, "You are logged out", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
