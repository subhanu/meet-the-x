package com.example.Fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PublicClass.BookingsCollection;
import com.example.PublicClass.CustomersCollection;
import com.example.PublicClass.DocumentId;
import com.example.PublicClass.EventsCollection;
import com.example.PublicClass.ServiceCollection;
import com.example.PublicClass.StylistDetails;
import com.example.aayushchaubey.meetdax.R;
import com.example.adapter.BookAppointmentAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.GraphRequest.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookAppointmentFragment extends Fragment {

    TextView durationTv, serviceNameTv, stylistname, dateTv, timetv;
    Button bookAppoinmentBtn;
    CircleImageView stylistImgView;
    static final String[] Months = new String[]{"January", "February",
            "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December"};
    String strDate, strMonth, strYear, strUserName, strUserEmail, strDateMonth, strDateTime, strCustomerid, strStylistid, strTime;
    private StylistDetails stylistDetails;
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth firebaseAuth;
    private StylistDetails data;
    LinearLayout startTime1, startTime2, startTime3, startTime4, startTime5, startTime6, startTime7, startTime8, endTime1, endTime2, endTime3,
            endTime4, endTime5, endTime6, endTime7, endTime8;
    BookAppointmentAdapter appointmentAdapter;
    RecyclerView recyclerView;
    DocumentId documentId;
    String docid, username;
    CustomersCollection customers;
    ServiceCollection serviceCollection;
    BookingsCollection bookingsCollection;
    ArrayList<String> serviceArray = new ArrayList<>();
    RelativeLayout datepicker;
    TextView startTimeTv1, startTimeTv2, startTimeTv3, startTimeTv4, startTimeTv5, startTimeTv6, startTimeTv7, startTimeTv8;
    Calendar myCalendar;


    public BookAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = (StylistDetails) getArguments().getParcelable("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.book_appoinments, container, false);

        firebaseAuth = firebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        durationTv = (TextView) view.findViewById(R.id.durationTv);
        serviceNameTv = (TextView) view.findViewById(R.id.serviceTv);
        stylistname = (TextView) view.findViewById(R.id.stylistNameTv);
        bookAppoinmentBtn = (Button) view.findViewById(R.id.bookAppoinment);
        timetv = (TextView) view.findViewById(R.id.timeTV);
        dateTv = (TextView) view.findViewById(R.id.datepickertv);

        datepicker = (RelativeLayout) view.findViewById(R.id.datepickerLayout);
        startTime1 = (LinearLayout) view.findViewById(R.id.startTimeLayout1);
        startTime2 = (LinearLayout) view.findViewById(R.id.startTimeLayout2);
        startTime3 = (LinearLayout) view.findViewById(R.id.startTimeLayout3);
        startTime4 = (LinearLayout) view.findViewById(R.id.startTimeLayout4);
        startTime5 = (LinearLayout) view.findViewById(R.id.startTimeLayout5);

        endTime1 = (LinearLayout) view.findViewById(R.id.endTimeLayout1);
        endTime2 = (LinearLayout) view.findViewById(R.id.endTimeLayout2);
        endTime3 = (LinearLayout) view.findViewById(R.id.endTimeLayout3);
        endTime4 = (LinearLayout) view.findViewById(R.id.endTimeLayout4);
        endTime5 = (LinearLayout) view.findViewById(R.id.endTimeLayout5);

        startTimeTv1 = (TextView) view.findViewById(R.id.startTime1);
        startTimeTv2 = (TextView) view.findViewById(R.id.startTime2);
        startTimeTv3 = (TextView) view.findViewById(R.id.startTime3);
        startTimeTv4 = (TextView) view.findViewById(R.id.startTime4);
        startTimeTv5 = (TextView) view.findViewById(R.id.startTime5);

//        recyclerView=(RecyclerView)view.findViewById(R.id.timeslot);
//        appointmentAdapter = new BookAppointmentAdapter();
//        recyclerView.setAdapter(appointmentAdapter);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(layoutManager);

        stylistImgView = (CircleImageView) view.findViewById(R.id.stylistImg);
        firebaseFirestore = FirebaseFirestore.getInstance();
        details();
        try {
            //geta data from data stylistDetails class
            Bundle bundle = getArguments();
            if (bundle != null) {
                stylistDetails = (StylistDetails) bundle.getParcelable("data");
                strStylistid = bundle.getString("stylistid", "");
            }
            stylistname.setText(stylistDetails.getStrStylistname());
            Picasso.get().load(stylistDetails.getStrStylistImg()).into(stylistImgView);

            //get currentUsername and email from firebase
            strUserName = firebaseAuth.getCurrentUser().getDisplayName();
            strUserEmail = firebaseAuth.getCurrentUser().getEmail();
            strCustomerid = firebaseAuth.getUid();
            serviceNameTv.setText(stylistDetails.getStrServiceArr().toString());
            durationTv.setText(strUserName);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startTime1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startTime1.setBackgroundResource(R.drawable.selectedbutton);
                    timetv.setText(startTimeTv1.getText().toString());

//                    strDateTime = strDateMonth.concat(timetv.toString());
//                    strDateTime = DateFormat.format("MMMM dd yyyy 'at' hh:mm:ss a zz", new Date()).toString();

//                    bookingsCollection = new BookingsCollection(strCustomerid, new String(), new String(), data.getStrStylistImg(), new String(), new String(), data.getServices(), strDateTime.toString(), data.getStrStylistid());
//                    Log.d("USERID", String.valueOf(bookingsCollection));
/*
                    final EventsCollection userDetails = new EventsCollection(strUserName, strUserEmail, stylistDetails.getStrStylistImg(), data.getStrServiceArr(), "" + strDateTime.toString(), data.getStrStylistname());

                    try {
                        firebaseFirestore.collection("events").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.d(TAG, "Error" + e.getMessage());
                                }
                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                                    if (doc.getType() == DocumentChange.Type.ADDED)
                                        docid = doc.getDocument().getId();
                                    documentId = new DocumentId(docid);
                                    documentId.setDocumentid(docid);
                                }
                            }
                        });
                        if (docid != null) {
                            firebaseFirestore.collection("events")
                                    .document(docid)
                                    .set(userDetails, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                        }*//*else {

                            firebaseFirestore.collection("bookings").add(bookingsCollection).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    String error = e.getMessage();
                                }
                            });
                        }
*//*

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            startTime2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startTime2.setBackgroundResource(R.drawable.selectedbutton);
                    timetv.setText(startTimeTv2.getText().toString());
                    strDateTime = strDateMonth.concat(timetv.toString());

                    strDateTime = DateFormat.format("MMMM dd yyyy 'at' hh:mm:ss a zz", new Date()).toString();

                    final EventsCollection userDetails = new EventsCollection(strUserName, strUserEmail, stylistDetails.getStrStylistImg(), data.getStrServiceArr(), "" + strDateTime.toString(), data.getStrStylistname());

                    try {
                        firebaseFirestore.collection("events").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.d(TAG, "Error" + e.getMessage());
                                }
                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                                    if (doc.getType() == DocumentChange.Type.ADDED)
                                        docid = doc.getDocument().getId();
                                    documentId = new DocumentId(docid);
                                    documentId.setDocumentid(docid);
                                }
                            }
                        });
                        if (docid != null) {
                            firebaseFirestore.collection("events")
                                    .document(docid)
                                    .set(userDetails, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                        } else {

                            firebaseFirestore.collection("events").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    String error = e.getMessage();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            startTime3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startTime3.setBackgroundResource(R.drawable.selectedbutton);
                    timetv.setText(startTimeTv3.getText().toString());
                    strDateTime = strDateMonth.concat(timetv.toString());

                    strDateTime = DateFormat.format("MMMM dd yyyy 'at' hh:mm:ss a zz", new Date()).toString();

                    final EventsCollection userDetails = new EventsCollection(strUserName, strUserEmail, stylistDetails.getStrStylistImg(), data.getStrServiceArr(), "" + strDateTime.toString(), data.getStrStylistname());

                    try {
                        firebaseFirestore.collection("events").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.d(TAG, "Error" + e.getMessage());
                                }
                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                                    if (doc.getType() == DocumentChange.Type.ADDED)
                                        docid = doc.getDocument().getId();
                                    documentId = new DocumentId(docid);
                                    documentId.setDocumentid(docid);
                                }
                            }
                        });
                        if (docid != null) {
                            firebaseFirestore.collection("events")
                                    .document(docid)
                                    .set(userDetails, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                        } else {

                            firebaseFirestore.collection("events").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    String error = e.getMessage();
                                }
                            });
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            startTime4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startTime4.setBackgroundResource(R.drawable.selectedbutton);
                    timetv.setText(startTimeTv4.getText().toString());
                    strDateTime = strDateMonth.concat(timetv.toString());

                    strDateTime = DateFormat.format("MMMM dd yyyy 'at' hh:mm:ss a zz", new Date()).toString();

                    final EventsCollection userDetails = new EventsCollection(strUserName, strUserEmail, stylistDetails.getStrStylistImg(), data.getStrServiceArr(), "" + strDateTime.toString(), data.getStrStylistname());

                    try {
                        firebaseFirestore.collection("events").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.d(TAG, "Error" + e.getMessage());
                                }
                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                                    if (doc.getType() == DocumentChange.Type.ADDED)
                                        docid = doc.getDocument().getId();
                                    documentId = new DocumentId(docid);
                                    documentId.setDocumentid(docid);
                                }
                            }
                        });
                        if (docid != null) {
                            firebaseFirestore.collection("events")
                                    .document(docid)
                                    .set(userDetails, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                        } else {

                            firebaseFirestore.collection("events").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    String error = e.getMessage();
                                }
                            });
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startTime5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startTime5.setBackgroundResource(R.drawable.selectedbutton);
                    timetv.setText(startTimeTv5.getText().toString());
                    strDateTime = strDateMonth.concat(timetv.toString());

                    strDateTime = DateFormat.format("MMMM dd yyyy 'at' hh:mm:ss a zz", new Date()).toString();

                    final EventsCollection userDetails = new EventsCollection(strUserName, strUserEmail, stylistDetails.getStrStylistImg(), data.getStrServiceArr(), "" + strDateTime.toString(), data.getStrStylistname());

                    try {
                        firebaseFirestore.collection("events").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.d(TAG, "Error" + e.getMessage());
                                }
                                for (DocumentChange doc : documentSnapshots.getDocumentChanges()) {
                                    if (doc.getType() == DocumentChange.Type.ADDED)
                                        docid = doc.getDocument().getId();
                                    documentId = new DocumentId(docid);
                                    documentId.setDocumentid(docid);
                                }
                            }
                        });
                        if (docid != null) {
                            firebaseFirestore.collection("events")
                                    .document(docid)
                                    .set(userDetails, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                        } else {
                            firebaseFirestore.collection("events").add(userDetails).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getContext(), "UPLOADED ", Toast.LENGTH_LONG).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    String error = e.getMessage();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void details() {
        myCalendar = Calendar.getInstance();

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1)
                                + "/" + String.valueOf(year);

                        strDate = date;
                        getDateFromString(strDate);

//                        dateTime.setText(strDate + " " + strTime);
                    }
                }, yy, mm, dd);
                datePicker.show();
            }
        });
        bookAppoinmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyaaptsFragment myaaptsFragment = new MyaaptsFragment();
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, myaaptsFragment).addToBackStack(null).commit();
            }
        });

    }

    static final SimpleDateFormat format = new SimpleDateFormat("MMMM dd,yyyy 'at' hh:mm:ss a Z");
    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public String getDateFromString(String datetoSaved) {
        try {
            Date date = format1.parse(datetoSaved);
//            dateTv.setText(date.toString());

            return format.format(date);
        } catch (ParseException e) {
            return null;
        }
    }

}
