package com.example.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.PublicClass.DocumentId;
import com.example.PublicClass.EventsCollection;
import com.example.aayushchaubey.meetdax.R;
import com.example.adapter.MyAppoinmentsAdapter;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.facebook.GraphRequest.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyaaptsFragment extends Fragment {
    String strEmail,strCustomer;
    TextView customerTv,serviceTv;
    ImageView StylistImg;
    private RecyclerView recyclerView;
    private List<EventsCollection> eventsList;
    String id;
    DocumentId documentId;


    ArrayList<Type> documents=new ArrayList();
    MyAppoinmentsAdapter myAppoinmentsAdapter;

    public MyaaptsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_myaapts, container, false);

        customerTv=(TextView)view.findViewById(R.id.customerNameTv);
        serviceTv=(TextView)view.findViewById(R.id.serviceTv);
        recyclerView=(RecyclerView)view.findViewById(R.id.myapptsRecycler);

        StylistImg=(ImageView)view.findViewById(R.id.stylistImg);

        eventsList=new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

       /* try {
            db.collection("events")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                    // here you can get the id.
                                    MyAppoinmentsAdapter myAppoinmentsAdapter=new MyAppoinmentsAdapter(task);
                                    recyclerView.setAdapter(myAppoinmentsAdapter);
                                    RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
                                    recyclerView.setLayoutManager(layoutManager);

                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
//*/
        db.collection("bookings").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                if(e!=null){
                    Log.d(TAG,"Error"+e.getMessage());
                }
                for (DocumentChange doc:documentSnapshots.getDocumentChanges()){
                    if(doc.getType()==DocumentChange.Type.ADDED)
                        id=doc.getDocument().getId();

                    documentId=new DocumentId(id);
                    documentId.setDocumentid(id);

                    myAppoinmentsAdapter=new MyAppoinmentsAdapter(doc,id);
                    recyclerView.setAdapter(myAppoinmentsAdapter);
                    RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                    myAppoinmentsAdapter.notifyDataSetChanged();

                }
            }
        });
        return view;
    }
}
