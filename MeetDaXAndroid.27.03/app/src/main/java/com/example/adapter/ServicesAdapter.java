package com.example.adapter;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.Fragment.BookAppointmentFragment;
import com.example.PublicClass.ServiceCollection;
import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;

import static com.facebook.GraphRequest.TAG;

/**
 * Created by DIS015 on 3/9/2018.
 */

public class ServicesAdapter extends RecyclerView.Adapter{

    private final Task<QuerySnapshot> task;
    private String serviceid;
    ArrayList<String> checkedServiceArray = new ArrayList<String>();
    ArrayList<String> checkedServiceid = new ArrayList<String>();

    String strstylistid,duration,strDescription;
    ServiceCollection serviceCollection;
    private FirebaseFirestore firebaseFirestore;
    private TextView servicename,serviceCost;
    String strServicename;
    int strServiceCost;
    TextView serviceTv;
    String id;
    public ServicesAdapter(Task<QuerySnapshot> task, String serviceid, TextView servicesTv) {
        this.task=task;
        this.serviceid=serviceid;
        this.serviceTv=servicesTv;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.services_item,parent,false);
        firebaseFirestore=FirebaseFirestore.getInstance();
        return new ServicesAdapter.listViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        try {
//            ((listViewHolder) holder).servicename.setText(task.getResult().getDocuments().get(position).getString("name"));
//            ((listViewHolder) holder).serviceCost.setText(task.getResult().getDocuments().get(position).getLong("price").toString());
            strstylistid=task.getResult().getDocuments().get(position).getString("stylist");
            duration=task.getResult().getDocuments().get(position).getLong("duration").toString();
            strDescription=task.getResult().getDocuments().get(position).getString("description").toString();

            firebaseFirestore.collection("services").document(serviceid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()){
                        strServicename=task.getResult().getString("name");
                        strServiceCost=task.getResult().getLong("price").intValue();
                        servicename.setText(strServicename);
//                        serviceCost.setText(strServiceCost);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });

            serviceCollection=new ServiceCollection(strDescription,duration,
                    task.getResult().getDocuments().get(position).getString("name"),
                    task.getResult().getDocuments().get(position).getLong("price").toString(),
                    strstylistid);

            Log.i("stylistdetails", String.valueOf(serviceCollection));


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<String> getSelectedItems(){
        return checkedServiceArray;
    }

    public String getStrstylistid(){
        return strstylistid;

    }
    public String getDuration(){
        return duration;
    }

    @Override
    public int getItemCount() {
        return task.getResult().getDocuments().size();
    }
    private class listViewHolder extends RecyclerView.ViewHolder{
        private CheckBox checkBox;

        public listViewHolder(View itemView){
            super(itemView);
            servicename=(TextView)itemView.findViewById(R.id.damerServiceTV);
            serviceCost=(TextView)itemView.findViewById(R.id.damerRsTV);
            checkBox=(CheckBox)itemView.findViewById(R.id.checkBox);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    checkedServiceArray.add(task.getResult().getDocuments().get(getAdapterPosition()).getString("name"));
                           serviceTv.setText(checkedServiceArray.toString());

//                    try{
//                        firebaseFirestore.collection("services").addSnapshotListener(new EventListener<QuerySnapshot>() {
//                            @Override
//                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
//                                if(e!=null){
//                                    Log.d(TAG,"Error"+e.getMessage());
//                                }
//                                for (DocumentChange doc:documentSnapshots.getDocumentChanges()) {
//                                    if (doc.getType() == DocumentChange.Type.ADDED)
//                                        id = doc.getDocument().getId();
//                                    checkedServiceid.add(doc.getDocument().get(id).toString());
//                                }
//                            }
//                        });
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }

                }
            });
        }
    }
}
