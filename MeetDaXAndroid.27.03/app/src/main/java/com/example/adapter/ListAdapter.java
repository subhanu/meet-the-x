package com.example.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.Fragment.ServicesFragment;
import com.example.PublicClass.BookingsCollection;
import com.example.PublicClass.StylistCollection;
import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.login.widget.ProfilePictureView.TAG;

/**
 * Created by DIS015 on 3/8/2018.
 */

public class ListAdapter extends RecyclerView.Adapter {

    private final Task<QuerySnapshot> task;
    String strbarberid;
    FirebaseFirestore db;
    ArrayList<String>servicesForDetails;
    ArrayList<String>serviceDetails;
    ArrayList<String> servicesforarray = new ArrayList<String>();
    ArrayList<String>servicesarray=new ArrayList<>();
    StylistCollection stylistCollection;
    BookingsCollection bookingsCollection;
    ArrayList<String>services=new ArrayList<>();

    public ListAdapter(Task<QuerySnapshot> task) {
        this.task = task;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
         db = FirebaseFirestore.getInstance();
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        try {
            db.collection("stylists").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                try {
                                servicesForDetails = (ArrayList<String>) document.get("serviceFor");
                                serviceDetails=(ArrayList<String>)document.get("services");

                                     ((listViewHolder) holder).stylistname.setText(task.getResult().getDocuments().get(position).getString("firstName"));
                                     Picasso.get().load(task.getResult().getDocuments().get(position).getString("imageUrl")).into(((listViewHolder) holder).stylistImg);
                                     ((listViewHolder) holder).lastnameTv.setText(task.getResult().getDocuments().get(position).getString("lastName"));
                                     ((listViewHolder) holder).specialist.setText((servicesForDetails.get(position).toString()));
                                     strbarberid = (task.getResult().getDocuments().get(position).getString("uid"));
                                     ((listViewHolder) holder).ChooseBtn.setBackgroundResource(R.drawable.mybutton);

                                 }catch (Exception e){
                                     e.printStackTrace();
                                 }
//                            ((listViewHolder)holder).specialist.setText(specialistarray.toString());
                            }
//                            HashMap<String,String>service=(HashMap<String,String>)document.get("serviceFor");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return task.getResult().getDocuments().size();
    }

    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView stylistname,lastnameTv,specialist;
        private ImageView stylistImg;
        private Button ChooseBtn;
        String strStylistname, strStylistImg, stylistid;

        @SuppressLint("ResourceAsColor")
        public listViewHolder(View itemView) {
            super(itemView);

            stylistImg = (ImageView) itemView.findViewById(R.id.stylistImg);
            stylistname = (TextView) itemView.findViewById(R.id.StylistTv);
            lastnameTv = (TextView) itemView.findViewById(R.id.lastnameTv);
            specialist = (TextView) itemView.findViewById(R.id.specialistTv);
            ChooseBtn = (Button) itemView.findViewById(R.id.chooseBtn);
            ChooseBtn.setOnClickListener(this);


        }

        @SuppressLint("ResourceAsColor")
        public void onClick(View view) {
            ChooseBtn.setBackgroundResource(R.drawable.selectedbutton);

            ServicesFragment servicesFragment = new ServicesFragment();
            Bundle stylist = new Bundle();
            stylist.putString("stylistname", task.getResult().getDocuments().get(getAdapterPosition()).getString("firstName"));
            stylist.putString("stylistImg", task.getResult().getDocuments().get(getAdapterPosition()).getString("imageUrl"));
            stylist.putString("specialist",servicesForDetails.get(getAdapterPosition()));
            stylist.putString("serviceid",serviceDetails.get(getAdapterPosition()));
            servicesFragment.setArguments(stylist);

            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, servicesFragment).addToBackStack(null).commit();
        }
    }

}





