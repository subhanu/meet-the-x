package com.example.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

/**
 * Created by DIS015 on 3/23/2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter {


    private final Task<QuerySnapshot> task;

    public ProductsAdapter(Task<QuerySnapshot> task) {
        this.task=task;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.products_layout,parent,false);
        return new ProductsAdapter.listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((listViewHolder) holder).productDescriptionTv.setText(task.getResult().getDocuments().get(position).getString("description"));
        ((listViewHolder)holder).productnameTv.setText(task.getResult().getDocuments().get(position).getString("productName"));
        ((listViewHolder)holder).productPriceTv.setText(task.getResult().getDocuments().get(position).getString("price"));
        Picasso.get().load(task.getResult().getDocuments().get(position).getString("imageUrl")).into(((ProductsAdapter.listViewHolder) holder).productImg);
    }

    @Override
    public int getItemCount() {
        return task.getResult().getDocuments().size();
    }
    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
           private TextView productnameTv,productDescriptionTv,productPriceTv;
           private ImageView productImg;
           private Button AddCartBtn;

        public listViewHolder(View itemView) {
            super(itemView);
            productImg=(ImageView)itemView.findViewById(R.id.productsImg);
            productnameTv=(TextView)itemView.findViewById(R.id.productNameTv);
            productDescriptionTv=(TextView)itemView.findViewById(R.id.productDesTv);
            productPriceTv=(TextView)itemView.findViewById(R.id.productQuantityTv);
            AddCartBtn=(Button)itemView.findViewById(R.id.AddCartBtn);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
