package com.example.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.Fragment.StylistsFragment;
import com.example.PublicClass.EventsCollection;
import com.example.aayushchaubey.meetdax.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

/**
 * Created by DIS015 on 3/16/2018.
 */

public class MyAppoinmentsAdapter extends RecyclerView.Adapter {
    private final DocumentChange task;
    private String id,strusername,struserEmail,strStartDate,strStylistname;
    private List<String>servicesArry;
    private Context context;
    FirebaseFirestore firebaseFirestore;

    EventsCollection userDetails;

    public MyAppoinmentsAdapter(DocumentChange task, String id) {
        this.task = task;
        this.id=id;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_appointments_recycleritem,parent,false);



        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((listViewHolder)holder).customernameTv.setText(task.getDocument().get("customer").toString());
        ((listViewHolder)holder).serviceTv.setText(task.getDocument().get("services").toString());
        ((listViewHolder)holder).dateTv.setText(task.getDocument().get("startDate").toString());

        id=task.getDocument().getId();
    }

    @Override
    public int getItemCount()
    {
        return task.getDocument().get("stylist").toString().length();
    }

    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView customernameTv, serviceTv,timeTV,dateTv,updateTv,deletv;
        private ImageView stylistImg;
        public Object strStartDate;


        public listViewHolder(View itemView) {
            super(itemView);

            customernameTv = (TextView) itemView.findViewById(R.id.customerNameTv);
            serviceTv = (TextView) itemView.findViewById(R.id.serviceTv);
            timeTV = (TextView) itemView.findViewById(R.id.timeTV);
            dateTv = (TextView) itemView.findViewById(R.id.nameTV);
            stylistImg=(ImageView)itemView.findViewById(R.id.stylistImg);
            deletv=(TextView)itemView.findViewById(R.id.deletv);
            firebaseFirestore = FirebaseFirestore.getInstance();


            itemView.setOnClickListener(this);

            updateTv=(TextView)itemView.findViewById(R.id.updateTv);

            updateTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StylistsFragment stylistsFragment = new StylistsFragment();

                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, stylistsFragment).addToBackStack(null).commit();

                }
            });

            deletv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        firebaseFirestore.collection("bookings")
                                .document(id)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.i("deleted","events");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                    }
                                });
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });
        }

        @Override
        public void onClick(View view) {


        }

    }

}
