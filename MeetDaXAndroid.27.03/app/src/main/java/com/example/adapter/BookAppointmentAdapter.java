package com.example.adapter;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.PublicClass.AppointmentTimes;
import com.example.aayushchaubey.meetdax.R;
import com.example.Fragment.ServicesFragment;

import java.util.ArrayList;

/**
 * Created by DIS015 on 3/9/2018.
 */

public class BookAppointmentAdapter extends RecyclerView.Adapter {
    String strStartItem;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.appointmentadapter,parent,false);
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((listViewHolder)holder).bindView(position);
    }

    public String getSelectedItems(){
        return strStartItem;
    }

    @Override
    public int getItemCount() {
        return  AppointmentTimes.startTime.length;
    }

    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView starttime,endtime;

        LinearLayout startTimeLayout;

        public listViewHolder(View itemView){
            super(itemView);
            starttime=(TextView)itemView.findViewById(R.id.startTime);
            endtime=(TextView)itemView.findViewById(R.id.endTime);
            startTimeLayout=(LinearLayout)itemView.findViewById(R.id.startTimeLayout);

            itemView.setOnClickListener(this);

        }

        public void bindView(int position){
            starttime.setText(AppointmentTimes.startTime[position]);
            endtime.setText(AppointmentTimes.endTime[position]);


        }
        public void onClick(View view){

            strStartItem=starttime.getText().toString().trim();

            startTimeLayout.setBackgroundResource(R.drawable.selectedbutton);

        }

    }
}
